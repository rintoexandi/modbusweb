try:

    import json
    import os
    import io
    import Queue
    import subprocess
    import sys
    import threading
    import time
    import logging
    import ConfigParser
    import MySQLdb
    import socket
    import memcache
    import uuid
    import datetime
    import time
    import calendar;

except:
    print "ERROR: missing one or more of the following python modules:"
    print "threading, Queue, sys, subprocess, time, os, json"
    sys.exit(2)

try:
    import MySQLdb
except:
    print "ERROR: missing the mysql python module:"
    print "On ubuntu: apt-get install python-mysqldb"
    print "On FreeBSD: cd /usr/ports/*/py-MySQLdb && make install clean"
    sys.exit(2)

#Test Config File
ob_install_dir = os.path.dirname(os.path.realpath(__file__))
config_file = ob_install_dir + '/modbus.cfg'

try:
    with open(config_file) as f:
        pass
except IOError as e:
    print "ERROR: Oh dear... %s does not seem readable" % config_file
    sys.exit(2)

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    #Set mysql param
    db_server = config.get("DEFAULT","database_server")
    db_dbname = config.get("DEFAULT","database_dbname")
    db_username = config.get("DEFAULT","database_username")
    db_password = config.get("DEFAULT","database_password")
    db_port = int(config.get("DEFAULT","database_port"))
    max_number_thread=int(config.get("DEFAULT","max_number_thread"))
    memcached_host=config.get("DEFAULT","memcached_host")
    memcached_port=config.get("DEFAULT","memcached_port")
    memcached_key_timeout=int(config.get("DEFAULT","memcached_key_timeout"))
    rrdfile = config.get("DEFAULT","rrdfile")
    
except ConfigParser.NoOptionError:
    print "ERROR: Options config is not available"
    sys.exit(2)

def db_open():
    try:
        if (db_port==3306):
            db = MySQLdb.connect(host=db_server, user=db_username, passwd=db_password, db=db_dbname)
        else :
            db = MySQLdb.connect(host=db_server,port=db_port, user=db_username, passwd=db_password, db=db_dbname)
        return db
    except ( MySQLdb.Error, MySQLdb.Warning) as err:
        print  err
        sys.exit(2)

#Get Device Port

def getportinfo(port_id):
    #get Devices List from Database
    query = "select name from ports where port_id='" + str(port_id) + "'"

    db = db_open()
    cursor = db.cursor()
    cursor.execute(query)
    devports = cursor.fetchall()
    db.close()
    #print devports[0]
    return devports[0]


def generaterrd(device_id):  
    global rrdfile
    #print 'RRD Generate' + str(device_id)
    try:
          
            #get ports
            #get Devices List from Database
            query = "SELECT deviceport_id,port_id FROM deviceport WHERE device_id='" + str(device_id) + "' AND isactive=1"

            #print query
            db = db_open()
            cursor = db.cursor()
            cursor.execute(query)
            devports = cursor.fetchall()
            
            #last_24hours =time.time()
            #Current Datetime
            #current date and time
            #now = datetime.now()

            timestamp = calendar.timegm(time.gmtime())

            last_24hours = timestamp - (60*60*24)
            #print 'Range ' + str(last_24hours) + ' - ' + str(timestamp)
            for ports in devports:
                port_info=getportinfo(ports[1])
                ds = port_info[0]
                rrdsource = ob_install_dir + rrdfile + str(device_id) + "/" + str(ports[0]) + ".rrd"
                jsonfile = ob_install_dir + rrdfile + str(device_id) + "/" + str(ports[0]) + ".json"
                
                exists = os.path.isfile(rrdsource)
                if exists:
             
                    #rrdtool xport --json -s 1554955800 -e 1554991800 --step 10 DEF:35A=13784.rrd:35A:AVERAGE XPORT:35A
                    #command = "rrdtool export --json %s %s" % (poller_path, device_id) #>> /dev/null 2>&1
                    command = "rrdtool xport --json -s %d -e %d --step 10 DEF:%s=%s:%s:AVERAGE XPORT:%s" %(last_24hours,timestamp,ds,rrdsource,ds,ds)
                    #subprocess.check_call(command, shell=True)
                    p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
                    (output, err) = p.communicate()
                    #print 'Ports' + str(ports[1])
                    #print 'Json file Generated ' + jsonfile
                    
                    #print 'Command' + str(command)

                    ## Wait for date to terminate. Get return returncode ##
                    p_status = p.wait()

                    f = open(jsonfile, "w")
                    f.write(output)
                    f.close()
                    #print "Command output : ", output
                    print "Command exit status/return code : ", p_status
                else:
                    print 'File ' + rrdsource + ' not exist, move on..'
                    continue

            db.close()
           
          
        
    
    except MySQLdb.Error, e:
        print (e)


#get Devices List from Database
query = "SELECT device_id FROM devices WHERE disabled = 0 ORDER BY last_polled"

db = db_open()
cursor = db.cursor()
cursor.execute(query)
devices = cursor.fetchall()

#Start Time
start_time = calendar.timegm(time.gmtime())


for row in devices:   
    generaterrd(row[0])
db.close()

#Worker for printing Result
def print_process():
    nodeso = 0  
    while True:
        global real_duration
        global polled_devices
        
        device_id, elapsed_time = print_queue.get()
       
        #name = threading.currentThread().getName()
        #print "PRINT WORKER   " + worker_id

        real_duration += elapsed_time
        polled_devices += 1

        if elapsed_time < 300:
            print "INFO: Device  %s finished  in %s seconds " % (device_id, elapsed_time)
        else:
            print "WARNING: Device  %s finished  in %s seconds" % (device_id, elapsed_time)
        
        print_queue.task_done()
       
#Worker for polling data
def process_polldata(q):
    while True:

        global rrdfile
        name = threading.currentThread().getName()
        device_id = q.get()
        
        try:
            
            #put to memcache
            

            memc_touch(dev,120)

            start_time = time.time()

            #get ports
            ports = getdeviceport(device_id)
            for devport in ports:
                rrdsource = rrdfile + "/" + device_id + "/" + devport + ".rrd"
                #rrdtool xport --json -s 1554955800 -e 1554991800 --step 10 DEF:35A=13784.rrd:35A:AVERAGE XPORT:35A
                command = "rrdtool export --json %s %s" % (poller_path, device_id) #>> /dev/null 2>&1
                subprocess.check_call(command, shell=True)
        
            elapsed_time = int(time.time() - start_time)
        
            #print_queue.put([threading.current_thread().name, device_id, elapsed_time])
            print_queue.put([device_id, elapsed_time])

        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            pass
        

        rrd_queue.task_done()

last_time = calendar.timegm(time.gmtime())

elapsed_time = int(last_time) - int(start_time)
print 'Start time ' + str(start_time)  + ' -- ' + ' End time ' + str(last_time)
print 'Elapsed time ' + str(elapsed_time)