try:

    import json
    import os
    import io
    import Queue
    import subprocess
    import sys
    import threading
    import time
    import logging
    import ConfigParser
    import MySQLdb
    import socket
  
except:
    print "ERROR: missing one or more of the following python modules:"
    print "threading, Queue, sys, subprocess, time, os, json"
    sys.exit(2)

try:
    import MySQLdb
except:
    print "ERROR: missing the mysql python module:"
    print "On ubuntu: apt-get install python-mysqldb"
    print "On FreeBSD: cd /usr/ports/*/py-MySQLdb && make install clean"
    sys.exit(2)

#Test Config File
ob_install_dir = os.path.dirname(os.path.realpath(__file__))
config_file = ob_install_dir + '/modbus.cfg'

try:
    with open(config_file) as f:
        pass
except IOError as e:
    print "ERROR: Oh dear... %s does not seem readable" % config_file
    sys.exit(2)

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    #Set mysql param
    db_server = config.get("DEFAULT","database_server")
    db_dbname = config.get("DEFAULT","database_dbname")
    db_username = config.get("DEFAULT","database_username")
    db_password = config.get("DEFAULT","database_password")
    db_port = int(config.get("DEFAULT","database_port"))
    max_number_thread=int(config.get("DEFAULT","max_number_thread"))
    memcached_host=config.get("DEFAULT","memcached_host")
    memcached_port=config.get("DEFAULT","memcached_port")
    use_memcached=bool(config.get("DEFAULT","use_memcached"))
    memcached_poller_master=config.get("DEFAULT","memcached_poller_master")
except ConfigParser.NoOptionError:
    print "ERROR: Options config is not available"
    sys.exit(2)

def db_open():
    try:
        if (db_port==3306):
            db = MySQLdb.connect(host=db_server, user=db_username, passwd=db_password, db=db_dbname)
        else :
            db = MySQLdb.connect(host=db_server,port=db_port, user=db_username, passwd=db_password, db=db_dbname)
        return db
    except ( MySQLdb.Error, MySQLdb.Warning) as err:
        print  err
        sys.exit(2)

def memc_alive():
    try:
        global memc
        key = str(uuid.uuid4())
        memc.set('processpoller.' + key, key, 60)
        if memc.get('processpoller.' + key) == key:
            memc.delete('processpoller.' + key)
            return True
        else:
            return False
    except:
        return False

def memc_touch(key, time):
    try:
        global memc
        val = memc.get(key)
        memc.set(key, val, time)
    except:
        pass

if (use_memcached==True):
    try:
        import memcache
        import uuid
        ##global memcached_poller_master
        ##global memcached_port
        ##global memcached_host
        ##global IsNode
        servers = ["127.0.0.1:11211"]
        #memc = memcache.Client(memcached_host + ':' +memcached_port)
        memc = memcache.Client(servers,debug=1)
        if memc_alive():
            if memc.get("poller.master") is None:
                print "Registered as Master"
                memc.set("poller.master", memcached_poller_master, 10)
                memc.set("poller.nodes", 0, 300)
                IsNode = False
            else:
                print "Registered as Node joining Master %s" % memc.get("poller.master")
                IsNode = True
                memc.incr("poller.nodes")
        else:
            print "Could not connect to memcached, disabling distributed poller."
            IsNode = False
    except SystemExit:
        raise
    except ImportError:
        print "ERROR: missing memcache python module:"
        print "On deb systems: apt-get install python-memcache"
        print "On other systems: easy_install python-memcached"
        print "Disabling distributed poller."
        distpoll = False
else:
    distpoll = False
# EOC1