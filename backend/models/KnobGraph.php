<?php

namespace backend\models;

use yii\base\Model;
use softcommerce\knob\Knob;

class KnobGraph extends Model{
    
    public $min=0;
    public $max=100;
    public $width=100;
    public $height=100;
    
    public $thicknes=0.25;
    public $fgcolor ='#9fc569';
    
    public $tresholdmin=0;
    public $tresholdwarning=32;
    public $treshholdinfo=28;
    public $tresholdmax=100;
    
    public $value=0;
    
    public function printGraph(){
        
       
        return Knob::widget([
                'name' => 'animated_knob_with_icon',
                'value' => $this->value,
                'options' => [
                        'data-min' => $this->min,
                        'data-max' => $this->max,
                        'data-width' => $this->width,
                        'data-height' => $this->height,
                ],
                'knobOptions' => [
                        'readOnly' => true,
                        'thickness' => $this->thicknes,
                        'dynamicDraw' => true,
                        'fgColor' => $this->changeColour($this->value),
                ],
        ]);
        

    }
    
    private function changeColour($value) {
        if ($value <= $this->treshholdinfo) {
            return '#00a65a';
        }
        else if (($value > $this->treshholdinfo) && ($value<=$this->tresholdwarning)){
            return '#f39c12';
        }
        else if ($value > $this->tresholdwarning){
            return '#dd4b39';
        }
        
        else {
            return '#ffffff';
        }
        
        
        
    }
}