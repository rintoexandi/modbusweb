<?php

namespace backend\models;
use scotthuangzl\googlechart\GoogleChart;
use yii\base\Model;

class Gauge extends Model {
    
    public $label="";
    public $value=0;
    public $width=150;
    public $height=150;
    public $greenFrom=0;
    public $greenTo=0;
    public $redFrom=0;
    public $redTo=0;
    public $yellowFrom=0;
    public $yellowTo=0;
    public $minorTicks=5;
    public $min=0;
    public $max=0;
   
    
    public function printGauge() {
        
        
        return GoogleChart::widget( array('visualization' => 'Gauge', 'packages' => 'gauge',
                'data' => array(
                        array('Label', 'Value'),
                        array($this->label, $this->value),
                        
                ),
                'options' => array(
                        'width' => $this->width,
                        'height' => $this->height,
                        'greenFrom'=>$this->greenFrom,
                        'greenTo'=>$this->greenTo,
                        'redFrom' => $this->redFrom,
                        'redTo' =>$this->redTo,
                        'yellowFrom' => $this->yellowFrom,
                        'yellowTo' => $this->yellowTo,
                        'minorTicks' => $this->minorTicks,
                        'min'=>$this->min,
                        'max'=>$this->max,
                )
        ));
          
    }
    
}