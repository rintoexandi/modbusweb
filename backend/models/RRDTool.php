<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */


namespace backend\models;

use common\models\Deviceport;
use Yii;
use yii\base\Model;

class RRDTool extends Model {
    
     
    public $rrddata;
    public $unit;
    public $dataset;
    public $graphtitle;
    public $grapunit;
    
    
    public function generateRRDfile($rrdfile) {
        
        $opts = array( "--step", "600", "--start", 0,
           "DS:". $this->dataset .":GAUGE:300:0:24000",
           "RRA:AVERAGE:0.5:1:864000",
           "RRA:AVERAGE:0.5:60:129600",
           "RRA:AVERAGE:0.5:3600:13392", 
           "RRA:AVERAGE:0.5:86400:3660"
        );
        
        return rrd_create($rrdfile,$opts);
    }
    
  
    
    public function updateRRDfile($rrdfile,$value){
        
        date_default_timezone_set("Asia/Jakarta");
       
        
        $data = array();
        $data[] = time();
        $data[] =$value;
        
        
        $string = implode(":", $data);
         
        //echo "Updating RRDfile " . $string . "\n";
        
        return rrd_update($rrdfile,array($string));
                    
    }
    
    /*
     * 
     */
    public function getRRdpath($device_id) {
        return Yii::getAlias('@rrd') . "/" . $device_id;
    }
    
    
    public function generateGraph($rrdfile,$output, $start) {
        
       
        $verticallabel="value";
        if (!empty($this->grapunit)) {
            
            $verticallabel = $this->grapunit;
        }
        
        $opts = array( "--start", $start, "--vertical-label=$verticallabel",
                        "--slope-mode",
                        "--title=$this->graphtitle",
                       "DEF:$this->dataset=$rrdfile:$this->dataset:AVERAGE",

                       "AREA:$this->dataset#00FF00:$this->dataset",

                       "CDEF:datas=$this->dataset,300,*",

                       "COMMENT:\\n",
                       "GPRINT:$this->dataset:AVERAGE:Avg Value\: %6.2lf",
                       "COMMENT:  ",
                       "GPRINT:$this->dataset:MAX:Max Value\: %6.2lf\\r",

                     );

       return rrd_graph($output, $opts);

    }
    
    public function getGraph($deviceport_id,$start){
        
        
        $deviceport = Deviceport::find()->where(['deviceport_id'=>$deviceport_id])->one();
       
        if (!empty($deviceport)) {
            
            $rrdpath = Yii::getAlias('@rrd') . "/" . $deviceport->device_id;
        
            $port = $deviceport->getPort();

                                            
            $uom =$port->getUom();

            $symbol="";
            if (!empty($uom)) {
                $symbol=$uom->symbol;
            }
            
            $graphtitle = $this->setGraphTitle($start) . '  ' . $port->name . ' - ' . $port->description;

            $dataset = $port->name;

            $rrdfile = $rrdpath . "/" . $deviceport->deviceport_id . ".rrd";
            
            $rrdcachedpath = $deviceport->device_id . "/" . $deviceport->deviceport_id . ".rrd";

            $rrd = new RRDTool();
            $rrd->dataset=$dataset;
            $rrd->grapunit=$symbol;
            $rrd->graphtitle=$graphtitle;

            
           //RRDFile is not exist
            if (!file_exists ($rrdfile)) {

                return "RRD Does Not exist";
               
            }

            //RRD file is exist
            else {

               
               $imgpath = Yii::$app->params['rrdgraph'] . "/" . $deviceport->deviceport_id  . $start . ".png";
               
               //flush rrcached before show image
            
               $this->flushRRDCached($rrdcachedpath);
            
               return $rrd->generateGraph($rrdfile, $imgpath,$start, $graphtitle);
               
            }
        }//end of device port is not empty
    }
    
    private function setGraphTitle($start) {
        $title="";
        switch ($start) {
            case "-1h":
                $title= "Hourly";
                break;
            case "-1d":
                $title = "Daily";
                break;
            case "-1w":
                $title = "Weekly";
                break;
            case "-1m":
                $title= "Monthly";
                break;
            case "-1y":
                $title= "Yearly";
                break;
            default:
                $title = "Daily";
            }
            
        return $title;
    }
    
    function createRRD($rrdfile) {
        
        // File descriptors for each subprocess.
        $descriptors = [
            0 => ['pipe', 'r'], // stdin
            1 => ['pipe', 'w'], // stdout
            2 => array('pipe', 'w'), // stderr is a pipe that the child will write to
        ];
       // $cmd = "rrdtool create --daemon unix:/tmp/rrdcached.sock $rrdfile --step 1s  --start now-2h DS:$this->dataset:GAUGE:300:0:24000  RRA:AVERAGE:0.5:1:864000  RRA:AVERAGE:0.5:60:129600  RRA:AVERAGE:0.5:3600:13392  RRA:AVERAGE:0.5:86400:3660";
        //$cmd = "rrdtool create  $rrdfile --step 1s  --start now-2h DS:$dataset:GAUGE:300:0:24000  RRA:AVERAGE:0.5:1:864000  RRA:AVERAGE:0.5:60:129600  RRA:AVERAGE:0.5:3600:13392  RRA:AVERAGE:0.5:86400:3660";
        $cmd ="rrdtool create --daemon localhost:42217 $rrdfile  DS:$this->dataset:GAUGE:300:U:U RRA:AVERAGE:0.5:1:40320 RRA:AVERAGE:0.5:2:43800 RRA:AVERAGE:0.5:10:52560 RRA:AVERAGE:0.5:20:52596 RRA:AVERAGE:0.5:30:175320";    
        $pipes = array();
         $process = proc_open($cmd, $descriptors, $pipes);
         if (!is_resource($process)) {
             throw new \RuntimeException("Failed to run command '{$cmd}'");
         }

         fclose($pipes[0]);
         $stdOut = stream_get_contents($pipes[1]);
         fclose($pipes[1]);
         $stdErr = stream_get_contents($pipes[2]);
         fclose($pipes[2]);
         if ($stdErr) {
             throw new \RuntimeException("Error executing {$cmd}: {$stdErr}");
         }
         // It is important that to close any pipes before calling
         // proc_close in order to avoid a deadlock
         proc_close($process);
         return $stdOut;
    
    }

    function updateRRD($rrdfile,$value) {

        
         date_default_timezone_set("Asia/Jakarta");
       
        
        $data = array();
        $data[] = time();
        $data[] =$value;
        
        
        $string = implode(":", $data);

        //echo "Updating RRDfile " . $string . "\n";
        // non-blocking-proc_open.php
        // File descriptors for each subprocess.
        $descriptors = [
            0 => ['pipe', 'r'], // stdin
            1 => ['pipe', 'w'], // stdout
            2 => array('pipe', 'w'), // stderr is a pipe that the child will write to
        ];
        $cmd = "rrdtool update  --daemon localhost:42217 $rrdfile $string";

        //$cmd = "rrdtool update $rrdfile $string";

        //print $cmd . "\n";

        $pipes = array();
         $process = proc_open($cmd, $descriptors, $pipes);
         if (!is_resource($process)) {
             throw new \RuntimeException("Failed to run command '{$cmd}'");
         }

         fclose($pipes[0]);
         $stdOut = stream_get_contents($pipes[1]);
         fclose($pipes[1]);
         $stdErr = stream_get_contents($pipes[2]);
         fclose($pipes[2]);
         if ($stdErr) {
             throw new \RuntimeException("Error executing {$cmd}: {$stdErr}");
         }
         // It is important that to close any pipes before calling
         // proc_close in order to avoid a deadlock
         proc_close($process);
         return $stdOut;
    
    }
    
    function flushRRDCached($rrdfile) {
        
        // File descriptors for each subprocess.
        $descriptors = [
            0 => ['pipe', 'r'], // stdin
            1 => ['pipe', 'w'], // stdout
            2 => array('pipe', 'w'), // stderr is a pipe that the child will write to
        ];
      
         $cmd = "rrdtool flushcached --daemon localhost:42217 $rrdfile";
         
        $pipes = array();
         $process = proc_open($cmd, $descriptors, $pipes);
         if (!is_resource($process)) {
             throw new \RuntimeException("Failed to run command '{$cmd}'");
         }

         fclose($pipes[0]);
         $stdOut = stream_get_contents($pipes[1]);
         fclose($pipes[1]);
         $stdErr = stream_get_contents($pipes[2]);
         fclose($pipes[2]);
         if ($stdErr) {
             throw new \RuntimeException("Error executing {$cmd}: {$stdErr}");
         }
         // It is important that to close any pipes before calling
         // proc_close in order to avoid a deadlock
         proc_close($process);
         return $stdOut;
    
    }
}