<?php


namespace backend\models;

use yii\base\Model;

class DigitalBox extends Model {
    
    public $label="";
    public $value=0;
    public $width=150;
    public $height=150;
    public $greenFrom=0;
    public $greenTo=0;
    public $redFrom=0;
    public $redTo=0;
    public $yellowFrom=0;
    public $yellowTo=0;
    public $minorTicks=5;
    public $min=0;
    public $max=0;
    public $icon="fa fa-comments-o";
    
    private function printPre() {
        $pre ='';
        $pre .= '<div class="info-box">';
        $pre .= '<span class="info-box-icon bg-aqua"><i class="'. $this->icon .'"></i></span>';
        $pre .= '<div class="info-box-content">';        
        $pre .= '<div class="col-sm-6">';
        
        return $pre;
    }
    
    public function printInfoBox(){
       
       $content =""; 
       $content .= $this->printPre();
        
        $knob = new KnobGraph();
        $knob->width=80;
        $knob->height=80;
        $knob->min=0;
        $knob->max=100;
        $knob->treshholdinfo=50;
        $knob->tresholdwarning=80;
        $knob->value=rand(10, 35);
        
        $content .= $knob->printGraph();
        
        $content .= $this->printPost();
        
        return $content;
    }
    
    private function printPost(){
        $post="";
        $post.='</div>';
        
        $post.='<div class="col-sm-6">';
        
        $post.='<p>';
            $post.='<span class="label label-danger">Alert</span>';
            $post.='<span class="label label-success">Info</span>';
            $post.='<small>Test</small>';
            $post.=' </p>';
        $post.='</div>';
        $post.='</div>';
        $post.='</div>';

       
        return $post;
    }
    
}