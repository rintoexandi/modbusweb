<?php

/*
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace backend\models;

use common\models\Deviceport;
use yii\base\Model;

/**
 * Description of OpenJson
 *
 * @author Rinto Exandi <rinto at sinaga.or.id>
 */
class ParseJsonFile extends Model{
    //put your code here
    
    public $deviceport_id;
    public $start;
    public $end;
    public $step;
    public $data;
    public $title;
    
    public function openJson($id,$type="daily"){
        
        $devport = Deviceport::findOne($id);
        
        $port = $devport->getPort();
        
        $this->title = $port->description;
        
        $json_data = $devport->openJsonFile($type);
        
        
        if (!empty($json_data)) {
            
           
            $this->start = $json_data["meta"]["start"];
            $this->end = $json_data["meta"]["end"];
            
            $this->data =$json_data["data"];  
            
            return true;
        }
        
       return false;
    }
    
    
}
