<?php

namespace  backend\customasset;

use kartik\base\AssetBundle;

class CounterAsset extends AssetBundle {
    
    public $sourcePath = '@vendor/softcommerce/yii2-knob/assets';
    public $js = ['js/jquery.knob.min.js'];
    
    public $depends = [
            'yii\web\JqueryAsset',
    ];
    
}