<?php

namespace backend\controllers;

use common\models\Ports;
use common\models\Widgetport;
use common\models\Widgets;
use kartik\grid\EditableColumnAction;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * WidgetportController implements the CRUD actions for Widgetport model.
 */
class WidgetportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Widgetport models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $query = Widgetport::find()->where(['widget_id'=>$id]);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'sequence' => SORT_ASC,

                ]
            ],
        ]);
   

        return $this->render('index', [
         
            'dataProvider' => $provider,
            'widget_id'=>$id,
        ]);
    }

    /**
     * Displays a single Widgetport model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Widgetport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Widgetport();

        $model->widget_id=$id;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Widgetport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Widgetport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
           $parent = $this->findModel($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index','id'=>$parent->widget_id]);
    }

    /**
     * Finds the Widgetport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Widgetport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Widgetport::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actions()
   {
       return ArrayHelper::merge(parent::actions(), [
          'updateport' => [                                       // identifier for your editable action
               'class' => EditableColumnAction::className(),     // action class name
               'modelClass' => Widgetport::className(),                // the update model class
               'outputValue' => function ($model, $attribute, $key, $index) {
                $fmt = Yii::$app->formatter;
                    $value = $model->$attribute;                 // your attribute value
                    if ($attribute === 'port_id') {           // selective validation by attribute
                       $port = Ports::find()->where(['port_id'=>$model->port_id])->one(); 
                       return $port->name;  
                        // return formatted value if desired
                    } 
                    elseif ($attribute === 'widget_id') {           // selective validation by attribute
                       $widget = Widgets::find()->where(['widget_id'=>$model->widget_id])->one(); 
                       return $widget->name;  
                        // return formatted value if desired
                    }
                    elseif ($attribute === 'sequence') {   // selective validation by attribute
                       // $port = Workshiftdetail::find()->where(['workshiftdetail_id'=>$model->workshiftdetail_id])->one(); 
                       return $value;  
                    }
                                                  // empty is same as $value
               },
               'outputMessage' => function($model, $attribute, $key, $index) {
                     return '';                                  // any custom error after model save
               },
               // 'showModelErrors' => true,                     // show model errors after save
               // 'errorOptions' => ['header' => '']             // error summary HTML options
               // 'postOnly' => true,
               // 'ajaxOnly' => true,
               // 'findModel' => function($id, $action) {},
               // 'checkAccess' => function($action, $model) {}
           ]
       ]);
   }
}
