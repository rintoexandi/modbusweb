<?php

namespace backend\controllers;

use backend\widgets\daemon\DaemonWidget;
use common\models\Daemon;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DaemonController implements the CRUD actions for Daemon model.
 */
class DaemonController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'enable' => ['POST'],
                    'disable' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Daemon models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Daemon::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    
     public function actionWidget()
    {
                
        return DaemonWidget::widget();
    }

    public function actionEnable($id) {
        
        $model = Daemon::findOne($id);
        $model->isactive = true;  
        $model->save(false);
        
        return $this->redirect(['index']);
                
    }
    
     public function actionDisable($id) {
        
        $model = Daemon::findOne($id);
        $model->isactive = false;  
        $model->save(false);
        
        return $this->redirect(['index']);
                
    }
    
   
    /**
     * Finds the Daemon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Daemon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Daemon::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
