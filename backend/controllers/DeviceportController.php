<?php

namespace backend\controllers;

use backend\widgets\rrdgraph\RRDGraphWidget;
use common\models\Alertrules;
use common\models\Deviceport;
use common\models\DeviceportSearch;
use kartik\grid\EditableColumnAction;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DeviceportController implements the CRUD actions for Deviceport model.
 */
class DeviceportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'disable'=> ['POST'],
                    'enable'=>['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Deviceport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeviceportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Deviceport model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    public function actionJsonGraph($id){
       
        return $this->render('graph', [
            'model' => $this->findModel($id),
        ]);
        
    }
    
    public function actionGraphwidget($id){
        
        return RRDGraphWidget::widget(['deviceport_id'=>$id]);
    }
    
    public function actionGraph($id){
        
        return $this->render('graph', [
            'model' => $this->findModel($id),
        ]); 
    }
    /**
     * Creates a new Deviceport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Deviceport();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->deviceport_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Deviceport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->deviceport_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Deviceport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDisable($id)
    {
         $model = $this->findModel($id);
         
         if ($model->reportalert==true) {
             $model->reportalert=false;
         }
         else {
             $model->reportalert=true;
         }
         
        $model->save(false);

        return $this->redirect(['device/ports/','id'=>$model->device_id]);
    }

    public function actionEnable($id)
    {
         $model = $this->findModel($id);
         
         if ($model->reportalert==true) {
             $model->reportalert=false;
         }
         else {
             $model->reportalert=true;
         }
         
        $model->save(false);

        return $this->redirect(['device/ports/','id'=>$model->device_id]);
    }

    /**
     * Finds the Deviceport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deviceport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deviceport::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    //Editaleactions
    
    public function actions()
   {
       return ArrayHelper::merge(parent::actions(), [
          'update' => [                                       // identifier for your editable action
               'class' => EditableColumnAction::className(),     // action class name
               'modelClass' => Deviceport::className(),                // the update model class
               'outputValue' => function ($model, $attribute, $key, $index) {
                $fmt = Yii::$app->formatter;
                    $value = $model->$attribute;                 // your attribute value
                    if ($attribute === 'alertrules_id') {           // selective validation by attribute
                       $alert = Alertrules::find()->where(['alertrules_id'=>$model->alertrules_id])->one(); 
                       return $alert->name;
                        // return formatted value if desired
                    } elseif ($attribute === 'reportalert') {   // selective validation by attribute
                       
                        if ($model->reportalert==true) {
                           return '<span class="btn btn-sm btn-success"><i class="glyphicon glyphicon-ok"></i></span>'; 
                        }
                        else {
                           return '<span class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-ban-circle"></i></span>'; 
                        }
                      
                    }
                    
                     elseif ($attribute === 'isactive') {   // selective validation by attribute
                       
                        if ($model->isactive==true) {
                           return '<span class="btn btn-sm btn-success">Enabled</i></span>'; 
                        }
                        else {
                           return '<span class="btn btn-sm btn-danger">Disabled</i></span>'; 
                        }
                      
                    }
                                                  // empty is same as $value
               },
               'outputMessage' => function($model, $attribute, $key, $index) {
                     return '';                                  // any custom error after model save
               },
               // 'showModelErrors' => true,                     // show model errors after save
               // 'errorOptions' => ['header' => '']             // error summary HTML options
               // 'postOnly' => true,
               // 'ajaxOnly' => true,
               // 'findModel' => function($id, $action) {},
               // 'checkAccess' => function($action, $model) {}
           ]
       ]);
   }
}
