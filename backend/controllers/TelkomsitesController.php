<?php

namespace backend\controllers;

use common\models\Sites;
use common\models\Sitetype;
use common\models\Telkomsites;
use common\models\TelkomsitesSearch;
use common\models\Witel;
use kartik\grid\EditableColumnAction;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SitesController implements the CRUD actions for Sites model.
 */
class TelkomsitesController extends Controller
{
   
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sites models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TelkomsitesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sites model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Telkomsites();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->site_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sites::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
      //Editaleactions
    
    public function actions()
   {
       return ArrayHelper::merge(parent::actions(), [
          'updatesites' => [                                       // identifier for your editable action
               'class' => EditableColumnAction::className(),     // action class name
               'modelClass' => Telkomsites::className(),                // the update model class
               'outputValue' => function ($model, $attribute, $key, $index) {
                $fmt = Yii::$app->formatter;
                    $value = $model->$attribute;                 // your attribute value
                    if ($attribute === 'sitetype_id') {           // selective validation by attribute
                       $sitetype = Sitetype::find()->where(['sitetype_id'=>$model->sitetype_id])->one(); 
                       return $sitetype->name;  
                        // return formatted value if desired
                    } elseif ($attribute === 'witel_id') {   // selective validation by attribute
                        $sitetype = Witel::find()->where(['witel_id'=>$model->witel_id])->one(); 
                       return $sitetype->name;  
                    }
                                                  // empty is same as $value
               },
               'outputMessage' => function($model, $attribute, $key, $index) {
                     return '';                                  // any custom error after model save
               },
               // 'showModelErrors' => true,                     // show model errors after save
               // 'errorOptions' => ['header' => '']             // error summary HTML options
               // 'postOnly' => true,
               // 'ajaxOnly' => true,
               // 'findModel' => function($id, $action) {},
               // 'checkAccess' => function($action, $model) {}
           ]
       ]);
   
 }
 
 public function actionUpload(){
        $model = new Telkomsites();
      
        if ($model->load(Yii::$app->request->post())) {
            
            $upload_file = $model->uploadFile();
            $model->filename = $model->getUploadedFile();
            
             if ($upload_file !== false) {
    	            $path = Yii::$app->params['uploadcsv'] . $model->getUploadedFile();
    	            if($upload_file->saveAs($path)) {
                        
                        //LOAD csv
                        
                         $filecsv = file($path);
                        
                         
                         
                         foreach($filecsv as $data){ 
                            
                             $hasil = explode(",",$data);
                             
                            
                             $witelname = trim(preg_replace('/\s\s+/', ' ', $hasil[0]));
                             $sitename =trim(preg_replace('/\s\s+/', ' ', $hasil[1]));
                             $sitedescription= trim(preg_replace('/\s\s+/', ' ', $hasil[2]));
                             
                             $import = new Telkomsites();
                             
                             $valid = true;
                             
                             $witel = Witel::validateWitel($witelname);
                             
                             if (empty($witel)){
                                 
                                  $valid=false;
                           
                             }
                             else {
                                 $import->witel_id=$witel->witel_id;
                             }
                             
                             
                             
                             $site = Telkomsites::validateSite($sitename);
                             
                             if (empty($site)) {
                                 
                                  $import->name=$sitename;
                                  $import->description=$sitedescription;
                             }
                            else {
                                
                                $valid=false;
                            }
                            
                            $import->sitetype_id=1;
                            
                            if ($valid==true) {
                             
                             $import->save(false);
                           
                            }
                             
                         }
                    }
             }
             
            return $this->redirect(['index']);
        }

        return $this->render('upload', [
            'model' => $model,
        ]);
    }
}
