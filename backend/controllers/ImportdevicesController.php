<?php

namespace backend\controllers;

use common\models\Importdevices;
use common\models\Telkomsites;
use common\models\Witel;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ImportdevicesController implements the CRUD actions for Importdevices model.
 */
class ImportdevicesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'truncate'=>['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Importdevices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Importdevices::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Importdevices model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Importdevices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Importdevices();
        $model->scenario='uploadcsv';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpload(){
        $model = new Importdevices();
      
        if ($model->load(Yii::$app->request->post())) {
            
            $upload_file = $model->uploadFile();
            $model->filename = $model->getUploadedFile();
            
             if ($upload_file !== false) {
    	            $path = Yii::$app->params['uploadcsv'] . $model->getUploadedFile();
    	            if($upload_file->saveAs($path)) {
                        
                        //LOAD csv
                        
                         $filecsv = file($path);
                        
                         foreach($filecsv as $data){ 
                            
                             $hasil = explode(",",$data);
                             
                           
                             $hostname = $hasil[2];
                             $ipaddress =trim(preg_replace('/\s\s+/', ' ', $hasil[3]));
                             
                             $valid=true;
                             
                             
                             $import = new Importdevices();
                             
                             $witel = Witel::validateWitel($hasil[0]);
                             
                             if (empty($witel)){
                                 
                                  $import->witelname='INVALID WITEL';
                                  $valid=false;
                             }
                             
                             else {
                                 
                                  $import->witelname=$witel->name;
                             }
                             
                             $sitename = Telkomsites::validateSite($hasil[1]);
                             
                             if (empty($sitename)) {
                                 
                                 $import->sitename='INVALID SITE';
                                 $valid=false;
                             }
                            else {
                                
                                $import->sitename=$sitename->name;
                            }
                             
                             
                             $import->hostname=$hostname;
                             $import->ipaddress=$ipaddress;
                            
                             $import->processed=false;
                             $import->isvalid=$valid;
                             
                             $import->save(false);
                             
                             
                         }
                    }
             }
             
            return $this->redirect(['index']);
        }

        return $this->render('upload', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Importdevices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Importdevices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Importdevices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Importdevices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Importdevices::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function  actionTruncate(){
        
        $models = Importdevices::find()->all();
        
        foreach ($models as $model){
            
            $model->delete();
            
        }
        
        return $this->redirect(['index']);
        
    }
    
    
    public function  actionProcess(){
        
        $models = Importdevices::find()->where(['processed'=>false,'isvalid'=>true])->all();
        
        foreach ($models as $model){
            
            $model->processImport();
            
        }
        
        return $this->redirect(['index']);
        
    }
}
