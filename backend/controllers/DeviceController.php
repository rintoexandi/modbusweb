<?php

namespace backend\controllers;

use backend\widgets\devices\DeviceinfoWidget;
use backend\widgets\devices\DevicesWidget;
use common\models\Devices;
use common\models\DevicesSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DeviceController implements the CRUD actions for Devices model.
 */
class DeviceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Devices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DevicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

      /**
     * Lists all Devices models.
     * @return mixed
     */
    public function actionSite($id)
    {
        $searchModel = new DevicesSearch();
        $searchModel->site_id=$id;
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id'=>$id,
        ]);
    }

    public function actionPorts($id){
        
         return $this->render('ports', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Displays a single Devices model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('main', [
            'model' => $this->findModel($id),
        ]);
    }

    
   
    public function actionDigitalajax($id)
    {
       
        return $this->renderPartial('listdigital', [
               'model' => $this->findModel($id),
        ]);
        
       // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
       // $deviceport = Deviceport::find()->where([])->all();
        
       // return $deviceport;
    }
    
    public function actionAnalogajax($id)
    {
        
        //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        return $this->renderPartial('portanalog', [
                'model' => $this->findModel($id),
        ]);
       
       // $deviceport = Deviceport::find()->where([])->all();
        
        //return $deviceport;
        
    }
    /*
     * Port Analog
     */
    public function actionAnalog($id) {
        return $this->render('portanalog', [
                'model' => $this->findModel($id),
        ]);
    }
    
    /*
     * Port Analog
     */
    public function actionDigital($id) {
        return $this->render('portdigital', [
                'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new Devices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Devices();
        $model->port=502;
      

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Devices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->device_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Devices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Devices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Devices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Devices::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    /*
     * For List of Devices on Widget
     */
    public function actionDeviceswidget(){
        return DevicesWidget::widget();
    }
    

     public function actionDeviceinfowidget($id)
    {   
        $model = $this->findModel($id);
        return DeviceinfoWidget::widget(['model'=>$model]);
    }
}
