<?php



namespace backend\components;

use yii\base\Widget;
use yii\bootstrap\Html;

class CounterWidget extends Widget {
    
    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'counter');
    }
    
    public function run()
    {
        
        KnobAsset::register($view);
        $pluginsJs = "";
        if (!is_null($this->icon)) {
            KnobIconAsset::register($view);
            $pluginsJs .= "addKnobIcon('#{$this->options['id']}', '".addslashes($this->icon)."');\n";
        }
        $knobOptions = empty($this->knobOptions) ? '' : Json::encode($this->knobOptions);
        $js = "jQuery('#{$this->options['id']}').knob({$knobOptions});\n";
        $js .= $pluginsJs;
        $view->registerJs($js);
    }
}