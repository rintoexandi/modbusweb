<?php

namespace backend\components;


class GraphComponents extends \yii\base\Component {
    
    
    public function getGraphType($type,$val){
        
        
        switch ($type) {
            
            case "N" :
                return $this->noneGraph($val);
                break;
            case "K" :
                return $this->knobGraph($val);
                break;
            case "L" :
                return $this->lineGraph($val);
                break;
            case "B" :
                return $this->barGraph($val);
                break;
            default:
                return $this->noneGraph($val);
                
        }
    }
    
    public function knobGraph($val) {
        
        
        if ($val<=35) {
            $fgcolor ="#00a65a";//green
        }
        else if ($val>35 && $val<=70) {
            $fgcolor ="#3c8dbc";//blue
        }
        else if ($val>70) {
            $fgcolor ="#f56954";//orange
        }
        else {
            $fgcolor ="#00a65a";//green
        }
        
        $knob  ='<input type="text" class="knob" value="' . $val .'" data-skin="tron" data-thickness="0.3" data-width="25" data-height="25" data-fgColor="'. $fgcolor .'" data-readonly="true">';
        return $knob;
    }
    
    public function barGraph($val) {
        return "";
    }
    
    public function lineGraph($val) {
        $line ='<span class="sparkline-1">' . implode(",",$this->generateData(30)) . '</span>';
        return $line;
    }
    
    public function noneGraph($val) {
        return "";
    }
    
    public function counterGraph($val) {
        $line ='<span class="sparkline-1">' . implode(",",$this->generateData(30)) . '</span>';
        return $line;
    }
    
    public function generateData($length){
        
        $arrdata = array();
        
        for ($i=0;$i<$length;$i++) {
            $arrdata[]=rand(50, 80);
        }
        
        return $arrdata;
    }
    
    public function generateDataStr($length){
        
        $arrdata = array();
        
        for ($i=0;$i<$length;$i++) {
            $arrdata[]=$i;
        }
        
        return $arrdata;
    }
    
    public function generateDataDummy($length,$low,$high){
        
        $arrdata = array();
        
        for ($i=0;$i<$length;$i++) {
            $arrdata[]=rand($low, $high);
        }
        
        return $arrdata;
    }
}