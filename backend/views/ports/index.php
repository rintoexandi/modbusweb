<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PortsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ports-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ports', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'port_id',
            'name',
            'description',
            'portgroup_id',
            'isactive',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
