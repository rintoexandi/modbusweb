<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ports */

$this->title = 'Create Ports';
$this->params['breadcrumbs'][] = ['label' => 'Ports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ports-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
