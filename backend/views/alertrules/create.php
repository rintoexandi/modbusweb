<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Alertrules */

$this->title = 'Create Alertrules';
$this->params['breadcrumbs'][] = ['label' => 'Alertrules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alertrules-create">

    <?= $this->render('form2', [
        'model' => $model,
    ]) ?>

</div>
