<?php

use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Alertrules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alertrules-index">

    <p>
        <?php
            
             $urlTo=Url::toRoute(['/alertrules/create']);
         
             echo Button::widget([
             'label' => 'Tambah Rule Details',
             'options' => ['value'=>Url::to($urlTo), 
             'class' => 'btn btn-success','id'=>'btnCreateRules'],                           
              ]);
        
            ?>
    </p>

    <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
            <div class="box-body">
       
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'alertrules_id',
            'name',
            'isdigital',
            'notify',
            'isactive',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
                
                	</div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</div>

<?php
 Modal::begin([
                 'header'=>'<h3 class="box-title">Create/Update Rules</h3>',
                 'id'=>'modalCreateRule',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalCreateRuleContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btnCreateRules').on('click', function() {     
    	$('#modalCreateRule').modal('show')
			.find('#modalCreateRuleContent')
			.load($(this).attr('value'));

 		});
    });
");


?>