<?php

use common\models\Alertrules;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model Alertrules */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Alertrules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alertrules-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            
                
            <!-- /.box-header -->
         
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        //'alertrules_id',
        'name',
        'isdigital',
        'notify',
        'isactive',
        ],
    ]) ?>
      
              <?php
             
             $url_create = '/alertrulesdetails/create/' . $model->alertrules_id; 
             $urlTo=Url::toRoute([$url_create]);
         
             echo Button::widget([
             'label' => 'Tambah Rule Details',
             'options' => ['value'=>Url::to($urlTo), 
             'class' => 'btn btn-success','id'=>'btnCreateRulesDetails'],                           
              ]);
        
            ?>
  	        
    
    <?php
    echo Yii::$app->controller->renderPartial('_alert_rules_details',['model'=>$model]);
    ?>
</div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</div>

<?php
       

        Modal::begin([
                 'id'=>'modalCreateRuleDetails',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalCreateDeviceContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btnCreateRulesDetails').on('click', function() {     
    	$('#modalCreateRuleDetails').modal('show')
			.find('#modalCreateDeviceContent')
			.load($(this).attr('value'));

 		});
    });
");

?>

<?php
 
        Modal::begin([
                 'id'=>'modalUpdateRuleDetails',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalUpdateRulesContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('.btnupdaterules').on('click', function() {   
        console.log($(this).attr('value'));
    	$('#modalUpdateRuleDetails').modal('show')
			.find('#modalUpdateRulesContent')
			.load($(this).attr('value'));

 		});
    });
");

?>