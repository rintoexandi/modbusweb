<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Alertrules */

$this->title = 'Update Alertrules: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Alertrules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->alertrules_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alertrules-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
