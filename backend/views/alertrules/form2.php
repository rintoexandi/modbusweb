<?php

use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;


/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

Pjax::begin(['id' => 'createrules']);

?>

<?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL,'options' => ['data-pjax' =>true]]);?>

          <div class="box box-danger">
           
            <div class="box-body">
             
              <?php
              echo Form::widget([ // fields with labels
        'model'=>$model,
        'form'=>$form,
        'columns'=>1,
        'attributes'=>[
             
                'name'=>['label'=>'Rules Name', 'options'=>['placeholder'=>'Rules Name']],
                
                'isdigital'=>[
                        'label'=>Html::label('Digtal Port'),
                        'type'=>Form::INPUT_RADIO_LIST,
                        'items'=>[true=>'Yes', false=>'No'],
                        'options'=>['inline'=>true]
                ],
                'notify'=>[
                        'label'=>Html::label('Send Notification'),
                        'type'=>Form::INPUT_RADIO_LIST,
                        'items'=>[true=>'Yes', false=>'No'],
                        'options'=>['inline'=>true]
                ],
                
                
        ]
]);
              
              ?>
              <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

 <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>