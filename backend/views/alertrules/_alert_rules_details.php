<?php

use common\models\Alertrulesdetails;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */


?>

<div class="row">
     <div class="col-md-12">
     <div class="box">
    	 <div class="box-header with-border">
    	 <div class="col-md-12">
          
              
         </div>
        </div>
         <div class="box-body">  
    
          <?php

                 
                 $query = Alertrulesdetails::find()->where(['alertrules_id'=>$model->alertrules_id])
                  ->indexBy('id')
                  ->orderBy(['sequence' => SORT_ASC]);
                 
                 
                // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                
                 $dataProvider = new ActiveDataProvider([
                 'query' => $query,
                 ]);
                
                 echo GridView::widget([
                'dataProvider' => $dataProvider,          
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                   // 'id',
                   // 'alertrules_id',
                    'sequence',
                    'lowervalue',
                    'uppervalue',
                    //'useboth',
                    //'useduration',
                    'duration',
                    //'useoccurence',
                    //'occurance',
                    'severity',
                   // 'isactive',

                    //['class' => 'yii\grid\ActionColumn'],
                    [
          'class' => 'yii\grid\ActionColumn',
          'header' => 'Actions',
          'headerOptions' => ['style' => 'color:#337ab7'],
          'template' => '{update},{delete}',
          'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'lead-view'),
                ]);
            },

            'update' => function ($url, $model) {
                
                //$urlTo=Url::toRoute($url);
         
                return Button::widget([
                'label' => 'Edit',
                'options' => ['value'=>Url::to($url), 
                'class' => 'btnupdaterules  btn btn-sm btn-success','id'=>'btnUpteRulesDetails'],                           
                 ]);
                
                //return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                //            'title' => Yii::t('app', 'lead-update'),
                //]);
            },
                    
             'delete' => function ($url) {
                return Html::a('<span class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-ban-circle"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                            'data-method' => 'post', 'data-pjax' => '0',
                ]);
            }

          ],
          'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'view') {
                $url ='/alertrulesdetails/view/'.$model->id;
                return $url;
            }

            if ($action === 'update') {
                $url ='/alertrulesdetails/update/'.$model->id;
                return $url;
            }
           if ($action === 'delete') {
               $url = Url::to(['/alertrulesdetails/delete/', 'id' => $model->id]);
                return $url;
                //$url ='/alertrulesdetails/delete/'.$model->id;
                //return $url;
            }

          }
          ],
                ],
            ]); 
                 
          ?>
             
        </div>
    </div> <!-- col-md-12 -->
</div> <!-- /r

