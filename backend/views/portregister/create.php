<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Portregister */

$this->title = 'Create Portregister';
$this->params['breadcrumbs'][] = ['label' => 'Portregisters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portregister-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
