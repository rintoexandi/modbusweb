<?php
use kartik\grid\GridView;
use common\models\Devices;
use common\models\Ports;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
?>

   <div class="box box-info">
    	<div class="box-header with-border">
              <h3 class="box-title">List ModBus Device(s)</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
        
                <?php 
                
                $query = Devices::find();
                
                $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                ]);
                 
                
                 echo GridView::widget([
                         'dataProvider' => $dataProvider,
                         'moduleId' => 'gridviewKartik',
                         'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                                 [
                                         'attribute'=> 'site.name',
                                         'header'=>'Site',
                                         'hAlign' => 'left',
                                         'vAlign' => 'middle',
                                 ],
                                 //'hostname',
                                
                                 [
                                         'attribute'=> 'ipaddress',
                                         'header'=>'IPv4',
                                         'hAlign' => 'left',
                                         'vAlign' => 'middle',
                                         'format'=>'raw',
                                         'value'=>function($data) {
                                             return Html::a($data->ipaddress,'/device/' . $data->device_id);
                                         }
                                 ],
                                 [
                                         'attribute'=> 'uptime',
                                         'header'=>'Up Time',
                                         'hAlign' => 'left',
                                         'vAlign' => 'middle',
                                 ],
                                 [
                                         'attribute'=> 'last_polled',
                                         'header'=>'Last Polled',
                                         'hAlign' => 'left',
                                         'vAlign' => 'middle',
                                 ],
                                 [
                                        'header'=>'Bar',
                                        'format'=>'raw',    
                                         'value'=>function($data){
                                         $span_value ='';
                                         $span_value .='<div class="col-sm-3 col-xs-3">';
                                         $span_value .='<div class="row"><small class="label label-danger"><i class="fa fa-clock-o"></i> 2 error</small></div>';
                                         $span_value .='<div class="row"><small class="label label-info"><i class="fa fa-clock-o"></i> 4 notice</small></div>';
                                         $span_value .='<div class="row"><small class="label label-warning"><i class="fa fa-clock-o"></i> 4 warning</small></div>';
                                         $span_value .='<div class="row"><small class="label label-success"><i class="fa fa-clock-o"></i> 15 running</small></div>';
                                         $span_value .='</div>';
                                         return $span_value;
                                         
                                         }
                                 ],                                 
                                 [
                                         'hAlign' => 'center',
                                         'vAlign' => 'middle',
                                         'width' => '150px',
                                         'format' => 'raw',
                                         'value'=>function ($data) {
                                         
                                         $btn = Html::a('<button class="btn btn-success glyphicon glyphicon-folder-open"></button>', ['device/view','id'=>$data->device_id]);
                                         //$btn .='&nbsp;';
                                         //$btn .= Html::a('<button class="btn btn-danger glyphicon glyphicon-trash"></button>',['device/disable','id'=>$data->device_id]);
                                         return $btn;
                                         }
                                         
                                         
                               ]
                   
                            ],
                    ]); 
                
                 ?>
 
            
     		</div> <!--  Box Body -->
    </div>