<?php
use dosamigos\chartjs\ChartJs;

?>

<section>
		
	<div class="box-body">
        <div class="row">
          <div class="col-xs-6 text-center">
            <?= ChartJs::widget([
    'type' => 'line',
    'options' => [
        'height' => 200,
        'width' => 800
    ],
    'data' => [
        'labels' => ["1", "2", "3", "4", "5", "6", "7"],
        'datasets' => [          
            [
                'label' => "Tegangan PLN",
                'backgroundColor' => "rgba(255,99,132,0.2)",
                'borderColor' => "rgba(255,99,132,1)",
                'pointBackgroundColor' => "rgba(255,99,132,1)",
                'pointBorderColor' => "#fff",
                'pointHoverBackgroundColor' => "#fff",
                'pointHoverBorderColor' => "rgba(255,99,132,1)",
                'data' => [28, 48, 40, 19, 96, 27, 100]
            ]
        ]
    ]
]);
?>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
	</div> <!-- /col-xs-12 -->
</section>
<?php
$this->registerJsFile(
  '@web/js/knob.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>