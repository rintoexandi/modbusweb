<?php

use common\models\Alertrules;
use common\models\Alertrulesdetails;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm as ActiveForm2;
use yii\widgets\Pjax;


/* @var $this View */
/* @var $model Alertrulesdetails */
/* @var $form ActiveForm2 */
?>

<?php Pjax::begin(['id' => 'createrulesdetails']); ?>
<div class="alertrulesdetails-form">

    <?php $form = ActiveForm::begin(['id' => 'create-ruledetails', 'options' => ['data-pjax' => true]]); ?>

    <?php
              echo Form::widget([ // fields with labels
        'model'=>$model,
        'form'=>$form,
        'columns'=>2,
        'attributes'=>[
             
                'alertrules_id'=>['label'=>'Rules ID', 'options'=>['placeholder'=>'Rules ID','disabled'=>'true']],
                'sequence'=>['label'=>'Sequence', 'options'=>['placeholder'=>'Sequence']],
                'lowervalue'=>['label'=>'Lower Treshold', 'options'=>['placeholder'=>'Lower Treshold']],
                'uppervalue'=>['label'=>'Upper Treshold', 'options'=>['placeholder'=>'Upper Treshold']],
               
               
                ]
        ]);
              
    ?>
    
    <?php
              echo Form::widget([ // fields with labels
        'model'=>$model,
        'form'=>$form,
        'columns'=>2,
        'attributes'=>[
             
               
                'useduration'=>[
                        'label'=>Html::label('Use Duration'),
                        'type'=>Form::INPUT_WIDGET,
                        'widgetClass'=>SwitchInput::classname(),
                       
                ],
            	               
                'duration'=>['label'=>'Duration (in minute)', 'options'=>['placeholder'=>'Duration']],

                'useoccurence'=>[
                        'label'=>Html::label('Use Occurence'),
                        'type'=>Form::INPUT_RADIO_LIST,
                        'items'=>[true=>'Yes', false=>'No'],
                        'options'=>['inline'=>true]
                ],            
                
                'occurance'=>['label'=>'Occurance', 'options'=>['placeholder'=>'Occurance']],

                'severity'=>[
                           'label'=>'Severity Level',
                           'type'=>Form::INPUT_WIDGET,
                           'widgetClass'=>Select2::classname(),
                           'options'=>['data'=>Alertrules::listSeverity()],
                          
                 ],
                ]
        ]);
              
    ?>
  

   
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>