<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alertrulesdetails';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alertrulesdetails-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alertrulesdetails', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'alertrules_id',
            'sequence',
            'lowervalue',
            'uppervalue',
            //'useboth',
            //'useduration',
            //'duration',
            //'useoccurence',
            //'occurance',
            //'severity',
            //'isactive',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
