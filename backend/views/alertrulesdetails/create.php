<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Alertrulesdetails */

$this->title = 'Create Alertrules detail';
$this->params['breadcrumbs'][] = ['label' => 'Alertrulesdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alertrulesdetails-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
