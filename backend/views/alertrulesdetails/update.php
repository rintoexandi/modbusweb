<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Alertrulesdetails */

$this->title = 'Update Alertrulesdetails: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Alertrulesdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alertrulesdetails-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
