<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DevicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Devices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devices-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Devices', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'device_id',
            'hostname',
            'ipaddress',
            'port',
            'hardware',
            //'status',
            //'status_reason',
            //'disabled',
            //'uptime:datetime',
            //'agent_uptime:datetime',
            //'last_polled',
            //'last_poll_attempted',
            //'last_polled_timetaken:datetime',
            //'serial',
            //'site_id',
            //'latitude',
            //'longitude',
            //'tahundeployment',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
