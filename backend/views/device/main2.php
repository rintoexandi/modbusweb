<?php

use yii\bootstrap\Html;
use yii\web\Controller;
use yii\widgets\Pjax;
use kartik\spinner\Spinner;
use richardfan\widget\JSRegister;

?>

<section class="content">
 <div class="row">
 <!-- Small boxes (Stat box) -->
    <div class="row">
    
    
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-flash"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total KWH PLN</span>
              <span class="info-box-number"><?php  echo  rand(500000, 1000000);?><small>KWh</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-bell"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Notification</span>
              <span class="info-box-number">5</span><small> new alert</small>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-clock-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Genset Running Hours</span>
              <span class="info-box-number">120 Hours</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-calendar-minus-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Fuel Level</span>
              <span class="info-box-number">2,000</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
<!-- small boxes -->
 
 </div>
 <div class="row">
  
  <div class="boxPjax">
   <div class="col-md-4">
   
   	<div class="row">
         <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Port Digital</h3>
            <div class="box-tools pull-right">
              <!-- Collapse Button -->
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              
            	 <div id="loading">
               	<?php 
                  
                    echo Spinner::widget(['preset' => 'small', 'align' => 'right', 'color' => '#5CB85C']);
                  
                  ?>
           		</div>
            </div>
            <!-- /.box-tools -->
          </div>
           <div class="box-body">
           <div class="row">
           
   		
    	<div id="digital">
    	<?php 
     	
     	echo Yii::$app->controller->renderPartial('listdigital',['model'=>$model]);
     	
     	?>
      </div>	
      
      	</div>
  		 </div>
  		 
  		 <div class="overlay">
              <i class="fa fa-refresh fa-spin"></i>
         </div>

      </div> 		 
   	</div>  <!-- /row -->       
   </div> <!-- left column -->
   
   <div class="col-md-8">
  	 <div id="analog"> 
     	<?php 
     	
     	echo Yii::$app->controller->renderPartial('portanalog',['model'=>$model]);
     	
     	?>
     	</div>
    	
   </div> <!-- right column -->
   </div> <!-- boxPjax -->
 </div><!-- main row -->
</section>



<?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('#loading').show(0);
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			                $('#loading').hide(0);
    			            },
    			            success: function() {
    			                $('.overlay').hide(0);
    			                $('#loading').hide(0);
    			            }
    			        });

    				var $container = $("#digital");
        		        $container.load("/device/digitalajax/<?=$model->device_id?>");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/device/digitalajax/<?=$model->device_id?>');
        		        }, 90000);


    		       // var $container2 = $("#analog");
    		       // $container2.load("/device/analogajax/<?=$model->device_id?>");
    		       // var refreshId2 = setInterval(function()
    		       // {
    		       //     $container2.load('/device/analogajax/<?=$model->device_id?>');
    		       // }, 10000);

        		 });
    </script>
<?php JSRegister::end(); ?>

<?php
  $this->registerJsFile(
  '@web/js/knob.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>