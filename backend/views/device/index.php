<?php

use common\models\DevicesSearch;
use common\models\Witel;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
/* @var $this View */
/* @var $searchModel DevicesSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Devices';
$this->params['breadcrumbs'][] = ['label' => 'Witel', 'url' => ['witel/index']];
//$this->params['breadcrumbs'][] = ['label' => 'Site', 'url' => ['witel/site','id'=>$id]];
$this->params['breadcrumbs'][] = $this->title;

?>

<section class="content">

      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        <p>
        <?php
         $urlTo=Url::toRoute(['/device/create']);
         
        echo Button::widget([
         'label' => 'Tambah RTU',
         'options' => ['value'=>Url::to($urlTo), 
         'class' => 'btn btn-success','id'=>'btn-create-devices'],                           
          ]);
                 
       ?>
       
    </p>
         
        <div id="listdevices">
<div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">List Devices</h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
            
       
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'rowOptions' => function ($model) {
                        if ($model->status == False) {
                            return ['class' => 'danger'];
                        }
                        else  {
                            return ['class' => 'success'];
                        }
                        },
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
            
                        //'device_id',
                        [
                            'header'=>'Witel',
                            'format'=>'raw',
                            'value'=>function($model){
                             
                            
                             $witel = Witel::find()->where(['witel_id'=>$model->site->site_id])->one();
                            
                             if(!empty($witel)) {
                                return $witel->name;
                             }
                             else {
                                 return 'Not Set';
                             }
                           
                             }
                        ],
                        
                        [
                            'header'=>'Site',
                            'format'=>'raw',
                            'value'=>function($model){
                                
                                return $model->site->name;
                            
                            }

                        ],
                       // 'hostname',
                        [
                            'header'=>'RTU',
                            'format'=>'raw',
                            'value'=>function($model) {
                                
                                $url_view ="/device/" . $model->device_id;
                                $hostname = $model->hostname;
                                $ip = $model->ipaddress;
                                
                                $last = $model->last_polled;
                                $uptime = $model->uptime;
                                
                                $result ='<strong><i class="fa fa-server"></i><a href="' . $url_view .'"> ' . $hostname . '</a></strong>';
                            
                                 $result .= '<p class="text-muted"><a href="' . $url_view . '">' .  $ip . '</a></p>';
                                 $result .= '<small>';
                                 $result .= '<p class="text-muted"><i class="fa  fa-clock-o"></i> Last Polled : ' .  $last;
                                // $result .= '<br><i class="fa  fa-hourglass-o"></i> Uptime : ' .  $uptime . '</p>';
                                 
                                 $result .='</small';
                               
                                return $result;
                            
                            }
                        ],
                        //'ipaddress',
                        //'port',
                        //'hardware',
                        //'status',
                        //'status_reason',
                        //'disabled',
                       // 'uptime',
                        //'agent_uptime:datetime',
                       // 'last_polled',
                       // 'last_poll_attempted',
                        //'last_polled_timetaken:datetime',
                        //'serial',
                        //'site_id',
                        //'latitude',
                        //'longitude',
                        //'tahundeployment',
                        //'created',
                        //'createdby',
                        //'updated',
                        //'updateby',
                        [
                          'header'=>'Status',
                          'format'=>'raw',
                          //'attribute'=>'status',
                                'value'=>function($model) {
                                
                                if ($model->status==False) {
                                    
                                    return '<span class="label label-danger"><i class="fa fa-arrow-circle-o-down"></i>Offline</span>';
                                }
                                else {
                                    return '<span class="label label-success"><i class="fa fa-arrow-circle-o-up"></i>Online</span>';
                                }
                                
                                }
                        ],
                        
                        [
                            'header'=>'Uptime',
                            'format'=>'raw',
                            'value'=>function($model){
                            return $model->uptime;
                            }
                        ],

                        [
                            'header'=>'Alert',
                            'format'=>'raw',
                            'value'=>function($model){
                            return 'Alert';
                            }
                        ],

                         [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {port} {update} {delete}',
                            'buttons' => ['view' => function($url, $model) {
                                    return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model->device_id], ['title' => 'View', 'id' => 'modal-btn-view']);
                                },
                                'update' => function($id, $model) {
                                    return Html::a('<span class="btn btn-sm btn-success"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model->device_id], ['title' => 'Update', 'id' => 'modal-btn-view']);
                                },
                                'port' => function($id, $model) {
                                    return Html::a('<span class="btn btn-sm btn-primary"><b class="fa fa-th-list"></b></span>', ['ports', 'id' => $model->device_id], ['title' => 'Port', 'id' => 'modal-btn-view']);
                                },
                                'delete' => function($url, $model) {
                                    return Html::a('<span class="btn btn-sm btn-warning"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model->device_id], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
                                }
                            ]
                        ],        
                    ],
                ]); ?>
              
             		</div> <!--  Box Body -->
   			 </div>    
            </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

 </section>

<?php
        Modal::begin([
                 'id'=>'modalCreateDevices',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalCreateDeviceContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btn-create-devices').on('click', function() {
    	$('#modalCreateDevices').modal('show')
				.find('#modalCreateDeviceContent')
				.load($(this).attr('value'));

 		});
    });
");

?>