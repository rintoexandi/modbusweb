<?php
use kartik\grid\GridView;
use common\models\Deviceport;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$this->title = "Port Digital";
$this->params['breadcrumbs'][] = ['label' => $model->ipaddress, 'url' => ['device/view','id'=>$model->device_id]];
$this->params['breadcrumbs'][] = $this->title;

?>

   <div class="box box-info">
    	<div class="box-header with-border">
              <h3 class="box-title">Port Digital</h3>
      </div>
            <!-- /.box-header -->
            <div class="box-body">
        
                <?php 
                
                $query = Deviceport::find()
                ->joinWith('ports')
                ->joinWith('ports.portgroup')
                ->where(['portgroup.isdigital'=>true,'device_id'=>$model->device_id]);
                
                $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => [
                                'pageSize' => 64,
                        ],
                ]);
                 
                
                 echo GridView::widget([
                         'dataProvider' => $dataProvider,
                         'moduleId' => 'gridviewKartik',
                         'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                                 //'deviceport_id',
                                 //'device_id',
                                 'ports.name',
                                 'ports.description',
                                 'value',
                                 'value_inpercent',
                                 
                                 
                                 [
                                    'header'=>'Status',
                                    'format'=>'raw',
                                    'value'=>function($data) {
                                             
                                            
                                            $value = rand(1,2);
                                            
                                            if ($value==1) {
                                                
                                                $status='<span class="btn btn-block btn-success"><span class="fa  fa-link" style="color:green"></span></span>';
                                                
                                            }
                                            
                                            else {
                                                
                                                
                                                $status='<span class="btn btn-block btn-warning"><span class="fa  fa-unlink" style="color:red"></span></span>';
                                                
                                            }
                                            return $status;
                                     }
                                 ],
                                // 'value_prev',
                                // 'port_status',
                                // 'port_status_prev',
                                 'last_uptime',
                                 'port_uptime:datetime',
                                 //'poll_time',
                                 //'poll_prev',
                                 //'isactive',
                   
                            ],
                    ]); 
                
                 ?>
 
            
     		</div> <!--  Box Body -->
    </div>
    
    <?php
$this->registerJsFile(
  '@web/js/knob.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>