<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Devices */

$this->title = 'Create Devices';
$this->params['breadcrumbs'][] = ['label' => 'Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h1 class="box-title"><?= Html::encode($this->title) ?></h1>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
   
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

		   </div> <!--  Box Body -->
   	     </div> <!-- Box Info -->         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

 </section>
