<?php

use common\models\Devices;
use common\models\Site;
use common\models\Sitetype;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model Devices */
/* @var $form ActiveForm */
?>


   <?php Pjax::begin(['id' => 'devices']) ?>
    <?php $form = ActiveForm::begin(['id' => 'create-devices', 'options' => ['data-pjax' => true]]); ?>

    <?php 

            // Usage with ActiveForm and model
            echo $form->field($model, 'site_id')->widget(Select2::classname(), [
            'data' => Site::listSite(),
            'options' => ['placeholder' => 'Pilih Site ...'],
            'pluginOptions' => [
            'allowClear' => false
            ],
            ]);

    ?>
    <?= $form->field($model, 'hostname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ipaddress')->textInput(['maxlength' => true]) ?>

   <?php 

            // Usage with ActiveForm and model
            echo $form->field($model, 'sitetype_id')->widget(Select2::classname(), [
            'data' => Sitetype::listSitetype(),
            'options' => ['placeholder' => 'Tipe Site'],
            'pluginOptions' => [
            'allowClear' => false
            ],
            ]);

    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

