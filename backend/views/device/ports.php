<?php

use common\models\DeviceportSearch;
use common\models\Devices;
use yii\web\View;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model Devices */

$this->title = $model->hostname;
$this->params['breadcrumbs'][] = ['label' => 'Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devices-view">

  <div class="row">
      
    <div class="col-md-12">
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">List Devices</h3>
          	</div>
            <!-- /.box-header -->
            <div class="box-body">
        
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'device_id',
            'hostname',
            'ipaddress',
        
        ],
    ]) ?>
         </div>
                    
       </div> <!--/box -->
    </div>
 
  
  <div clas="row">
      <div class="col-md-12">
         
                
                <?php 
                
                   $searchModel = new DeviceportSearch();
                   $searchModel->device_id=$model->device_id;
                   $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
                  echo Yii::$app->controller->renderPartial('_deviceport',
                         [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                         ]);
                 ?>
    
      </div>
      
  </div> <!--/row -->
  
</div> <!--/row -->
</div>
