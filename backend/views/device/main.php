<?php

use backend\widgets\battery\BatteryWidget;
use backend\widgets\devices\DeviceinfoWidget;
use backend\widgets\electric\ElectricWidget;
use backend\widgets\temperature\TemperatureWidget;

$this->title="Overview";
$this->params['breadcrumbs'][] = ['label' => 'List RTU', 'url' => ['index']];

$this->params['breadcrumbs'][] = $model->hostname;
?>

<div class="container-fluid"> 
<div class="row">
    <div class="col-md-10">
      <?= DeviceinfoWidget::widget(['model'=>$model]) ?>
    </div>    
</div>    
</div>    
<div class="container-narrow"> 
<div class="row">
       
        <div class="col-md-6">
            <?= ElectricWidget::widget(['device_id'=>$model->device_id]) ?>
        </div>    
        <div class="col-md-3">
            
            <!-- Temperature -->
             <?= TemperatureWidget::widget(['device_id'=>$model->device_id]) ?>
          
        </div>
    
     <div class="col-md-3">
            
           <?= BatteryWidget::widget(['device_id'=>$model->device_id]) ?>
          
        </div>
</div>
</div>

 

