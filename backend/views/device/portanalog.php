<?php
use kartik\grid\GridView;
use common\models\Deviceport;
use common\models\Ports;
use scotthuangzl\googlechart\GoogleChart;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use common\models\Portgroup;
use backend\models\Gauge;
use backend\models\KnobGraph;
use softcommerce\knob\Knob;



$this->title = "Port Analog";
$this->params['breadcrumbs'][] = ['label' => $model->ipaddress, 'url' => ['device/view','id'=>$model->device_id]];
$this->params['breadcrumbs'][] = $this->title;

?>




<?php 

   $portgroup = Portgroup::find()->where(['isdigital'=>false,'usegraph'=>true])->all();
   
   foreach ($portgroup as $group) {
       
       $deviceport = Deviceport::find()
       ->innerJoin('ports','deviceport.port_id=ports.port_id')
       ->andWhere(['deviceport.device_id'=>$model->device_id])
       ->andWhere(['ports.portgroup_id'=>$group->portgroup_id])
       ->all();
       
       //var_dump($deviceport);
       ?>
       
       <div class="box box-info">
           <!-- /.box-header -->
           <div class="box-body">
           <div class="box-header ui-sortable-handle" style="cursor: move;">
           <i class="fa fa-cog"></i>
           <h3 class="box-title"><?=$group->name?></h3>
           <!-- tools box -->
           <div class="pull-right box-tools">
           <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
           <i class="fa fa-times"></i></button>
           </div>
           <!-- /. tools -->
           </div>
           <div class="row">
       
       <?php 
       foreach ($deviceport as $vdcport) {
           ?>
        <div class="col-md-3">
          
          <div class="row">
          
              <div class="col-md-6">
                 <?php 
                  
                 $port_info = Ports::find()->where(['port_id'=>$vdcport->port_id])->one();
                 
                 switch ($group->graphtype) {
                 
                     case Portgroup::$GRAPH_TYPE_BAR :
                         
                         break;
                     case Portgroup::$GRAPH_TYPE_LINE  :
                         
                         break;
                     case Portgroup::$GRAPH_TYPE_VOLTMETER  :
                         
                         $gauge = new Gauge();
                         $gauge->greenFrom=$group->minvalue;
                         $gauge->greenTo=$group->noticevalue;
                         $gauge->yellowFrom=$group->noticevalue;
                         $gauge->yellowTo=$group->warningvalue;
                         $gauge->redFrom=$group->warningvalue;
                         $gauge->redTo=$group->maxvalue;
                         $gauge->min=$group->minvalue;
                         $gauge->max=$group->maxvalue;
                         $gauge->label=$port_info->name;
                         $gauge->value=$vdcport->value;
                          
                         
                         echo $gauge->printGauge();
                         
                         break;
                    case Portgroup::$GRAPH_TYPE_KNOB  :
                         $knob = new KnobGraph();
                         $knob->min=$group->minvalue;
                         $knob->max=$group->maxvalue;
                         $knob->height=75;
                         $knob->width=75;
                         $knob->treshholdinfo=$group->noticevalue;
                         $knob->tresholdwarning=$group->warningvalue;
                         $knob->value=$vdcport->value;
                         
                         echo $knob->printGraph();
                         
                         break;
                     case Portgroup::$GRAPH_TYPE_THERMOMETER :
                         
                         $gauge = new Gauge();
                         $gauge->greenFrom=$group->minvalue;
                         $gauge->greenTo=$group->noticevalue;
                         $gauge->yellowFrom=$group->noticevalue;
                         $gauge->yellowTo=$group->warningvalue;
                         $gauge->redFrom=$group->warningvalue;
                         $gauge->redTo=$group->maxvalue;
                         $gauge->min=$group->minvalue;
                         $gauge->max=$group->maxvalue;
                         $gauge->label=$port_info->name;
                         $gauge->value=$vdcport->value;
                         
                         echo $gauge->printGauge();
                         
                         break;
                     case Portgroup::$GRAPH_TYPE_NOGRAPH :
                         
                         //echo '<div class="count">value=rand(, 35)</div>';
                         break;
                     case Portgroup::$GRAPH_TYPE_HUMIDITY  :
                         
                         $gauge = new Gauge();
                        
                         $gauge->greenFrom=$group->minvalue;
                         $gauge->greenTo=$group->noticevalue;
                         $gauge->yellowFrom=$group->noticevalue;
                         $gauge->yellowTo=$group->warningvalue;
                         $gauge->redFrom=$group->warningvalue;
                         $gauge->redTo=$group->maxvalue;
                         $gauge->min=$group->minvalue;
                         $gauge->max=$group->maxvalue;
                         $gauge->label=$port_info->name;
                         $gauge->value=$vdcport->value;
                         
                         echo $gauge->printGauge();
                         
                         
                         break;
                         
                     case Portgroup::$GRAPH_TYPE_GAUGE  :
                         
                         $gauge = new Gauge();
                         
                         $gauge->greenFrom=$group->minvalue;
                         $gauge->greenTo=$group->noticevalue;
                         $gauge->yellowFrom=$group->noticevalue;
                         $gauge->yellowTo=$group->warningvalue;
                         $gauge->redFrom=$group->warningvalue;
                         $gauge->redTo=$group->maxvalue;
                         $gauge->min=$group->minvalue;
                         $gauge->max=$group->maxvalue;
                         $gauge->label=$port_info->name;
                         $gauge->value=$vdcport->value;
                         
                         echo $gauge->printGauge();
                         
                         
                         break;
                     case Portgroup::$GRAPH_TYPE_COUNTER :
                        
                         echo '<div class="count">' . rand(50000, 100000) .'</div>';
                         
                         break;
                        
                     default:
                        
                 }
                 
                
                     
                 ?>
              
              </div>
          </div>
          <div class="row">
          
           	<div class="col-md-12">
             
              <span>
              	 <?php echo $port_info->description;?>
                </span>
             
              
              </div>
          </div>
                   
       </div>  <!-- /col-md-4 --> 
         	
     
     <?php 
       }//end loop deviceport       
    ?>
    
    </div> <!-- /row -->
         </div><!-- /box body -->
         
     </div><!-- /box-info -->
<?php     
} //End Loop for $port
?>


    
    




<?php 

//$this->registerCssFile("https://fonts.googleapis.com/css?family=Montserrat:300,400,700", [
//        'depends' => [\yii\bootstrap\BootstrapAsset::className()],
//         ]);

?>
<?php 

//$this->registerCss(".count {font-family: 'Montserrat'; height: 50px; max-width: 300px; border-radius: 5px; background-color: #fff; color: #000; text-align: center; line-height: 50px; font-size: 45px; margin: 0 auto;}");

?>