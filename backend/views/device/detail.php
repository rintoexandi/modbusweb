<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use backend;

$this->title = $model->hostname;
$this->params['breadcrumbs'][] = ['label' => 'Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content">
	
	<!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-flash"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total KWH PLN</span>
              <span class="info-box-number">25.000.000 <small>KWh</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-bell"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Notification</span>
              <span class="info-box-number">5</span><small> new alert</small>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-clock-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Genset Running Hours</span>
              <span class="info-box-number">120 Hours</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-calendar-minus-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Fuel Level</span>
              <span class="info-box-number">2,000</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
<!-- small boxes -->
	
      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
           <div class="box box-info">
        	<div class="box-header with-border">
                  <p>
                <?= Html::a('Port Digital', ['digital', 'id' => $model->device_id], ['class' => 'btn btn-primary']) ?> 
                <?= Html::a('Port Analog', ['analog', 'id' => $model->device_id], ['class' => 'btn btn-primary']) ?> 
                 
                <?= Html::a('Update', ['update', 'id' => $model->device_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->device_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            	</p>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
   
               <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'device_id',
            'hostname',
            'ipaddress',
            'port',
            'hardware',
            'status',
            'status_reason',
            'disabled',
            'uptime:datetime',
            'agent_uptime:datetime',
            'last_polled',
            'last_poll_attempted',
            'last_polled_timetaken:datetime',
            'serial',
            'site_id',
            'latitude',
            'longitude',
            'tahundeployment',
            'created',
            'createdby',
            'updated',
            'updateby',
        ],
    ]) ?>

	       
			
        
		   </div> <!--  Box Body -->
   	     </div> <!-- Box Info --> 
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


    </section>
  
 <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
           <div class="box box-info">
        	<div class="box-body">
        	<?= \yii2visjs\yii2visjs::widget([
      'clientOptions' => [
        'editable' => false
      ],
      'visualization'=>'Network',
      'dataSet' => [
              'nodes'=>[
                      ['id'=>1,
                       'label'=>'Node 1'
                      ],
                      ['id'=>2,
                       'label'=>'Node 2'
                      ],
                      ['id'=>3,
                              'label'=>'Node 3'
                      ],
                      ['id'=>4,
                              'label'=>'Node 4'
                      ],
              ],
              'edges'=>[
                      ['from'=>1,
                              'to'=>'3'
                      ],
                      ['from'=>1,
                              'to'=>'2'
                      ],
                      ['from'=>3,
                              'to'=>'4'
                      ],
              ]
      ],
    ]); ?>
       	  
         </div> <!--  box info -->
       </div><!-- box body -->  
       </div>
  </div> <!-- /row -->
  