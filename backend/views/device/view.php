<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Devices */

$this->title = $model->device_id;
$this->params['breadcrumbs'][] = ['label' => 'Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devices-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->device_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->device_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'device_id',
            'hostname',
            'ipaddress',
            'port',
            'hardware',
            'status',
            'status_reason',
            'disabled',
            'uptime:datetime',
            'agent_uptime:datetime',
            'last_polled',
            'last_poll_attempted',
            'last_polled_timetaken:datetime',
            'serial',
            'site_id',
            'latitude',
            'longitude',
            'tahundeployment',
            'created',
            'createdby',
            'updated',
            'updateby',
        ],
    ]) ?>

</div>
