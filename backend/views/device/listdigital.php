<?php

use common\models\Deviceport;
use common\models\Ports;
use kartik\spinner\Spinner;
use backend\models\KnobGraph;
use backend\models\Gauge;
use backend\models\DigitalBox;

$query = Deviceport::find()
->joinWith('ports')
->joinWith('ports.portgroup')
->where(['portgroup.isdigital'=>true,'device_id'=>$model->device_id])->all();

//var_dump($query);
//die();

?>



           
 <?php 
 
 foreach ($query as $vdcport) {
     
     

 ?>
                     
              <?php 
              
              echo '<div class="col-md-6 col-sm-12 col-xs-12">';
              
              if ($vdcport->value==1) {
                 echo '<div class="box box-solid box-success">';
                // echo '<span class="info-box-icon"><i class="fa fa-power-off"></i></span>';
              }
              
              elseif ($vdcport->value==0) {
                  echo '<div class="box box-solid box-warning">';
                 // echo '<span class="info-box-icon"><i class="fa fa-power-off text-warning"></i></span>';
              }
              
              else {
                  echo '<div class="box">';
                //  echo '<span class="info-box-icon"><i class="fa fa-power-off text-warning"></i></span>';
              }
              ?>
              
              		<?php 
               			 $vdcportinfo = Ports::find()->where(['port_id'=>$vdcport->port_id])->one();
               			
               			?>
               			
              <div class="box-header with-border">
                <span class="box-title"><?php echo $vdcportinfo->name;?></span>
                 <i class="<?= $vdcportinfo->icon ?>" style="font-size: 1.4em;text-shadow: 1px 1px 1px #ccc;color:#fff"></i>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <a href="#"  class="fa fa-info-circle"></a>
                </div>
                <!-- /.box-tools -->
              </div>
  
              <div class="box-body">
              
               	
               		 <p>	<span class="info-box-text"><?php echo $vdcportinfo->description;?></span>
                            <?php 
                              
                            if ($vdcport->value==True) {
                                echo '<span class="label label-success">Status OK</span>';
                                }
                                else {
                                    echo '<span class="label label-warning">Off-line</span>';
                                    
                                }
                             ?>
                            <span class="info-box-text"><span class="fa fa-clock-o"></span><?php echo $vdcport->last_uptime; ?></span>
                     </p>
                
                
              </div>
              <!-- /.info-box-content -->
            </div>
           
           </div>
<?php 
 } //end foreach

?>   
 		
   
  