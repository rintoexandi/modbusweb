<?php

use common\models\Alertrules;
use common\models\Deviceport;
use common\models\Ports;
use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

?>
<section class="content">

      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">List Ports on Device </h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
		    <?php Pjax::begin(['id'=>'deviceportajax']); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                     'rowOptions' => function ($model) {
                        if ($model->isactive == false) {
                            return ['class' => 'danger'];
                        }
                        else  {
                            return ['class' => ''];
                        }
                        },
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
            
                        //'port_id',
                        //'port.name',
                        [
                            'header'=>'Port Name',
                            'format'=>'raw',
                           
                            'attribute'=>'name',
                            'value'=>function($model){
                            
                                //return '<i class="fa fa-area-chart"></i>' . $model->port->name;
                                
                                return Html::a('<i class="fa fa-area-chart"></i>' . $model->port->name , ['deviceport/graph', 'id' => $model->deviceport_id]);
                            }
                        ],
                             [
                            'header'=>'Port Type',
                            'format'=>'raw',
                            'attribute'=>'isdigital',
                            'value'=>function($model){
                            
                                $ptype =$model->port->isdigital;
                                
                                if ($ptype==1) {
                                    //digital
                                    return '<span class="label label-success">Digital</span>';
                                }
                                else {
                                    //analog
                                     return '<span class="label label-warning">Analog</span>';
                                }
                                
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filter' => Ports::listPortType(),
                            'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                            ],
                            'filterInputOptions' => ['placeholder' => 'Port Type...'],
                        ],
                        [
                            'header'=>'Port Description',
                            'format'=>'raw',
                          
                            'value'=>function($model){
                                return $model->port->description;
                            }
                        ],
                       
                       
                        [
                            'header'=>'Current Value',
                            'format'=>'raw',
                           
                          
                            'value'=>function($model){
                            
                                $portinfo = $model->getPort();

                                $uom =$portinfo->getUom();

                                $symbol="";
                                if (!empty($uom)) {
                                    $symbol=$uom->symbol;
                                }

                                $decimal=0;

                                if (!empty($portinfo->decimalpoint)) {
                                    $decimal=$portinfo->decimalpoint;
                                }

                                $val = number_format($model->value, $decimal, '.', ',') . ' ' . $symbol;
                                
                                return $val;
                            }
                        ],
                        
                       
                      //  'value',
                        //'port.isdigital',
                        
                      //  'reportalert',
                       [
                        'header'=>'Alert Rules',
                        'format'=>'raw',
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute'=>'alertrules_id',
                        'value'=>function($model){

                            $alert = $model->alertrules;

                                if (!empty($alert)) {
                                    return $alert->name;
                                }
                                else {
                                    return 'Global Config';
                                }
                            },
                        'editableOptions' => function ($model, $key, $index, $widget) {

                             return [
                                 'header' => 'Alert Rules',
                                 'formOptions' => ['action' => ['/deviceport/update']], // point to the new action        
                                 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                 'data' => Alertrules::listRules(),
                             ];
                            },           
                         ],
                        [
                        'header'=>'Report Alert',
                        'format'=>'raw',
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute'=>'reportalert',
                        'value'=>function($model){

                            if ($model->reportalert != true) {
                                return '<span class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-ban-circle"></i></span>';
                            }
                            else {
                                return '<span class="btn btn-sm btn-success"><i class="glyphicon glyphicon-ok"></i></span>';
                            }

                            },
                        'editableOptions' => function ($model, $key, $index, $widget) {

                             return [
                                 'header' => 'Alert Rules',
                                 'formOptions' => ['action' => ['/deviceport/update']], // point to the new action        
                                 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                 'data' => Deviceport::listReportAlert(),
                             ];
                            },           
                         ],
                        [
                        'header'=>'Port Status',
                        'format'=>'raw',
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute'=>'isactive',
                        'value'=>function($model){

                            if ($model->isactive != true) {
                                return '<span class="btn btn-sm btn-danger">Disabled</span>';
                            }
                            else {
                                return '<span class="btn btn-sm btn-success">Enabled</span>';
                            }

                            },
                        'editableOptions' => function ($model, $key, $index, $widget) {

                             return [
                                 'header' => 'Alert Rules',
                                 'formOptions' => ['action' => ['/deviceport/update']], // point to the new action        
                                 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                 'data' => Deviceport::listReportAlert(),
                             ];
                            },           
                         ],
                        
                        
                    ],
                ]); ?>
                <?php Pjax::end(); ?>

				</div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

 </section>
