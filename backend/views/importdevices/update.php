<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Importdevices */

$this->title = 'Update Importdevices: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Importdevices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="importdevices-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
