<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Importdevices */

$this->title = 'Create Importdevices';
$this->params['breadcrumbs'][] = ['label' => 'Importdevices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importdevices-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_uploadfile', [
        'model' => $model,
    ]) ?>

</div>
