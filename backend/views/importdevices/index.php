<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Importdevices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importdevices-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Import CSV', ['upload'], ['class' => 'btn btn-success']) ?>
         
          <?php
        
         $options = [
                    'title' => Yii::t('yii', 'Process Import'),
                    'aria-label' => Yii::t('yii', 'Process Import'),
                    'data-confirm' => Yii::t('yii', 'Process Imported data? Data will be insert to table'),
                    'data-method' => 'post',
                    //'data-pjax' => '10',
                     ];
          $url='/importdevices/process';
          echo Html::a('<span class="btn btn-primary">Process Import Data</span>', $url, $options);
        
        ?>
        <?php
        
         $options2 = [
                    'title' => Yii::t('yii', 'Truncate data'),
                    'aria-label' => Yii::t('yii', 'Truncate Data'),
                    'data-confirm' => Yii::t('yii', 'Truncate data? it will not affect imported data'),
                    'data-method' => 'post',
                    //'data-pjax' => '10',
                     ];
          $url2='/importdevices/truncate';
          echo Html::a('<span class="pull-right"><span class="btn btn-warning">Delete Data</span></span>', $url2, $options2);
        
        ?>
    </p>

    <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">List Devices</h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
  
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'hostname',
            'ipaddress',
            'sitename',
            'witelname',
            'message',
            'isvalid',
            //'sitetype',
            'processed',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            
        ],
    ]); ?>
             </div> <!--  Box Body -->
   			  
            </div>
</div>
