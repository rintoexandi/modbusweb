<?php

$this->title = 'Graph';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="graph">
<div class="row">
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header">
              <h3 class="box-title text-blue">Daily Graph</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body text-center">
              <div class="sparkline" data-type="line" data-spot-Radius="3" data-highlight-Spot-Color="#f39c12" data-highlight-Line-Color="#222" data-min-Spot-Color="#f56954" data-max-Spot-Color="#00a65a" data-spot-Color="#39CCCC" data-offset="90" data-width="100%" data-height="200px" data-line-Width="2" data-line-Color="#39CCCC" data-fill-Color="rgba(57, 204, 204, 0.08)">
                <?php echo implode(",", Yii::$app->graphComponents->generateData(200)); ?>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
        </div> <!--  /col-md-6 -->
        
        <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header">
              <h3 class="box-title text-warning">Weekly Graph</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body text-center">
              <div class="sparkline" data-type="bar" data-width="100%" data-height="200px" data-bar-Width="10" data-bar-Spacing="2" data-bar-Color="#f39c12">
               <?php echo implode(",", Yii::$app->graphComponents->generateData(40)); ?>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
</div>


<div class="row">
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header">
              <h3 class="box-title text-blue">Monthly </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body text-center">
              <div class="sparkline" data-type="line" data-spot-Radius="3" data-highlight-Spot-Color="#f39c12" data-highlight-Line-Color="#222" data-min-Spot-Color="#f56954" data-max-Spot-Color="#00a65a" data-spot-Color="#39CCCC" data-offset="90" data-width="100%" data-height="200px" data-line-Width="2" data-line-Color="#39CCCC" data-fill-Color="rgba(57, 204, 204, 0.08)">
                <?php echo implode(",", Yii::$app->graphComponents->generateData(100)); ?>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
        </div> <!--  /col-md-6 -->
        
        <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header">
              <h3 class="box-title text-warning">Yearly Graph</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body text-center">
              <div class="sparkline" data-type="bar" data-width="100%" data-height="200px" data-bar-Width="10" data-bar-Spacing="2" data-bar-Color="#f39c12">
               <?php echo implode(",", Yii::$app->graphComponents->generateData(40)); ?>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
</div>
</div>

<?php
$this->registerJsFile(
  '@web/js/knob.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>