<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Alertlog */

$this->title = 'Update Alertlog: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Alertlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alertlog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
