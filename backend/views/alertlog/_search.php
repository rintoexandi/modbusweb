<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AlertlogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alertlog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'devicealert_id') ?>

    <?= $form->field($model, 'message') ?>

    <?= $form->field($model, 'port_value') ?>

    <?= $form->field($model, 'prev_value') ?>

    <?php // echo $form->field($model, 'alerted') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
