<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Alertlog */

$this->title = 'Create Alertlog';
$this->params['breadcrumbs'][] = ['label' => 'Alertlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alertlog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
