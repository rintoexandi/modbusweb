<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Alertlog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alertlog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'devicealert_id')->textInput() ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port_value')->textInput() ?>

    <?= $form->field($model, 'prev_value')->textInput() ?>

    <?= $form->field($model, 'alerted')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
