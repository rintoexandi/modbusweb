<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Devicealert */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="devicealert-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'deviceport_id')->textInput() ?>

    <?= $form->field($model, 'open')->textInput() ?>

    <?= $form->field($model, 'severity')->textInput() ?>

    <?= $form->field($model, 'alerted')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
