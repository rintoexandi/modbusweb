<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Deviceport;
use common\models\Ports;
use common\models\Devices;
use common\models\Devicealert;
use common\models\Alertlog;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DevicealertSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alerts';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content">

      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
         
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
           // 'devicealert_id',
           // 'deviceport_id',
            [
                'header'=>'device',
                'format'=>'raw',
                    'value'=>function($data) {
                        $dev_port = Deviceport::find()->where(['deviceport_id'=>$data->deviceport_id])->one();
                        
                        $device = Devices::find()->where(['device_id'=>$dev_port->device_id])->one();
                        
                        return $device->hostname;
                    }
            ],
            
            [
                    'header'=>'Port',
                    'format'=>'raw',
                    'value'=>function($data) {
                    $dev_port = Deviceport::find()->where(['deviceport_id'=>$data->deviceport_id])->one();
                    
                    $port = Ports::find()->where(['port_id'=>$dev_port->port_id])->one();
                    
                    return $port->name;
                    }
             ],
            //'open',
            //'severity',
            
            [
                    'header'=>'Port Message',
                    'format'=>'raw',
                    'value'=>function($data) {
                         $alertLog = Alertlog::find()->where(['devicealert_id'=>$data->devicealert_id])
                         ->orderBy(['id'=>SORT_DESC])->one();
                         
                         $message = json_decode($alertLog->message);
                         //print_r($message);
                         $err = "";
                         
                         $err .= 'Warning Value <span class="label label-info">' . $message->warningvalue . '</span> ';
                         
                         $err .= 'Previous Value <span class="label label-info">' . $message->previous_value . '</span> ';
                         $err .= 'Current Value <span class="label label-info">' . $message->current_value . '</span> ' ;
                         $err .= ' Process time <span class="label label-success">' . $message->process_time . '</span> ';
                         return $err;
                         
                         
                    }
            ],
            
            [
                    'header'=>'Port Value',
                    'format'=>'raw',
                    'value'=>function($data) {
                    $alertlog = Alertlog::find()->where(['devicealert_id'=>$data->devicealert_id])
                    ->orderBy(['id'=>SORT_DESC])->one();
                    
       
                    return '<span class="label label-warning">' . $alertlog->port_value . '</span>';
                    
                    
                    }
                    ],
            [
                   'header'=>'Severity',
                    'format'=>'raw',
                    'value'=>function($data) {
                        
                        if ($data->severity == Devicealert::SEVERITY_CRITICAL) {
                           
                            return '<span class="label label-danger">Critical</span>';
                        }
                        
                        elseif ($data->severity == Devicealert::SEVERITY_WARNING) {
                            
                            return '<span class="label label-warning">Warning</span>';
                        }
                        
                        elseif ($data->severity == Devicealert::SEVERITY_INFO) {
                            
                            return '<span class="label label-info">Info</span>';
                        }
                        elseif ($data->severity == Devicealert::SEVERITY_OK) {
                            
                            return '<span class="label label-success">Status OK</span>';
                        }
                    }
            ],
        
            //'alerted',
            //'created',
            //'updated',

           // ['class' => 'yii\grid\ActionColumn'],
            
            [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'template' => '{view}',
                    'buttons' => [
                            'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                            ]);
                            },
                            
                          
                            
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'view') {
                                $url ='/devicealert/view?id='.$model->devicealert_id;
                                return $url;
                            }
                            
                           
                            
                            }
                   ],
        ],
    ]); ?>

    	</div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

 </section>