<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Devicealert */

$this->title = 'Update Devicealert: ' . $model->devicealert_id;
$this->params['breadcrumbs'][] = ['label' => 'Devicealerts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->devicealert_id, 'url' => ['view', 'id' => $model->devicealert_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="devicealert-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
