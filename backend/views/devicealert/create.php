<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Devicealert */

$this->title = 'Create Devicealert';
$this->params['breadcrumbs'][] = ['label' => 'Devicealerts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devicealert-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
