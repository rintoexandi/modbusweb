<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Devicealert */

$this->title = 'Detail Device Alert';
$this->params['breadcrumbs'][] = ['label' => 'Alert', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content">

      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
         

           <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
        
                    'id',
                    'devicealert_id',
                    'message',
                    'port_value',
                    'prev_value',
                    //'alerted',
                    'created',
                    //'updated',
        
                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

	</div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

 </section>
