<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Witel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="witel-form">

    
    <?php $form = ActiveForm::begin(['id' => 'create-witel', 'options' => ['data-pjax' => true]]); ?>

    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'supervisor_id')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

     
    <?php ActiveForm::end(); ?>

   
</div>
