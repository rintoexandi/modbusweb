<?php

use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Witel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="witel-index">

    
    <p>
        <?php
         $urlTo=Url::toRoute(['/witel/create']);
         
        echo Button::widget([
         'label' => 'Tambah Witel',
         'options' => ['value'=>Url::to($urlTo), 
         'class' => 'btn btn-success','id'=>'btn-create-witel'],                           
          ]);
                 
       ?>
       
    </p>

     <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
             
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'site_id',
            'name',
            'description',
            //'isactive',
            //'isdefault',
            //'supervisor_id',
            //'created',
            //'createdby',
            //'updated',
            //'updatedby',
             
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
             
             </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</div>

<?php
        Modal::begin([
                 'id'=>'modalCreateWitel',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalCreateWitelContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btn-create-witel').on('click', function() {
    	$('#modalCreateWitel').modal('show')
				.find('#modalCreateWitelContent')
				.load($(this).attr('value'));

 		});
    });
");

?>
