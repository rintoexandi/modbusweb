<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
//NavBar::begin([
//    'brandLabel' => 'My Company',
//    'brandUrl' => Yii::$app->homeUrl,
//    'options' => [
//        'class' => 'navbar-inverse navbar-fixed-top',
//    ],
//]);
$menuItems= [
        //['label' => 'Menu', 'options' => ['class' => 'header']],
        [
                'label' => 'Surveillance',
                'icon' => 'dashboard',
                'url' => ['/site/index']
        ],
        [
                'label' => 'Report',
                'icon' => 'fa fa-slideshare',
                'url' => '#',
                'items' => [
                        ['label' => 'Alarm', 'icon' => 'list-ol', 'url' => ['/devicealert/index']],
                        ['label' => 'Operational', 'icon' => 'download','url' => ['/devicealert/index']]
                ],
        ],
        [
                'label' => 'Monitoring',
                'icon' => 'id-card',
                'url' => '#',
                'items'=>[
                     ['label' => 'Arnet', 'icon' => 'list-ol', 'url' => ['/device/index']],
                     ['label' => 'RTU', 'icon' => 'list-ol', 'url' => ['/device/index']],
                ],
        ],
        [
                'label' => 'Administrative',
                'icon' => 'sitemap',
                'url' => '#',
                'items' =>[
                        ['label' => 'Background Process', 'icon' => 'fas fa-circle-o', 'url' => ['/daemon/index']],
                        ['label' => 'Pengguna', 'icon' => 'users', 'url' => ['/user/index']],
                        ['label' => 'Lokasi', 'icon' => 'fas fa-circle-o', 'url' => ['/site/index']],
                        [
                        'label' => 'Hak Akses',
                        'icon' => 'key',
                        'url' => '#',
                        'items' => [
                                ['label' => 'User', 'icon' => 'user', 'url' => ['/user/index'],],
                                ['label' => 'Level Akses', 'icon' => 'level-up', 'url' => ['/authorization/role/index'],],
                                ['label' => 'Modul', 'icon' => 'random', 'url' => ['/authorization/route'],],
                                ['label' => 'Assignment', 'icon' => 'th-large', 'url' => ['/authorization/assignment'],],
                            ],
                        ],
                 ],
                
        ],
        
        
        [
                'label' => 'Parameter',
                'icon' => 'wrench',
                'url' => '#',
                'items' => [
                       
                        ['label' => 'Port Digital', 'icon' => 'list', 'url' => ['/port-digital/index']],
                        ['label' => 'Port Analog', 'icon' => 'list', 'url' => ['/port-analog/index']],
                        ['label' => 'Setting', 'icon' => 'upload', 'url' => ['/settings/index']],
                        ['label' => 'Alert Rules', 'icon' => 'wrenct', 'url' => ['/alertrules/index']],
                        ['label' => 'Log Poller', 'icon' => 'list', 'url' => ['/poll/index']],
                        
                ]
        ],
        
        
        [
                'label' => Yii::t('app', 'Logout') . '(' . Yii::$app->user->identity->username . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']
        ]
];

/*
$menuItems = [
       [
                'label' => Yii::t('app', 'Logout') . '(' . Yii::$app->user->identity->username . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']
        ]
];
*/

echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav navbar-left'],
        'items' => $menuItems,
]);

/**
$menuItemsMain = [
        [
                'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'News Update'),
                'url' => ['#'],
                'active' => false,
                'items' => [
                        [
                                'label' => '<i class="fa fa-user"></i> ' . Yii::t('app', 'Catalog'),
                                'url' => ['/blog/blog-catalog'],
                        ],
                        [
                                'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Post'),
                                'url' => ['/blog/blog-post'],
                        ],
                        [
                                'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Comment'),
                                'url' => ['/blog/blog-comment'],
                        ],
                        [
                                'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Tag'),
                                'url' => ['/blog/blog-tag'],
                        ],
                ],
        ],
        [
                'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Report'),
                'url' => ['#'],
                'active' => false,
                'items' => [
                        [
                                'label' => '<i class="fa fa-user"></i> ' . Yii::t('app', 'Monthly'),
                                'url' => ['/blog/default/blog-catalog'],
                        ],
                        [
                                'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Weekly'),
                                'url' => ['/blog/default/blog-post'],
                        ],
                        [
                                'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Yearly'),
                                'url' => ['/blog/default/blog-comment'],
                        ],
                        [
                                'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Tag'),
                                'url' => ['/blog/default/blog-tag'],
                        ],
                ],
        ],
        [
                'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'System'),
                'url' => ['#'],
                'active' => false,
                'items' => [
                        [
                                'label' => '<i class="fa fa-user"></i> ' . Yii::t('app', 'User'),
                                'url' => ['/user'],
                        ],
                        [
                                'label' => '<i class="fa fa-lock"></i> ' . Yii::t('app', 'Role'),
                                'url' => ['/role'],
                        ],
                ],
        ],
];
echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menuItemsMain,
        'encodeLabels' => false,
]);
**/

//NavBar::end();