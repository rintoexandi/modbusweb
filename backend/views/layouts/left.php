<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use mdm\admin\components\MenuHelper;
use mdm\admin\components\Helper;


/* @var $this \yii\web\View */
/* @var $content string */
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="user-panel">
            <div class="pull-left image">
            
            <img src="../img/logo-telkom.png" alt="Logo Telkom" class="img-responsive">
            </div>
            <div class="pull-left info">
                <p>  <?=Yii::$app->user->identity->name ?></p>
             </div>
        </div>
        <?php } ?>
        
       <?php
               // 'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
               $menuItems= [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Dashboard',
                        'icon' => 'dashboard',
                        'url' => ['/site/index']
                    ],
                    [
                        'label' => 'MD',
                        'icon' => 'id-card',
                        'url' => ['/device/index']
                    ],
                    [
                        'label' => 'Organisasi',
                        'icon' => 'sitemap',
                        'url' => '#',
                        'items' =>[
                            ['label' => 'Arnet', 'icon' => 'star', 'url' => ['/device/index']],
                            ['label' => 'Pengguna', 'icon' => 'users', 'url' => ['/user/index']],
                            ['label' => 'Lokasi', 'icon' => 'fas fa-circle-o', 'url' => ['/site/index']],
                         ],
                    ],

                    [
                        'label' => 'Laporan',
                        'icon' => 'fa fa-slideshare',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Poller', 'icon' => 'list-ol', 'url' => ['/poll/index']],
                            ['label' => 'Laporan 2', 'icon' => 'download','url' => ['/']]
                        ],
                    ],
                    [
                        'label' => 'Tools',
                        'icon' => 'wrench',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Port Type', 'icon' => 'upload', 'url' => ['/port-type/index']],
                            ['label' => 'Port Digital', 'icon' => 'list', 'url' => ['/port-digital/index']],
                            ['label' => 'Port Analog', 'icon' => 'list', 'url' => ['/port-analog/index']],                            
                            ['label' => 'Setting', 'icon' => 'upload', 'url' => ['/settings/index']],
                            ['label' => 'Log Poller', 'icon' => 'list', 'url' => ['/poll/index']],
                            
                        ]
                    ],
                    [
                        'label' => 'Hak Akses',
                        'icon' => 'key',
                        'url' => '#',
                        'items' => [
                            ['label' => 'User', 'icon' => 'user', 'url' => ['/user/index'],],
                            ['label' => 'Level Akses', 'icon' => 'level-up', 'url' => ['/authorization/role/index'],],
                            ['label' => 'Modul', 'icon' => 'random', 'url' => ['/authorization/route'],],
                            ['label' => 'Assignment', 'icon' => 'th-large', 'url' => ['/authorization/assignment'],],
                        ],
                    ],
                   
                   /** [
                    'label' => 'Approval-Admin',
                    'icon' => 'check',
                    'url' => ['/approval/admin'] 
                    ], */
                ];      
$menuItems = Helper::filter($menuItems);
echo dmstr\widgets\Menu::widget([
    'options' => ['class' => 'sidebar-menu tree','data-widget'=> 'tree'],
    'items' => $menuItems,
]); ?>

    </section>

</aside>
