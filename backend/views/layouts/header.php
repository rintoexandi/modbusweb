<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use common\models\Notification;


/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    
    <nav class="navbar navbar-static-top" role="navigation">

       
		
        <div class="navbar-custom-menu">
			
			<ul class="nav navbar-nav">
          
          		<?= $this->render(
                    'top-menu.php',
                    ['directoryAsset' => $directoryAsset]
                ) ?>
        
            	<!-- Notification -->
             		<li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?= Notification::countNotification(Yii::$app->user->identity->id)?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?= Notification::countNotification(Yii::$app->user->identity->id)?> notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                
                  <li>
                    <a href="/notification/index">
                      <i class="fa fa-warning text-yellow"></i> Warning Notification 
                    </a>
                  </li>
                  
                  
                  
                </ul>
              </li>
              <li class="footer"><a href="/notification/index">View all</a></li>
            </ul>
          </li>
             	<!-- /Notification -->
           </ul>
	
            
        </div>
    </nav>
</header>
