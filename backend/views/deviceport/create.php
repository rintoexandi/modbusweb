<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Deviceport */

$this->title = 'Create Deviceport';
$this->params['breadcrumbs'][] = ['label' => 'Deviceports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceport-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
