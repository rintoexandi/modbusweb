<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Deviceport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deviceport-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'device_id')->textInput() ?>

    <?= $form->field($model, 'port_id')->textInput() ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <?= $form->field($model, 'value_inpercent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value_prev')->textInput() ?>

    <?= $form->field($model, 'port_status')->textInput() ?>

    <?= $form->field($model, 'port_status_prev')->textInput() ?>

    <?= $form->field($model, 'last_uptime')->textInput() ?>

    <?= $form->field($model, 'port_uptime')->textInput() ?>

    <?= $form->field($model, 'poll_time')->textInput() ?>

    <?= $form->field($model, 'poll_prev')->textInput() ?>

    <?= $form->field($model, 'isactive')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'updateby')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
