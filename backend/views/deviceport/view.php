<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Deviceport */

$this->title = $model->deviceport_id;
$this->params['breadcrumbs'][] = ['label' => 'Deviceports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceport-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->deviceport_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->deviceport_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'deviceport_id',
            'device_id',
            'port_id',
            'value',
            'value_inpercent',
            'value_prev',
            'port_status',
            'port_status_prev',
            'last_uptime',
            'port_uptime:datetime',
            'poll_time',
            'poll_prev',
            'isactive',
            'created',
            'createdby',
            'updated',
            'updateby',
        ],
    ]) ?>

</div>
