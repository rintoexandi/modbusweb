<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DeviceportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deviceport-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'deviceport_id') ?>

    <?= $form->field($model, 'device_id') ?>

    <?= $form->field($model, 'port_id') ?>

    <?= $form->field($model, 'value') ?>

    <?= $form->field($model, 'value_inpercent') ?>

    <?php // echo $form->field($model, 'value_prev') ?>

    <?php // echo $form->field($model, 'port_status') ?>

    <?php // echo $form->field($model, 'port_status_prev') ?>

    <?php // echo $form->field($model, 'last_uptime') ?>

    <?php // echo $form->field($model, 'port_uptime') ?>

    <?php // echo $form->field($model, 'poll_time') ?>

    <?php // echo $form->field($model, 'poll_prev') ?>

    <?php // echo $form->field($model, 'isactive') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'createdby') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <?php // echo $form->field($model, 'updateby') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
