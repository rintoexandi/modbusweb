<?php

use backend\models\ParseJsonFile;
use common\models\Deviceport;
use dosamigos\chartjs\ChartJs;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model Deviceport */

$this->title = $model->port->description;
$this->params['breadcrumbs'][] = ['label' => 'Device', 'url' => ['device/ports','id'=>$model->device_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceport-view">

    <div class="row">
        <div class="col-md-12">
        
             <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Daily Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                 <div class="box-body">
                   
                     <?php
                      
                        $dailydata = new ParseJsonFile();
        
                        $result = $dailydata->openJson($model->deviceport_id,"daily");

                       
                     
                     ?>
                    <?= ChartJs::widget([
                        'type' => 'line',
                        'options' => [
                            'height' => 200,
                            'width' => 800
                        ],
                        'data' => [
                            //'labels' => ["1", "2", "3", "4", "5", "6", "7"],
                            'datasets' => [          
                                [
                                    'label' => $dailydata->title,
                                    'backgroundColor' => "rgba(255,99,132,0.2)",
                                    'borderColor' => "rgba(255,99,132,1)",
                                    'pointBackgroundColor' => "rgba(255,99,132,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                    'data' => $dailydata->data,
                                ]
                            ]
                        ]
                    ]);
                    ?>
                     
                 </div><!-- /box-body -->
             </div> <!--/box -->    
            
        </div> <!--col-md-12 -->
        
    </div><!--/row -->

    <div class="row">
        <div class="col-md-12">
        
             <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Monthly Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                 <div class="box-body">
                   
                     <?php
                      
                        $monthly = new ParseJsonFile();
        
                        $resultw = $monthly->openJson($model->deviceport_id,"monthly");

                       
                     
                     ?>
                    <?= ChartJs::widget([
                        'type' => 'line',
                        'options' => [
                            'height' => 200,
                            'width' => 800
                        ],
                        'data' => [
                            //'labels' => ["1", "2", "3", "4", "5", "6", "7"],
                            'datasets' => [          
                                [
                                    'label' => $monthly->title,
                                    'backgroundColor' => "rgba(255,99,132,0.2)",
                                    'borderColor' => "rgba(255,99,132,1)",
                                    'pointBackgroundColor' => "rgba(255,99,132,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                    'data' => $monthly->data,
                                ]
                            ]
                        ]
                    ]);
                    ?>
                     
                 </div><!-- /box-body -->
             </div> <!--/box -->    
            
        </div> <!--col-md-12 -->
        
    </div><!--/row -->
    
    <div class="row">
        <div class="col-md-12">
        
             <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"> Yearly Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                 <div class="box-body">
                     <?php
                      
                        $yearly = new ParseJsonFile();
        
                        $resulty = $yearly->openJson($model->deviceport_id,"yearly");

                       
                     
                     ?>
                    <?= ChartJs::widget([
                        'type' => 'line',
                        'options' => [
                            'height' => 200,
                            'width' => 800
                        ],
                        'data' => [
                            //'labels' => ["1", "2", "3", "4", "5", "6", "7"],
                            'datasets' => [          
                                [
                                    'label' => $yearly->title,
                                    'backgroundColor' => "rgba(255,99,132,0.2)",
                                    'borderColor' => "rgba(255,99,132,1)",
                                    'pointBackgroundColor' => "rgba(255,99,132,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                    'data' => $yearly->data,
                                ]
                            ]
                        ]
                    ]);
                    ?>
                     
                 </div><!-- /box-body -->
             </div> <!--/box -->    
            
        </div> <!--col-md-12 -->
        
    </div><!--/row -->
    
</div>
