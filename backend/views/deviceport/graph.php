<?php

use backend\widgets\rrdgraph\RRDGraphWidget;
use common\models\Deviceport;
use yii\web\View;

/* @var $this View */
/* @var $model Deviceport */

$this->title = $model->port->description;
$this->params['breadcrumbs'][] = ['label' => 'Device', 'url' => ['device/ports','id'=>$model->device_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceport-view">
                 
 <?= RRDGraphWidget::widget(['deviceport_id'=>$model->deviceport_id]) ?>
                
    
</div>
