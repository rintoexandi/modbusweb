<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DeviceportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deviceports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceport-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Deviceport', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'deviceport_id',
            'device_id',
            'port_id',
            'value',
            'value_inpercent',
            //'value_prev',
            //'port_status',
            //'port_status_prev',
            //'last_uptime',
            //'port_uptime:datetime',
            //'poll_time',
            //'poll_prev',
            //'isactive',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
