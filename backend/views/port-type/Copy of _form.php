<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;


/* @var $this yii\web\View */
/* @var $model common\models\Portgroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portgroup-form">

<div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">Update Port Type</h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
         <?php 
         $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL, 'formConfig'=>['labelSpan'=>4]]);
         echo Form::widget([
                 'model'=>$model,
                 'form'=>$form,
                 'columns'=>1,
                 'attributes'=>[
                         'address_detail' => [   // complex nesting of attributes along with labelSpan and colspan
                                 'label'=>'Nama',
                                 'labelSpan'=>2,
                                 'columns'=>6,
                                 'attributes'=>[
                                         'name'=>[
                                                 'type'=>Form::INPUT_TEXT,
                                                 'options'=>['placeholder'=>'Nama Port'],
                                                 'columnOptions'=>['colspan'=>3],
                                         ],
                                         'description'=>[
                                                 'type'=>Form::INPUT_TEXT,
                                                 'options'=>['placeholder'=>'description...'],
                                                 'columnOptions'=>['colspan'=>2],
                                         ],
                                        
                                         'isdigital'=>[
                                                 'type'=>Form::INPUT_TEXT,
                                                 'options'=>['placeholder'=>'Digital...'],
                                                 'columnOptions'=>['colspan'=>2],
                                         ],
                                         
                                         'settingtype'=>[
                                                 'type'=>Form::INPUT_TEXT,
                                                 'options'=>['placeholder'=>'settingtype...'],
                                                 'columnOptions'=>['colspan'=>2],
                                         ],
                                         
                                         'minvalue'=>[
                                                 'type'=>Form::INPUT_TEXT,
                                                 'options'=>['placeholder'=>'minvalue...'],
                                                 'columnOptions'=>['colspan'=>2],
                                         ],
                                         
                                         
                                         'maxvalue'=>[
                                                 'type'=>Form::INPUT_TEXT,
                                                 'options'=>['placeholder'=>'maxvalue...'],
                                                 'columnOptions'=>['colspan'=>2],
                                         ],
                                 ]
                         ]
                 ]
         ]);
         ?>
         
         echo '<div class="text-right">' . Html::resetButton('Reset', ['class'=>'btn btn-secondary']) . '</div>';
		ActiveForm::end();

   
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isdigital')->textInput() ?>

    <?= $form->field($model, 'settingtype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minvalue')->textInput() ?>

    <?= $form->field($model, 'maxvalue')->textInput() ?>

    <?= $form->field($model, 'noticevalue')->textInput() ?>

    <?= $form->field($model, 'warningvalue')->textInput() ?>

    <?= $form->field($model, 'errorvalue')->textInput() ?>

    <?= $form->field($model, 'scalefactor')->textInput() ?>

    <?= $form->field($model, 'isactive')->textInput() ?>

    <?= $form->field($model, 'notify')->textInput() ?>

    <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usegraph')->textInput() ?>

    <?= $form->field($model, 'graphtype')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
