<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Portgroup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Portgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portgroup-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->portgroup_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->portgroup_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'portgroup_id',
            'name',
            'description',
            'isdigital',
            'settingtype',
            'minvalue',
            'maxvalue',
            'noticevalue',
            'warningvalue',
            'errorvalue',
            'scalefactor',
            'isactive',
            'notify',
            'unit',
            'usegraph',
            'graphtype',
            'created',
            'createdby',
            'updated',
            'updateby',
        ],
    ]) ?>

</div>
