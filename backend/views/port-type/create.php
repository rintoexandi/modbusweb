<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Portgroup */

$this->title = 'Create Portgroup';
$this->params['breadcrumbs'][] = ['label' => 'Portgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portgroup-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
