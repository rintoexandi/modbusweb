<?php

use common\models\Alertrules;
use common\models\Portgroup;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm as ActiveForm2;

/* @var $this View */
/* @var $model Portgroup */
/* @var $form ActiveForm2 */
?>

<?php 
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
?>
<div class="portgroup-form">

<div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">Update Port Type</h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
         <?php 
           echo Form::widget([ // continuation fields to row above without labels
                 'model'=>$model,
                 'form'=>$form,
                 'columns'=>2,
                 'attributes'=>[
                         'name'=>['label'=>'Nama Port'],
                         'description'=>['label'=>'Keterangan'],
                 ]
         ]);
         
         
         ?>
      
		<?php 
		echo Form::widget([ // continuation fields to row above without labels
		        'model'=>$model,
		        'form'=>$form,
		        'columns'=>2,
		        'attributes'=>[
		                'isdigital'=>[
		                         'type'=>Form::INPUT_WIDGET,
		                        'widgetClass'=>'\kartik\select2\Select2',
		                        'options'=>['data'=>Portgroup::listPortType()],
		                        'label'=>'Tipe Port'],
		                
		                'alertrules_id'=>[
		                        'type'=>Form::INPUT_WIDGET,
		                        'widgetClass'=>'\kartik\select2\Select2',
		                        'options'=>['data'=> Alertrules::listRules()],
		              ],
		        ]
		]);
		
		
		?>
	
	

   

</div>
</div> <!-- Box -->

<div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">Port Setting</h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
         	
		 <?php 
		echo Form::widget([ // continuation fields to row above without labels
		        'model'=>$model,
		        'form'=>$form,
		        'columns'=>4,
		        'attributes'=>[
		                'scalefactor'=>['label'=>'Faktor Skala'],
		                'unit'=>['label'=>'Satuan'],
		                'usegraph'=>[
		                        'type'=>Form::INPUT_WIDGET,
		                        'widgetClass'=>SwitchInput::classname(),
		                        
		                ],
		                'graphtype'=>[
		                        'type'=>Form::INPUT_WIDGET,
		                        'widgetClass'=>'\kartik\select2\Select2',
		                        'options'=>['data'=>Portgroup::listGraphType()]
		                ],
		                
		                
		        ]
		]);
		
		echo Form::widget([ // continuation fields to row above without labels
		        'model'=>$model,
		        'form'=>$form,
		        'columns'=>4,
		        'attributes'=>[
		               'notify'=>[
		                        'type'=>Form::INPUT_WIDGET,
		                        'widgetClass'=>SwitchInput::classname(),
		                        
		                ],
		                'isactive'=>[
		                        'type'=>Form::INPUT_WIDGET,
		                        'widgetClass'=>SwitchInput::classname(),
		                        
		                ],
		                
		                
		        ]
		]);
		
		?>
		
	
  
</div>
</div> <!-- Box -->

  <div class="form-group">
       <?php 
       echo Html::submitButton('Save', ['class' => 'btn btn-md btn-success']);
       ActiveForm::end();
       
       ?>
    </div>

</div>
