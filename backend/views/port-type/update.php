<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Portgroup */

$this->title = 'Update Portgroup: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Portgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->portgroup_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="portgroup-update">
   <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
