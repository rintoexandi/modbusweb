<?php

use common\models\Alertrules;
use common\models\PortgroupSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $searchModel PortgroupSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Portgroups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portgroup-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Portgroup', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	<div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">List Devices</h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
         
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'portgroup_id',
            'name',
            'description',
            // 'isdigital',
            [
                'header'=>'Jenis Port',
                'format'=>'raw',
                'value'=>function($data){
                   
                 if ($data->isdigital) {
                     
                     return "Digital";
                 }
                 else {
                     return "Analog";
                 }
        
                }
                
            ],
            //'settingtype',
            //'minvalue',
            //'maxvalue',
            //'noticevalue',
            //'warningvalue',
            //'errorvalue',
            'scalefactor',
            //'isactive',
            //'notify',
            'unit',
            //'usegraph',
            //'graphtype_id',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',
           // 'alertrules_id',
            [
                'header'=>'Alert Rules',
                'format'=>'raw',
                'value'=>function($data){
                    
                 $alertrules = Alertrules::find()->where(['alertrules_id'=>$data->alertrules_id])->one();
                 
                 if (!empty($alertrules)) {
                     return $alertrules->name;
                 }
                 else {
                     return 'Not Set';
                 }
                
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
   </div>
</div>
