<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PolldataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="polldata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'poll_id') ?>

    <?= $form->field($model, 'isdigital') ?>

    <?= $form->field($model, 'data') ?>

    <?= $form->field($model, 'data_length') ?>

    <?php // echo $form->field($model, 'processed') ?>

    <?php // echo $form->field($model, 'created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
