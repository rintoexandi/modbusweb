<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Polldata */

$this->title = 'Create Polldata';
$this->params['breadcrumbs'][] = ['label' => 'Polldatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polldata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
