<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PortsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ports-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'port_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'portgroup_id') ?>

    <?= $form->field($model, 'isdigital') ?>

    <?php // echo $form->field($model, 'registeredports') ?>

    <?php // echo $form->field($model, 'isactive') ?>

    <?php // echo $form->field($model, 'minvalue') ?>

    <?php // echo $form->field($model, 'maxvalue') ?>

    <?php // echo $form->field($model, 'scalefactor') ?>

    <?php // echo $form->field($model, 'unit') ?>

    <?php // echo $form->field($model, 'usegraph') ?>

    <?php // echo $form->field($model, 'graphtype_id') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'createdby') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <?php // echo $form->field($model, 'updateby') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
