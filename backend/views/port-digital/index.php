<?php

use common\models\PortsSearch;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $searchModel PortsSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'List Port Digital';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> </h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <?php
                   $urlTo=Url::toRoute(['/port-digital/create']);

                   echo Button::widget([
                    'label' => 'Create Port Digital',
                    'options' => ['value'=>Url::to($urlTo), 
                    'class' => 'btn btn-success','id'=>'btn-create-port-digital'],                           
                     ]);
                   echo ' '; 
                   echo Html::a('Reset Search',['/port-digital/index'], 
                    ['class' => 'btn btn-warning']                         
                     );        
                  ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'port_id',
            'name',
            'description',
          //  'uom.name',
          //  'isdigital',
            'alertrules.name',
            //'portgroup_id',
            //'isdigital',
            //'isactive',
            //'minvalue',
            //'maxvalue',
            //'scalefactor',
            //'unit',
            //'usegraph',
            //'graphtype.name',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

         
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                  'view' => function ($url, $model) {
                      return Html::a('<span class="btn btn-sm btn-success">View</span>', $url, [
                                  'title' => Yii::t('app', 'lead-view'),
                      ]);
                  },

                  'update' => function ($url, $model) {

                      //$urlTo=Url::toRoute($url);
                      $buttonId="updatePortAnalog".$model->port_id ;
                      return Button::widget([
                      'label' => 'Edit',
                      'options' => ['value'=>Url::to($url), 
                      'class' => 'btnupdateportdigital  btn btn-sm btn-success','id'=>$buttonId],                           
                       ]);

                      //return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                      //            'title' => Yii::t('app', 'lead-update'),
                      //]);
                  },

                   'delete' => function ($url) {
                      return Html::a('<span class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-ban-circle"></span>', $url, [
                                  'title' => Yii::t('app', 'Delete'),
                                  'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                                  'data-method' => 'post', 'data-pjax' => '0',
                      ]);
                  }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                  if ($action === 'view') {
                      $url = Url::to(['/port-digital/view','id'=>$model->port_id]);
                      return $url;
                  }

                  if ($action === 'update') {
                      $url = Url::to(['/port-digital/update','id'=>$model->port_id]);
                      return $url;
                  }
                 if ($action === 'delete') {
                     $url = Url::to(['/port-digital/delete', 'id' => $model->port_id]);
                      return $url;
                      //$url ='/alertrulesdetails/delete/'.$model->id;
                      //return $url;
                  }

                }
                ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

 </section>

<?php
 
        Modal::begin([
                 'id'=>'modalCreatePortDigital',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                 'header'=>'<h3>Create Port Digital</h3>',
                

                ]);

                echo "<div id='modalCreatePortDigitalContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btn-create-port-digital').on('click', function() {   
       
    	$('#modalCreatePortDigital').modal('show')
			.find('#modalCreatePortDigitalContent')
			.load($(this).attr('value'));

 		});
    });
");

?>

<?php
 
        Modal::begin([
                 'id'=>'modalPortDigital',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                'header'=>'<h3>Update Port Digital</h3>',

                ]);

                echo "<div id='modalPortDigitalContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('.btnupdateportdigital').on('click', function() {   
       
    	$('#modalPortDigital').modal('show')
			.find('#modalPortDigitalContent')
			.load($(this).attr('value'));

 		});
    });
");

?>