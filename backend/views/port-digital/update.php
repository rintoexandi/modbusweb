<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ports */

$this->title = 'Update Ports: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->port_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content">

      <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	
            <!-- /.box-header -->
         <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

		</div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

 </section>
