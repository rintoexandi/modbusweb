<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Deviceportsetting */

$this->title = 'Create Deviceportsetting';
$this->params['breadcrumbs'][] = ['label' => 'Deviceportsettings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceportsetting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
