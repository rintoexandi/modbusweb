<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Deviceportsetting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deviceportsetting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'deviceport_id')->textInput() ?>

    <?= $form->field($model, 'settingtype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minvalue')->textInput() ?>

    <?= $form->field($model, 'maxvalue')->textInput() ?>

    <?= $form->field($model, 'scalefactor')->textInput() ?>

    <?= $form->field($model, 'isactive')->textInput() ?>

    <?= $form->field($model, 'notify')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'updateby')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
