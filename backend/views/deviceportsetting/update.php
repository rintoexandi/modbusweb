<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Deviceportsetting */

$this->title = 'Update Deviceportsetting: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Deviceportsettings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="deviceportsetting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
