<?php

use common\models\Widgets;
use richardfan\widget\JSRegister;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model Widgets */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Widgets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgets-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <div class="row">
    <div class="col-md-12">

                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= Html::encode($this->title) ?> <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
   <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'widget_id',
            'name',
            'description',
            'isactive',
           // 'notify',
          //  'created',
          //  'createdby',
          //  'updated',
          //  'updateby',
            'widgetgroup_id',
          //  'parent_id',
            'port_id',
          //  'sameline',
          //  'sequence',
        ],
    ]) ?>
   </div>
     </div>
    </div>

     
</div>

     <p>
         <?php
             
             $url_create = '/widgets/addchild'; 
             $urlTo=Url::toRoute([$url_create,'id'=>$model->widget_id]);
         
             echo Button::widget([
             'label' => 'Add Child Widget',
             'options' => ['value'=>Url::to($urlTo), 
             'class' => 'btn btn-success','id'=>'btnAddChildWidget'],                           
              ]);
        
            ?>
       
    </p>
                                
 <?= Yii::$app->controller->renderPartial('_widgetdetail',['model'=>$model]) ?>

    <?php
       

        Modal::begin([
                 'id'=>'modalAddWidget',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalAddWidgetContent'></div>";


        Modal::end();

?>


<?php JSRegister::begin(); ?>
    <script>
    	$("document").ready(function(){
            
        $('#btnAddChildWidget').on('click', function() {     
    	$('#modalAddWidget').modal('show')
			.find('#modalAddWidgetContent')
			.load($(this).attr('value'));

 	});
       
        
        }); //document
    </script>
<?php JSRegister::end(); ?>  
                             
    
