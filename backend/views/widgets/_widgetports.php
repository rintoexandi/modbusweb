<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

?>
<div class="row">
    <div class="col-md-12">

                        <div class="box box-success box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Widget member <?= $model->name ?></h3>
                            </div>
    <div class="box-body">
   <?php
   
    $query = Widgets::find()->where(['parent_id'=>$model->widget_id]);
    $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10,
        ],
        'sort' => [
            'defaultOrder' => [
                'sequence' => SORT_ASC,
              
            ]
        ],
    ]);
   
   ?>
    <?= GridView::widget([
        'dataProvider' => $provider,
       
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'widget_id',
            'name',
            'description',
           // 'isactive',

           // 'widgetgroup_id',
            [
                'header'=>'Group',
                'format'=>'raw',
                'value'=>function($data){
                    return $data->widgetgroup->name;
                }
            ],
            //'parent_id',
            /**[
                'header'=>'Parent',
                'format'=>'raw',
                'value'=>function($data){

                $parent = Widgets::findOne($data->parent_id);

                if (!empty($parent)) {
                    return $parent->name;
                }
                else {
                    return '';
                }

                }
            ],**/
            'port_id',
           // 'sameline',
            /**[
                'header' => 'Same Line',
                'format' => 'raw',
                'value' => function($data) {

                    if ($data->sameline == true) {
                        return '<span class="badge bg-green"><i class="fa fa-check-o"></i>True</span>';
                    } else {
                        return '<span class="badge bg-red"><i class="fa fa-check-o"></i>False</span>';
                    }
                }
            ],**/
            //'sequence',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',
            //'widgetgroup_id',
            //'parent_id',
            //'port_id',
            //'sameline',
            //'sequence',

            [
                'header' => 'Status',
                'format' => 'raw',
                'value' => function($data) {

                    if ($data->isactive == true) {
                        return '<span class="badge bg-green"><i class="fa fa-check-o"></i>Active</span>';
                    } else {
                        return '<span class="badge bg-red"><i class="fa fa-check-o"></i>Inactive</span>';
                    }
                }
            ],

           [
          'class' => 'yii\grid\ActionColumn',
          'header' => 'Actions',
          'headerOptions' => ['style' => 'color:#337ab7'],
          'template' => '{view} {update} {delete}',
          'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="btn btn-sm btn-primary"><span class="glyphicon glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'lead-view'),
                ]);
            },

            'update' => function ($url, $model) {
                
                //$urlTo=Url::toRoute($url);
         
                return Button::widget([
                'label' => 'Edit',
                'options' => ['value'=>Url::to($url), 
                'class' => 'btnupdaterules  btn btn-sm btn-success','id'=>'btnUpteRulesDetails'],                           
                 ]);
                
                //return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                //            'title' => Yii::t('app', 'lead-update'),
                //]);
            },
                    
             'delete' => function ($url) {
                return Html::a('<span class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-ban-circle"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                            'data-method' => 'post', 'data-pjax' => '0',
                ]);
            }

          ],
          'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'view') {
                $url ='/widgets/ports/'.$model->widget_id;
                return $url;
            }

            if ($action === 'update') {
                $url ='/widgets/update/'.$model->widget_id;
                return $url;
            }
           if ($action === 'delete') {
               $url = Url::to(['/widgets/delete/', 'id' => $model->widget_id]);
                return $url;
                //$url ='/alertrulesdetails/delete/'.$model->id;
                //return $url;
            }

          }
          ],
        ],
    ]); ?>
        
    </div>
  </div>                  
  </div>
 </div>