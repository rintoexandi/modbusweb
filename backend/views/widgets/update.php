<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Widgets */

$this->title = 'Update Widgets: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Widgets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->widget_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="widgets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
