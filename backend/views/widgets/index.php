<?php

use common\models\Widgets;
use common\models\WidgetsSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $searchModel WidgetsSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Widgets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgets-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Widgets', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="row">
    <div class="col-md-12">

                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= Html::encode($this->title) ?> <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                        <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'widget_id',
            'name',
            'description',
           // 'isactive',

           // 'widgetgroup_id',
            [
                'header'=>'Group',
                'format'=>'raw',
                'value'=>function($data){
                    return $data->widgetgroup->name;
                }
            ],
            //'parent_id',
            /**[
                'header'=>'Parent',
                'format'=>'raw',
                'value'=>function($data){

                $parent = Widgets::findOne($data->parent_id);

                if (!empty($parent)) {
                    return $parent->name;
                }
                else {
                    return '';
                }

                }
            ],**/
            'port_id',
           // 'sameline',
            /**[
                'header' => 'Same Line',
                'format' => 'raw',
                'value' => function($data) {

                    if ($data->sameline == true) {
                        return '<span class="badge bg-green"><i class="fa fa-check-o"></i>True</span>';
                    } else {
                        return '<span class="badge bg-red"><i class="fa fa-check-o"></i>False</span>';
                    }
                }
            ],**/
            //'sequence',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',
            //'widgetgroup_id',
            //'parent_id',
            //'port_id',
            //'sameline',
            //'sequence',

            [
                'header' => 'Status',
                'format' => 'raw',
                'value' => function($data) {

                    if ($data->isactive == true) {
                        return '<span class="badge bg-green"><i class="fa fa-check-o"></i>Active</span>';
                    } else {
                        return '<span class="badge bg-red"><i class="fa fa-check-o"></i>Inactive</span>';
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


                             </div> <!--/box-body -->
                        </div> <!-- /box -->
         </div> <!--col-md-12 -->
    </div> <!--/row -->
    <?php Pjax::end(); ?>
</div>
