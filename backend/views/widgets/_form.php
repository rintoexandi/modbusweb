<?php

use common\models\Ports;
use common\models\Widgets;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Widgets */
/* @var $form ActiveForm */
?>

<div class="widgets-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
 
  
    <?php
    
    // Usage with ActiveForm and model
        echo $form->field($model, 'port_id')->widget(Select2::classname(), [
            'data' => Ports::listPorts(),
            'options' => ['placeholder' => 'Select Port...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    
    ?>
    
    <?= $form->field($model, 'sequence')->textInput() ?>

    
	<?php 
	echo $form->field($model, 'sameline')->widget(SwitchInput::classname(), []);
	
	?>
   
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
