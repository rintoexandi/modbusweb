<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Ports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ports-view">

    <h1><?= Html::encode($this->title) ?></h1>


    
    <div class="row">
        
        <div class="col-md-6">
	<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#ports" data-toggle="tab" aria-expanded="true">Port</a></li>
              <li class=""><a href="#portregister" data-toggle="tab" aria-expanded="false">Port Register</a></li>
              
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="ports">
                            <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'port_id',
                        'name',
                        'description',
                        'portgroup_id',
                        'isactive',
                        ],
                ]) ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="portregister">
                 <?php 
                  echo Yii::$app->controller->renderPartial('_port_register',
                         [
                            'port'=>$model,
                         ]);
                 ?>
              </div>
              <!-- /.tab-pane -->


            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>   
        </div> <!-- / row -->
 </div>

