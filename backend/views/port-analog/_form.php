<?php

use common\models\Alertrules;
use common\models\Ports;
use common\models\Uom;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model Ports */
/* @var $form ActiveForm */
?>

<div class="ports-form">
 <?php Pjax::begin(['id' => 'port-analog']); ?>
    <?php $form = ActiveForm::begin(['id' => 'create-port-analog', 'options' => ['data-pjax' => true]]); ?>

    <div class="row">
        <div class="col-md-12">    

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

        </div>
    </div>    
    
    <div class="row">    
    
        <div class="col-md-6">
     <?= $form->field($model, 'decimalpoint')->textInput(['maxlength' => false]) ?>
        </div>
        <div class="col-md-6">
    <?= $form->field($model, 'scalefactor')->textInput(['maxlength' => false]) ?>
        </div>
    
    </div>

    <div class="row">
    
   <div class="col-md-6"> 
    <?php 
    echo '<label class="control-label">Unit</label>';
    echo Select2::widget([
            'model' => $model,
            'attribute' => 'uom_id',
            'data' => Uom::listUom(),    	      
            'pluginOptions' => [
                    'allowClear' => true
            ],
    ]);

    ?>
    </div>   
    <div class="col-md-6"> 
     <?php 
    echo '<label class="control-label">Alert Rules</label>';
    echo Select2::widget([
            'model' => $model,
            'attribute' => 'alertrules_id',
            'data' => Alertrules::listRules(),    	      
            'pluginOptions' => [
                    'allowClear' => true
            ],
    ]);

    ?>
    </div>
    </div>

   <div class="row">    
       <div class="col-md-12">
    <?php 
    echo $form->field($model, 'isactive')->widget(SwitchInput::classname(), []);

    ?>
       </div>    
     </div> <!--/row -->
     
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
 <?php Pjax::end(); ?>
</div>
