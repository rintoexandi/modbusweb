<?php

use common\models\Portregister;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
?>

<?php 

$query = Portregister::find()->where(['port_id'=>$port->port_id])->indexBy('id');

$dataProvider = new ActiveDataProvider([
        'query' => $query,
]);

?>

<?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
            
                        'port_id',
                        'name',
                        'registeredports',
                        'memory',
                        'isactive',
                        //'minvalue',
                        //'maxvalue',
                        //'scalefactor',
                        //'unit',
                        //'created',
                        //'createdby',
                        //'updated',
                        //'updateby',
            
                     //   ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>