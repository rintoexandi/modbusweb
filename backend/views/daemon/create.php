<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Daemon */

$this->title = 'Create Daemon';
$this->params['breadcrumbs'][] = ['label' => 'Daemons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daemon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
