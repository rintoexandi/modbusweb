<?php

use backend\widgets\daemon\DaemonWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Background Process';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daemon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <?php
    
    echo DaemonWidget::widget();
    
    
    ?>
    
    <?php Pjax::end(); ?>
</div>
