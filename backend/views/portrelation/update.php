<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Portrelation */

$this->title = 'Update Portrelation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Portrelations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="portrelation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
