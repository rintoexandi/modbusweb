<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Portrelations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portrelation-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Portrelation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'widget_id',
            'port_id',
            'portanalog_id',
            'isactive',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
