<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Portrelation */

$this->title = 'Create Portrelation';
$this->params['breadcrumbs'][] = ['label' => 'Portrelations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portrelation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
