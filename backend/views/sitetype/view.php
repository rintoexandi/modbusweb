<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sitetype */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sitetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sitetype-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sitetype_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sitetype_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sitetype_id',
            'name',
            'importance_level',
            'isactive',
            'created',
            'createdby',
            'updated',
            'updatedby',
        ],
    ]) ?>

</div>
