<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sitetype */

$this->title = 'Update Sitetype: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sitetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->sitetype_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sitetype-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
