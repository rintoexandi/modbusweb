<?php

use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Sitetypes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sitetype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?php
         $urlTo=Url::toRoute(['/sitetype/create']);
         
        echo Button::widget([
         'label' => 'Tambah Tipe Site',
         'options' => ['value'=>Url::to($urlTo), 
         'class' => 'btn btn-success','id'=>'btn-create-sitetype'],                           
          ]);
                 
       ?>
       
    </p>

    <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sitetype_id',
            'name',
            'importance_level',
            'isactive',
            'created',
            //'createdby',
            //'updated',
            //'updatedby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
             
               </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
   
    <?php Pjax::end(); ?>
             
</div>

<?php
        Modal::begin([
                 'id'=>'modalCreateSiteType',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalCreateSiteTypeContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btn-create-sitetype').on('click', function() {
    	$('#modalCreateSiteType').modal('show')
				.find('#modalCreateSiteTypeContent')
				.load($(this).attr('value'));

 		});
    });
");

?>