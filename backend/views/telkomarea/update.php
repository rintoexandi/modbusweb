<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Telkomarea */

$this->title = 'Update Telkomarea: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Telkomareas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telkomarea-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
