<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Sites */

$this->title = 'Create Sites';
$this->params['breadcrumbs'][] = ['label' => 'Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sites-create">

    <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

              </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</div>
