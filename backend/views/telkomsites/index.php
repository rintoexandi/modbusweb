<?php

use common\models\SitesSearch;
use common\models\Sitetype;
use common\models\Witel;
use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $searchModel SitesSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Sites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sites-index">

   
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
         $urlTo=Url::toRoute(['/telkomsites/create']);
         
        echo Button::widget([
         'label' => 'Tambah Site',
         'options' => ['value'=>Url::to($urlTo), 
         'class' => 'btn btn-success','id'=>'btn-create-sites'],                           
          ]);
        echo ' '; 
        echo Html::a('Reset Search',['/telkomsites/index'], 
         ['class' => 'btn btn-warning']                         
          );  
        
         echo Html::a('Import CSV',['/telkomsites/upload'], 
         ['class' => 'btn btn-primary']                         
          );  
       ?>
       
    </p>
<div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
             <div class="table-responsive">
              
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                    'class' => 'table table-striped',
                    ],
                    
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'site_id',
                        [
                            'header'=>'Witel',
                            'format'=>'raw',
                             'class' => 'kartik\grid\EditableColumn',
                             'attribute' => 'witel_id',
                            'value'=>function($model) {
                              return $model->witel->name;
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                                'filter' => Witel::listWitel(),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                    
                               'editableOptions' => function ($model, $key, $index, $widget) {

                                return [
                                    'header' => 'Site Type',
                                    'formOptions' => ['action' => ['/telkomsites/updatesites']], // point to the new action        
                                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                    'data' => Witel::listWitel(),
                                ];
                            },       
                        ],
                               
                       
                        [
                            'header'=>'Site Name',
                            'format'=>'raw',
                            'attribute'=>'name',
                            'value'=>'name',
                            'class' => 'kartik\grid\EditableColumn',
                             'editableOptions' => function ($model, $key, $index, $widget) {

                                return [
                                    'header' => 'Site Name',
                                    'formOptions' => ['action' => ['/telkomsites/updatesites']], // point to the new action        
                                    'inputType' => Editable::INPUT_TEXT,
                                 
                                ];
                            },
                        ],
                        [
                            'header'=>'Site Description',
                            'format'=>'raw',
                            'attribute'=>'description',
                            'value'=>'description',
                            'class' => 'kartik\grid\EditableColumn',
                             'editableOptions' => function ($model, $key, $index, $widget) {

                                return [
                                    'header' => 'Site Description',
                                    'formOptions' => ['action' => ['/telkomsites/updatesites']], // point to the new action        
                                    'inputType' => Editable::INPUT_TEXT,
                                 
                                ];
                            },
                        ],
                      //  'description',
                        //'isactive',
                        //'isdefault',
                        //'supervisor_id',
                        //'created',
                        //'createdby',
                        //'updated',
                        //'updatedby',
                       // 'witel_id',
                        
                                
                        [
                            'header'=>'Type',
                            'format'=>'raw',
                            'attribute' => 'sitetype_id',
                             'class' => 'kartik\grid\EditableColumn',
                            'value'=>function($model) {
                              return $model->sitetype->name;
                            },                           
                            'filterType' => GridView::FILTER_SELECT2,
                                'filter' => Sitetype::listSitetype(),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                            'editableOptions' => function ($model, $key, $index, $widget) {

                                return [
                                    'header' => 'Site Type',
                                    'formOptions' => ['action' => ['/telkomsites/updatesites']], // point to the new action        
                                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                    'data' => Sitetype::listSitetype(),
                                ];
                            },
                        ],

                  //      ['class' => 'yii\grid\ActionColumn'],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update} {delete}',
                            'buttons' => ['view' => function($url, $model) {
                                    return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['site_id']], ['title' => 'View', 'id' => 'modal-btn-view']);
                                },
                                'update' => function($id, $model) {
                                    return Html::a('<span class="btn btn-sm btn-success"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['site_id']], ['title' => 'Update', 'id' => 'modal-btn-view']);
                                },
                                'delete' => function($url, $model) {
                                    return Html::a('<span class="btn btn-sm btn-warning"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['site_id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
                                }
                            ]
                        ],
                    ],
                ]); ?>
             
   
             </div>
               </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    <?php Pjax::end(); ?>
</div>

<?php
        Modal::begin([
                 'id'=>'modalCreateSites',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                 'clientOptions' => ['backdrop' => false]

                ]);

                echo "<div id='modalCreateSitesContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btn-create-sites').on('click', function() {
    	$('#modalCreateSites').modal('show')
				.find('#modalCreateSitesContent')
				.load($(this).attr('value'));

 		});
    });
");

?>