<?php

use common\models\Sites;
use common\models\Sitetype;
use common\models\Witel;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model Sites */
/* @var $form ActiveForm */
?>

<div class="sites-form">
    <?php Pjax::begin(['id' => 'sites']) ?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>

     <?=$form->field($model, 'witel_id')->dropDownList(
        Witel::listWitel(),
        ['prompt'=>'Select Witel...']
        );
    ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

 
   
    
    <?=$form->field($model, 'sitetype_id')->dropDownList(
        Sitetype::listSitetype(),
        ['prompt'=>'Site Type']
        );
    ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php   Pjax::end(); ?>
</div>
