<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Uploadlog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uploadlog-form">

<div class="row">
     <div class="col-md-12">
     <div class="box box-solid box-default">
     <div class="box-header with-border">
                      <h3 class="box-title">Upload CSV File</h3>
                    </div>
  		<div class="box-body">


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->errorSummary($model); ?>


    <?= $form->field($model, 'csvfile')->fileInput(); ?>
    
   
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

			</div> <!-- box-body -->
		</div><!-- box -->
	</div><!-- col-md -->
</div><!-- row -->

</div>
