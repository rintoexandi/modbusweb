<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DeviceporthistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deviceporthistories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceporthistory-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Deviceporthistory', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'deviceport_id',
            'device_id',
            'port_id',
            'value',
            //'value_prev',
            //'poll_time',
            //'isactive',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
