<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Deviceporthistory */

$this->title = 'Create Deviceporthistory';
$this->params['breadcrumbs'][] = ['label' => 'Deviceporthistories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deviceporthistory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
