<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\WidgetgroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Widgetgroups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgetgroup-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Widgetgroup', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="row">
    <div class="col-md-12">
                 
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= Html::encode($this->title) ?> <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                             <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'widgetgroup_id',
            'name',
            'description',
           // 'isactive',
             [
                'header'=>'Status',
                'format'=>'raw',
                'value'=>function($data){
        
                    if ($data->isactive==true) {
                        return '<span class="badge bg-green"><i class="fa fa-check-o"></i>Active</span>';
                    } 
                    else {
                        return '<span class="badge bg-red"><i class="fa fa-check-o"></i>Inactive</span>';
                    } 
                    
                }  
             ],
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
                                 
                             </div> <!--/box-body -->
                        </div> <!-- /box -->
         </div> <!--col-md-12 -->
    </div> <!--/row -->
</div>
