<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Widgetgroup */

$this->title = 'Update Widgetgroup: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Widgetgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->widgetgroup_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="widgetgroup-update">

   <div class="row">
    <div class="col-md-12">
                  
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= Html::encode($this->title) ?> <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                        <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
                                            </div> <!--/box-body -->
                        </div> <!-- /box -->
         </div> <!--col-md-12 -->
    </div> <!--/row -->
</div>
