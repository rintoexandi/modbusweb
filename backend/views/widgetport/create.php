<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Widgetport */

$this->title = 'Create Widgetport';
$this->params['breadcrumbs'][] = ['label' => 'Widgetports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgetport-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
