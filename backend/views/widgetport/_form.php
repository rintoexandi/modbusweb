<?php

use common\models\Ports;
use common\models\Widgetport;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Widgetport */
/* @var $form ActiveForm */
?>

<div class="widgetport-form">

    <?php $form = ActiveForm::begin(); ?>
 
    <?php
    
    // Usage with ActiveForm and model
        echo $form->field($model, 'port_id')->widget(Select2::classname(), [
            'data' => Ports::listPorts(),
            'options' => ['placeholder' => 'Select Port...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    
    ?>
    
    <?= $form->field($model, 'sequence')->textInput() ?>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
