<?php

use common\models\Portregister;
use common\models\Ports;
use common\models\Widgetport;
use common\models\WidgetportSearch;
use common\models\Widgets;
use kartik\editable\Editable;
use kartik\grid\GridView;
use richardfan\widget\JSRegister;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;
/* @var $this View */
/* @var $searchModel WidgetportSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Widgetports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgetport-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="row">
    <div class="col-md-12">

                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= Html::encode($this->title) ?> <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
   <div class="box-body">
    <?php
    $widget = Widgets::find()->where(['widget_id'=>$widget_id])->one();
    
    ?>
    <?= DetailView::widget([
        'model' => $widget,
        'attributes' => [
            'widget_id',
            'name',
            'description',
            'isactive',
           // 'notify',
          //  'created',
          //  'createdby',
          //  'updated',
          //  'updateby',
            'widgetgroup_id',
          //  'parent_id',
            'port_id',
          //  'sameline',
          //  'sequence',
        ],
    ]) ?>
   </div>
     </div>
    </div>

     
</div>

  
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

     <p>
         <?php
             
             $url_create = '/widgetport/create'; 
             $urlTo=Url::toRoute([$url_create,'id'=>$widget->widget_id]);
         
             echo Button::widget([
             'label' => 'Add Widget Port',
             'options' => ['value'=>Url::to($urlTo), 
             'class' => 'btn btn-success','id'=>'btnAdddWidgetPort'],                           
              ]);
        
            ?>
       
    </p>

    
    <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
            <div class="box-body">
    <?= GridView::widget([
        'moduleId'=>'gridviewKartik',
        'dataProvider' => $dataProvider,
        'pjax'=>true,
        'responsive'=>true,
            
        
      
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
           // 'widget_id',
            [ 
                'label'=>'Widget',
                'attribute'=>'widget_id',
                'class'=>'kartik\grid\EditableColumn',
               
                'editableOptions'=>function ($model, $key, $index, $widget){

                return  [
                    'header'=>'Ports',
                    'formOptions'=>['action' => ['/widgetport/updateport']], // point to the new action        
                    'inputType'=>Editable::INPUT_DROPDOWN_LIST,
                    'data' => Widgets::listWidgets($model->widget_id),

                ];

                },


                'format'=>'raw',
               
                'value'=>function($data){

                
                   return $data->widget->name;



                },
                                    

                                 
            ],//Widget
            [ 
                'label'=>'Port',
                'attribute'=>'port_id',
                'class'=>'kartik\grid\EditableColumn',
               
                'editableOptions'=>function ($model, $key, $index, $widget){

                return  [
                    'header'=>'Ports',
                    'formOptions'=>['action' => ['/widgetport/updateport']], // point to the new action        
                    'inputType'=>Editable::INPUT_DROPDOWN_LIST,
                    'data' => Ports::listPorts(),

                ];

                },


                'format'=>'raw',
               
                'value'=>function($data){

                
                   return $data->port->name;



                },
                                    

                                 
            ],//Ports
            
                
                    [
                'header' => 'Port Description',
                'format' => 'raw',
                'value' => function($data) {
                    
                     return $data->port->description;
        
                 
                }
            ],
           
            [
                'header' => 'Memory',
                'format' => 'raw',
                'value' => function($data) {
                   $register = Portregister::find()->where(['port_id'=>$data->port_id])->all();
                   $memory = "";
                   foreach ($register as $reg) {
                      $memory .= $reg->memory . " "; 
                   }
                 
                   return $memory;
                }
            ],
                 
           // 'port_id',
           // 'sequence',
                    
                     [ 
                'label'=>'Sequence',
                'attribute'=>'sequence',
                'class'=>'kartik\grid\EditableColumn',
               
                'editableOptions'=>function ($model, $key, $index, $widget){

                return  [
                    'header'=>'Ports',
                    'formOptions'=>['action' => ['/widgetport/updateport']], // point to the new action        
                    'inputType'=>Editable::INPUT_DROPDOWN_LIST,
                    'data' => Widgetport::listSequence(),

                ];

                },


                'format'=>'raw',
               
                'value'=>function($data){

                
                   return $data->sequence;



                },
                                    

                                 
            ],//Ports
            'isactive',
            //'created',
            //'createdby',
            //'updated',
            //'updateby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
                
                </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
   
                
</div>

<?php
       

        Modal::begin([
                 'id'=>'modalAddWidget',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalAddWidgetContent'></div>";


        Modal::end();

?>


<?php JSRegister::begin(); ?>
    <script>
    	$("document").ready(function(){
            
        $('#btnAdddWidgetPort').on('click', function() {     
    	$('#modalAddWidget').modal('show')
			.find('#modalAddWidgetContent')
			.load($(this).attr('value'));

 	});
       
        
        }); //document
    </script>
<?php JSRegister::end(); ?>  