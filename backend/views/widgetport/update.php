<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Widgetport */

$this->title = 'Update Widgetport: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Widgetports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="widgetport-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
