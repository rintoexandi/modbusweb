<?php

use common\models\PollSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $searchModel PollSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Polls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poll-index">
<?php Pjax::begin(); ?>
    
      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Poll Data</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'poll_id',
           // 'device_id',
            [
                'header'=>'Device',
                'format'=>'raw',
                'value'=>function($model) {
        
                    return Html::a($model->device->hostname, ['device/view', 'id' => $model->device->device_id], ['class' => 'profile-link']);
        
        
         //$model->device->name;
                
                }
            ],
           
            'poll_attempted',
            'poll_prevattempted',
             [
                 'header'=>'Status',
                 'format'=>'raw',
                 'value'=>function($model) {
                
                    if ($model==true) {
                        return '<span class="fa fa-check-square text-success"></span>';
                    }
                    else {
                           return '<span class="fa fa-arrow-circle-down text-warning"></span>';  
                    }
                 }
             ],         

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
                
                    </div>
            <!-- /.box-body -->
          
          </div>
    <?php Pjax::end(); ?>
</div>
