<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Poll */

$this->title = 'Update Poll: ' . $model->poll_id;
$this->params['breadcrumbs'][] = ['label' => 'Polls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->poll_id, 'url' => ['view', 'id' => $model->poll_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="poll-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
