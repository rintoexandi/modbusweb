<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Witel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Witels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="witel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->witel_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->witel_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'witel_id',
            'area_id',
            'name',
            'description',
            'parent_id',
            'isactive',
            'created',
            'createdby',
            'updated',
            'updatedby',
        ],
    ]) ?>

</div>
