<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Witel */

$this->title = 'Update Witel: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Witels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->witel_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="witel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
