<?php

use common\models\Devicealert;
use common\models\Witel;
use common\models\WitelSearch;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
/* @var $this View */
/* @var $searchModel WitelSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Witel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="witel-index">

    
    <p>
        <?php
         $urlTo=Url::toRoute(['/witel/create']);
         
        echo Button::widget([
         'label' => 'Tambah Witel',
         'options' => ['value'=>Url::to($urlTo), 
         'class' => 'btn btn-success','id'=>'btn-create-witel'],                           
          ]);
                 
       ?>
       
    </p>

     <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
             <?= GridView::widget([
             'dataProvider' => $dataProvider,
             'layout' => '{items}{pager}',
             'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],

                // 'site_id',
                // 'name',
                 [
                     'header'=>'Witel',
                     'format'=>'raw',
                     'value'=>function($model){
                            $url='/witel/site/'.$model->witel_id;
                            
                            return '<a href="'.$url .'">'. $model->name . '</a>';
                     }
                 ],
                 
                 [
                     'header'=>'Normal',
                     'format'=>'raw',
                     'value'=>function($model){
                      $normal = Witel::countDevices($model->witel_id,true);
                            return '<span class="btn btn-success">' .$normal . '</span>';
                     }
                 ],
                 [
                     'header'=>'Critical',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                      $critical = Witel::getAlerts(Devicealert::SEVERITY_CRITICAL,$model->witel_id);
                    //  $warning = Witel::getAlerts(Devicealert::SEVERITY_WARNING,$model->witel_id);
                    //  $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                     
                      $cr = '<span class="btn btn-danger">' . $critical . '</span>';
                     // $cr .= '<span class="btn btn-warning">' . $warning . '</span>';
                     // $cr .= '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                         [
                     'header'=>'Mayor',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                   //   $critical = Witel::getAlerts(Devicealert::SEVERITY_CRITICAL,$model->witel_id);
                      $warning = Witel::getAlerts(Devicealert::SEVERITY_WARNING,$model->witel_id);
                   //   $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                     
                    //  $cr = '<span class="btn btn-danger">' . $critical . '</span>';
                      $cr = '<span class="btn btn-warning">' . $warning . '</span>';
                     // $cr .= '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                         [
                     'header'=>'Minor',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                    
                      $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                      
                      $cr = '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                 [
                     'header'=>'Down',
                     'format'=>'raw',
                     'value'=>function($model){
                         $down = Witel::countDevices($model->witel_id,false);
                            return '<span class="btn btn-default">' .$down . '</span>';
                     }
                 ],
                 
                 
                 //'isactive',
                 //'isdefault',
                 //'supervisor_id',
                 //'created',
                 //'createdby',
                 //'updated',
                 //'updatedby',


               //  ['class' => 'yii\grid\ActionColumn'],
             ],
         ]); ?>
             
               
    
             </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</div>

<?php
        Modal::begin([
                 'id'=>'modalCreateWitel',
                 'size'=>'modal-md',
                 'class'=>'modal fade',
                

                ]);

                echo "<div id='modalCreateWitelContent'></div>";


        Modal::end();


$this->registerJs("
    $(function(){
	$('#btn-create-witel').on('click', function() {
    	$('#modalCreateWitel').modal('show')
				.find('#modalCreateWitelContent')
				.load($(this).attr('value'));

 		});
    });
");

?>