<?php

use common\models\Telkomarea;
use common\models\Witel;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Witel */
/* @var $form ActiveForm */
?>

<div class="witel-form">

    
    <?php $form = ActiveForm::begin(['id' => 'create-witel', 'options' => ['data-pjax' => true]]); ?>

     
    <?=$form->field($model, 'area_id')->dropDownList(
        Telkomarea::listTelkomarea(),
        ['prompt'=>'Pilih Area']
        );
    ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
  
    <?= $form->field($model, 'parent_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

     
    <?php ActiveForm::end(); ?>

   
</div>


