<?php

use common\models\Devicealert;
use common\models\Telkomsites;
use common\models\Witel;
use yii\grid\GridView;
use yii\helpers\Html;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

$this->title = 'Site';
$this->params['breadcrumbs'][] = ['label' => 'Witel', 'url' => ['witel/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="witel-index">

     <div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
             
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'site_id',
           // 'name',
            [
                'header'=>'Site Name',
                'format'=>'raw',
                'value'=>function($model){
                    $url='/device/site/'.$model->site_id;
                            
                    return '<a href="'.$url .'">'. $model->name . '</a>';
                }
            ],
            [
                     'header'=>'Normal',
                     'format'=>'raw',
                     'value'=>function($model){
                      $normal = Telkomsites::countDevicesOnSite($model->site_id,true);
                            return '<span class="btn btn-success">' .$normal . '</span>';
                     }
                 ],
                 [
                     'header'=>'Critical',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                      $critical = Telkomsites::getAlertsOnSite(Devicealert::SEVERITY_CRITICAL,$model->site_id);
                    //  $warning = Witel::getAlerts(Devicealert::SEVERITY_WARNING,$model->witel_id);
                    //  $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                     
                      $cr = '<span class="btn btn-danger">' . $critical . '</span>';
                     // $cr .= '<span class="btn btn-warning">' . $warning . '</span>';
                     // $cr .= '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                         [
                     'header'=>'Mayor',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                   //   $critical = Witel::getAlerts(Devicealert::SEVERITY_CRITICAL,$model->witel_id);
                      $warning = Telkomsites::getAlertsOnSite(Devicealert::SEVERITY_WARNING,$model->site_id);
                   //   $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                     
                    //  $cr = '<span class="btn btn-danger">' . $critical . '</span>';
                      $cr = '<span class="btn btn-warning">' . $warning . '</span>';
                     // $cr .= '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                         [
                     'header'=>'Minor',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                    
                      $info = Telkomsites::getAlertsOnSite(Devicealert::SEVERITY_INFO,$model->site_id);
                      
                      $cr = '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                 [
                     'header'=>'Down',
                     'format'=>'raw',
                     'value'=>function($model){
                         $down = Telkomsites::countDevicesOnSite($model->site_id,false);
                            return '<span class="btn btn-default">' .$down . '</span>';
                     }
                 ],
            
          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
             
             </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</div>

