<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Witel */

$this->title = 'Create Witel';
$this->params['breadcrumbs'][] = ['label' => 'Witels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="witel-create">
<div class="row">
         <!-- /.col -->
        <div class="col-md-12">
        
        <div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

              </div> <!--  Box Body -->
   			 </div>         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</div>
