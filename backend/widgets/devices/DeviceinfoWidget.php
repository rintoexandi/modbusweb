<?php
namespace backend\widgets\devices;
use yii\base\Widget;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */


class DeviceinfoWidget extends Widget{
    
    public $model;
    
    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();
      
                
        return $this->render('deviceinfo',[
                'model'=>$this->model
                ]);
    }
    
}
