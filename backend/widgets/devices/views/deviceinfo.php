<?php

use common\models\Telkomsites;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

?>
<div id="deviceinfo">
    <div class="row">
        <div class="col-md-12">
          <h3>
              <?php
              
              $site= Telkomsites::findOne($model->site_id);
              
             
              
              $sitename = "";
              
              if ($site!==null) {
                  
                  $sitename=$site->description;
              }
              ?>
              <i class="fa fa-server"></i>  <?= Html::a($model->hostname, ['device/ports/','id'=>$model->device_id]) ?>   
              <small><?= $sitename ?></small>
             <span class="pull-right"><small> Last Polled : <i class="fa fa-clock-o"></i><?=$model->last_polled ?></span>
          </h3>
        </div>
        <!-- /.col -->
    </div>
</div> <!-- /deviceinfo -->
  
  
  <?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			             },
    			            success: function() {
    			                $('.overlay').hide(0);
    			            }
    			        });

    				var $container = $("#deviceinfo");
        		        $container.load("/device/deviceinfowidget/<?=$model->device_id?>");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/device/deviceinfowidget/<?=$model->device_id?>');
        		        }, 10000);


        		 });
    </script>
<?php JSRegister::end(); ?>