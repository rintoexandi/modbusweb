<?php

use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<div id="listdevices">
<div class="box box-info">
        	<div class="box-header with-border">
                  <h3 class="box-title">List Devices</h3>
          	</div>
            <!-- /.box-header -->
         <div class="box-body">
            
       
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'rowOptions' => function ($model) {
                        if ($model->status == False) {
                            return ['class' => 'danger'];
                        }
                        else  {
                            return ['class' => 'success'];
                        }
                        },
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
            
                        //'device_id',
                        'site.name',
                        'hostname',
                        'ipaddress',
                        //'port',
                        //'hardware',
                        //'status',
                        //'status_reason',
                        //'disabled',
                        'uptime',
                        //'agent_uptime:datetime',
                        'last_polled',
                        'last_poll_attempted',
                        //'last_polled_timetaken:datetime',
                        //'serial',
                        //'site_id',
                        //'latitude',
                        //'longitude',
                        //'tahundeployment',
                        //'created',
                        //'createdby',
                        //'updated',
                        //'updateby',
                        [
                          'header'=>'Status',
                          'format'=>'raw',
                          'attribute'=>'status',
                                'value'=>function($model) {
                                
                                if ($model->status==False) {
                                    
                                    return '<span class="label label-danger">Offline</span>';
                                }
                                else {
                                    return '<span class="label label-success">Online</span>';
                                }
                                
                                }
                        ],
                            
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
              
             		</div> <!--  Box Body -->
   			 </div>    
   </div>
   			 
   <?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('#loading').show(0);
    			            },
    			            complete: function() {
    			                $('#loading').hide(0);
    			             },
    			            success: function() {
    			                $('#loading').hide(0);
    			            }
    			        });

    				var $container = $("#listdevices");
        		        $container.load("/device/deviceswidget");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/device/deviceswidget');
        		        }, 10000);


        		 });
    </script>
<?php JSRegister::end(); ?>   
    
    