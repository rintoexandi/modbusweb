<?php

namespace backend\widgets\devices;

use Yii;
use yii\base\Widget;
use common\models\DevicesSearch;

class DevicesWidget extends Widget{
    
    public function init(){
        parent::init();
    }
    
    public function run() {
        parent::run();
        
        $searchModel = new DevicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('devices', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }
}