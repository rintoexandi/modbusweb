<?php

use backend\models\RRDTool;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

$imgpath = Yii::getAlias('@web') ."/" . Yii::$app->params['rrdgraph'] . $deviceport_id;
        
?>

<div id="rrdgraph">
     
             <div class="row">
                 
                 <div class="col-md-6">
                      <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Hourly Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                       <div class="box-body">  
                       
                        <div class="col-md-12">   
                            <?php
                              //Hourly
                              $hourly = new RRDTool();
                              $reth = $hourly->getGraph($deviceport_id, "-1h");


                              echo Html::img($imgpath . "-1h.png",['alt'=>'Hourly Graph'], ['class'=>'img-thumbnail']); 
                            ?>
                        </div>
                       
                      </div>
                   </div>
                 </div>
                 
                 <div class="col-md-6">
                      <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Daily Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                       <div class="box-body">   
                            <div class="col-md-12">   
                     <?php
                       //Daily
                       $daily = new RRDTool();
                       $retd = $daily->getGraph($deviceport_id, "-1d");
                       echo Html::img($imgpath . "-1d.png", ['alt'=>'Daily Graph']); 
                     ?>
                      </div>
                       </div>
                   </div>
                 </div>
                 
             </div>
             
             <div class="row">
                 <div class="col-md-6">
                      <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Weekly Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                       <div class="box-body">  
                            <div class="col-md-12">   
                     <?php
                      
                       $weekly = new RRDTool();
                       $retw = $weekly->getGraph($deviceport_id, "-1w");
                       echo Html::img($imgpath . "-1w.png", ['alt'=>'Weekly Graph']); 
                     ?>
                            </div>
                      </div>
                           
                   </div>
                 </div>
                 <div class="col-md-6">
                      <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Monthly Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                       <div class="box-body">   
                            <div class="col-md-12">   
                     <?php
                       //Hourly
                       $montlhy = new RRDTool();
                       $retm = $montlhy->getGraph($deviceport_id, "-1m");
                       echo Html::img($imgpath . "-1m.png", ['alt'=>'Monthly Graph']); 
                     ?>
                      </div>
                       </div>
                   </div>
                 </div>
                 
                
             </div>
             
             <div class="row">
                 <div class="col-md-6">
                     
                     <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Yearly Graph <i class="fa fa-arrow-circle-right"></i></h3>
                            </div>
                       <div class="box-body">   
                            <div class="col-md-12">   
                     <?php
                       //Hourly
                       $yearly = new RRDTool();
                       $rety = $yearly->getGraph($deviceport_id, "-1y");
                       echo Html::img($imgpath . "-1y.png", ['alt'=>'Yearly Graph']); 
                     ?>
                      </div>
                       </div>
                   </div>
                 </div>
                 
                 
             </div> 
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
         

            </div> <!--box-body-->
    </div> <!-- box -->
</div> <!-- /rrdgraph -->

<?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			             },
    			            success: function() {
    			                $('.overlay').hide(0);
    			            }
    			        });

    				var $container = $("#rrdgraph");
        		        $container.load("/deviceport/graphwidget/<?=$deviceport_id?>");
        		        var refreshId = setInterval(function()
        		        {
                                   //
        		            $container.load('/deviceport/graphwidget/<?=$deviceport_id?>');
        		        }, 100000);


                        
                         
        		 });
 

    </script>
<?php JSRegister::end(); ?>