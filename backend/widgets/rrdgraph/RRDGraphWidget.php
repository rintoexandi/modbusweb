<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace backend\widgets\rrdgraph;

use yii\base\Widget;


class RRDGraphWidget extends Widget {
    
     public $deviceport_id;
     
    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();
      
                
        return $this->render('rrdgraph',[
                'deviceport_id'=>$this->deviceport_id
                ]);
    }
    
    
}