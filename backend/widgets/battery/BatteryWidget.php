<?php

namespace backend\widgets\battery;

use yii\base\Widget;

class BatteryWidget extends Widget {
    
    
    public $widgetgroup_id=2;
    public $device_id=0;
    
    
    public function init(){
        parent::init();
        
    }
    
    public function run() {
        parent::run();
        return $this->render('battery',['widgetgroup_id'=>$this->widgetgroup_id,
                'device_id'=>$this->device_id
                ]);
    }
    
}