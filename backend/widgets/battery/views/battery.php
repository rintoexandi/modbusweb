<?php

use common\models\Deviceport;
use common\models\Widgetport;
use common\models\Widgets;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

?>

<div id="battery">
 
      <div class="box box-solid box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Rectifier</h3>
              
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           

          

    <?php
 
 //$widgetgroup = Widgetgroup::find()->where(['isactive'=>true,'widgetgroup_id'=>$widgetgroup_id])->all();
 
 
 //var_dump($parents);
 //die();

 //foreach ($parents as $parent) {
     
    
    
    //$widgets = Widgets::find()->where(['isactive'=>true,
    //                                    'widgetgroup_id'=>$group->widgetgroup_id,
    //                                    'parent_id'=>0])->all();
    
    
    
     $parents = Widgetport::find()
                      ->join('INNER JOIN','widget as child','child.widget_id=widgetport.widget_id')    
                      ->join('INNER JOIN','widget','child.parent_id=widget.widget_id')                             
                      ->join('INNER JOIN','deviceport','widgetport.port_id=deviceport.port_id')
                      ->where(['deviceport.isactive'=>true,'widgetport.sequence'=>1])
                      ->andWhere(['deviceport.device_id'=>$device_id,'widget.widgetgroup_id'=>$widgetgroup_id])
                      ->orderBy([
                            'sequence' => SORT_ASC])->all();
  
   // var_dump($widgets);
   // die();
    
    foreach ($parents as $parent) {
                 
           
            $childs = Widgetport::find()->where(['isactive'=>true,
                'widget_id'=>$parent->widget_id,                   
                     'sequence'=>2]) ->orderBy(['sequence' => SORT_ASC])->all();   
         
            // $childs = Widgets::find()->where(['isactive'=>true,'parent_id'=>$widget->widget_id])
            //          ->orderBy([
            //                'sequence' => SORT_ASC])->all();
                
          
          foreach ($childs as $child) {
                
            $vdc= Widgetport::find()->where(['widget_id'=>$child->widget_id,'sequence'=>1])->one();
           
            $vdcport = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$vdc->port_id])->one();

            $vdcportinfo = $vdcport->getPort();   
            
            $widgetinfo = Widgets::find()->where(['widget_id'=>$parent->widget_id])->one();
            $uom =$vdcportinfo->getUom();
                    
                    $symbol="";
                    if (!empty($uom)) {
                        $symbol=$uom->symbol;
                    }

                    $decimal=0;

                    if (!empty($vamportinfo->decimalpoint)) {
                        $decimal=$vamportinfo->decimalpoint;
            }        
                     ?>
          <div class="row">      
          <div class="col-md-12">                     
          <div class="small-box bg-green-gradient">
            

            <div class="inner">
           
                 <span class="info-box-text"><?=$widgetinfo->name?></span>
               <?php

               
                 ?>  
                
                <?php
                 $vam= Widgetport::find()->where(['widget_id'=>$child->widget_id,'sequence'=>2])->one();
           
                    $vamport = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$vam->port_id])->one();
                    
                    $vamportinfo = $vamport->getPort();
                    
                    $uom2 =$vamportinfo->getUom();
                    
                    $symbol2="";
                    if (!empty($uom)) {
                        $symbol2=$uom2->symbol;
                    }

                    $decimal2=0;

                    if (!empty($vamportinfo->decimalpoint)) {
                        $decimal2=$vamportinfo->decimalpoint;
                    }
                 ?>  
                   
                 <strong class="box-title"><i class="fa fa-flash"> </i> 
                     
                                            
                    <?=number_format($vdcport->value,$decimal, '.', ',')?> <?=$symbol?> </strong>
                 <small>    <?=Html::a('<i class="fa fa-area-chart"></i>' , ['deviceport/graph', 'id' => $vdcport->deviceport_id]) ?></small>
                
                 <i class="fa fa-dashboard"> </i><strong> <?=number_format($vamport->value,$decimal2, '.', ',')?> <?=$symbol2?></strong> Vamp
                   <small>    <?=Html::a('<i class="fa fa-area-chart"></i>' , ['deviceport/graph', 'id' => $vamport->deviceport_id]) ?></small>
             
                 <p>
                 
                 </p>
                <?php
             
                
                 $failure = Widgetport::find()->where(['widget_id'=>$child->widget_id,'sequence'=>3])->one();
           
                    $failureport = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$failure->port_id])->one();
                    

                    $failportinfo = $failureport->getPort();
                   ?>
                 
                 <span class="text"><?=$failportinfo->description?> : <?=$failureport->getValueAsNormal()?> </span>
             
                  
                  <?php
             
                
                 $failure2 = Widgetport::find()->where(['widget_id'=>$child->widget_id,'sequence'=>4])->one();
           
                    $failureport2 = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$failure2->port_id])->one();
                    

                    $failportinfo2 = $failureport2->getPort();
                   ?>
                 <br>
                  <span class="text"><?=$failportinfo2->description?>: <?=$failureport2->getValueAsNormal()?>
                  </span>
                
        
              </div> <!-- /inner -->
            </div> <!-- /small-box -->
          </div> <!-- /col-lg-3 -->
          <!-- /.info-box -->
           </div> <!-- /row --> 
   <?php
             }
   }//end foreach widget
   
 //}//end for widgetgroup

 ?> 
          
          </div> <!--box-body-->
    </div> <!-- box -->
</div> <!-- /battery -->


<?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			             },
    			            success: function() {
    			                $('.overlay').hide(0);
    			            }
    			        });

    				var $container = $("#battery");
        		        $container.load("/widgetgroup/batterywidget/<?=$device_id?>");
        		        var refreshId = setInterval(function()
        		        {
                                   //
        		            $container.load('/widgetgroup/batterywidget/<?=$device_id?>');
        		        }, 10000);


                        
                         
        		 });
 

    </script>
<?php JSRegister::end(); ?>