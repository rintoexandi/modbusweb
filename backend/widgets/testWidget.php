<?php

namespace backend\widgets;

use yii\base\Widget;

class testWidget extends Widget {
    
    public $percent=0;
    
    public function init(){
        parent::init();
        //$this->posts = \app\models\Post::find()->limit($this->limit)->all();
    }
    
    public function run(){
        return $this->render('testwidget',['percent'=>$this->percent]);
    }
}