<?php

use common\models\Deviceport;
use common\models\Widgetport;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
?>

<div id="temperature">

   <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Room Temperature</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
<?php
 
 
 $parents = Widgetport::find()
                      ->join('INNER JOIN','widget as child','child.widget_id=widgetport.widget_id')    
                      ->join('INNER JOIN','widget','child.parent_id=widget.widget_id')                             
                      ->join('INNER JOIN','deviceport','widgetport.port_id=deviceport.port_id')
                      ->where(['deviceport.isactive'=>true,'widgetport.sequence'=>1])
                      ->andWhere(['deviceport.device_id'=>$device_id,'widget.widgetgroup_id'=>$widgetgroup_id])
                      ->orderBy([
                            'sequence' => SORT_ASC])->all();
    
    foreach ($parents as $parent) {
                 
           
             $childs = Widgetport::find()->where(['isactive'=>true,'widget_id'=>$parent->widget_id,
                     
                     'sequence'=>2])
                      ->orderBy([
                            'sequence' => SORT_ASC])->all();
            
             
             //var_dump($childs);
            // die();
          
             foreach ($childs as $child) {
                 
                     ?>
                              
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="glyphicon glyphicon-home"></i></span>

            <div class="info-box-content">
           
                      
            <?php

                $temp = Widgetport::find()->where(['widget_id'=>$child->widget_id,'sequence'=>1])->one();
           
          

                    $tempport = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$temp->port_id])->one();
                    

                    $portinfo = $tempport->getPort();
                    $symbol="";
                    if (!empty($uom)) {
                        $symbol=$uom->symbol;
                    }

                    $decimal=0;

                    if (!empty($portinfo->decimalpoint)) {
                        $decimal=$portinfo->decimalpoint;
                    }
              
                ?>    
                 
                 <span class="info-box-text"><?=$portinfo->description?></span>
                
             
                 <span class="info-box-number"><small>
                         <i class="fa fa-thermometer-three-quarters fa-lg icon-theme" aria-hidden="true"></i></small> <?=number_format($tempport->value,$decimal, '.', ',')?> <sup style="font-size: 15px">o</sup>C
                         <small>  <?=Html::a('<i class="fa fa-area-chart"></i>' , ['deviceport/graph', 'id' => $tempport->deviceport_id]) ?></small>  
                </span>
             <?php    
                 $humid = Widgetport::find()->where(['widget_id'=>$child->widget_id,'sequence'=>2])->one();
           
             

                    $humidport = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$humid->port_id])->one();
                    

                   $humidportinfo = $humidport->getPort();
                   $uom2 =$humidportinfo->getUom();
                    
                    $symbol2="";
                    if (!empty($uom2)) {
                        $symbol2=$uom2->symbol;
                    }

                    $decimal2=0;

                    if (!empty($humidportinfo->decimalpoint)) {
                        $decimal2=$humidportinfo->decimalpoint;
                    }
              
                ?>    
                 <div class="progress">
                        <div class="progress-bar" style="width: <?=$tempport->value?>%"></div>
                </div>
                
                 <span class="progress-description">
                    <i class="glyphicon glyphicon-tint fa-lg icon-theme" aria-hidden="true"></i><?=$humidportinfo->description?> :   <?=number_format($humidport->value,$decimal, '.', ',')?>
                    <small>  <?=Html::a('<i class="fa fa-area-chart"></i>' , ['deviceport/graph', 'id' => $humidport->deviceport_id]) ?></small>   
                 </span>
                 
               
             </div>  <!-- /.info-box-content -->
         
        </div>
          <!-- /.info-box -->
          
  <?php     
        } //end of child   
   
    
   }//end foreach widget
 

 ?>           </div>
              </div>
              </div>
              </div>
   </div> <!-- /temperature -->
   
   <?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			             },
    			            success: function() {
    			                $('.overlay').hide(0);
    			            }
    			        });

    				var $container = $("#temperature");
        		        $container.load("/widgetgroup/temperaturewidget/<?=$device_id?>");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/widgetgroup/temperaturewidget/<?=$device_id?>');
        		        }, 10000);


        		 });
    </script>
<?php JSRegister::end(); ?>       