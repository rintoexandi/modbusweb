<?php

namespace backend\widgets\temperature;

use yii\base\Widget;

class TemperatureWidget extends Widget{
    
    public $widgetgroup_id=10;
    public $device_id=0;
    
    public function init(){
        parent::init();
     }
    
    public function run() {
        parent::run();
        return $this->render('temperature',['widgetgroup_id'=>$this->widgetgroup_id,
                'device_id'=>$this->device_id
                ]);
    }
    
}