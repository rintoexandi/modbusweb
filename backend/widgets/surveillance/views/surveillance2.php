<?php

use common\models\Devicealert;
use common\models\Devices;
use common\models\Witel;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

?>

<div id="surveillance">
 <!-- Main row -->
 <div class="row">
     
     <?php
      foreach ($model as $witel) {
     ?>
     <div class="col-sm-3">
          <!-- DIRECT CHAT DANGER -->
          <div class="box box-danger direct-chat direct-chat-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><?=Html::a($witel->name, ['/surveillance/view/', 'id' => $witel->witel_id]) ?></h3>

              <div class="box-tools pull-right">
                
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
                <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                              <?php
                              
                              $alert_critical = Witel::getAlerts(Devicealert::SEVERITY_CRITICAL,$witel->witel_id);
                               $alert_warning = Witel::getAlerts(Devicealert::SEVERITY_WARNING,$witel->witel_id);
                              $alert_info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$witel->witel_id);
                             
                              ?>
                                
                              <li><a href="#">RTU Online <span class="pull-right badge bg-green"><?= Devices::countDevicesUp($witel->witel_id)?></span></a></li>
                              <li><a href="#">Critical <span class="pull-right badge bg-red"><?=$alert_critical?></span></a></li>
                              <li><a href="#">Warning <span class="pull-right badge bg-orange"><?=$alert_warning?></span></a></li>
                              <li><a href="#">Info <span class="pull-right badge bg-aqua"><?=$alert_info?></span></a></li>
                            </ul>
                </div>
            </div>
            <!-- /.box-body -->
                      
          </div>
          <!--/.direct-chat -->
        </div>
     
     <?php
      }
     ?>
 </div><!--/row -->
 
 </div> <!--/surveillance -->

 <?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('#loading').show(0);
    			            },
    			            complete: function() {
    			                $('#loading').hide(0);
    			             },
    			            success: function() {
    			                $('#loading').hide(0);
    			            }
    			        });

    				var $container = $("#surveillance");
        		        $container.load("/site/surveillancewidget");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/site/surveillancewidget');
        		        }, 10000);


        		 });
    </script>
<?php JSRegister::end(); ?>   