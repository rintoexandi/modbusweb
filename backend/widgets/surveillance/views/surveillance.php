<?php

use common\models\Devicealert;
use common\models\Witel;
use richardfan\widget\JSRegister;
use yii\grid\GridView;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : PT Telekomunikasi Indonesia
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 * 
 * This surveilance is the main widget on site/index
 */

?>

<div class="container-viewport">
<div id="surveillance">
 <!-- Main row -->
      <div class="row">
        <!-- Left col -->
       <!-- /.row (main row) -->
       <div class="box box-primary">
    	<div class="box-header with-border">
              <h3 class="box-title">RTU NON POP</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?= GridView::widget([
             'dataProvider' => $dataProvider,
             'layout' => '{items}{pager}',
             'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],

                // 'site_id',
                // 'name',
                 [
                     'header'=>'Witel',
                     'format'=>'raw',
                     'value'=>function($model){
                            $url='/witel/site/'.$model->witel_id;
                            
                            return '<a href="'.$url .'">'. $model->name . '</a>';
                     }
                 ],
                 
                 [
                     'header'=>'Normal',
                     'format'=>'raw',
                     'value'=>function($model){
                      $normal = Witel::countDevices($model->witel_id,true);
                            return '<span class="btn btn-success">' .$normal . '</span>';
                     }
                 ],
                 [
                     'header'=>'Critical',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                      $critical = Witel::getAlerts(Devicealert::SEVERITY_CRITICAL,$model->witel_id);
                    //  $warning = Witel::getAlerts(Devicealert::SEVERITY_WARNING,$model->witel_id);
                    //  $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                     
                      $cr = '<span class="btn btn-danger">' . $critical . '</span>';
                     // $cr .= '<span class="btn btn-warning">' . $warning . '</span>';
                     // $cr .= '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                         [
                     'header'=>'Mayor',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                   //   $critical = Witel::getAlerts(Devicealert::SEVERITY_CRITICAL,$model->witel_id);
                      $warning = Witel::getAlerts(Devicealert::SEVERITY_WARNING,$model->witel_id);
                   //   $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                     
                    //  $cr = '<span class="btn btn-danger">' . $critical . '</span>';
                      $cr = '<span class="btn btn-warning">' . $warning . '</span>';
                     // $cr .= '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                         [
                     'header'=>'Minor',
                     'format'=>'raw',
                     'value'=>function($model){
                     
                    
                      $info = Witel::getAlerts(Devicealert::SEVERITY_INFO,$model->witel_id);
                      
                      $cr = '<span class="btn btn-info">' . $info . '</span>';
                      
                            return $cr;
                     }
                 ],
                 [
                     'header'=>'Down',
                     'format'=>'raw',
                     'value'=>function($model){
                         $down = Witel::countDevices($model->witel_id,false);
                            return '<span class="btn btn-default">' .$down . '</span>';
                     }
                 ],
                 
                 
                 //'isactive',
                 //'isdefault',
                 //'supervisor_id',
                 //'created',
                 //'createdby',
                 //'updated',
                 //'updatedby',


               //  ['class' => 'yii\grid\ActionColumn'],
             ],
         ]); ?>
             
               
 
           </div> <!--  Box Body -->
    </div>
   </div>
 
 </div> <!--/surveillance -->
</div>
 <?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('#loading').show(0);
    			            },
    			            complete: function() {
    			                $('#loading').hide(0);
    			             },
    			            success: function() {
    			                $('#loading').hide(0);
    			            }
    			        });

    				var $container = $("#surveillance");
        		        $container.load("/site/surveillancewidget");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/site/surveillancewidget');
        		        }, 10000);


        		 });
    </script>
<?php JSRegister::end(); ?>   