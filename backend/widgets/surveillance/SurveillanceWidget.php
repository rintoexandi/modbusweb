<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : PT Telekomunikasi Indonesia
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace backend\widgets\surveillance;

use common\models\Witel;
use yii\data\ActiveDataProvider;
use yii\jui\Widget;

class SurveillanceWidget extends Widget {
   
    //public $witel_id=0;
    
    public function init(){
        parent::init();

    }
    
    public function run(){
        
        $query = Witel::find()->where(['isactive'=>true]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        return $this->render('surveillance',['dataProvider'=>$dataProvider]);
    }
    
}

