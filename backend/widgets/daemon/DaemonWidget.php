<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace backend\widgets\daemon;

use common\models\Daemon;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

class DaemonWidget extends Widget {
    
     public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();
      
         $dataProvider = new ActiveDataProvider([
            'query' => Daemon::find(),
        ]);

        return $this->render('daemon', [
            'dataProvider' => $dataProvider,
        ]);        
 
    }
}