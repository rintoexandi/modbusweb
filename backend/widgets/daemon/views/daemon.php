<?php

use richardfan\widget\JSRegister;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

?>

<div id="daemon">
<div class="box">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            if ($model->status === 1) {
                return ['class' => 'success'];
            }
            else  {
                
                if ($model->isactive==true) {
                    return ['class' => 'default'];
                }
                else {
                   return ['class' => 'danger'];  
                }               
            }
         },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description',
            
            'last_started',
            'last_finished',
             [
                'header'=>'Execution Duration (s)',
                'format'=>'raw',
                'value'=>function($model){
                   $datestart = new DateTime($model->last_started);
                   $datetfinish = new DateTime($model->last_finished);
                   
                   if ($model->status===1) {
                       $datetfinish = new DateTime("now");
                   }
                   
                   $interval = $datestart->diff($datetfinish);
                   
                   $duration = $interval->format('%s');
                   
                   if ($duration>300) {
                       
                        return '<span class="label label-warning">' . $duration . '</span>'; 
                   }
                   
                   else {
                      return '<span class="label label-success">'  . $duration . '</span>'; 
                   }
                } 
            ],
            'message',
            
            [
                'header'=>'Status',
                'format'=>'raw',
                'value'=>function($model){
                    if ($model->status==1) {
                        
                        return '<span class="label label-success">RUNNING</span>';
                    }
                    
                    else {
                       return '<span class="label label-default">IDLE</span>'; 
                    }
                
                }
            ],
               
             [
                'header'=>'Active',
                'format'=>'raw',
                'value'=>function($model){
                    if ($model->isactive==1) {
                        
                        return '<span class="label label-success"><i class="fa fa-check"></i></span>';
                    }
                    
                    else {
                       return '<span class="label label-danger"><i class="fa fa-close"></i></span>';
                    }
                
                } 
            ],
                           
            [
                'header'=>'Action',
                'format'=>'raw',
                'value'=>function($model){
                    if ($model->isactive==1) {
                        
                         $options = [
                                'title' => Yii::t('yii', 'Disable Process'),
                                'aria-label' => Yii::t('yii', 'Disable Process'),
                                'data-confirm' => Yii::t('yii', 'This will be disabled on next execution, continue?'),
                                'data-method' => 'post',
                                //'data-pjax' => '10',
                            ];
                         
                        return Html::a('<span class="btn  btn-sm btn-warning">DISABLE</span>',Url::to(['/daemon/disable','id'=>$model->id]), $options);
                    }
                    
                    else {
                         $options = [
                                'title' => Yii::t('yii', 'Enable Process'),
                                'aria-label' => Yii::t('yii', 'Enable Process'),
                                'data-confirm' => Yii::t('yii', 'This will be enable on next execution, continue?'),
                                'data-method' => 'post',
                                //'data-pjax' => '10',
                            ];
                       return Html::a('<span class="btn btn-sm btn-success">ENABLE</span>',Url::to(['/daemon/enable','id'=>$model->id]), $options);
                    }
                
                } 
            ],
                   
        ],
    ]); ?>
    </div>
</div>

<?php JSRegister::begin(); ?>
    <script>
    $("document").ready(function(){

                    $.ajaxSetup(
                    {
                        cache: false,
                        beforeSend: function() {
                            $('.overlay').show(0);
                        },
                        complete: function() {
                            $('.overlay').hide(0);
                         },
                        success: function() {
                            $('.overlay').hide(0);
                        }
                    });

                    var $container = $("#daemon");
                    $container.load("/daemon/widget");
                    var refreshId = setInterval(function()
                    {
                       //
                        $container.load('/daemon/widget');
                    }, 10000);




             });


    </script>
<?php JSRegister::end(); ?>