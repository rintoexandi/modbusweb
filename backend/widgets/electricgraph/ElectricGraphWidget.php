<?php

namespace backend\widgets\electricgraph;

use yii\base\Widget;

class ElectricGraphWidget extends Widget{
    
    public function init(){
        parent::init();
       
    }
    
    public function run(){
        return $this->render('electricgraph');
    }
}