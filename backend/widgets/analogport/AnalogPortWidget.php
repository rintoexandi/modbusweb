<?php

namespace backend\widgets\analogport;


use yii\base\Widget;
use common\models\Deviceport;
use common\models\Portrelation;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class AnalogPortWidget extends Widget{
    
    public $model;
    public $device_id;
    
    public function init(){
        parent::init();
    }
    
    public function run() {
        parent::run();
        
        $portRelationAnalog = ArrayHelper::getColumn(Portrelation::find()->where(['isactive'=>1])->asArray()->all(),'portanalog_id');
        
        $query = Deviceport::find();
        $query->joinWith('ports');
        $query->join('INNER JOIN','portgroup','ports.portgroup_id = portgroup.portgroup_id');
        $query->andWhere(['portgroup.isdigital'=>false]);
        $query->andWhere(['deviceport.device_id'=>$this->device_id]);
        $query->andWhere(['NOT IN','deviceport.port_id',$portRelationAnalog]);
        // add conditions that should always apply here
        
        $model = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                        'pageSize' => 100,
                ],
        ]);
        
        return $this->render('analog',['model'=>$model,'device_id'=>$this->device_id]);
    }
}