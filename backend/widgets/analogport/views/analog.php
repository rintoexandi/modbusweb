<?php


use common\models\Ports;
use kartik\grid\GridView;
use kartik\spinner\Spinner;
use richardfan\widget\JSRegister;
use common\models\Portgroup;
use backend\widgets\inlinegraph\InlineGraphWidget;

?>

<div id="analogport">
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Analog Ports</h3>

              <div class="box-tools pull-right">
              	 <div id="loading">
               	<?php 
                  
                    echo Spinner::widget(['preset' => 'small', 'align' => 'right', 'color' => '#5CB85C']);
                  
                  ?>
           		</div>
           		
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <?php 
             
             
             
             echo GridView::widget([
                     'dataProvider' => $model,
                     'moduleId' => 'gridviewKartik',
                     'columns' => [
                             ['class' => 'yii\grid\SerialColumn'],
                             
                             //'ports.name',
                             [
                                'header'=>'Port',
                                'format'=>'raw',  
                                     'value'=>function($data) {
                                       
                                       $icon='<i class="fa fa-cog"></i>';
                                       return $icon;
                                     }
                             ],
                             [
                               'header'=>'Port Name',
                                'attribute'=>'ports.name',    
                                 'format'=>'raw',
                                     'value'=>function($data) {
                                      
                                     $portinfo = Ports::find()->where(['port_id'=>$data->port_id])->one();
                                     
                                     $port_name ='<div class="clearfix">';
                                     
                                     $port_name='<div class="float-left">';
                                     $port_name .='<strong>'. $portinfo->name . '</strong>';
                                     $port_name.='</div>';
                                     $port_name.='<div class="float-right">';
                                     $port_name .='<small class="text-muted">' . $portinfo->description . '</small>';
                                     $port_name .='</div>';
                                     $port_name .='</div>';
                                     
                                     return $port_name;
                                     
                                     }
                             ],
                             //'ports.description',
                             [
                                 'header'=>'Port Message',
                                 'format'=>'raw',             
                                     'value'=>function($data) {
                                        
                                     
                                     $portinfo = Ports::find()->where(['port_id'=>$data->port_id])->one();
                                     $port_name ='<strong>'. $portinfo->description . '</strong>';
                                     
                                     return $port_name;
                                     }
                                     
                             ],
                             //'deviceport_id',
                             //'device_id',
                            // 'port_id',
                            // 'value',
                             [
                                'header'=>'Value',
                                'format'=>'raw',
                                     'value'=>function($data){
                                         
                                         $portinfo = Ports::find()->where(['port_id'=>$data->port_id])->one();
                                        
                                         $portgroup = Portgroup::find()->where(['portgroup_id'=>$portinfo->portgroup_id])->one();
                                     
                                         $min = $portgroup->minvalue;
                                         $max = $portgroup->maxvalue;
                                         $notice = $portgroup->noticevalue;
                                         $warning = $portgroup->warningvalue;
                                         
                                         $percent = ($data->value / $max)*100;
                                         
                                         $result='<div class="clearfix">';
                                         $result.='<div class="float-left">';
                                         $result.='<strong>' . number_format($data->value,2) .'</strong>';
                                         $result.='</div>';
                                         $result.='<div class="float-right">';
                                         $result.='<small class="text-muted">'. $portgroup->unit  .'</small>';
                                         $result.='</div>';
                                         $result.='</div>' ;
                                         $result.='<div class="progress progress-xs">';
                                        
                                         if ($data->value>$min) {
                                             
                                             $result.='<div class="progress-bar bg-default role="progressbar" style="width:' . $percent .'%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>';
                                             
                                         }
                                         
                                         elseif ($data->value>=$notice && $data->value<$warning) {
                                             $result.='<div class="progress-bar bg-green" role="progressbar" style="width: ' . $percent .'%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>';
                                             
                                             
                                         }
                                         else {
                                             $result.='<div class="progress-bar bg-red" role="progressbar" style="width: ' . $percent .'%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>';
                                            
                                            
                                         }
                                       
                                         $result.='</div>';
                                         
                                         return $result;
                                     }
                             ],
                             //'value_inpercent',
                             //'value_prev',
                             //'port_status',
                             //'poll_time:datetime',
                             [
                               'header'=>'Poll Time',
                               'format'=>'datetime',
                                     'value'=>function($data){
                                         return $data->poll_time;
                                     }
                             ],
                             [
                                'header'=>'Port Status',
                                'format'=>'raw',
                                     'value'=>function($data) {
                                     return '<span class="label label-success">Online</span>';
                                     }
                             ],
                            
                             //'port_status_prev',
                             //'last_uptime',
                            // 'poll_time:datetime',
                             //'poll_time',
                             //'poll_prev',
                             //'isactive',
                             //'created',
                             //'createdby',
                             //'updated',
                             //'updateby',
                             
                            
                            ],
             ]);
             
             
             ?>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
          </div>
</div>

<?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('#loading').show(0);
    			            },
    			            complete: function() {
    			                $('#loading').hide(0);
    			             },
    			            success: function() {
    			                $('#loading').hide(0);
    			            }
    			        });

    				var $container = $("#analogport");
        		        $container.load("/portrelation/analogwidget/<?=$device_id?>");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/portrelation/analogwidget/<?=$device_id?>');
        		        }, 10000);


        		 });
    </script>
<?php JSRegister::end(); ?>

<?php
$this->registerJsFile(
  '@web/js/knob.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>