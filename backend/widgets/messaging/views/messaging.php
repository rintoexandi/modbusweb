<?php


use common\models\Notification;
use yii\grid\GridView;

?>


          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
               <div class="table-responsive mailbox-messages">
               
                    
                <?= GridView::widget([
                        'dataProvider' => $dataProvider,                     
                        'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                
                                'key',
                                'id',
                               // 'message',
                                [
                                        'header'=>'Message',
                                        'format'=>'raw',
                                        'value'=>function($data) {
                                            
                                            $msg = json_decode($data->message);
                                            
                                            $prev = $msg->old_value;
                                            $newv = $msg->new_value;
                                            
                                            return $msg->message . ' From ' . $prev . ' change to ' . $newv;
                                        
                                        }
                                        
                                ],
                                'read',
                                'deviceport_id',
                                'created',
                                
                                ['class' => 'yii\grid\ActionColumn'],
                        ],
                ]); ?>
                
                   
               </div>
              
              
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /. box -->
