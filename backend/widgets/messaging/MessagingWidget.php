<?php

namespace backend\widgets\messaging;

use common\models\Notification;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

class MessagingWidget extends Widget{
    
    public $model;
    public $user;
    
    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();
        
        $dataProvider = new ActiveDataProvider([
                'query' => Notification::find()->where(['user_id'=>Yii::$app->user->identity->id]),
        ]);
        
        return $this->render('messaging',['dataProvider'=>$dataProvider]);
    }
    
  
}