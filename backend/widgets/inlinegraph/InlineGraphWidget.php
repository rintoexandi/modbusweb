<?php

namespace backend\widgets\inlinegraph;

use yii\base\Widget;

class InlineGraphWidget extends  Widget{
    
    public $deviceport_id;
    
    public function init(){
        parent::init();
        
    }
    
    public function run(){
        
        return $this->render('inlinegraph',['deviceport_id'=>$this->deviceport_id]);
    }
}