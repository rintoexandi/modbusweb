<?php

namespace backend\widgets\electric;

use yii\web\AssetBundle;

class ElectricWidgetAssets extends AssetBundle{
    
    public $sourcePath = '@backend/widgets/electric/assets';
    public $depends = ['yii\bootstrap\BootstrapAsset'];
    
    /**
     * @inherit
     */
    public $css = [
        'cssled.css',
        'single-line.css',
        'https://github.hubspot.com/odometer/themes/odometer-theme-default.css',
        
    ];
    
    /**
     * @inherit
     */
    public $js = [
        'https://github.hubspot.com/odometer/odometer.js',
    ];
}