<?php

namespace backend\widgets\electric;

use yii\base\Widget;

class ElectricWidget extends Widget {
  
   public $widgetgroup_id=3;
   public $device_id=0;
   
    public function init(){
            ElectricWidgetAssets::register($this->getView());
            parent::init();
    }
    
    public function run() {
 
        return $this->render('electricwidget',['widgetgroup_id'=>$this->widgetgroup_id,
                'device_id'=>$this->device_id
                ]);
    }
    
}