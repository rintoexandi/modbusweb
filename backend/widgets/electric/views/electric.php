<?php

use common\models\Deviceport;
use common\models\Portrelation;
use richardfan\widget\JSRegister;

?>


<div id="electric">
<?php
$portrelation = Portrelation::find()->where(['widget_id'=>$widget_id,'isactive'=>1 ])->all();

foreach ($portrelation as $relation) {
    
    $dev_port = Deviceport::find()->where(['port_id'=>$relation->portanalog_id])->one();
    
    $digital_port = Deviceport::find()->where(['port_id'=>$relation->port_id])->one();
    
    
    $vdcportinfo = $dev_port->getPort();
    
    ?>


        <div class="col-md-4">
          <!-- small box -->
          
           <div class="box-primary">
                <div class="box-body box-profile">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <?php if ($digital_port->value==1){ ?>
                                <div class="led-box">
                                <div class="led-green"></div>
                               
                                 </div>
                                <?php } else { ?>
                                <div class="led-box">
                                <div class="led-gray"></div>
                               
                                 </div>
                                <?php } ?>
                                
                                <h3><?= $vdcportinfo->description ?></h3>
              
                                <?= $vdcportinfo->name ?>
                            </div>
                        </div>
                        <div class="row">
                           
                            
                        <div class="col-md-12">
                            
                             <div class="panel panel-success panel-condensed bg-gray">
                                <div class="panel-heading"><a href="#"><i class="fa fa-bolt fa-flip-horizontal fa-lg icon-theme" aria-hidden="true"></i><strong> Current</strong></a>      </div>
                              
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                    <strong>KVA Max</strong>
                                  <span class="badge badge-primary badge-pill">14</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                  <strong>KVA Current</strong>
                                  <span class="badge badge-primary badge-pill">2</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                   <strong>T. Power</strong>
                                  <span class="badge badge-primary badge-pill">1</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                   <strong>KWH PLN</strong>
                                  <span class="badge badge-primary badge-pill">1</span>
                                </li>
                          </ul>
                             </div>
                        
                                
                        </div>
                     
                    </div>
                     
                    </div>
               
              <div class="row">
                 
                  <div class="col-md-12">
                  
                    <div class="col-md-6">
                        
                        <div class="panel panel-default bg-gray">
                        <!-- Default panel contents -->
                        <div class="panel-heading">  
                            <i class="fa fa-thermometer-three-quarters fa-lg icon-theme" aria-hidden="true"></i><strong> Voltage</strong>
                        </div>

                      
                        <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 4px 4px;">
                                    <strong>RN</strong>
                                  <span class="pull-right">14</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 4px 4px;">
                                  <strong>SN</strong>
                                  <span class="pull-right">2</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 4px 4px;">
                                   <strong>TN</strong>
                                  <span class="pull-right">1</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 4px 4px;">
                                   <strong>RS</strong>
                                  <span class="pull-right">1</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 4px 4px;">
                                   <strong>ST</strong>
                                  <span class="pull-right">1</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 4px 4px;">
                                   <strong>TR</strong>
                                  <span class="pull-right">1</span>
                                </li>
                          </ul>
                        </div>                                          
                    </div>

                    <div class="col-md-6">
                          <div class="panel panel-default bg-gray">
                        <!-- Default panel contents -->
                        <div class="panel-heading">  
                            <i class="fa fa-thermometer-three-quarters fa-lg icon-theme" aria-hidden="true"></i><strong> Ampere</strong>
                        </div>
            
                        <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                    <strong>R</strong>
                                  <span class="pull-right">14</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                  <strong>S</strong>
                                  <span class="pull-right">2</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                   <strong>T</strong>
                                  <span class="pull-right">1</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                   <strong>RN</strong>
                                  <span class="pull-right">1</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                   <strong>SN</strong>
                                  <span class="pull-right">1</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 2px 2px;">
                                   <strong>T</strong>
                                  <span class="pull-right">1</span>
                                </li>
                          </ul>
                          </div>
                
              </div>
              
              </div>
              </div>
            </div>
              
              
        
            <a href="#" class="small-box-footer">
              <?php 
                     
                    if ($digital_port->value==1)
                    {
                        echo "Status : Running";
                    }
                    else {
                        
                        echo "Status : Idle";
                    }
                    ?>
                   <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        
   <?php 
    }
   ?>  
 
   
    
 </div> <!-- /electric -->
   
   <?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			             },
    			            success: function() {
    			                $('.overlay').hide(0);
    			            }
    			        });

    				var $container = $("#electric");
        		        $container.load("/portrelation/electricwidget/<?=$widget_id?>");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/portrelation/electricwidget/<?=$widget_id?>');
        		        }, 1000000);


        		 });
                         
    
    </script>
<?php JSRegister::end(); ?>