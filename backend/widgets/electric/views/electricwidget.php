<?php

use common\models\Deviceport;
use common\models\Widgetgroup;
use common\models\Widgetport;
use common\models\Widgets;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

?>

<div id="electric">
  
    <?php 
     
    $widgetgroup = Widgetgroup::find()->where(['isactive'=>true,'widgetgroup_id'=>$widgetgroup_id])->all();
    
    foreach ($widgetgroup as $group) {
     
        $widgets = Widgets::find()->where(['isactive'=>true,'widgetgroup_id'=>$group->widgetgroup_id])
                   ->andWhere(['parent_id'=>0])->all();
        
        foreach ($widgets as $widget) {
            
            $portstatus =   Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$widget->port_id])->one();
            
            $statusport = 0;
            
            if (!empty($portstatus->value)) {
                $statusport=$portstatus->value;
            }
            
            ?>
            
            <div class="col-md-6">
                 
                <div class="box <?= Widgetgroup::bgBox($statusport)  ?> box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= $widget->name; ?> <i class="<?= Widgetgroup::arrowStatus($statusport)  ?>"></i></h3>
                                <span class="pull-right">
                                    
                                     <?php
                    if ($statusport==1) {
                        echo "Status : Running";
                    } else {

                        echo "Status : Idle";
                    }
                    ?>
                                    
                                </span>
                            
                            </div>
                          
                            <p></p>
                               
            
            
            <?php
        
              $childs = Widgets::find()->where(['isactive'=>true,'sameline'=>0,'parent_id'=>$widget->widget_id])
                      ->orderBy([
                            'sequence' => SORT_ASC])->all();
     
              
              foreach ($childs as $child) {
                  
                  ?>                 
                
                            <div class="col-md-12">
                               
                               
                                <div class="panel <?= Widgetgroup::panelBox($statusport)  ?>">
                                    <div class="panel-heading">
                                      <a href="#"><i class="fa fa-bolt fa-flip-horizontal fa-lg icon-theme" aria-hidden="true"></i><strong>  <?=$child->name?></strong></a> </div>
                                      <div class="panel-body">
                                          
                                          <ul class="list-group">
                                           <?php
                                            $vdc = Widgetport::find()
                                                    ->joinWith('deviceport')
                                                    ->where(['deviceport.device_id'=>$device_id])
                                                    ->andWhere(['deviceport.isactive'=>true])
                                                    ->andWhere(['widget_id'=>$child->widget_id])
                                                    ->orderBy(['sequence'=>'ORDER_ASC'])
                                                    ->all();
                                            
                                            foreach ($vdc as $ports) {
                                                
                                            $vdcport = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$ports->port_id])->one();
    
    
                                            $vdcportinfo = $vdcport->getPort();
                                            
                                            $uom =$vdcportinfo->getUom();
                                            
                                            $symbol="";
                                            if (!empty($uom)) {
                                                $symbol=$uom->symbol;
                                            }
                                            
                                            $decimal=0;
                                            
                                            if (!empty($vdcportinfo->decimalpoint)) {
                                                $decimal=$vdcportinfo->decimalpoint;
                                            }
                                            
                                           ?>   
                                           <li class="list-group-item d-flex justify-content-between align-items-center" style="height: 25px; padding: 4px 4px;">
                                             
                                              <?=Html::a('<i class="fa fa-area-chart"></i>' . $vdcportinfo->description , ['deviceport/graph', 'id' => $vdcport->deviceport_id]) ?>
                                              
                                                  <span class="pull-right"><?=number_format($vdcport->value,$decimal, '.', ',') ?> <small><?= $symbol ?></small></span>
                                           </li>
                                           
                                            <?php } ?>
                                          </ul>
                                          
                                      </div>
                                
                                </div><!-- /panel -->
                                                              
                                
                            </div> <!--/col-md-12 -->
                    
                       
                    
                   
                  <?php
                  }//end foreach child
                  
                  //Same Line
             $childs2 = Widgets::find()
                      ->where(['isactive'=>true,'sameline'=>1,'parent_id'=>$widget->widget_id])
                      ->orderBy([
                            'sequence' => SORT_ASC])->all();
              ?>
               
                <div class="col-md-12">
                <div class="row">            
              <?php
              foreach ($childs2 as $child2) {
                  
                  ?>                 
                    <small>  
                    <div class="col-sm-6">
                                <div class="panel <?= Widgetgroup::panelBox($statusport)  ?>">
                                    <div class="panel-heading">
                                      <a href="#"><i class="fa fa-bolt fa-flip-horizontal fa-lg icon-theme" aria-hidden="true"></i><strong>  <?=$child2->name?></strong></a> </div>
                                      <div class="panel-body">
                                         <div class="single category">    
                                          <ul class="list-unstyled">
                                           <?php
                                            $vdc = Widgetport::find()
                                                    ->joinWith('deviceport')
                                                    ->where(['deviceport.device_id'=>$device_id])
                                                    ->andWhere(['deviceport.isactive'=>true])
                                                   
                                                    ->andWhere(['widget_id'=>$child2->widget_id])
                                                    ->orderBy(['sequence'=>'ORDER_ASC'])
                                                    ->all();
                                            
                                            foreach ($vdc as $ports) {
                                                
                                            $vdcport = Deviceport::find()->where(['device_id'=>$device_id,'port_id'=>$ports->port_id])->one();
    
    
                                            $vdcportinfo = $vdcport->getPort();
                                            
                                            $uom =$vdcportinfo->getUom();
                                            
                                            $symbol="";
                                            if (!empty($uom)) {
                                                $symbol=$uom->symbol;
                                            }
                                            
                                            $decimal=0;
                                            
                                            if (!empty($vdcportinfo->decimalpoint)) {
                                                $decimal=$vdcportinfo->decimalpoint;
                                            }
                                            
                                           ?>   
                                           <li>
                                              
                                            <strong><?=$vdcportinfo->description ?></strong>
                                              
                                              <span class="tarik-kanan">
                                                      <small><?=Html::a(number_format($vdcport->value, $decimal, '.', ',').' ' . $symbol, ['deviceport/graph', 'id' => $vdcport->deviceport_id]) ?></small>
                                           
                                                
                                              </span>
                                           </li>
                                           
                                            <?php } ?>
                                          </ul>
                                             </div>   
                                      </div>
                                
                                </div><!-- /panel -->
                                                              
                                
                       
                       </div> <!--/col-sm-6 -->
                    </small>
                 
                  <?php
                  }//end foreach child2
             ?> 
                </div>
              </div> <!--/col-md-12 -->
                               
                <a href="#" class="small-box-footer">
                  
                    <?php
                    if ($statusport==1) {
                        echo "Running";
                    } else {

                        echo "Idle";
                    }
                    ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
                       
            </div>
        </div> <!-- col-md-4 -->

                            
          <?php  
        
        }//end foreach widget
   
    }//end foreach group
  
    ?>

</div> <!-- /electric -->

 <?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			             },
    			            success: function() {
    			                $('.overlay').hide(0);
    			            }
    			        });

    				var $container = $("#electric");
        		        $container.load("/widgetgroup/electricwidget/<?=$device_id?>");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/widgetgroup/electricwidget/<?=$device_id?>');
        		        }, 10000);


        		 });
                         
    
    </script>
<?php JSRegister::end(); ?>