<?php


namespace backend\widgets\dashboard;

use common\models\Devices;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

class DashboardWidget extends Widget{
    
    public $witel_id=0;
    
    public function init(){
        parent::init();

    }
    
    public function run(){
        
        $query = Devices::find()->where(['witel_id'=>$this->witel_id]);
        
        $dataProvider = new ActiveDataProvider([
                'query' => $query,
        ]);
        
        return $this->render('dashboard',['dataProvider'=>$dataProvider]);
    }
}