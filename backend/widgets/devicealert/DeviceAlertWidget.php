<?php
namespace backend\widgets\devicealert;

use common\models\Deviceport;
use yii\base\Widget;
use common\models\Devicealert;
use yii\data\ActiveDataProvider;

class DeviceAlertWidget extends Widget{
   
    public $severity;
    
    public function init(){
        parent::init();
    }
    
    public function run() {
        parent::run();
        
        $query = Devicealert::find();
        $query->joinWith('deviceport');
        $query->join('INNER JOIN','devices','deviceport.device_id = devices.device_id');
        $query->join('INNER JOIN','site','devices.site_id = site.site_id');
        $query->andWhere(['devicealert.open'=>true]);
        $query->andWhere(['devicealert.severity'=>$this->severity]);
        
        $model = new ActiveDataProvider([
                'query' => $query,
        ]);
        
        return $this->render('devicealert',['model'=>$model,'severity'=>$this->severity]);
    }
}