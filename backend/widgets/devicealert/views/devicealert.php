<?php

use common\models\Devicealert;
use richardfan\widget\JSRegister;
?>

<div id="devicealert<?=$severity?>">

	<div class="col-lg-3 col-xs-6">
          <!-- small box -->
         
         <?php 
         
         if ($severity == Devicealert::SEVERITY_CRITICAL) {
             
             echo '<div class="small-box bg-red">';
         }
         
         elseif ($severity == Devicealert::SEVERITY_WARNING) {
             
             echo '<div class="small-box bg-yellow">';
         }
         
         elseif ($severity == Devicealert::SEVERITY_INFO) {
             
             echo '<div class="small-box bg-aqua">';
         }
         
         else {
             
             echo '<div class="small-box bg-green">';
         }
         
         ?>
         
            <div class="inner">
              <h3><?= $model->getTotalCount() ?><sup style="font-size: 20px"></sup></h3>
				
				<?php 
         
         if ($severity == Devicealert::SEVERITY_CRITICAL) {
             
              echo '<p>Critical</p>';
         }
         
         elseif ($severity == Devicealert::SEVERITY_WARNING) {
             
             echo '<p>Warning</p>';
         }
         
         elseif ($severity == Devicealert::SEVERITY_INFO) {
             
             echo '<p>Info</p>';
         }
         
         else {
             
             echo '<p>Info</p>';
         }
         
         ?>
              
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="/devicealert/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
</div> <!-- /devicealert -->


<?php JSRegister::begin(); ?>
    <script>
    		$("document").ready(function(){

    				$.ajaxSetup(
    			        {
    			            cache: false,
    			            beforeSend: function() {
    			                $('.overlay').show(0);
    			            },
    			            complete: function() {
    			                $('.overlay').hide(0);
    			             },
    			            success: function() {
    			                $('.overlay').hide(0);
    			            }
    			        });

    				var $container = $("#devicealert<?=$severity?>");
        		        $container.load("/devicealert/alert-widget?severity=<?=$severity?>");
        		        var refreshId = setInterval(function()
        		        {
        		            $container.load('/devicealert/alert-widget?severity=<?=$severity?>');
        		        }, 10000);


        		 });
    </script>
<?php JSRegister::end(); ?>