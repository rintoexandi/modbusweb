<?php
return [
    'adminEmail' => 'admin@example.com',
    'uploadcsv' => 'upload/csv/',
    'rrdgraph' => 'graph/',
    'userrdcached'=>true,
    'rrdcached'=>'/tmp/rrdcached.sock',
    'rrdpath'=>'rrd/',
];
