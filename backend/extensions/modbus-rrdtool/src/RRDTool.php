<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */


namespace modbus\rrdtool;



class RRDTool {
    
    
    public $rrdpath;
    public $device_id;
    public $timestamp;
    public $rrddata;
    public $unit;
    public $dataset;
    
    
    public function generateRRDfile($rrdfile) {
        
        $opts = array( "--step", "300", "--start", 0,
           "DS:". $this->dataset .":COUNTER:600:U:U",
           "RRA:AVERAGE:0.5:1:288",
           "RRA:AVERAGE:0.5:12:168",
           "RRA:AVERAGE:0.5:228:365",         
        );
        
        return rrd_create($rrdfile,$opts);
    }
    
    public function updateRRDfile($rrdfile) {
        
        $updater = new RRDUpdater($rrdfile);
        
       return $updater->update($this->rrddata, $this->timestamp);
        
    }
    
    public function generateGraph($graphfile,$rrdfile){
        $opts = array( "--start", "-1d", "--vertical-label=" . $this->unit ."\"",
                 "DEF:" . $this->dataset ."=" . $rrdfile .":" .  $this->dataset .":AVERAGE",        
                 "AREA:" . $this->dataset . "#00FF00:" . $this->dataset . "\"",         
                 "CDEF:" . $this->dataset . "=" . $this->dataset . ",300,*",        
                 "COMMENT:\\n",
                 "GPRINT:" . $this->dataset . ":AVERAGE:Avg\: %6.2lf %S" . $this->unit . "\"",
                 "COMMENT:  ",
                 "GPRINT:" . $this->dataset . ":MAX:Max\: %6.2lf %S"  . $this->unit . "\"\\r",
               
               );
        
        return rrd_graph($graphfile,$opts);
    }
}