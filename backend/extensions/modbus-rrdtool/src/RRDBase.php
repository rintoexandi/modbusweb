<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace modbus\rrdtool;

class RRDBase {
    
    /**
     * @var string
     */
    protected $filename;
    public function __construct(string $filename)
    {
        if (strpos($filename, '.rrd') === false) {
            $filename.='.rrd';
        }
        $this->filename = $filename;
    }
}