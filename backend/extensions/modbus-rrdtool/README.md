Yii2 RRDTool
============
Yii2 RRDTool Extension

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist rintoexa/yii2-rrdtool "*"
```

or add

```
"rintoexa/yii2-rrdtool": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \rintoexa\\rrdtool\\AutoloadExample::widget(); ?>```