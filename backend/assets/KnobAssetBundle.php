<?php
/**
 * AssetBundle.php
 * @author Rinto Exandi
 */

namespace backend\assets;

/**
 * Class AssetBundle
 */
class KnobAssetBundle extends \yii\web\AssetBundle
{

    /**
     * @inherit
     */
    public $sourcePath = '@vendor/almasaeed2010/adminlte/bower_components/jquery-knob';
    /**
     * @inherit
     */
    #public $css = [
    #    'jquery-knob/font-awesome.min.css',
    #];

    public $js = [
        'js/jquery.knob.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    /**
     * Initializes the bundle.
     * Set publish options to copy only necessary files (in this case css and font folders)
     * @codeCoverageIgnore
     */
    public function init()
    {
        parent::init();

        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            return preg_match('%(/|\\\\)(js|fonts|css)%', $from);
        };
    }
}
