<?php
/**
 * AssetBundle.php
 * @author Rinto Exandi
 */

namespace backend\assets;

/**
 * Class AssetBundle
 */
class VisJsAsset extends \yii\web\AssetBundle
{
    
    /**
     * @inherit
     */
    public $sourcePath = '@app/customasset/visjs';
    /**
     * @inherit
     */
    #public $css = [
    #    'jquery-knob/font-awesome.min.css',
    #];
    
    public $js = [
            'js/vis.min.js',
    ];
    
    public $css = [
            'css/vis.min.css',
    ];
    
    public $depends = [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',
            'yii\bootstrap\BootstrapPluginAsset',
    ];
    /**
     * Initializes the bundle.
     * Set publish options to copy only necessary files (in this case css and font folders)
     * @codeCoverageIgnore
     */
    public function init()
    {
        parent::init();
        
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            return preg_match('%(/|\\\\)(js|fonts|css)%', $from);
        };
    }
}
