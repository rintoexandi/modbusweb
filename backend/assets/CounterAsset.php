<?php

namespace backend\assets;

use kartik\base\AssetBundle;

class CounterAsset extends  AssetBundle{
    
    /**
     * @inherit
     */
    public $sourcePath = '@app/customasset/counter';
    /**
     * @inherit
     */
    public $css = [            
           'https://fonts.googleapis.com/css?family=Montserrat:300,400,700',
           'css/counter.css'
     ,
    ];
    
    
    public $depends = [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',
            'yii\bootstrap\BootstrapPluginAsset',
    ];
    /**
     * Initializes the bundle.
     * Set publish options to copy only necessary files (in this case css and font folders)
     * @codeCoverageIgnore
     */
    public function init()
    {
        parent::init();
        
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            return preg_match('%(/|\\\\)(js|fonts|css)%', $from);
        };
    }
}