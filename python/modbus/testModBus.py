#!/usr/bin/env python
# -*- coding: utf-8 -*-

# read_register
# read 10 registers and print result on stdout

# you can use the tiny modbus server "mbserverd" to test this code
# mbserverd is here: https://github.com/sourceperl/mbserverd

# the command line modbus client mbtget can also be useful
# mbtget is here: https://github.com/sourceperl/mbtget

from pyModbusTCP.client import ModbusClient
import time
import socket

socket_timeout=3
socket_port=80
#Set Total Register need to take care of
max_register=100
max_bits=64
   
SERVER_HOST = "127.0.0.1"
SERVER_PORT = 502

def isAlive(ipaddress) :
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(socket_timeout)
    result = sock.connect_ex((str(ipaddress),SERVER_PORT))
    if result == 0:
       return True
    else:
       return False


c = ModbusClient()

# uncomment this line to see debug message
#c.debug(True)

# define modbus server host, port
c.host(SERVER_HOST)
c.port(SERVER_PORT)

if (isAlive(SERVER_HOST)==True) :
    while True:
        # open or reconnect TCP to server
       
        #if not c.is_open():
        #    if not c.open():
        #        print("unable to connect to "+SERVER_HOST+":"+str(SERVER_PORT))
    
        # if open() is ok, read register (modbus function 0x03)
        c.open()
        
        if c.is_open():
            print ("Reading modbus function 0x03 on     " + SERVER_HOST + " register 0-125")
                    # read 10 registers at address 0, store result in regs list
            regs = c.read_holding_registers(0, max_register)
                    # if success display registers
            if regs:
                print("reg ad #0 to 100: "+str(regs))
    
        # sleep 2s before next polling
        time.sleep(2)
else :
    
    print("Host "+SERVER_HOST+":"+str(SERVER_PORT) + " maybe going somewhere, cannot contacted")
        