import subprocess
import sys

host = '192.168.100.223'
cmd = ['ping', '-c2', '-W 5', host ]
done = False
timeout = 2 # default time out after ten times, set to -1 to disable timeout

while not done and timeout:
        response = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        stdout, stderr = response.communicate()
        if response.returncode == 0:
            print "Server up!"
            done = True
        else:
            sys.stdout.write('.')
            timeout -= 1
if not done:
       print "\nServer failed to respond"