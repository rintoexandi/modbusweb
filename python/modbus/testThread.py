try:

    import json
    import os
    import io
    import Queue
    import subprocess
    import sys
    import threading
    import time
    import logging
    import ConfigParser
    import MySQLdb
    import socket
except:
    print "ERROR: missing one or more of the following python modules:"
    print "threading, Queue, sys, subprocess, time, os, json"
    sys.exit(2)
    
from pyModbusTCP.client import ModbusClient

try:
    import MySQLdb
except:
    print "ERROR: missing the mysql python module:"
    print "On ubuntu: apt-get install python-mysqldb"
    print "On FreeBSD: cd /usr/ports/*/py-MySQLdb && make install clean"
    sys.exit(2)
    
    
#Set logfile
logger = logging.getLogger('modbus-poller')
hdlr = logging.FileHandler('modbuspoller.log','a')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)

ob_install_dir = os.path.dirname(os.path.realpath(__file__))
config_file = ob_install_dir + '/modbus.cfg'


def get_config_data():
    
    #config_cmd = ['/usr/bin/env', 'php', '%s/config_to_json.php' % ob_install_dir]
    
    config_cmd = ['/usr/bin/env', 'php', '%s/config_to_json.php' % ob_install_dir]
    try:
        proc = subprocess.Popen(config_cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    except:
        print "ERROR: Could not execute: %s" % config_cmd
        sys.exit(2)
    return proc.communicate()[0]

#Test Config File
try:
    with open(config_file) as f:
        pass
except IOError as e:
    print "ERROR: Oh dear... %s does not seem readable" % config_file
    sys.exit(2)



#params

socket_timeout=3
socket_port=502
s_time = time.time()
real_duration = 0
per_device_duration = {}
polled_devices = 0
total_down=0
total_up=0

devices_list = []


#Test MySQL

#try:
#    config = json.loads(get_config_data())
    
#except:
#    print "ERROR: Could not load or parse configuration, are PATHs correct?"
#    sys.exit(2)

#Set mysql param
db_server = "localhost"
db_dbname = "modbus"
db_username = "modbus"
db_password = "modbus"
db_port = 0

def db_open():
    try:
        if db_port == 0:
            db = MySQLdb.connect(host=db_server, user=db_username, passwd=db_password, db=db_dbname)
        else:
            db = MySQLdb.connect(host=db_server, port=db_port, user=db_username, passwd=db_password, db=db_dbname)
        return db
    except:
        print "ERROR: Could not connect to MySQL database!"
        sys.exit(2)


#Modbus Poller

def getModBus(host) :
      
    global total_down
    global total_up

    c = ModbusClient()
    # define modbus server host, port
    c.host(host)
    c.port(socket_port)
    
    if (isAlive(host)==True) :
        try:
            
            c.open()
             
            if c.is_open():

                total_up+=1
                    #for x in range(0, 10):
                    # read 10 registers at address 0, store result in regs list
                regs = c.read_holding_registers(0, 100)
                    # if success display registers
                if regs:
                    #print('receved from host'+ DEVICE_HOST)
                    #print('reg ad #0 to 100: '+str(regs))
                    return regs
                # sleep 2s before next polling
                #time.sleep(2)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            pass
    else :
        total_down+=1


#Check if Address is Alive
#
def isAlive(ipaddress) :
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(socket_timeout)
    result = sock.connect_ex((str(ipaddress),socket_port))
    if result == 0:
       return True
    else:
       return False

#Worker for printing Result
def printworker():
    
    while True:
        
        worker_id, device_id, elapsed_time = print_queue.get()
        global real_duration
        global per_device_duration
        global polled_devices
        real_duration += elapsed_time
        per_device_duration[device_id] = elapsed_time
        polled_devices += 1
        if elapsed_time < 300:
            print "INFO: worker %s finished device %s in %s seconds" % (worker_id, device_id, elapsed_time)
        else:
            print "WARNING: worker %s finished device %s in %s seconds" % (worker_id, device_id, elapsed_time)
        print_queue.task_done()

#Worker for polling data
def poller_modbus(q):
    while True:
        
        global total_down
        global total_up

        name = threading.currentThread().getName()
        #print "Thread: {0} start get item from queue[current size = {1}] at time = {2} \n".format(name, poll_queue.qsize(), time.strftime('%H:%M:%S'))
        device_id = poll_queue.get()
        #print 'Task Name : ' + device_id

        try:
            start_time = time.time()

            result = getModBus(device_id)
            print('reg ad #0 to 100: '+str(result))
            
            elapsed_time = int(time.time() - start_time)
            print_queue.put([threading.current_thread().name, device_id, elapsed_time])      
            #print "Thread: {0} finish process item from queue[current size = {1}] at time = {2} \n".format(name, poll_queue.qsize(), time.strftime('%H:%M:%S'))
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            pass

        poll_queue.task_done()  



poll_queue = Queue.Queue()
print_queue = Queue.Queue()

num_threads = 10

#device =["10.1.173.53","10.10.0.1","202.146.4.100","192.168.100.9","192.168.100.10","www.google.com"]

device =["127.0.0.1","10.16.35.10","10.16.55.103","10.16.65.203","10.16.55.105","10.16.59.201","10.16.40.201","10.16.52.201","10.16.58.201","10.16.55.101","10.16.76.201"]

#for x in range(100) :
for i in range(num_threads):
  worker = threading.Thread(name=i,target=poller_modbus, args=(poll_queue,))
  worker.setDaemon(True)
  worker.start()

#devices_list = []
#query = "select address from device where disabled = 0 order by last_polled"

#db = db_open()
#cursor = db.cursor()
#cursor.execute(query)
#devices = cursor.fetchall()

#for row in devices:
#    print(row[0])
#    devices_list.append(row[0])
#db.close()

#Get data from file text
#hosts = []
#with open("hostfilenew.txt") as file:
#    for l in file:
#       hosts.append(l.strip())

for dev in device :
        poll_queue.put(dev)

#Get data from DB
#for device_id in devices_list:
#   poll_queue.put(device_id)

    
p = threading.Thread(target=printworker)
p.setDaemon(True)
p.start()

try:
    poll_queue.join()
    print_queue.join()
except (KeyboardInterrupt, SystemExit):
    raise

total_time = int(time.time() - s_time) 

print "INFO: poller-wrapper polled %s devices in %s seconds with %s workers" % (polled_devices, real_duration, num_threads)
print " Total Devices UP %s total DOWN %s" % (total_up,total_down)