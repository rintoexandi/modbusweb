try:

    import json
    import os
    import io
    import Queue
    import subprocess
    import sys
    import threading
    import time
    import logging
    import ConfigParser
    import MySQLdb
    import socket
except:
    print "ERROR: missing one or more of the following python modules:"
    print "threading, Queue, sys, subprocess, time, os, json"
    sys.exit(2)

from pyModbusTCP.client import ModbusClient

#Test Config File
ob_install_dir = os.path.dirname(os.path.realpath(__file__))
config_file = ob_install_dir + '/modbus.cfg'
socket_timeout=2
socket_port=502
s_time = time.time()
real_duration = 0
per_device_duration = {}
polled_devices = 0
total_down=0
total_up=0

devices_list = []

poll_queue = Queue.Queue()
print_queue = Queue.Queue()


#Set Total Register need to take care of
max_register=100 #Change this
max_bits=64


#Set logfile
logger = logging.getLogger('modbus-poller')
hdlr = logging.FileHandler('modbuspoller.log','a')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)

try:
    with open(config_file) as f:
        pass
except IOError as e:
    print "ERROR: Oh dear... %s does not seem readable" % config_file
    sys.exit(2)

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    #Set mysql param
    db_server = config.get("DEFAULT","database_server")
    db_dbname = config.get("DEFAULT","database_dbname")
    db_username = config.get("DEFAULT","database_username")
    db_password = config.get("DEFAULT","database_password")
    db_port = int(config.get("DEFAULT","database_port"))

except ConfigParser.NoOptionError:
    print "ERROR: Options config is not available"
    sys.exit(2)

#Check if pidfile file_exists
"""
fpidfilename = "pollerwrapper.pid"
if os.path.isfile(fpidfilename):
    logger.warning('ERROR : pid file exist, it means that some wrapper still running ')
    print("ERROR : pid file exist, it means that some wrapper still running ")
    sys.exit(2)
else:
    #Create file if not file_exists
    file = open(fpidfilename,"w")
    file.close()
    logger.warning('pid succesfully created')
"""
#open mysql

def db_open():
    try:
        if (db_port==3306):
            db = MySQLdb.connect(host=db_server, user=db_username, passwd=db_password, db=db_dbname)
        else :
            db = MySQLdb.connect(host=db_server,port=db_port, user=db_username, passwd=db_password, db=db_dbname)
        return db
    except ( MySQLdb.Error, MySQLdb.Warning) as err:
        print  err
        sys.exit(2)


def get_device_id(address):
    db = db_open()
    cursor = db.cursor()
    query = "SELECT device_id FROM devices WHERE ipaddress='%s'" % (address)
    cursor.execute(query)
    row = cursor.fetchone()
    device_id=row[0]
    db.close()

    return device_id

def generate_poll(device_id):
    try:
        #print ("Save data")
        #Save data to db
        db = db_open()
        cursor = db.cursor()
        query = "insert into poll(device_id,success) values('%d','%d')" % (device_id,0)
        response = cursor.execute(query)

        if response == 1:
           db.commit()
           poll_id = cursor.lastrowid

        db.close()
        return poll_id
    except MySQLdb.Error as e:
        print e
        pass


def save_poller_data(poll_id,isanalog,regs,data_length):
    try:
         #Save data to db
        db = db_open()
        cursor = db.cursor()
        query = "insert into polldata(poll_id,isdigital,data,data_length) values('%d','%d','%s','%d')" % (poll_id,isanalog,
                str(regs),data_length)
        response = cursor.execute(query)
        if response == 1:
           db.commit()
        db.close()
        return response
    except MySQLdb.Error as e:
        print e
        pass

def update_poll_status(poll_id):
    try:
         #Save data to db
        db = db_open()
        cursor = db.cursor()
        query = "update  poll set success='%' where poll_id='%d'" % (1,poll_id)
        response = cursor.execute(query)
        if response == 1:
           db.commit()
        db.close()
        return response
    except MySQLdb.Error as e:
        print e
        pass

#Update device status after success or failed polling
def update_device_status(device_id,success):
    try:
         #Save data to db
        db = db_open()
        cursor = db.cursor()

        if (success==True):

            query = "update devices set last_polled=NOW() where ipaddress='%s'" % (device_id)
        else:
            query = "update devices set last_polled=NOW(),status=0 where ipaddress='%s'" % (device_id)  
        
        response = cursor.execute(query)
        if response == 1:
           db.commit()
        db.close()
        return response
    except MySQLdb.Error as e:
        print e
        pass

#Main task, get modbus data from RTU
def get_modbus_data(host) :

    global total_up

    c = ModbusClient()
    # define modbus server host, port
    c.host(host)
    c.port(socket_port)

    try:

        c.open()


        if c.is_open():

            #update total up

            total_up+=1

            # read registers at address 0, store result in regs list
            regs = c.read_holding_registers(0, max_register)

            # read coils at address 0, store result in regs list
            bits = c.read_coils(0,max_bits)

            device_id=get_device_id(host)

            id_poll = generate_poll(device_id)

            if regs:
                data_reg = str(regs)
                 #Save data analog
                save_poller_data(id_poll,0,data_reg,max_register)   #Analog

            if bits:
                data_bits = str(bits)
                #Save databits
                save_poller_data(id_poll,1,data_bits,max_bits)   #Digital

            #updata status poll

            update_poll_status(id_poll)
           

    except (KeyboardInterrupt, SystemExit):
            raise
    except:
            pass

#Check if Address is Alive
#
def isAlive(ipaddress) :
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(socket_timeout)
    result = sock.connect_ex((str(ipaddress),socket_port))

    if result == 0:
       return True
    else:
       return False

#Worker for printing Result
def printworker():  
    while True:
        global real_duration
        global polled_devices
        
        worker_id, device_id, elapsed_time,result = print_queue.get()
     
        name = threading.currentThread().getName()
        #print "PRINT WORKER   " + worker_id

        real_duration += elapsed_time
        polled_devices += 1

        if (result==True) :
            device_status = "UP"
        else:
            device_status = "DOWN"
        #print "PRINT WORKER --polled devices : %s  real duration :%s  " % (polled_devices,elapsed_time)
       
        if elapsed_time < 300:
            print "INFO: worker %s finished device %s in %s seconds , device status : %s" % (worker_id, device_id, elapsed_time,device_status)
        else:
            print "WARNING: worker %s finished device %s in %s seconds, device status : %s" % (worker_id, device_id, elapsed_time,device_status)
        print_queue.task_done()

        
#Worker for polling data
def poller_modbus(q):
    while True:

        global total_down
        global polled_devices
        
        name = threading.currentThread().getName()

        device_id = q.get()

        try:
        
            start_time = time.time()

            result = isAlive(device_id)
            if (result==True):
               
                #Get ModBus Data
                get_modbus_data(device_id)
               
                #Update device Status
                update_device_status(device_id,True)
                
            else :
                #Count down devices
                total_down+=1
                #Update device Status
                update_device_status(device_id,False)
            

            elapsed_time = int(time.time() - start_time)
          
            print_queue.put([threading.current_thread().name, device_id, elapsed_time,result])
   
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            pass

        poll_queue.task_done()



num_threads = 10
print "INFO: starting the poller at %s with %s threads, slowest devices first" % (time.strftime("%Y-%m-%d %H:%M:%S"),
        num_threads)

#get Devices List from Database
query = "select ipaddress from devices where disabled = 0 order by last_polled"

db = db_open()
cursor = db.cursor()
cursor.execute(query)
devices = cursor.fetchall()

for row in devices:
    devices_list.append(row[0])
db.close()



for i in range(num_threads):
  worker = threading.Thread(name=i,target=poller_modbus, args=(poll_queue,))
  worker.setDaemon(True)
  worker.start()

#Load devices and put to queue
for dev in devices_list :
    poll_queue.put(dev)

p = threading.Thread(target=printworker)
p.setDaemon(True)
p.start()

try:
    poll_queue.join()
    print_queue.join()
except (KeyboardInterrupt, SystemExit):
    raise

total_time = int(time.time() - s_time)

print "INFO: poller-wrapper polled %s devices in %s seconds with %s workers finished at %s" % (polled_devices, real_duration, num_threads,time.strftime("%Y-%m-%d %H:%M:%S"))
print " Total Devices UP %s total DOWN %s" % (total_up,total_down)

#remove pidfile
"""
try:
    os.remove(fpidfilename)

    logger.warning('pid file successfully removed')
except OSError:
    pass

    sys.exit(2)
"""