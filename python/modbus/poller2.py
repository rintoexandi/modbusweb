try:

    import json
    import os
    import io
    import Queue
    import subprocess
    import sys
    import threading
    import time
    import logging
    import ConfigParser
    import MySQLdb
    import socket
except:
    print "ERROR: missing one or more of the following python modules:"
    print "threading, Queue, sys, subprocess, time, os, json"
    sys.exit(2)

from pyModbusTCP.client import ModbusClient

#Test Config File
ob_install_dir = os.path.dirname(os.path.realpath(__file__))
config_file = ob_install_dir + '/modbus.cfg'
socket_timeout=2
socket_port=502
s_time = time.time()
real_duration = 0
per_device_duration = {}
polled_devices = 0
total_down=0
total_up=0

devices_list = []


#Set Total Register need to take care of
max_register=125
max_bits=64


#Set logfile
logger = logging.getLogger('modbus-poller')
hdlr = logging.FileHandler('modbuspoller.log','a')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)

try:
    with open(config_file) as f:
        pass
except IOError as e:
    print "ERROR: Oh dear... %s does not seem readable" % config_file
    sys.exit(2)

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    #Set mysql param
    db_server = config.get("DEFAULT","database_server")
    db_dbname = config.get("DEFAULT","database_dbname")
    db_username = config.get("DEFAULT","database_username")
    db_password = config.get("DEFAULT","database_password")
    db_port = int(config.get("DEFAULT","database_port"))

except ConfigParser.NoOptionError:
    print "ERROR: Options config is not available"
    sys.exit(2)


#open mysql

def db_open():
    try:
        if (db_port==3306):
            db = MySQLdb.connect(host=db_server, user=db_username, passwd=db_password, db=db_dbname)
        else :
            db = MySQLdb.connect(host=db_server,port=db_port, user=db_username, passwd=db_password, db=db_dbname)
        return db
    except ( MySQLdb.Error, MySQLdb.Warning) as err:
        print  err
        sys.exit(2)


def get_device_id(address):
    db = db_open()
    cursor = db.cursor()
    query = "SELECT device_id FROM devices WHERE ipaddress='%s'" % (address)
    cursor.execute(query)
    row = cursor.fetchone()
    device_id=row[0]
    db.close()

    return device_id

def generate_poll(device_id):
    try:
        #print ("Save data")
        #Save data to db
        db = db_open()
        cursor = db.cursor()
        query = "insert into poll(device_id,success) values('%d','%d')" % (device_id,0)
        response = cursor.execute(query)

        if response == 1:
           db.commit()
           poll_id = cursor.lastrowid

        db.close()
        return poll_id
    except MySQLdb.Error as e:
        print e
        pass


def save_poller_data(poll_id,isanalog,regs,data_length):
    try:
         #Save data to db
        db = db_open()
        cursor = db.cursor()
        query = "insert into polldata(poll_id,isdigital,data,data_length) values('%d','%d','%s','%d')" % (poll_id,isanalog,
                str(regs),data_length)
        response = cursor.execute(query)
        if response == 1:
           db.commit()
        db.close()

        #print "Data Inserted " + regs;
        return response
    except MySQLdb.Error as e:
        print e
        pass

def update_poll_status(poll_id):
    try:
         #Save data to db
        db = db_open()
        cursor = db.cursor()
        query = "update  poll set success='%' where poll_id='%d'" % (1,poll_id)
        response = cursor.execute(query)
        if response == 1:
           db.commit()
        db.close()
        return response
    except MySQLdb.Error as e:
        print e
        pass




def get_modbus_data(host) :

    global total_down
    global total_up

    c = ModbusClient()
    # define modbus server host, port
    c.host(host)
    c.port(socket_port)

    try:

        c.open()


        if c.is_open():
            # read registers at address 0, store result in regs list
            regs = c.read_holding_registers(0, max_register)

            # read coils at address 0, store result in regs list
            bits = c.read_coils(0,max_bits)

            device_id=get_device_id(host)

            print(device_id + 'reg ad #0 to 100: '+str(regs))
            print(device_id + 'reg ad #0 to 100: '+str(bits))

            id_poll = generate_poll(device_id)

            if regs:
                data_reg = str(regs)
                 #Save data analog
                save_poller_data(id_poll,0,data_reg,max_register)   #Analog

            if bits:
                data_bits = str(bits)
                #Save databits
                save_poller_data(id_poll,1,data_bits,max_bits)   #Digital

            #updata status poll

            update_poll_status(id_poll)

    except (KeyboardInterrupt, SystemExit):
            raise
    except:
            pass

#Check if Address is Alive
#
def isAlive(ipaddress) :
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(socket_timeout)
    result = sock.connect_ex((str(ipaddress),socket_port))

    if result == 0:
       return True
    else:
       return False

#Worker for printing Result
def printworker():

    while True:

        worker_id, device_id, elapsed_time = print_queue.get()
        global real_duration
        global per_device_duration
        global polled_devices
        real_duration += elapsed_time
        per_device_duration[device_id] = elapsed_time
        polled_devices += 1

        if elapsed_time < 300:
            print "INFO: worker %s finished device %s in %s seconds" % (worker_id, device_id, elapsed_time)
        else:
            print "WARNING: worker %s finished device %s in %s seconds" % (worker_id, device_id, elapsed_time)
        print_queue.task_done()

#Worker for polling data
def poller_modbus(q):
    while True:

        global total_down
        global total_up
        global polled_devices
        global s_time
        global real_duration
        name = threading.currentThread().getName()
        print "Thread: {0} start get item from queue[current size = {1}] at time = {2} \n".format(name, poll_queue.qsize(), time.strftime('%H:%M:%S'))
        device_id = q.get()
        print 'Task Name : ' + device_id

        try:
            start_time = time.time()

            result = isAlive(device_id)
            if (result==True):
                get_modbus_data(device_id)
                print('reg ad #0 to 100: '+str(regs))
                total_up+=1
                query = "update devices set last_polled=NOW() where address='%s'" % (device_id)
            else :

                print 'Device Down ' + device_id;
                total_down+=1
                query = "update devices set last_polled=NOW(),status=0 where address='%s'" % (device_id)

            elapsed_time = int(time.time() - start_time)
            real_duration += elapsed_time

            #Update Database
            db = db_open()
            cursor = db.cursor()

            response = cursor.execute(query)
            if response == 1:
                db.commit()
            db.close()

            print_queue.put([threading.current_thread().name, device_id, elapsed_time])
            print "Thread: {0} finish process item from queue[current size = {1}] at time = {2} \n".format(name, poll_queue.qsize(), time.strftime('%H:%M:%S'))
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            pass

        poll_queue.task_done()


#Worker for polling data
def poller_ping(q):
    while True:

        global total_down
        global total_up
        global polled_devices
        global s_time
        global real_duration

        name = threading.currentThread().getName()
        #print "Thread: {0} start get item from queue[current size = {1}] at time = {2} \n".format(name, poll_queue.qsize(), time.strftime('%H:%M:%S'))
        device_id = q.get()
        #print 'Task Name : ' + device_id
        polled_devices += 1

        try:

            start_time = time.time()

            if (isAlive(device_id)==True) :

                total_up +=1
            else :
                total_down +=1

            elapsed_time = int(time.time() - s_time)
            real_duration += elapsed_time

            print_queue.put([threading.current_thread().name, device_id, elapsed_time])
            #print "Thread: {0} finish process item from queue[current size = {1}] at time = {2} \n".format(name, poll_queue.qsize(), time.strftime('%H:%M:%S'))
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            pass

        poll_queue.task_done()

poll_queue = Queue.Queue()
print_queue = Queue.Queue()



#get Devices List from Database
query = "select ipaddress from devices where disabled = 0 order by last_polled"

db = db_open()
cursor = db.cursor()
cursor.execute(query)
devices = cursor.fetchall()

for row in devices:
    devices_list.append(row[0])
db.close()


#Load devices and put to queue
for dev in devices_list :
    poll_queue.put(dev)



num_threads = 10

for i in range(num_threads):
  worker = threading.Thread(name=i,target=poller_modbus, args=(poll_queue,))
  worker.setDaemon(True)
  worker.start()


p = threading.Thread(target=printworker)
p.setDaemon(True)
p.start()

try:
    poll_queue.join()
    print_queue.join()
except (KeyboardInterrupt, SystemExit):
    raise

total_time = int(time.time() - s_time)

print "INFO: poller-wrapper polled %s devices in %s seconds with %s workers" % (polled_devices, total_time, num_threads)
print " Total Devices UP %s total DOWN %s" % (total_up,total_down)
