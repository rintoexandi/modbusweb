#!/usr/bin/env python
# -*- coding: utf-8 -*-

# read_register
# read 10 registers and print result on stdout

# you can use the tiny modbus server "mbserverd" to test this code
# mbserverd is here: https://github.com/sourceperl/mbserverd

# the command line modbus client mbtget can also be useful
# mbtget is here: https://github.com/sourceperl/mbtget

from pyModbusTCP.client import ModbusClient
import time
import os

SERVER_HOST = "10.16.31.21"
SERVER_PORT = 502
#HOSTS =["192.168.100.6","192.168.100.7","192.168.100.8","192.168.100.9","192.168.100.10"]
HOSTS=["10.16.31.21"]


for HOST in HOSTS:

    
    c = ModbusClient()
    # uncomment this line to see debug message
    c.debug(True)

    # define modbus server host, port
    c.host(HOST)
    c.port(SERVER_PORT)

    
    #while True:
    HOST_UP  = True # if os.system("ping -c 1 " + HOST) is 0 else False
        # open or reconnect TCP to server
    if(HOST_UP==True):
        if not c.is_open():
            if not c.open():
                print("unable to connect to "+SERVER_HOST+":"+str(SERVER_PORT))

                # if open() is ok, read register (modbus function 0x03)
            if c.is_open():
                #for x in range(0, 10):
                # read 10 registers at address 0, store result in regs list
                regs = c.read_holding_registers(0, 100)

                coil = c.read_coils(0, 100)
                # if success display registers
                if regs:
                    print("receved from host" + SERVER_HOST)
                    print("reg ad #0 to 100: "+str(regs))

                if coil:
                    print("receved from host" + SERVER_HOST)
                    print("reg ad #0 to 100: "+str(coil))
            # sleep 2s before next polling
            #time.sleep(2)
    else:
        print 'general.connectivity() Exception'
    