<?php

namespace console\controllers;

use backend\models\ParseJsonFile;
use yii\console\Controller;

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

class OpenjsonController extends Controller {
    
    public function actionIndex($id){
       
        $this->readJson($id);
    }
    
    private function readJson($id) {
       
        $jsonparse = new ParseJsonFile();
        
        $result = $jsonparse->openJson($id,"monthly");
        
        if ($result) {
        
        echo "About " . $jsonparse->title;
        echo "Start " . date("F d, Y h:i:s A",$jsonparse->start);
        echo "End " . date("F d, Y h:i:s A",$jsonparse->end);
        
        //echo(date("F d, Y h:i:s A", $jsonparse->start)); 
        
        //print_r($jsonparse->data);
        }
    }
}