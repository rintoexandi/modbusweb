<?php

namespace console\controllers;



use common\models\Devices;
use common\models\Poll;
use common\models\Polldata;
use common\models\Ports;
use common\models\Deviceport;
use Exception;
use common\models\Portregister;
use yii\db\Expression;
use DateTime;
use common\models\Notification;
use common\models\Deviceporthistory;
use common\models\Portgroup;
use common\models\Devicealert;
use common\models\Alertlog;

class ProcesspolldataController extends \yii\console\Controller
{
       
    
    /*
     * 
     */
    public function actionIndex() {
        
        $this->processData();
    }
    
    /*
     * 
     */
    public function processData() {
       
       $date1 = new DateTime("now");
        
       $devices = $this->listDevices();
       
       foreach ($devices as $device) {               
         
          // print "Processing poll data for device : " . $device->ipaddress . "\n";
           $updateDigital=0;
           $updateAnalog=0;
           
           $polls = $this->getPoll($device->device_id);
                    
           foreach ($polls as $poll) {
             
             print "Processing poll data for device : " . $device->ipaddress .  "  Poll Id " . $poll->poll_id  ."\n";
               
             $getDataPoll1 = $this->getPollData($poll->poll_id,true);
             
             if (isset($getDataPoll1)) {
                 $digital=$getDataPoll1["data"];
                 //print_r($digital);
                 //die();
                 //Update Device Ports
                 
                 $updateDigital=$this->updateDevicePort($digital,$device->device_id,true,$getDataPoll1["poll_time"]);
                 
                 echo "Update Digital " . $updateDigital;
                 
                 if ($updateDigital==0) {
                     
                     $this->setPollDataProcessed($getDataPoll1["id"]);
                 }
             }                     
             
             $getDataPoll2=$this->getPollData($poll->poll_id,false);
             
             if (isset($getDataPoll2)) {
                 $analog = $getDataPoll2["data"];
                 
                 //print_r($analog);
                 
                 $updateAnalog = $this->updateDevicePort($analog,$device->device_id,false,$getDataPoll2["poll_time"]);
                 
                 echo "Update Analog " . $updateAnalog;
                 
                 if ($updateAnalog==0) {
                     
                     $this->setPollDataProcessed($getDataPoll2["id"]);
                 }
             }
             
             
             if($updateAnalog==0 && $updateDigital==0) {
                 
                 $this->setPollProcessed($poll->poll_id);
             }
                          
             }//end loop polls
           
                
           
       }//end loop device
        
       $date2 = new DateTime("now");
       $interval = $date1->diff($date2);
       
        
       print "Processing " . count($devices) . " device(s) in " . $interval->format('%R%S seconds') . "\n";
    }
    
    /*
     * Update Processed PollData
     */
    public function setPollDataProcessed($id){
       $polldata = Polldata::findOne(['id'=>$id]); 
       
       $polldata->processed=true;
       
       $polldata->save();
        
    }
    
    /*
     * Update Processed PollData
     */
    public function setPollProcessed($id){
        $polldata = Poll::findOne(['poll_id'=>$id]);
        
        $polldata->processed=true;
        
        $polldata->save();
        
    }
    /*
     * Get Value data based on registered ports
     */
    public function getDataValue($objdata,$port_id,$digital){
        
        $dataLength = count($objdata)-1;
         
        $data = $objdata;
        
        //print_r($data);
        //die(); 
        //echo "Port Register ---------------> Port ID" . $portregister->registeredports ;
        
       // print_r($data);
        //die();
        
        $datavalue=0;
        
        
        //Digital Port Only one register per port
        if ($digital==True) {
        
            $portregister =  Portregister::find()->where(['port_id'=>$port_id])->one();
            
            if ($portregister->registeredports > $dataLength) {
                
                $datavalue=0;
            }
            
            else {
                
                
                //echo "Data Value----" . $data[$portregister->registeredports];
                $datavalue = $this->changeDigitalValue($data[$portregister->registeredports]);
            }
            
           //Create Notification on Error
           
           
        }
        
        //Analog port can have multiple register
        else {
           
            $portregister =  Portregister::find()->where(['port_id'=>$port_id])->all();
            
            $strBin="";
            
            foreach ($portregister as $register) {
             
                //TODO add processing for multile pregister to concat binary value
                
                $strValue = $data[$register->registeredports];
                 
                $strBin .= decbin((int)$strValue); //Binary Concatenation
                
                //print "Binary for Port name " . $register->name  .  " Registered Ports " . $register->registeredports . " Decimal " . $strValue . " Binary " . $strBin;
            }
            
            $datavalue = bindec($strBin);
                  
            //print "port id" . $port_id . " Binary Value" .  $strBin . " Data Value " . $datavalue ."\n";
        }
           
      
        
        return $datavalue;
        
    } //end getDataValue
    
    /*
     * Update Device Ports
     */
    public function updateDevicePort($objdata,$device_id,$digital,$timestamp){
       
        //print_r($objdata);
        //die();
      //echo "Get Device Ports-----------------------------------------------";
       $counter=0;
       
       $deviceports = $this->listPorts($device_id,$digital);
       
       //print_r($deviceports);
       //die(); 
       foreach($deviceports as $port) {
          
           print "Updating Device Port " . $port->deviceport_id .  "\n";
           
           $deviceport = Deviceport::find()->where(['deviceport_id'=>$port->deviceport_id])->one();
          
           $prev_value = $deviceport->value;
           
           $value = $this->getDataValue($objdata,$port->port_id,$digital);
           
           $poll_prev = $deviceport->poll_time;
           
           $deviceport->value = $value;
           $deviceport->value_prev=$prev_value;
           $deviceport->updated= new Expression('CURRENT_TIMESTAMP');
           $deviceport->poll_prev=$poll_prev;
           $deviceport->poll_time=$timestamp;
           
           //Add to History
           
           Deviceporthistory::addtoHistory($deviceport);
           
           if(!$deviceport->save()) {
               $counter++;
           }
           
           //Create Notification
           
           
           if ($digital) {
               
               //Value is changed
               if ($value!=$prev_value) {
                   
                   date_default_timezone_set('Asia/Jakarta');
                   $message = [
                           'message'=>'Value Changed',
                           'minvalue'=>'1',
                           'noticevalue'=>'0',
                           'warningvalue'=>'0',
                           'previous_value'=>$prev_value,
                           'current_value'=>$value,
                           'importance'=>Notification::TYPE_ERROR,
                           'deviceport_id'=>$port->deviceport_id,
                           'process_time'=>date('d-m-Y H:i:s'),
                   ];
                   
                  // Notification::notify(1, $port->deviceport_id,$message);
                   
                   $dseverity = Devicealert::SEVERITY_INFO;
                   
                   if($value==True) {
                       
                       $dseverity = Devicealert::SEVERITY_INFO;
                       $devAlert = new Devicealert();
                       $devAlert = $devAlert->close($port->deviceport_id);
                        
                       //$devAlert->alertlog($message, $value, $prev_value);
                   }
                   else {
                       
                       $dseverity = Devicealert::SEVERITY_CRITICAL;
                       
                       //Devicealert::alert($port->deviceport_id,$severity);
                       $devAlert = new Devicealert();
                       $devAlert = $devAlert->alert($port->deviceport_id,$dseverity);
                       
                       //print_r($result);
                       //die();
                       
                       $devAlert->alertlog($message, $value, $prev_value);
                   }
                   
                   
                   
               }
               
           }// end if digital
           
           //Generate Alert
           if(!$digital) {
               
               //get PortGroup
               $portS = $this->getPort($port->port_id);
               $portgroup = $this->getPortGroup($portS->portgroup_id);
               
               $message = [
                       'message'=>'Value Changed',
                       'minvalue'=>$portgroup->minvalue,
                       'noticevalue'=>$portgroup->noticevalue,
                       'warningvalue'=>$portgroup->warningvalue,
                       'current_value'=>$value,
                       'previous_value'=>$prev_value,
                       'process_time'=>date('d-m-Y H:i:s'),
               ];
               
               $severity = Devicealert::SEVERITY_NONE;
               
               if ($value<$portgroup->minvalue) {
                   
                   //Value below minimum
                   $severity = Devicealert::SEVERITY_INFO;
               }
               
               elseif ($value>$portgroup->noticevalue) {
                  //Notice Value 
                   $severity = Devicealert::SEVERITY_WARNING;
               }
               
               elseif ($value>$portgroup->warningvalue) {
                   //Warning Value
                   $severity = Devicealert::SEVERITY_CRITICAL;
               }
               
               //Close if none severit$vy
               if ($severity==Devicealert::SEVERITY_NONE) {
                  
                  $devalert = new Devicealert();
                  
                  $devalert->close($port->deviceport_id);
                  $devalert->alertlog($message, $value, $prev_value);
                  
                  //print_r("dev alert" . $devalert);
                  //die();
                  //Create Log
                 // Alertlog::alert($devalert, $message,$value,$prev_value);
               }
               
               else {
                   
                   //Devicealert::alert($port->deviceport_id,$severity);     
                   $deviceAlert = new Devicealert();
                   $deviceAlert = $deviceAlert->alert($port->deviceport_id,$severity);  
                   
                   //print_r($result);
                   //die();
                   
                   $deviceAlert->alertlog($message, $value, $prev_value);
                   
                   //print_r("dev alert" . $devalert);
                   //die();
                   //Create Log
                   
               }
           }
              
           
           
       }
       
       return $counter;
   
    }//end UpdateDevicePort
    
    
    
    /*
     * 
     */
    public function mapPorts($registeredports){
        
        $ports = Ports::find()->where(['registeredports'=>$registeredports])->one();
        
        return $ports->port_id;
        
    }
  
    /*
     * List Ports standard
     */
    public function listPorts($device_id,$isdigital){
        
        $port = Deviceport::find()
               ->joinWith('ports')
               ->joinWith('ports.portgroup')         
               ->where(['deviceport.device_id'=>$device_id,'portgroup.isdigital'=>$isdigital,'deviceport.isactive'=>true])->all();
         
        return $port;
        
    }
    
    /*
     * List Devices
     */
    public function listDevices(){
        
        $device = Devices::find()->where(['disabled'=>false])->all();
        
        return $device;
    }
    
    /*
     * Get Poll Data
     */
    public function getPoll($device_id) {
        
        $poll = Poll::find()->where(['device_id'=>$device_id,'processed'=>false])->all();
        return $poll;
    }
    
    
    /*
     * 
     */
    public function getPollData($poll_id,$isdigital) {
    
        //print_r($poll_id);
        //print_r($isdigital);
        
        //die();
        
        $polldata = Polldata::find()->where(['poll_id'=>$poll_id,
                'isdigital'=>$isdigital,'processed'=>false])->one();
        
        //print_r($polldata);
        //die();
        
        //$polldata = new Polldata();
        //$datapoller = json_encode($polldata['data']);
        
        $strdata = str_replace("[", "", $polldata->data);
        
        $strdata = str_replace("]", "", $strdata);
         
        //print_r($polldata);
        
        $result = explode(",", trim($strdata));
        
        $arrResult = array("id"=>$polldata->id,"data"=>$result,'poll_time'=>$polldata->created);
        
        //print_r($polldata);
        
        return $arrResult;
        
    }
    
    public function changeDigitalValue($val){
        if ($val=="True") {
            return 1;
        }
        else {
            return 0;
        }
    }
    
    public function getPort($port_id) {
        
        $port = Ports::find()->where(['port_id'=>$port_id])->one();
        return $port;
    }
    
    
    public function getPortRegister($port_id) {
        
        $portregister = Portregister::find()->where(['port_id'=>$port_id])->all();
        
       // print_r($portregister);
        
        return $portregister;
    }
    
    /*
     * 
     */
    public function getPortGroup($portgroup_id) {
        
        $portgroup = Portgroup::find()->where(['portgroup_id'=>$portgroup_id])->one();
        
        // print_r($portregister);
        
        return $portgroup;
    }
}