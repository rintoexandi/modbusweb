<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace console\controllers;

use backend\models\RRDTool;
use common\models\Deviceport;
use Yii;
use yii\console\Controller;

class GeneraterrdController extends Controller {
    
    
    public function actionIndex($id) {
        
     
        $this->createRRD($id);
        
       
    }
        
    
    public function createRRD($device_id) {
        
        $path = Yii::getAlias('@rrd') . "/" . $device_id;
        
        if (!file_exists ($path )) {
            
            mkdir($path);
        }
        
        
        $deviceport = Deviceport::find()->where(['device_id'=>$device_id,'isactive'=>true])->all();
        
        foreach ($deviceport as $ports) {
            
            $port = $ports->getPort();
            
            //var_dump($port);
            //die();
            
            $dataset = $port->name;
            
            $rrdfile = $path . "/" . $ports->deviceport_id . ".rrd";
            
            $rrd = new RRDTool();
            $rrd->dataset=$dataset;
           
            
            
            $ret = $rrd->generateRRDfile($rrdfile);
            
           if( $ret == 0 )
            {
              $err = rrd_error();
              echo "Create error: $err\n";
            }
        }
        
    }
    
    public function actionUpdaterrd($device_id) {
        
        $path = Yii::getAlias('@rrd') . "/" . $device_id;
        
        if (!file_exists ($path )) {
            
            die('Folder not found !');
        }
        
        
        $deviceport = Deviceport::find()->where(['device_id'=>$device_id,'isactive'=>true])->all();
        
        foreach ($deviceport as $ports) {
            
            $port = $ports->getPort();
            
            //var_dump($port);
            //die();
            
            $dataset = $port->name;
            
            $rrdfile = $path . "/" . $ports->deviceport_id . ".rrd";
            
            if (!file_exists ($rrdfile)) {
                
                die('RRD file is not found !');
            }
            
            $rrd = new RRDTool();
            $rrd->dataset=$dataset;
            
            $value = 0;
            
            if (!empty($ports->value)) {
                $value=intVal($ports->value);
            }
            
            $data = [
                $dataset =>$value,
            ];
            
            $rrd->rrddata=$data;
            
           $ret = $rrd->updateRRDfile($rrdfile,$value);
            
           if( $ret == 0 )
            {
              $err = rrd_error();
              echo "Create error: $err\n";
            }
        }
        
    }
    
     public function actionGenerategraph($device_id) {
        
        $path = Yii::getAlias('@rrd') . "/" . $device_id;
        
        if (!file_exists ($path )) {
            
            die('Folder not found !');
        }
        
        
        $deviceport = Deviceport::find()->where(['device_id'=>$device_id,'isactive'=>true])->all();
        
        foreach ($deviceport as $ports) {
            
            $port = $ports->getPort();
            
            //var_dump($port);
            //die();
            
            
            
            $rrdfile = $path . "/" . $ports->deviceport_id . ".rrd";
            
            if (!file_exists ($rrdfile)) {
                
                die('RRD file is not found !');
            }
            
            $graphfile = $path . "/" . $ports->deviceport_id . ".png";
            $rrd = new RRDTool();
            
            $rrd->dataset = $port->name;
                      
            
            $uom =$port->getUom();
                                            
            $symbol="";
            
            if (!empty($uom)) {
                $symbol=$uom->symbol;
            }
           
            $rrd->unit = $symbol;
            
           $ret = $rrd->generateGraph($graphfile,$rrdfile,$port->description,"Daily Graph");
            
           if( $ret == 0 )
            {
              $err = rrd_error();
              echo "Create error: $err\n";
            }
        }
        
    }
}