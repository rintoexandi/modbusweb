<?php


namespace console\controllers;

use common\models\Devices;
use common\models\Poll;
use common\models\Polldata;

class GenerateSampleDataController extends \yii\console\Controller {
    
    public $max_register=125;
    public $max_bits=64;
    
    public function actionIndex(){
        
        if ($this->checkUnprocessed()) {
            
            $this->listDevices();
        }
        else {
            echo "Unprocessed data still exist.. !" . "\n";
        }
    }
    
    public function listDevices(){
        
        $devices = Devices::findAll(['disabled'=>false]);
        
        foreach($devices as $device) {
            
           
            $this->generateData($device);
        }
        
    }
    
    private function checkUnprocessed(){
        $poll = Polldata::find()->where(['processed'=>false])->all();
        
        if (!empty($poll)) {
            return false;
        }
        else {
            return true;
        }
     }
    public function generateData($device){
       
        $poll=new Poll();
        $poll->device_id=$device->device_id;
        $poll->success=0;
        $poll->processed=false;
       
        if($poll->save()) {
          
            $this->generatePollData($poll,true);
            $this->generatePollData($poll,false);
        }
                
    }
    
    public function generatePollData($poll,$digital) {
        
        $data = new Polldata();
        $data->poll_id=$poll->poll_id;
        $data->isdigital=$digital;
        
        if ($digital==True) {
        
            $data->data_length=$this->max_bits;
            $data->data = $this->dummyData($digital,$this->max_bits);
            $data->processed=false;
        }
        
        else {
            
            $data->data_length=$this->max_register;
            $data->data = $this->dummyData($digital,$this->max_register);
            $data->processed=false;
        }
        
        $data->save(false);
    }
    
    public function dummyData($digital,$length){
        
        $data="[";
        
        if ($digital==True) {
           
            for ($i=0;$i<$length;$i++) {
                
                $value=rand(0,1);
                
                if ($value==0) {
                   
                    $data .="False";
                }
                
                else {
                    $data .="True";
                }
                
                if ($i!=$length-1) {
                    $data.=",";
                }
                
            }
            
           
        }
        
        else {
            
            
            for ($i=0;$i<$length;$i++) {
                
                $value =rand(100,1000);
                $data .=$value;
                if ($i!=$length-1) {
                    $data.=",";
                }
            }
            
        }
        
        $data.="]";
        
       // print $data;
        
        return $data;
    }
    
    
    
    
    
}