<?php

namespace console\controllers;

use common\models\Alertrules;
use common\models\Alertrulesdetails;
use common\models\Devicealert;
use common\models\Deviceport;
use common\models\Notification;
use common\models\Portgroup;
use Yii;
use yii\console\Controller;
use devmustafa\amqp\PhpAmqpLib\Connection\AMQPConnection;

class CheckalertController extends Controller{
    
    public function actionIndex($device_id=0) {
        
        if($device_id!=0) {
            
            //$time_start = microtime(true); 

            $this->actionCreateAlert($device_id);
            
            //$time_end = microtime(true);
            
            //echo "start : " . $time_start . "\n";
            
            //echo "end :" . $time_end . "\n";
            
            //$execution_time = ($time_end - $time_start)/60;
            //echo 'Total Execution Time for  device ' .$device_id . ' : '.$execution_time.' Mins' . "\n";
        }
        
        else {
            print ('Master, I have to ignore this request,you dont specify device_id, I confused \n');
        }
    }
    
    public function sendtoBot($message){
        
        $result = Yii::$app->telegram->sendMessage([
	'chat_id' => '-185714778',
	'text' => $message]);
        
        //var_dump($result);
    }
    
    /**
     * 
     */
    public function actionCreateAlert($device_id){
        
        $deviceports = Deviceport::find()->where([
            'device_id'=>$device_id,'reportalert'=>true])
            ->andWhere(['<>','value','prev_value'])
            ->all();    
        
        if (!empty($deviceports)) {
            
            foreach ($deviceports as $deviceport) {
            
                $port = $deviceport->getPort();
                
                $portgroup = $port->Portgroup();
                
                $alertrules = $this->getAlertRules($portgroup,$deviceport);
                
                //date_default_timezone_set('Asia/Jakarta');
               
                $message = [
                           'message'=>'Notification',           
                           'previous_value'=>$deviceport->value_prev,
                           'current_value'=>$deviceport->value,
                           'importance'=>Notification::TYPE_ERROR,
                           'deviceport_id'=>$deviceport->deviceport_id,
                           'process_time'=>date('d-m-Y H:i:s'),
                ];
                   
                
                //Digital Port
                if($portgroup->isdigital){
                    
                   $validatealert = $this->validateRulesDigital($alertrules,$deviceport);
                  
                }
                //Analog Port
                else {
                    
                   
                   $validatealert = $this->validateRulesAnalog($alertrules, $deviceport);
                  
                }
                
                
                
                $devAlert = new Devicealert();
                $alert = $devAlert->alert($deviceport->deviceport_id,$validatealert);
                $alert->alertlog($message, $deviceport->value, $deviceport->value_prev,$deviceport->poll_time);
                
                 //print_r($message);
                 
                 //  $this->sendtoMQ(json_encode($message));
                    
                 //$this->sendtoBot(json_encode($message));
            }//end for each
            
        }//end if not empty
        
    }
    
    
   
    /**
     * 
     * @param Portgroup $portgroup
     * @param Deviceport $deviceport
     * @return Alertrules
     */
    private function getAlertRules($portgroup,$deviceport){
       
        //print_r($deviceport);
        //die();
        
        if ($deviceport->configtype==Deviceport::$CONFIG_TYPE_CUSTOM) {
            
            return $this->checkAlertRules($deviceport->alertrule_id);
                       
        }
        
        else {
            return $this->checkAlertRules($portgroup->alertrules_id);
        }
            
    }

    /**
     * 
     * @param int $alertrules_id
     * @return Alertrules alertrules
     */
    private function checkAlertRules($alertrules_id){
        $alertrules = Alertrules::find()->where(['alertrules_id'=>$alertrules_id])->one();
        
        return $alertrules;
    }
    
    
    /**
     * 
     * @param Alertrules $alertrules
     * @param Deviceport $deviceport
     * @return string Alertrules Severity
     */
    private function validateRulesDigital($alertrules,$deviceport){
        
        
        if ((!empty($alertrules)) && (!empty($deviceport))) {
           
             $ruledetails = Alertrulesdetails::find()
                 ->where(['alertrules_id'=>$alertrules->alertrules_id])
                 ->orderBy(['sequence' =>'SORT_ASC'])
                ->all();
        
            foreach($ruledetails as $rules){
            
            if (($deviceport->value==$rules->lowervalue) && 
                ($deviceport->port_uptime>=$rules->occurance)) {

                return $rules->severity;
            }
            
           return Alertrules::$SEVERITY_OK;
            }
        }
        
        else {
            return Alertrules::$SEVERITY_INVALID;
        }
       
    }
   
  
    /**
     * 
     * @param Alertrules $alertrules
     * @param Deviceport $deviceport
     * @return string Alertrules 
     */
    private function validateRulesAnalog($alertrules,$deviceport){
        
        if ((!empty($alertrules)) && (!empty($deviceport))) {
           
                $ruledetails = Alertrulesdetails::find()
                     ->where(['alertrules_id'=>$alertrules->alertrules_id])
                     ->orderBy(['sequence' =>'SORT_ASC'])
                    ->all();

            foreach($ruledetails as $rules){

               if (($deviceport->value>=$rules->lowervalue) && 
                   ($deviceport->value<=$rules->uppervalue) &&   
                   ($deviceport->port_uptime>=$rules->occurance)) {

                   return $rules->severity;
               }

               return Alertrules::$SEVERITY_OK;
            } 
        }
        
        else {
            return Alertrules::$SEVERITY_INVALID;
        }
        
    }
    
     public function sendtoMQ($message) {
    
        $producer = Yii::$app->rabbitmq->getProducer('yii2-producer');
       
        $producer->publish($message, 'yii2-exchange', '123456789');

        // $exchange = 'exchange';
        //$queue = 'queue';
       
        //Yii::$app->amqp->declareExchange($exchange, $type = 'direct', $passive = false, $durable = true, $auto_delete = false);
        //Yii::$app->amqp->declareQueue($queue, $passive = false, $durable = true, $exclusive = false, $auto_delete = false);
        //Yii::$app->amqp->bindQueueExchanger($queue, $exchange, $routingKey = $queue);
        //Yii::$app->amqp->publish_message($message, $exchange, $routingKey = $queue, $content_type = 'applications/json', $app_id = Yii::$app->name);
    }
    
}