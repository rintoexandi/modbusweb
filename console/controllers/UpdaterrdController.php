<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace console\controllers;

use backend\models\RRDTool;
use common\models\Deviceport;
use Yii;
use yii\console\Controller;

class UpdaterrdController extends Controller {
    
    public function actionIndex($deviceport_id) {
        
        $this->updatePortRRD($deviceport_id);
    }
    
    /**
     * 
     * @param type $device_id
     */
    public function updatePortRRD($deviceport_id) {
       
        
        $deviceport = Deviceport::findOne($deviceport_id);
        
       
        if (!empty($deviceport)) {
            
            $rrdpath = Yii::getAlias('@rrd') . "/" . $deviceport->device_id;
        
           
            if (!file_exists ($rrdpath )) {

               mkdir($rrdpath);
            }
        
        
            $port = $deviceport->getPort();

            //var_dump($port);
            //die();

            $dataset = $port->name;

            
            $rrdabsolutefile = $rrdpath . "/" . $deviceport->deviceport_id . ".rrd";

            //RRCached cannot accep absolute path
            
            $rrdfile = $deviceport->device_id . "/" . $deviceport->deviceport_id . ".rrd";
            
            $rrd = new RRDTool();
            $rrd->dataset=$dataset;

           //RRDFile is not exist
            if (!file_exists ($rrdabsolutefile)) {

                

                $rrd->createRRD($rrdfile);
              /**
               
                //$ret = $rrd->generateRRDfile($rrdfile);

               if( $ret == 0 )
                {
                  $err = rrd_error();
                  echo "Create error: $err\n";
                }**/
            }

            //RRD file is exist
            else {

                $value = 0;

                if (!empty($deviceport->value)) {
                    $value=intVal($deviceport->value);
                }

                $data = [
                    $dataset =>$value,
                ];

             
               $rrd->updateRRD($rrdfile,$value);

               /**
               if( $ret == 0 )
                {
                  $err = rrd_error();
                  echo "Create error: $err\n";
                }**/
            }
            
            //Set RRD Flag processed
            
            Deviceport::setRRDCreated($deviceport_id,true);
        }//end of device port is not empty
        
    }//end of updateRRD
}
