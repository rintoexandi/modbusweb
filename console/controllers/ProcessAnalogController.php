<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Deviceport;
use common\models\Deviceporthistory;
use common\models\Devices;
use common\models\Notification;
use common\models\Poll;
use common\models\Polldata;
use common\models\Portgroup;
use common\models\Portregister;
use common\models\Ports;
use yii\db\Expression;
use DateTime;
use common\models\Devicealert;

class ProcessAnalogController extends  Controller{
    
    public function actionIndex($device_id=0) {
         $this->processData($device_id);
    }
    
    public function processData($device_id=0) {
        
        $date1 = new DateTime("now");
        
        $updateAnalog=0;
        
        $polls = Devices::getPoll($device_id);
        
        foreach ($polls as $poll) {
            
            print "Processing poll data for device_id : " . $device_id.  "  Poll Id " . $poll->poll_id  ."\n";
              
            $getDataPollAnalog=Poll::getPollData($poll->poll_id,false);
            
            if (isset($getDataPollAnalog)) {
                $analog = $getDataPollAnalog["data"];
                
                //print_r($analog);
                
                $updateAnalog = $this->updateDevicePort($analog,$device_id,false,$getDataPollAnalog["poll_time"]);
                
                echo "Update Analog " . $updateAnalog;
                
                if ($updateAnalog==0) {
                    
                    $this->setPollDataProcessed($getDataPollAnalog["id"]);
                }
            }
            
            
            if($updateAnalog==0) {
                
                $this->setPollProcessed($poll->poll_id);
            }
            
        }//end loop polls
            
            
            
      
        
        $date2 = new DateTime("now");
        $interval = $date1->diff($date2);
        
        
        print "Processing  device(s) in " . $interval->format('%R%S seconds') . "\n";
    }
    
    
    /*
     * Update Device Ports
     */
    public function updateDevicePort($objdata,$device_id,$digital,$timestamp){
        
        //print_r($objdata);
        //die();
        //echo "Get Device Ports-----------------------------------------------";
        $counter=0;
        
        $deviceports = $this->listPorts($device_id,$digital);
        
        //print_r($deviceports);
        //die();
        foreach($deviceports as $port) {
            
            print "Updating Device Port " . $port->deviceport_id .  "\n";
            
            $deviceport = Deviceport::find()->where(['deviceport_id'=>$port->deviceport_id])->one();
            
            $prev_value = $deviceport->value;
            
            $value = $this->getDataValue($objdata,$port->port_id,$digital);
            
            $poll_prev = $deviceport->poll_time;
            
            $deviceport->value = $value;
            $deviceport->value_prev=$prev_value;
            $deviceport->updated= new Expression('CURRENT_TIMESTAMP');
            $deviceport->poll_prev=$poll_prev;
            $deviceport->poll_time=$timestamp;
            
            //Add to History
            
            Deviceporthistory::addtoHistory($deviceport);
            
            if(!$deviceport->save()) {
                $counter++;
            }
            
            //Create Notification
            
            
            if ($digital) {
                
                //Value is changed
                if ($value!=$prev_value) {
                    
                    date_default_timezone_set('Asia/Jakarta');
                    $message = [
                            'message'=>'Value Changed',
                            'minvalue'=>'1',
                            'noticevalue'=>'0',
                            'warningvalue'=>'0',
                            'previous_value'=>$prev_value,
                            'current_value'=>$value,
                            'importance'=>Notification::TYPE_ERROR,
                            'deviceport_id'=>$port->deviceport_id,
                            'process_time'=>date('d-m-Y H:i:s'),
                    ];
                    
                    // Notification::notify(1, $port->deviceport_id,$message);
                    
                    $dseverity = Devicealert::SEVERITY_INFO;
                    
                    if($value==True) {
                        
                        $dseverity = Devicealert::SEVERITY_INFO;
                        $devAlert = new Devicealert();
                        $devAlert = $devAlert->close($port->deviceport_id);
                        
                        //$devAlert->alertlog($message, $value, $prev_value);
                    }
                    else {
                        
                        $dseverity = Devicealert::SEVERITY_CRITICAL;
                        
                        //Devicealert::alert($port->deviceport_id,$severity);
                        $devAlert = new Devicealert();
                        $devAlert = $devAlert->alert($port->deviceport_id,$dseverity);
                        
                        //print_r($result);
                        //die();
                        
                        $devAlert->alertlog($message, $value, $prev_value);
                    }
                    
                    
                    
                }
                
            }// end if digital
            
            //Generate Alert
            if(!$digital) {
                
                //get PortGroup
                $portS = $this->getPort($port->port_id);
                $portgroup = $this->getPortGroup($portS->portgroup_id);
                
                $message = [
                        'message'=>'Value Changed',
                        'minvalue'=>$portgroup->minvalue,
                        'noticevalue'=>$portgroup->noticevalue,
                        'warningvalue'=>$portgroup->warningvalue,
                        'current_value'=>$value,
                        'previous_value'=>$prev_value,
                        'process_time'=>date('d-m-Y H:i:s'),
                ];
                
                $severity = Devicealert::SEVERITY_NONE;
                
                if ($value<$portgroup->minvalue) {
                    
                    //Value below minimum
                    $severity = Devicealert::SEVERITY_INFO;
                }
                
                elseif ($value>$portgroup->noticevalue) {
                    //Notice Value
                    $severity = Devicealert::SEVERITY_WARNING;
                }
                
                elseif ($value>$portgroup->warningvalue) {
                    //Warning Value
                    $severity = Devicealert::SEVERITY_CRITICAL;
                }
                
                //Close if none severit$vy
                if ($severity==Devicealert::SEVERITY_NONE) {
                    
                    $devalert = new Devicealert();
                    
                    $devalert->close($port->deviceport_id);
                    $devalert->alertlog($message, $value, $prev_value);
                    
                    //print_r("dev alert" . $devalert);
                    //die();
                    //Create Log
                    // Alertlog::alert($devalert, $message,$value,$prev_value);
                }
                
                else {
                    
                    //Devicealert::alert($port->deviceport_id,$severity);
                    $deviceAlert = new Devicealert();
                    $deviceAlert = $deviceAlert->alert($port->deviceport_id,$severity);
                    
                    //print_r($result);
                    //die();
                    
                    $deviceAlert->alertlog($message, $value, $prev_value);
                    
                    //print_r("dev alert" . $devalert);
                    //die();
                    //Create Log
                    
                }
            }
            
            
            
        }
        
        return $counter;
        
    }//end UpdateDevicePort
    
    
    /**
     * 
     * @param Array $objdata
     * @param integer $port_id
     * @param boolean $digital
     * @return number
     */
    public function getDataValue($objdata,$port_id,$digital){
        
        $dataLength = count($objdata)-1;
        
        $data = $objdata;
        
        $datavalue=0;
         
        //Digital Port Only one register per port
        if ($digital==True) {
            
            $portregister =  Portregister::find()->where(['port_id'=>$port_id])->one();
            
            if ($portregister->registeredports > $dataLength) {
                
                $datavalue=0;
            }
            
            else {
                
                
                //echo "Data Value----" . $data[$portregister->registeredports];
                $datavalue = $this->changeDigitalValue($data[$portregister->registeredports]);
            }
            
            //Create Notification on Error
            
            
        }
        
        //Analog port can have multiple register
        else {
            
            $portregister =  Portregister::find()->where(['port_id'=>$port_id])->all();
            
            $strBin="";
            
            foreach ($portregister as $register) {
                
                //TODO add processing for multile pregister to concat binary value
                
                $strValue = $data[$register->registeredports];
                
                $strBin .= decbin((int)$strValue); //Binary Concatenation
                
                //print "Binary for Port name " . $register->name  .  " Registered Ports " . $register->registeredports . " Decimal " . $strValue . " Binary " . $strBin;
            }
            
            $datavalue = bindec($strBin);
            
            //print "port id" . $port_id . " Binary Value" .  $strBin . " Data Value " . $datavalue ."\n";
        }
        
        
        
        return $datavalue;
        
    } //end getDataValue
    
    public function changeDigitalValue($val){
        if ($val=="True") {
            return 0;
        }
        else {
            return 1;
        }
    }
    
    
    
    /*
     *
     */
    public function getPortGroup($portgroup_id) {
        
        $portgroup = Portgroup::find()->where(['portgroup_id'=>$portgroup_id])->one();
        
        // print_r($portregister);
        
        return $portgroup;
    }
    
    /*
     * Update Processed PollData
     */
    public function setPollProcessed($id){
        $polldata = Poll::findOne(['poll_id'=>$id]);
        
        $polldata->processed=true;
        
        $polldata->save();
        
    }
    
    /*
     * List Ports standard
     */
    public function listPorts($device_id,$isdigital){
        
        $port = Deviceport::find()
        ->joinWith('ports')
        ->joinWith('ports.portgroup')
        ->where(['deviceport.device_id'=>$device_id,'portgroup.isdigital'=>$isdigital,'deviceport.isactive'=>true])->all();
        
        return $port;
        
    }
    
    public function getPort($port_id) {
        
        $port = Ports::find()->where(['port_id'=>$port_id])->one();
        return $port;
    }
    
    /*
     * Update Processed PollData
     */
    public function setPollDataProcessed($id){
        $polldata = Polldata::findOne(['id'=>$id]);
        
        $polldata->processed=true;
        
        $polldata->save();
        
    }
}