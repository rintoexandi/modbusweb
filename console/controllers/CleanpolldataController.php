<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Poll;
use common\models\Polldata;

class CleanpolldataController extends Controller{
    
    
    public function actionIndex(){
        
        $this->deletePollData();
        $this->deletePoll();
        
    }
    
    /**
     * Delete processed polldata
     */
    private function deletePollData(){
        
        $models = Polldata::find()->where(['processed'=>True])->all();
        foreach ($models as $model) {
            $model->delete();
        }
        
    }
    
    /**
     * Delete processed poll
     */
    private function deletePoll(){
        $models = Poll::find()->where(['processed'=>True])->all();
        foreach ($models as $model) {
            $model->delete();
        }
    }
    
}