<?php
namespace console\controllers;


use common\models\Deviceport;
use common\models\Poll;
use common\models\Polldata;
use common\models\Portregister;
use common\models\Ports;
use Yii;
use yii\console\Controller;
use yii\db\ActiveRecord;
use yii\db\Expression;

class ProcesspollController extends Controller {
    
    /*
     *
     */
    public function actionIndex($device_id=0) {
        
        $this->processData($device_id);
    }
    
    /*
     *
     */
    public function processData($device_id=0) {
        
        //$date1 = new DateTime("now");
        
        //Date minus 30 minutes for poll data
        //$datepoll = date("Y-m-d H:i:s");
        //$timepoll = strtotime($datepoll);
        //$timepoll = $timepoll - (30 * 60); //Minus 30 minutes
        //$datepoll = date("Y-m-d H:i:s", $timepoll);
        
        //$devices = $this->listDevices();
        
        //foreach ($devices as $device) {
            
            // print "Processing poll data for device : " . $device->ipaddress . "\n";
            $updateDigital=0;
            $updateAnalog=0;
            
            $poll = Poll::getPoll($device_id);
            
            
            //print_r($polls);
            
            //die();
            
           // foreach ($polls as $poll) {
            
            if (!empty($poll)) {
                //print "Processing poll data for device : " . $device_id .  "  Poll Id " . $poll->poll_id  ."\n";
            
            
                $getDigitalDataPoll = $poll->getData(true);
                
               // print_r($getDataPoll1);
               // die();
                
                
                if (!empty($getDigitalDataPoll)) {
                    $digital=$getDigitalDataPoll["data"];
                    //print_r($digital);
                    //die();
                    //Update Device Ports
                    
                    $updateDigital=$this->updateDevicePort($digital,$device_id,true,$getDigitalDataPoll["poll_time"]);
                    
                    //echo "Update Digital " . $updateDigital;
                    
                    if ($updateDigital==0) {
                        
                        //$this->setPollDataProcessed($getDataPoll1["id"]);
                        Polldata::Process($getDigitalDataPoll["id"]);
                    }
                }
                
                $getAnalogDataPoll=$poll->getData(false);
                
                //print_r($getDataPoll2);
                // die();
                
                if (!empty($getAnalogDataPoll)) {
                    $analog = $getAnalogDataPoll["data"];
                    
                    //print_r($analog);
                    
                    $updateAnalog = $this->updateDevicePort($analog,$device_id,false,$getAnalogDataPoll["poll_time"]);
                    
                    //echo "Update Analog " . $updateAnalog;
                    
                    if ($updateAnalog==0) {
                        
                        Polldata::Process($getAnalogDataPoll["id"]);
                        //$this->setPollDataProcessed($getDataPoll2["id"]);
                    }
                }
                
                
                if($updateAnalog==0 && $updateDigital==0) {
                    
                    Poll::Process($poll->poll_id);
                    //$this->setPollProcessed($poll->poll_id);
                }
                
            }//end if
            
            
            
       // }//end loop device
        
       // $date2 = new DateTime("now");
       // $interval = $date1->diff($date2);
        
        
        //print "Processing " .  " device(s) in " . $interval->format('%R%S seconds') . "\n";
    }
    
   
    /*
     * Get Value data based on registered ports
     */
    public function getDataValue($objdata,$port_id,$digital){
        
        $dataLength = count($objdata)-1;
        
        $data = $objdata;
        
        $datavalue=0;
        
        
        //Digital Port Only one register per port
        if ($digital==True) {
            
            $portregister =  Portregister::find()->where(['port_id'=>$port_id])->one();
            
            if ($portregister->registeredports > $dataLength) {
                
                $datavalue=0;
            }
            
            else {
                              
                //echo "Data Value----" . $data[$portregister->registeredports];
                $datavalue = $this->changeDigitalValue($data[$portregister->registeredports]);
                
                //echo "Data Value ---->" . $data[$portregister->registeredports] . " Values " . $datavalue . "\n";
            }
            
            //Create Notification on Error           
            
        }
        
        //Analog port can have multiple register
        else {
            
            $portregister =  Portregister::find()->where(['port_id'=>$port_id])->all();
            
            $strBin="";
            
            foreach ($portregister as $register) {
                
                //TODO add processing for multile pregister to concat binary value
                
                $strValue = $data[$register->registeredports];
                
                $strBin .= decbin((int)$strValue); //Binary Concatenation
                
                //print "Binary for Port name " . $register->name  .  " Registered Ports " . $register->registeredports . " Decimal " . $strValue . " Binary " . $strBin;
            }
            
            $datavalue = bindec($strBin);
            
            //print "port id" . $port_id . " Binary Value" .  $strBin . " Data Value " . $datavalue ."\n";
        }
        
        return $datavalue;
        
    } //end getDataValue
    
    /**
     * 
     * @param Array $objdata
     * @param integer $device_id
     * @param boolean $digital
     * @param TimeStamp $timestamp
     * @return number
     */
    public function updateDevicePort($objdata,$device_id,$digital,$timestamp){
        
        //echo "Get Device Ports-----------------------------------------------";
        $counter=0;
        
        $deviceports = $this->listPorts($device_id,$digital);
        
        //print_r($deviceports);
        //die();
        foreach($deviceports as $devport) {
            
            //print "Updating Device Port " . $devport->deviceport_id .  "\n";
            
            //$deviceport = Deviceport::find()->where(['deviceport_id'=>$port->deviceport_id])->one();
            
            $port = $devport->getPort();
            
           
            //$portgroup = $port->Portgroup();
            
            $scalefactor = $port->scalefactor;
            
            if (!is_numeric($scalefactor)) {
                $scalefactor=1;
            }
            
            $prev_value = $devport->value;
            
            $value = $this->getDataValue($objdata,$devport->port_id,$digital);
            
            if (!$digital) {
            
                $value = $this->getDataValue($objdata,$devport->port_id,$digital) * $scalefactor;
            }
            
            $poll_prev = $devport->poll_time;
            
            $uptime = $this->checkUptime($value,$devport,$digital,$timestamp);
            
            $last_uptime = $this->setLastUptime($value,$devport,$digital,$timestamp);
            
            $devport->value = $value;
            $devport->value_prev=$prev_value;
            $devport->updated= new Expression('CURRENT_TIMESTAMP');
            $devport->poll_prev=$poll_prev;
            $devport->poll_time=$timestamp;
            $devport->last_uptime=$last_uptime;
            $devport->port_uptime=$uptime;
            
            //Add to History
            
            #SDeviceporthistory::addtoHistory($deviceport);
            
            if(!$devport->save(false)) {
                               
           
             $counter++;
             
            }
            
            else {
                //Set RRD processed = false
                
                Deviceport::setRRDCreated($devport->deviceport_id,false);
            }
          
       
        }
        
        return $counter;
        
    }//end UpdateDevicePort
    
    /**
     * 
     * This function is to calculate duration since the last time value changed
     * and store it on field port_uptime
     * 
     * @param int $new_val
     * @param Deviceport $devport
     * @param boolean $isdigital
     * @param timestamp $timestamp
     * @return integer
     */
    private function checkUptime($new_val,$devport,$isdigital,$timestamp){
        date_default_timezone_set('Asia/Jakarta'); 
        
        $currenttime = strtotime($devport->last_uptime);
        
        $newtime= strtotime($timestamp);
        
        $uptime=0;
        
        if ($isdigital==True) {
            
            if ($new_val==$devport->value_prev) {
                
                 $uptime=$newtime-$currenttime;
            }
            
        }
        
        return $uptime;
        
    }
    
    /**
     * This function is to set value on field last_uptime, the calculation is 
     * based on value changed, if the value is not changed then leave it alone
     * if changed, then update the last_uptime for $timestamp variable
     * @param int $new_val
     * @param Deviceport $devport
     * @param boolean $isdigital
     * @param timestamp $timestamp
     * @return timestamp
     */        
    private function setLastUptime($new_val,$devport,$isdigital,$timestamp){
        
        
        $last_uptime=$timestamp;
        
        if ($isdigital==True) {
            
            if ($new_val==$devport->value_prev) {
                
                $last_uptime = $devport->last_uptime;
            }
            
        }
        
        return $last_uptime;
        
    }
    
    /**
     * 
     * @param integer $registeredports
     * @return integer $port_id
     */
    public function mapPorts($registeredports){
        
        $ports = Ports::find()->where(['registeredports'=>$registeredports])->one();
        
        return $ports->port_id;
        
    }
    
    /**
     * 
     * @param integer $device_id
     * @param boolean $isdigital
     * @return array|ActiveRecord[]
     */
    public function listPorts($device_id,$isdigital){
        
        $port = Deviceport::find()
        ->joinWith('ports')      
        ->where(['deviceport.device_id'=>$device_id,'ports.isdigital'=>$isdigital,'deviceport.isactive'=>true])->all();
        
        return $port;
        
    }
     
    
    
    /**
     * 
     * @param string $val
     * @return number
     */
    public function changeDigitalValue($val){
        
        //print("Change Value of " . $val);
        
        if (strtoupper($val)=="FALSE") {
            return 1;
        }
        else {
            return 0;
        }
    }
    
   
    
}