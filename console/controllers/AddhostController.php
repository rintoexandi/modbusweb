<?php

namespace console\controllers;

use common\models\Devices;
use common\models\Telkomsites;
use common\models\Witel;
use yii\console\Controller;

class AddhostController extends Controller{
    
    public function actionIndex($host,$ipaddress,$sitename,$witelname, $sitetype) {
        
        $this->addDevice($host,$ipaddress,$sitename,$witelname, $sitetype);
    }
    
    public function addDevice($host,$ipaddress,$sitename,$witelname, $sitetype){
        
        $device = new Devices();
        $device->ipaddress=$ipaddress;
        $device->hostname=$host;
        $device->site_id=$this->getSite($sitename,$witelname)->site_id;
        $device->sitetype_id=$sitetype;
           
        
        if($device->Save(false)) {
            print 'Device ' . $host . ' is added';
        }
        else {
            print 'Unable to add ' . $host . ' to database';
        }
        
    }
    
    private function getSite($sitename,$witelname){
        
        $site = Telkomsites::find()->where(['name'=>$sitename])->one();
        
        if ($site !== null) {
            return $site;
        }
        else {
            
            return $this->addSite($sitename, $witelname);
        }
        
    }
    
    private function addSite($sitename,$witelname){
        
        $site=new Telkomsites();
        
        $site->name=$sitename;
        $site->description=$sitename;
       
        $site->witel_id=$this->getWitel($witelname);
        $site->sitetype_id=1;
        
        if($site->save(false)) {
            
            return $site;
        }
        
        return false;
    }
    
    private function getWitel($witelname){
        
        $witel = Witel::find()->where(['name'=>$witelname])->one();
        
        if ($witel !== null){
            
            return $witel;
        }
        
        else {
            $witel = Witel::find()->where(['witel_id'=>1])->one();
            return $witel;
        }
        
    }
}
