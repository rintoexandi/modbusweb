<?php

/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */

namespace console\controllers;

use common\models\Devices;
use common\models\Poll;
use common\models\Polldata;
use yii\console\Controller;

class SetemptydataController extends Controller {
    
    public $max_register=100;
    public $max_bits=64;
    
    public function actionIndex($ipaddress) {
        
        $this->generateData($this->getDevice($ipaddress)->device_id);
    }
    
    private function getDevice($ipaddress) {
        
        $device = Devices::find()->where(['ipaddress'=>$ipaddress])->one();
        
        return $device;
    }
    
    private function generateData($device_id){
       
        $poll=new Poll();
        $poll->device_id=$device_id;
        $poll->success=0;
        $poll->processed=false;
        
        if($poll->save()) {
          
            $this->generateEmptyData($poll,true);
            $this->generateEmptyData($poll,false);
        }
                
    }
    
    private function generateEmptyData($poll,$digital) {
        
        $data = new Polldata();
        $data->poll_id=$poll->poll_id;
        $data->isdigital=$digital;
        
        if ($digital==True) {
        
            $data->data_length=$this->max_bits;
            $data->data = $this->emptyData($digital,$this->max_bits);
            $data->processed=false;
        }
        
        else {
            
            $data->data_length=$this->max_register;
            $data->data = $this->emptyData($digital,$this->max_register);
            $data->processed=false;
        }
        
        $data->save(false);
    }
    
    private function emptyData($digital,$length){
        
        $data="[";
        
        if ($digital==True) {
           
            for ($i=0;$i<$length;$i++) {
                
                $value=0;
                
                if ($value==0) {
                   
                    $data .="False";
                }
                
                else {
                    $data .="True";
                }
                
                if ($i!=$length-1) {
                    $data.=",";
                }
                
            }
            
           
        }
        
        else {
            
            
            for ($i=0;$i<$length;$i++) {
                
                $value =0;
                $data .=$value;
                if ($i!=$length-1) {
                    $data.=",";
                }
            }
            
        }
        
        $data.="]";
        
       // print $data;
        
        return $data;
    }
}

