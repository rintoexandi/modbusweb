<?php

use yii\db\Migration;

/**
 * Handles the creation of table `devicealert`.
 */
class m190110_124619_create_devicealert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('devicealert', [
            'devicealert_id' => $this->primaryKey(),
            'deviceport_id'=>$this->integer()->notNull(),
            'open'=>$this->boolean(),
            'severity'=>$this->integer(),
            'alerted'=>$this->boolean(),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')       
        ]);
        
        $this->addForeignKey('fk_devicealert_deviceport',
                'devicealert', 'deviceport_id', 'deviceport', 'deviceport_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_devicealert_deviceport', 'devicealert');
        $this->dropTable('devicealert');
    }
}
