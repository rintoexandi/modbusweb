<?php

use yii\db\Migration;

/**
 * Handles the creation of table `alertrulesdetails`.
 */
class m190131_035737_create_alertrulesdetails_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('alertrulesdetails', [
            'id' => $this->primaryKey(),
            'alertrules_id'=>$this->integer()->notNull(),
            'sequence'=> $this->integer()->notNull(),
            'lowervalue'=>$this->float(),
            'uppervalue'=>$this->float(),
            'useboth'=>$this->boolean()->notNull()->defaultValue(false),
            'useduration'=>$this->boolean()->notNull(),
            'duration'=>$this->integer(), //in minutes
            'useoccurence'=>$this->boolean()->notNull(),
            'occurance'=>$this->integer(),
            'severity'=>$this->string(1)->notNull()->defaultValue('O'), //Warn,Info,Critical,Ok
            'isactive'=>$this->boolean()->defaultValue(True),
        ]);
        
        $this->addForeignKey('fk_alertrulesdetail_alertrules', 'alertrulesdetails', 'alertrules_id', 'alertrules', 'alertrules_id');
    
        $this->insert('alertrulesdetails',[
                'id'=>1,
                'alertrules_id'=>1,
                'sequence'=>1,
                'lowervalue'=>0,  
                'uppervalue'=>0,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>5, //in minutes
                'severity'=>'W',
                'isactive'=>True             
        ]);
        
        $this->insert('alertrulesdetails',[
                'id'=>2,
                'alertrules_id'=>1,
                'sequence'=>2,
                'lowervalue'=>0,  
                'uppervalue'=>0,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'C',
                'isactive'=>True             
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_alertrulesdetail_alertrules', 'alertrulesdetails');
        $this->dropTable('alertrulesdetails');
    }
}
