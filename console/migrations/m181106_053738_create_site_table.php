<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site`.
 */
class m181106_053738_create_site_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('site', [
            'site_id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull(),
            'description'=> $this->string(255),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'isdefault'=>$this->boolean()->notNull()->defaultValue(false),
            'supervisor_id'=>$this->integer(4),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' =>$this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updatedby'=>$this->integer(4)
        ]);
        
        $this->insert('site',[
             'site_id'=>1,
             'name'=>'Default',
             'isactive'=>1
        ]);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('site');
    }
}
