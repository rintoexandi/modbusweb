<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m181110_104043_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(100)->notNull()->unique(),
            'value'=>$this->string(255)->notNull(),
            'isactive' => $this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')
        
        ]);
        
        $this->insert('settings', [
                'id' => 1,
                'name'=>'max_analog_register',
                'value'=>'128',
                'isactive'=>'1'              
        ]);
        
        $this->insert('settings', [
                'id' => 2,
                'name'=>'max_digital_register',
                'value'=>'64',
                'isactive'=>'1'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
