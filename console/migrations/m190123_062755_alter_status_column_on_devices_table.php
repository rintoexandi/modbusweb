<?php

use yii\db\Migration;

/**
 * Class m190123_062755_alter_status_column_on_devices_table
 */
class m190123_062755_alter_status_column_on_devices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('devices', 'status', $this->boolean()->defaultValue(true));
        $this->alterColumn('devices', 'prev_status', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190123_062755_alter_status_column_on_devices_table cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190123_062755_alter_status_column_on_devices_table cannot be reverted.\n";

        return false;
    }
    */
}
