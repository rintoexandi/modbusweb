<?php

use yii\db\Migration;

/**
 * Class m190320_111738_alter_table_site2
 */
class m190320_111738_alter_table_site2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('site', 'sitetype_id', $this->integer(4));
         $this->update('site', ['sitetype_id' => 1]);
   
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site','sitetype_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190320_111738_alter_table_site2 cannot be reverted.\n";

        return false;
    }
    */
}
