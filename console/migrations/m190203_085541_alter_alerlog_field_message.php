<?php

use yii\db\Migration;

/**
 * Class m190203_085541_alter_alerlog_field_message
 */
class m190203_085541_alter_alerlog_field_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('alertlog', 'message', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190203_085541_alter_alerlog_field_message cannot be reverted.\n";

        return false;
    }
    */
}
