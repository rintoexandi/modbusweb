<?php

use yii\db\Migration;

/**
 * Class m190205_090240_alter_rules_alert
 */
class m190205_090240_alter_rules_alert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('alertrules',[
                'alertrules_id'=>3,
                'name'=>'Room Temperature',
                'isdigital'=>false,  
                'notify'=>True,                
                'isactive'=>True             
        ]);
         $this->insert('alertrulesdetails',[
                'id'=>3,
                'alertrules_id'=>3,
                'sequence'=>1,
                'lowervalue'=>0,  
                'uppervalue'=>22,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'O',
                'isactive'=>True             
        ]);
         
         $this->insert('alertrulesdetails',[
                'id'=>4,
                'alertrules_id'=>3,
                'sequence'=>2,
                'lowervalue'=>23,  
                'uppervalue'=>24,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'I',
                'isactive'=>True             
        ]); 
         
        $this->insert('alertrulesdetails',[
                'id'=>5,
                'alertrules_id'=>3,
                'sequence'=>3,
                'lowervalue'=>26,  
                'uppervalue'=>28,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'W',
                'isactive'=>True             
        ]); 
        
        $this->insert('alertrulesdetails',[
                'id'=>6,
                'alertrules_id'=>3,
                'sequence'=>4,
                'lowervalue'=>29,  
                'uppervalue'=>40,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'C',
                'isactive'=>True             
        ]); 

         $this->insert('alertrules',[
                'alertrules_id'=>4,
                'name'=>'Electricity',
                'isdigital'=>false,  
                'notify'=>True,                
                'isactive'=>True             
        ]);
         
       $this->insert('alertrulesdetails',[
                'id'=>7,
                'alertrules_id'=>4,
                'sequence'=>1,
                'lowervalue'=>0,  
                'uppervalue'=>179,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'C',
                'isactive'=>True             
        ]); 
        $this->insert('alertrulesdetails',[
                'id'=>8,
                'alertrules_id'=>4,
                'sequence'=>2,
                'lowervalue'=>180,  
                'uppervalue'=>224,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'O',
                'isactive'=>True             
        ]); 
        
         $this->insert('alertrulesdetails',[
                'id'=>9,
                'alertrules_id'=>4,
                'sequence'=>3,
                'lowervalue'=>225,  
                'uppervalue'=>280,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'I',
                'isactive'=>True             
        ]); 
         
        $this->insert('alertrules',[
                'alertrules_id'=>5,
                'name'=>'Humidity',
                'isdigital'=>false,  
                'notify'=>True,                
                'isactive'=>True             
        ]);
          
           $this->insert('alertrulesdetails',[
                'id'=>10,
                'alertrules_id'=>5,
                'sequence'=>1,
                'lowervalue'=>0,  
                'uppervalue'=>500,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>15, //in minutes
                'severity'=>'O',
                'isactive'=>True             
        ]); 
         
        $this->insert('alertrules',[
                'alertrules_id'=>6,
                'name'=>'Voltage',
                'isdigital'=>false,  
                'notify'=>True,                
                'isactive'=>True             
        ]);
          
          $this->insert('alertrulesdetails',[
                'id'=>11,
                'alertrules_id'=>6,
                'sequence'=>1,
                'lowervalue'=>0,  
                'uppervalue'=>20,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>5, //in minutes
                'severity'=>'C',
                'isactive'=>True             
        ]); 
          
           $this->insert('alertrulesdetails',[
                'id'=>12,
                'alertrules_id'=>6,
                'sequence'=>2,
                'lowervalue'=>21,  
                'uppervalue'=>40,  
                'useduration'=>true,
                'useoccurence'=>false,
                'useboth'=>false,
                'duration'=>5, //in minutes
                'severity'=>'O',
                'isactive'=>True             
        ]); 
  
      
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190205_090240_alter_rules_alert cannot be reverted.\n";

        return true;
    }

    
}
