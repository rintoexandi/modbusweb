<?php

use yii\db\Migration;

/**
 * Class m190403_034541_alter_ports_add_uom_field
 */
class m190403_034541_alter_ports_add_uom_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ports', 'uom_id', $this->integer());
        $this->addColumn('ports','decimalpoint',$this->integer(1));
     
        $this->update('ports', ['decimalpoint' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('ports', 'uom_id');
       $this->dropColumn('ports','decimalpoint');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190403_034541_alter_ports_add_uom_field cannot be reverted.\n";

        return false;
    }
    */
}
