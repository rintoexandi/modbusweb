<?php

use yii\db\Migration;

/**
 * Class m190203_081458_alter_table_alertlog
 */
class m190203_081458_alter_table_alertlog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('devicealert', 'severity', $this->string(1)->notNull());
       
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190203_081458_alter_table_alertlog cannot be reverted.\n";

        return false;
    }
    */
}
