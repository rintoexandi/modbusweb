<?php

use yii\db\Migration;

/**
 * Handles the creation of table `graphtype`.
 */
class m181109_052026_create_graphtype_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('graphtype', [
            'graphtype_id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull()->unique(),
            'description'=> $this->string(255),
            'isactive'=>$this->boolean()->notNull()->defaultExpression('1'),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4)
                
                
        ]);
        
        $this->insert('graphtype',[
                'graphtype_id'=>1,
                'name'=>'NONE',
                'description'=>'No Graph'
        ]);
        
        $this->insert('graphtype',[
                'graphtype_id'=>2,
                'name'=>'LINE'
        ]);
        
        $this->insert('graphtype',[
                'graphtype_id'=>3,
                'name'=>'KNOB'
        ]);
        
        $this->insert('graphtype',[
                'graphtype_id'=>4,
                'name'=>'BAR'
        ]);
        
        $this->insert('graphtype',[
                'graphtype_id'=>5,
                'name'=>'PIE'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('graphtype');
    }
}
