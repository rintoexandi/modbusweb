<?php

use yii\db\Migration;

/**
 * Handles the creation of table `datapoll`.
 */
class m181106_094410_create_poll_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('poll', [
            'poll_id' => $this->primaryKey(),
            'device_id'=>$this->integer()->notNull(),
            'success'=>$this->boolean()->defaultValue(true),
            'processed'=>$this->boolean()->defaultExpression('0'),
            'poll_attempted'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'poll_prevattempted'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')
                
        ]);
        
        $this->addForeignKey(
                'fk-poll-device-id',
                'poll',
                'device_id',
                'devices',
                'device_id',
                'RESTRICT'
                );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //Drop ForeignKey
        $this->dropForeignKey(
                'fk-poll-device-id',
                'poll'
                );
        
        $this->dropTable('poll');
    }
}
