<?php

use yii\db\Migration;

/**
 * Class m190320_090848_alter_table_site
 */
class m190320_090848_alter_table_site extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('site', 'witel_id', $this->integer(4));
         $this->update('site', ['witel_id' => 1]);
       
      
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site','witel_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190320_090848_alter_table_site cannot be reverted.\n";

        return false;
    }
    */
}
