<?php

use yii\db\Migration;

/**
 * Class m190304_112711_alter_portrelation_table
 */
class m190304_112711_alter_portrelation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('portregister', 'memory', $this->string(5));
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('portregister', 'memory');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190304_112711_alter_portrelation_table cannot be reverted.\n";

        return false;
    }
    */
}
