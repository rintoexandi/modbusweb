<?php

use yii\db\Migration;

/**
 * Class m190125_144514_alter_value_colum_deviceport_table
 */
class m190125_144514_alter_value_colum_deviceport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('deviceport', 'value', $this->float());
        $this->alterColumn('deviceport', 'value_prev', $this->float());
        $this->addColumn('deviceport', 'configtype', $this->string(1)->defaultValue('G'));
        
        
        $this->alterColumn('portgroup', 'scalefactor', $this->float());
        $this->alterColumn('portgroup', 'minvalue', $this->float());
        $this->alterColumn('portgroup', 'maxvalue', $this->float());
        $this->alterColumn('portgroup', 'warningvalue', $this->float());
        $this->alterColumn('portgroup', 'errorvalue', $this->float());
        
        $this->alterColumn('deviceportsetting', 'minvalue', $this->float());
        $this->alterColumn('deviceportsetting', 'maxvalue', $this->float());
       
      
        $this->addColumn('deviceportsetting', 'noticevalue', $this->float());
        $this->addColumn('deviceportsetting', 'warningvalue', $this->float());
        
        $this->alterColumn('deviceporthistory', 'value', $this->float());
        $this->alterColumn('deviceporthistory', 'value_prev', $this->float());
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //echo "m190125_144514_alter_value_colum_deviceport_table cannot be reverted.\n";

        //$this->dropColumn('deviceportsetting', 'minvalue');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190125_144514_alter_value_colum_deviceport_table cannot be reverted.\n";

        return false;
    }
    */
}
