<?php

use yii\db\Migration;

/**
 * Class m190302_112440_alter_widget_table
 */
class m190302_112440_alter_widget_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('widget', 'widgetgroup_id', $this->integer(4));
        
        $this->addColumn('widget', 'parent_id', $this->integer(4));
        $this->addColumn('widget', 'port_id', $this->integer(4));
        //Group PLN
        $this->update('widget', ['widgetgroup_id' => 1], ['widget_id' => 1]);
        $this->update('widget', ['widgetgroup_id' => 2], ['widget_id' => 2]);
        $this->update('widget', ['widgetgroup_id' => 3], ['widget_id' => 3]);
        $this->update('widget', ['widgetgroup_id' => 4], ['widget_id' => 4]);
        $this->update('widget', ['widgetgroup_id' => 5], ['widget_id' => 5]);
        $this->update('widget', ['widgetgroup_id' => 6], ['widget_id' => 6]);
        $this->update('widget', ['widgetgroup_id' => 7], ['widget_id' => 7]);
        $this->update('widget', ['widgetgroup_id' => 8], ['widget_id' => 8]);
        $this->update('widget', ['widgetgroup_id' => 9], ['widget_id' => 9]);
        
        //$this->addForeignKey('fg_widget_widgetgroup_id', 'widget',
       //         'widgetgroup_id', 'widgetgroup', 'widgetgroup_id');
       
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // $this->dropForeignKey('fg_widget_widgetgroup_id', 'widget');
        $this->dropColumn('widget', 'widgetgroup_id');
        $this->dropColumn('widget', 'parent_id');
        $this->dropColumn('widget', 'port_id');

      
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190302_112440_alter_widget_table cannot be reverted.\n";

        return false;
    }
    */
}
