<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pollreportdetail`.
 */
class m181109_052902_create_pollreportdetail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pollreportdetail', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pollreportdetail');
    }
}
