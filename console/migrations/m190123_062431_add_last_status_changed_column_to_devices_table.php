<?php

use yii\db\Migration;

/**
 * Handles adding last_status_changed to table `devices`.
 */
class m190123_062431_add_last_status_changed_column_to_devices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('devices', 'last_status_changed', $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')->after('prev_status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('devices', 'last_status_changed');
    }
}
