<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portrelation`.
 */
class m181109_054218_create_portrelation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('portrelation', [
            'id' => $this->primaryKey(),
            'widget_id'=>$this->integer(),    
            'port_id'=>$this->integer()->notNull()->unique(),
            'portanalog_id'=>$this->integer()->notNull(),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4)
            
        ]);
        
        $this->addForeignKey('fk_portrelation_widget',
                'portrelation', 'widget_id', 'widget', 'widget_id');
        
        
        $this->addForeignKey('fk_portrelation_ports_digital',
                'portrelation', 'port_id', 'ports', 'port_id');
        
        $this->addForeignKey('fk_portrelation_ports_analog',
                'portrelation', 'portanalog_id', 'ports', 'port_id');
        
        
        $this->insert('portrelation',['id'=>1,'widget_id'=>1,'port_id'=>14,'portanalog_id'=>'50','isactive'=>'1']);
        $this->insert('portrelation',['id'=>2,'widget_id'=>1,'port_id'=>15,'portanalog_id'=>'51','isactive'=>'1']);
        $this->insert('portrelation',['id'=>3,'widget_id'=>1,'port_id'=>16,'portanalog_id'=>'52','isactive'=>'1']);
        $this->insert('portrelation',['id'=>4,'widget_id'=>1,'port_id'=>17,'portanalog_id'=>'53','isactive'=>'1']);
        $this->insert('portrelation',['id'=>5,'widget_id'=>1,'port_id'=>18,'portanalog_id'=>'54','isactive'=>'0']);
        $this->insert('portrelation',['id'=>6,'widget_id'=>1,'port_id'=>19,'portanalog_id'=>'55','isactive'=>'0']);
        $this->insert('portrelation',['id'=>7,'widget_id'=>1,'port_id'=>20,'portanalog_id'=>'56','isactive'=>'0']);
        $this->insert('portrelation',['id'=>8,'widget_id'=>1,'port_id'=>21,'portanalog_id'=>'57','isactive'=>'0']);
        $this->insert('portrelation',['id'=>9,'widget_id'=>4,'port_id'=>22,'portanalog_id'=>'58','isactive'=>'1']);
        $this->insert('portrelation',['id'=>10,'widget_id'=>4,'port_id'=>23,'portanalog_id'=>'59','isactive'=>'1']);
        $this->insert('portrelation',['id'=>11,'widget_id'=>4,'port_id'=>24,'portanalog_id'=>'60','isactive'=>'1']);
        $this->insert('portrelation',['id'=>12,'widget_id'=>4,'port_id'=>25,'portanalog_id'=>'61','isactive'=>'1']);
        $this->insert('portrelation',['id'=>13,'widget_id'=>2,'port_id'=>26,'portanalog_id'=>'62','isactive'=>'1']);
        $this->insert('portrelation',['id'=>14,'widget_id'=>2,'port_id'=>27,'portanalog_id'=>'63','isactive'=>'1']);
        $this->insert('portrelation',['id'=>15,'widget_id'=>2,'port_id'=>28,'portanalog_id'=>'64','isactive'=>'1']);
        $this->insert('portrelation',['id'=>16,'widget_id'=>2,'port_id'=>29,'portanalog_id'=>'65','isactive'=>'1']);
        $this->insert('portrelation',['id'=>17,'widget_id'=>5,'port_id'=>30,'portanalog_id'=>'68','isactive'=>'1']);
        $this->insert('portrelation',['id'=>18,'widget_id'=>6,'port_id'=>31,'portanalog_id'=>'77','isactive'=>'1']);
        $this->insert('portrelation',['id'=>19,'widget_id'=>6,'port_id'=>32,'portanalog_id'=>'78','isactive'=>'1']);
        $this->insert('portrelation',['id'=>20,'widget_id'=>6,'port_id'=>33,'portanalog_id'=>'79','isactive'=>'1']);
        $this->insert('portrelation',['id'=>21,'widget_id'=>6,'port_id'=>34,'portanalog_id'=>'105','isactive'=>'1']);
        $this->insert('portrelation',['id'=>22,'widget_id'=>7,'port_id'=>35,'portanalog_id'=>'106','isactive'=>'1']);
        $this->insert('portrelation',['id'=>23,'widget_id'=>9,'port_id'=>37,'portanalog_id'=>'108','isactive'=>'1']);
    
        #Electric 
        
        $this->insert('portrelation',['id'=>24,'widget_id'=>3,'port_id'=>1,'portanalog_id'=>'1','isactive'=>'1']);
        $this->insert('portrelation',['id'=>25,'widget_id'=>3,'port_id'=>2,'portanalog_id'=>'2','isactive'=>'1']);
        $this->insert('portrelation',['id'=>26,'widget_id'=>3,'port_id'=>3,'portanalog_id'=>'3','isactive'=>'1']);
    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('fk_portrelation_widget','portrelation');
        $this->dropIndex('fk_portrelation_ports_digital','portrelation');
        $this->dropIndex('fk_portrelation_ports_digital', 'portrelation');
        $this->dropTable('portrelation');
    }
}
