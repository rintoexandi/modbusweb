<?php

use yii\db\Migration;

/**
 * Handles the creation of table `polldata`.
 */
class m181107_091346_create_polldata_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('polldata', [
            'id' => $this->primaryKey(),
            'poll_id'=>$this->integer()->notNull(),
            'isdigital'=>$this->boolean()->defaultValue(true),
            'data'=>$this->text(),
            'data_length'=>$this->integer(4),
            'processed'=>$this->boolean()->defaultValue(false),
            'created'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
        
        $this->addForeignKey(
                'fk-poll-data-id',
                'polldata',
                'poll_id',
                'poll',
                'poll_id',
                'RESTRICT'
                );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //Drop ForeignKey
        $this->dropForeignKey(
                'fk-poll-data-id',
                'polldata'
                );
        
        $this->dropTable('polldata');
    }
}
