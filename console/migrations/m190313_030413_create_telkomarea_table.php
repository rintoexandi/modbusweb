<?php

use yii\db\Migration;

/**
 * Handles the creation of table `telkomarea`.
 */
class m190313_030413_create_telkomarea_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('telkomarea', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull(),
            'description'=> $this->string(255),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' =>$this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updatedby'=>$this->integer(4)
        ]);
        
         $this->insert('telkomarea',[
             'id'=>1,
             'name'=>'Default Area',
             'isactive'=>1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('telkomarea');
    }
}
