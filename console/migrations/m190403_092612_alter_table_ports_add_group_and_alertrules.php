<?php

use yii\db\Migration;

/**
 * Class m190403_092612_alter_table_ports_add_group_and_alertrules
 */
class m190403_092612_alter_table_ports_add_group_and_alertrules extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ports', 'isdigital', $this->boolean());
        $this->addColumn('ports','alertrules_id',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('ports', 'isdigital');
       $this->dropColumn('ports', 'alertrules_id');
    }

    
}
