<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sitetype`.
 */
class m190320_111140_create_sitetype_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sitetype', [
            'sitetype_id' => $this->primaryKey(),
            'name'=>$this->string(150),
            'importance_level'=>$this->integer(1), //importance level, 1,2,3,4,5 | 1 ->normal, 5-->highest
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' =>$this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updatedby'=>$this->integer(4)
        ]);
        
        $this->insert('sitetype',[
             'sitetype_id'=>1,
             'importance_level'=>1,
             'name'=>'Site',
  
             'isactive'=>1
        ]);
        
        $this->insert('sitetype',[
             'sitetype_id'=>2,
             'importance_level'=>5,
             'name'=>'Primary POP',
   
             'isactive'=>1
        ]);
        
        $this->insert('sitetype',[
             'sitetype_id'=>3,
             'importance_level'=>4,
             'name'=>'Secondary POP',  
             'isactive'=>1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sitetype');
    }
}
