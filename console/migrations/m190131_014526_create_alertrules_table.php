<?php

use yii\db\Migration;

/**
 * Handles the creation of table `alertrules`.
 */
class m190131_014526_create_alertrules_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('alertrules', [
            'alertrules_id' => $this->primaryKey(),
            'name'=>$this->string(150)->notNull(),     
            'isdigital'=>$this->boolean()->notNull(),
            'notify'=>$this->boolean()->notNull()->defaultValue(true),
            'isactive'=>$this->boolean()->defaultValue(True),
            
        ]);
        
        $this->addColumn('portgroup', 'alertrules_id', $this->integer());
        $this->addColumn('deviceport', 'alertrules_id', $this->integer());
        
        $this->addForeignKey('fk_portgroup_alertrules', 'portgroup', 'alertrules_id', 'alertrules', 'alertrules_id');
        $this->addForeignKey('fk_deviceport_alertrules', 'deviceport', 'alertrules_id', 'alertrules', 'alertrules_id');
        
        $this->insert('alertrules',[
                'alertrules_id'=>1,
                'name'=>'Default Digital',
                'isdigital'=>True,  
                'notify'=>True,                
                'isactive'=>True             
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_portgroup_alertrules', 'portgroup');
        $this->dropForeignKey('fk_deviceport_alertrules', 'deviceport');
        $this->dropTable('alertrules');
    }
}
