<?php

use yii\db\Migration;

/**
 * Class m190302_144315_add_fk_widget_group
 */
class m190302_144315_add_fk_widget_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_widget_widgetgroup_id', 'widget',
                'widgetgroup_id', 'widgetgroup', 'widgetgroup_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
         $this->dropForeignKey('fk_widget_widgetgroup_id', 'widget');
         $this->dropIndex('fk_widget_widgetgroup_id', 'widget');
        
         return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190302_144315_add_fk_widget_group cannot be reverted.\n";

        return false;
    }
    */
}
