<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%daemon}}`.
 */
class m190418_044731_create_daemon_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%daemon}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(25)->notNull()->unique(),
            'description'=> $this->string(255),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'last_started'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'last_finished'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'message'=>$this->string(250),
            'status'=>$this->boolean()->defaultValue(false),
           
        ]);
        
        $this->insert('{{%daemon}}',[
            'id'=>1,
            'name'=>'MODBUSPOLLER',
            'description'=>'Polling data from Modbus',
            'isactive'=>true,
        ]);
        
         $this->insert('{{%daemon}}',[
            'id'=>2,
            'name'=>'PROCESSPOLLER',
            'description'=>'Process data that polled from Modbus',
            'isactive'=>true,
        ]);
         
         $this->insert('{{%daemon}}',[
            'id'=>3,
            'name'=>'UPDATERRD',
            'description'=>'Update Round Robin Database',
            'isactive'=>true,
        ]);
         
        $this->insert('{{%daemon}}',[
            'id'=>4,
            'name'=>'CHECKALERT',
            'description'=>'Check Alert based on alert rules',
            'isactive'=>true,
        ]);
        
         $this->insert('{{%daemon}}',[
            'id'=>5,
            'name'=>'SENDALERT',
            'description'=>'Send alert that occur from CHECKALERT',
            'isactive'=>true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%daemon}}');
    }
}
