<?php

use yii\db\Migration;

/**
 * Handles the creation of table `registergroup`.
 */
class m181109_053349_create_portgroup_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('portgroup', [
            'portgroup_id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull(),
            'description'=> $this->string(255),
            'isdigital'=>$this->boolean()->notNull()->defaultValue(false),
            'settingtype'=>$this->string(1)->notNull()->defaultValue('R'), 
            'minvalue'=>$this->float(),
            'maxvalue'=>$this->float(),
            'noticevalue'=>$this->float(),
            'warningvalue'=>$this->float(),
            'errorvalue'=>$this->float(),
            'scalefactor'=>$this->float(),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'notify'=>$this->boolean()->notNull()->defaultValue(true),
            'unit'=>$this->string(5),
            'usegraph'=>$this->boolean()->notNull()->defaultExpression('1'),
            'graphtype'=>$this->string(1)->notNull()->defaultValue('N'),            
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4),             
        ]);
        
         
        $this->insert('portgroup',[
                'portgroup_id'=>1,
                'name'=>'Default',
                'isdigital'=>false,
                'settingtype'=>'R', //Range
                'description'=>'Default Range',
                'minvalue'=>0,
                'maxvalue'=>999999,
                'noticevalue'=>999999,
                'warningvalue'=>999999,
                'errorvalue'=>999999,    
                'graphtype'=>'C',
                'scalefactor'=>0.1,
                'isactive'=>true,
                'notify'=>true    
        ]);
        
        
        $this->insert('portgroup',[               
                'portgroup_id'=>2,
                'name'=>'Electricity',
                'isdigital'=>false,
                'settingtype'=>'R', //Range
                'description'=>'Default Range',
                'minvalue'=>0,
                'maxvalue'=>999,
                'noticevalue'=>180,
                'warningvalue'=>240,
                'errorvalue'=>0,
                'scalefactor'=>0.1,
                'usegraph'=>1,
                'graphtype'=>'V', //L:line,B:Bar,G:Gauge,K:Knob,N:None
                'isactive'=>true,
                'notify'=>true   
        ]);
        
        $this->insert('portgroup',[
                'portgroup_id'=>3,
                'name'=>'Temperature',
                'isdigital'=>false,
                'settingtype'=>'R', //Range
                'description'=>'Default Range',
                'minvalue'=>-50,
                'maxvalue'=>150,
                'noticevalue'=>28,
                'warningvalue'=>30,
                'errorvalue'=>45,
                'scalefactor'=>0.1,
                'usegraph'=>1,
                'graphtype'=>'T',
                'isactive'=>true,
                'notify'=>true   
        ]);
        
        $this->insert('portgroup',[
                'portgroup_id'=>4,
                'name'=>'Humidity',
                'settingtype'=>'R', //Range
                'description'=>'Default Range',
                'minvalue'=>-50,
                'maxvalue'=>150,
                'noticevalue'=>28,
                'warningvalue'=>30,
                'errorvalue'=>45,
                'scalefactor'=>0.1,
                'usegraph'=>1,
                'graphtype'=>'G',
                'isactive'=>true,
                'notify'=>true   
        ]);
        
        $this->insert('portgroup',[
                'portgroup_id'=>5,
                'name'=>'Voltage',
                'settingtype'=>'R', //Range
                'description'=>'Voltage Range',
                'minvalue'=>0,
                'maxvalue'=>250,
                'noticevalue'=>28,
                'warningvalue'=>30,
                'errorvalue'=>45,
                'scalefactor'=>0.1,
                'usegraph'=>1,
                'graphtype'=>'G',
                'isactive'=>true,
                'notify'=>true   
        ]);
        
        $this->insert('portgroup',[
                'portgroup_id'=>6,
                'name'=>'Fuel Level',
                'settingtype'=>'R', //Range
                'description'=>'Fuel Level Range',
                'minvalue'=>0,
                'maxvalue'=>999999,
                'noticevalue'=>999999,
                'warningvalue'=>999999,
                'errorvalue'=>999999,
                'scalefactor'=>0.1,
                'usegraph'=>1,
                'graphtype'=>'G',
                'isactive'=>true,
                'notify'=>true  
        ]);
        
        $this->insert('portgroup',[
                'portgroup_id'=>7,
                'name'=>'Genset Watts',
                'settingtype'=>'R', //Range
                'description'=>'Genset Watts Range',
                'minvalue'=>-999999,
                'maxvalue'=>999999,
                'noticevalue'=>999999,
                'warningvalue'=>999999,
                'errorvalue'=>999999,
                'scalefactor'=>0.1,
                'usegraph'=>1,
                'graphtype'=>'G',
                'isactive'=>true,
                'notify'=>true  
        ]);
        
        $this->insert('portgroup',[
                'portgroup_id'=>8,
                'name'=>'Oil Pressure',
                'settingtype'=>'R', //Range
                'description'=>'Oil Pressure Range',
                'minvalue'=>0,
                'maxvalue'=>10000,
                'noticevalue'=>10000,
                'warningvalue'=>10000,
                'errorvalue'=>10000,
                'scalefactor'=>0.1,
                'usegraph'=>1,
                'graphtype'=>'G',
                'isactive'=>true,
                'notify'=>true  
        ]);
        
        $this->insert('portgroup',[
                'portgroup_id'=>9,
                'name'=>'Default Digital',
                'isdigital'=>true,
                'settingtype'=>'T', //True False
                'description'=>'Default Digital',
                'usegraph'=>1,
                'minvalue'=>0,
                'maxvalue'=>99,
                'noticevalue'=>99,
                'warningvalue'=>99,
                'errorvalue'=>99,
                'scalefactor'=>1,
                'usegraph'=>0,
                'graphtype'=>'N',
                'isactive'=>true,
                'notify'=>true
        ]);
    }

  
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('registergroup');
    }
}
