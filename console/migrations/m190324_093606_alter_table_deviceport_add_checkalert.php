<?php

use yii\db\Migration;

/**
 * Class m190324_093606_alter_table_deviceport_add_checkalert
 */
class m190324_093606_alter_table_deviceport_add_checkalert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('deviceport', 'reportalert', $this->boolean());
         $this->update('deviceport', ['reportalert' => true]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('deviceport', 'reportalert');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190324_093606_alter_table_deviceport_add_checkalert cannot be reverted.\n";

        return false;
    }
    */
}
