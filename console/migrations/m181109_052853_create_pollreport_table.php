<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pollreport`.
 */
class m181109_052853_create_pollreport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pollreport', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pollreport');
    }
}
