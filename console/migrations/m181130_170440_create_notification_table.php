<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification`.
 */
class m181130_170440_create_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
             'key'=>$this->string(32)->notNull(),
             'message'=>$this->string(255),
             'read'=>$this->boolean()->defaultValue(false),        
             'importance'=>$this->integer(),
             'user_id'=>$this->integer()->notNull(),   
             'deviceport_id'=>$this->integer()->notNull(),
             'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
             'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')              
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notification');
    }
}
