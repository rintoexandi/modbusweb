<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%uom}}`.
 */
class m190403_033821_create_uom_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%uom}}', [
            'uom_id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull(),
            'symbol'=> $this->string(10)->notNull()->unique(),           
            'isactive'=>$this->boolean()->defaultValue(true),
            'created' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4) 
            
        ]);
        
        $this->insert('uom',[
            'uom_id'=>1,
            'name'=>'Volt',
            'symbol'=>'V'
        ]);
        
        $this->insert('uom',[
            'uom_id'=>2,
            'name'=>'Watt',
            'symbol'=>'W'
        ]);
        
        $this->insert('uom',[
            'uom_id'=>3,
            'name'=>'Kilowatt Hour',
            'symbol'=>'KWh'
        ]);
        
        $this->insert('uom',[
            'uom_id'=>4,
            'name'=>'Celcius',
            'symbol'=>'C'
        ]);
        
         $this->insert('uom',[
            'uom_id'=>5,
            'name'=>'Vold DC',
            'symbol'=>'VDC'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%uom}}');
    }
}
