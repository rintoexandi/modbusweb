<?php

use yii\db\Migration;

/**
 * Class m190130_121557_alter_devicealert_table
 */
class m190130_121557_alter_devicealert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('alertlog','poll_time',$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'));
        //$this->addColumn('devicealert','port_value',$this->float());
        //$this->addColumn('devicealert','prev_value',$this->float());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //echo "m190130_121557_alter_devicealert_table cannot be reverted.\n";
        $this->dropColumn('alertlog', 'poll_time');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190130_121557_alter_devicealert_table cannot be reverted.\n";

        return false;
    }
    */
}
