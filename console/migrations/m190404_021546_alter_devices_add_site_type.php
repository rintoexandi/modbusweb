<?php

use yii\db\Migration;

/**
 * Class m190404_021546_alter_devices_add_site_type
 */
class m190404_021546_alter_devices_add_site_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('devices', 'sitetype_id',  $this->integer());
        $this->update('devices', ['sitetype_id'=>1]);
        
        $this->dropColumn('devices', 'importance');
        
        $this->addForeignKey('fk_devices_site_type', 'devices', 'sitetype_id',
                'sitetype', 'sitetype_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->addColumn('devices', 'importance', $this->string(1));
       $this->dropForeignKey('fk_devices_site_type', 'devices');
       $this->dropColumn('devices','sitetype_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190404_021546_alter_devices_add_site_type cannot be reverted.\n";

        return false;
    }
    */
}
