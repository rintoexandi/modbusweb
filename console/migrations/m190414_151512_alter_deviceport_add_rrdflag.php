<?php

use yii\db\Migration;

/**
 * Class m190414_151512_alter_deviceport_add_rrdflag
 */
class m190414_151512_alter_deviceport_add_rrdflag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('deviceport', 'rrdcreated', $this->boolean());
         $this->update('deviceport', ['rrdcreated' => false]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('deviceport', 'rrdcreated');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190414_151512_alter_deviceport_add_rrdflag cannot be reverted.\n";

        return false;
    }
    */
}
