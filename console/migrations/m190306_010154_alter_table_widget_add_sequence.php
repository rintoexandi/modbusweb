<?php

use yii\db\Migration;

/**
 * Class m190306_010154_alter_table_widget_add_sequence
 */
class m190306_010154_alter_table_widget_add_sequence extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('widget', 'sameline', $this->boolean()->defaultValue(true));
            $this->addColumn('widget', 'sequence', $this->integer()->defaultValue(1));
            
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('widget', 'sameline');
       $this->dropColumn('widget', 'sequence');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190306_010154_alter_table_widget_add_sequence cannot be reverted.\n";

        return false;
    }
    */
}
