<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deviceport`.
 */
class m181109_062024_create_deviceport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('deviceport', [
            'deviceport_id' => $this->primaryKey(),
            'device_id'=>$this->integer()->notNull(),
            'port_id'=>$this->integer()->notNull(),  
            'value'=>$this->integer(),
            'value_inpercent'=>$this->decimal(),
            'value_prev'=>$this->integer(),
            'port_status'=>$this->boolean(),
            'port_status_prev'=>$this->boolean(),
            'last_uptime'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'port_uptime'=>$this->integer(),                
            'poll_time'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'poll_prev'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4)               
        ]);
        
          
        $this->addForeignKey('fk_deviceport_ports', 
                'deviceport', 'port_id', 'ports', 'port_id');
        
        $this->addForeignKey('fk_deviceport_devices',
                'deviceport', 'device_id', 'devices', 'device_id');
        
        $this->createIndex('idx_deviceport_device_and_port',
                'deviceport',
                ['device_id','port_id'],1);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_deviceport_ports', 'deviceport');        
        $this->dropForeignKey('fk_deviceport_devices', 'deviceport');
        $this->dropIndex('idx_deviceport_device_and_port', 'deviceport');
        $this->dropTable('deviceport');
    }
}
