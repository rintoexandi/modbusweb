<?php

use yii\db\Migration;

/**
 * Handles the creation of table `telkomwitel`.
 */
class m190313_030502_create_telkomwitel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('witel', [
            'witel_id' => $this->primaryKey(),
            'area_id'=>$this->integer(4)->notNull(),
            'name'=> $this->string(100)->notNull(),
            'description'=> $this->string(255),
            'parent_id'=>$this->integer(4),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' =>$this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updatedby'=>$this->integer(4)
            
        ]);
        
          
        $this->insert('witel',[
             'witel_id'=>1,
             'area_id'=>1,
             'name'=>'Default',
             'parent_id'=>0,
             'isactive'=>1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('telkomwitel');
    }
}
