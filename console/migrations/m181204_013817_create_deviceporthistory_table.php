<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deviceporthistory`.
 */
class m181204_013817_create_deviceporthistory_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('deviceporthistory', [
            'id' => $this->primaryKey(),
            'deviceport_id' => $this->integer()->notNull(),
            'device_id'=>$this->integer()->notNull(),
            'port_id'=>$this->integer()->notNull(),
            'value'=>$this->integer(),
            'value_prev'=>$this->integer(),
            'poll_time'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4)   
        ]);
        
        $this->addForeignKey('fk_deviceporthistory_ports',
                'deviceporthistory', 'port_id', 'ports', 'port_id');
        
        $this->addForeignKey('fk_deviceporthistory_devices',
                'deviceporthistory', 'device_id', 'devices', 'device_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_deviceporthistory_ports', 'deviceporthistory');
        $this->dropForeignKey('fk_deviceporthistory_devices', 'deviceporthistory');
        
        $this->dropTable('deviceporthistory');
    }
}
