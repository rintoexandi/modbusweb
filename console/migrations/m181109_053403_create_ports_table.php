<?php

use yii\db\Migration;

/**
 * Handles the creation of table `register`.
 */
class m181109_053403_create_ports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ports', [
            'port_id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull()->unique(),
            'description'=> $this->string(255),
            'portgroup_id'=>$this->integer()->notNull(),
            'icon'=> $this->string(255),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4) 
        ]);
        
        $this->addForeignKey('fk_port_portgroup',
                'ports', 'portgroup_id', 'portgroup', 'portgroup_id');
        
         
        //Digital
        $this->insert('ports',['port_id'=>1,'name'=>'01D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'PLN']);
        $this->insert('ports',['port_id'=>2,'name'=>'02D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Genset-1']);
        $this->insert('ports',['port_id'=>3,'name'=>'03D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Genset-2']);
        $this->insert('ports',['port_id'=>4,'name'=>'04D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Mains Fail Rectifier #1']);
        $this->insert('ports',['port_id'=>5,'name'=>'05D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Rectifier #1 Fault']);
        $this->insert('ports',['port_id'=>6,'name'=>'06D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Mains Fail Rectifier #2']);
        $this->insert('ports',['port_id'=>7,'name'=>'07D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Rectifier #2 Fault']);
        $this->insert('ports',['port_id'=>8,'name'=>'08D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>9,'name'=>'09D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>10,'name'=>'10D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>11,'name'=>'11D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>12,'name'=>'12D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>13,'name'=>'13D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'PLN & GENSET']);
        $this->insert('ports',['port_id'=>14,'name'=>'14D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 1']);
        $this->insert('ports',['port_id'=>15,'name'=>'15D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 2']);
        $this->insert('ports',['port_id'=>16,'name'=>'16D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 3']);
        $this->insert('ports',['port_id'=>17,'name'=>'17D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 4']);
        $this->insert('ports',['port_id'=>18,'name'=>'18D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 5']);
        $this->insert('ports',['port_id'=>19,'name'=>'19D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 6']);
        $this->insert('ports',['port_id'=>20,'name'=>'20D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 7']);
        $this->insert('ports',['port_id'=>21,'name'=>'21D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Temperatur Room 8']);
        $this->insert('ports',['port_id'=>22,'name'=>'22D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Humidity Room 1']);
        $this->insert('ports',['port_id'=>23,'name'=>'23D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Humidity Room 2']);
        $this->insert('ports',['port_id'=>24,'name'=>'24D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Humidity Room 3']);
        $this->insert('ports',['port_id'=>25,'name'=>'25D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Humidity Room 4']);
        $this->insert('ports',['port_id'=>26,'name'=>'26D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'DC Voltage Rect #1']);
        $this->insert('ports',['port_id'=>27,'name'=>'27D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'DC Voltage Rect #2']);
        $this->insert('ports',['port_id'=>28,'name'=>'28D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'DC Voltage Rect #3']);
        $this->insert('ports',['port_id'=>29,'name'=>'29D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'DC Voltage Rect #4']);
        $this->insert('ports',['port_id'=>30,'name'=>'30D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Total Power Factor']);
        $this->insert('ports',['port_id'=>31,'name'=>'31D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Voltage R-N']);
        $this->insert('ports',['port_id'=>32,'name'=>'32D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Voltage S-N']);
        $this->insert('ports',['port_id'=>33,'name'=>'33D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-flash','description'=>'Voltage T-N']);
        $this->insert('ports',['port_id'=>34,'name'=>'34D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'High Temperatur DEG']);
        $this->insert('ports',['port_id'=>35,'name'=>'35D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Low Oil Pressure DEG']);
        $this->insert('ports',['port_id'=>36,'name'=>'36D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'General Alarm DEG']);
        $this->insert('ports',['port_id'=>37,'name'=>'37D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'Fuel Level DEG']);
        $this->insert('ports',['port_id'=>38,'name'=>'38D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-dashboard','description'=>'']);
        $this->insert('ports',['port_id'=>39,'name'=>'39D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>40,'name'=>'40D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>41,'name'=>'41D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>42,'name'=>'42D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>43,'name'=>'43D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>44,'name'=>'44D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>45,'name'=>'45D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>46,'name'=>'46D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>47,'name'=>'47D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>48,'name'=>'48D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        $this->insert('ports',['port_id'=>49,'name'=>'49D','portgroup_id'=>9,'icon'=>'glyphicon glyphicon-asterisk','description'=>'']);
        //Analog
        $this->insert('ports',['port_id'=>50,'name'=>'01A','portgroup_id'=>3,'description'=>'Temperatur SWT']);
        $this->insert('ports',['port_id'=>51,'name'=>'02A','portgroup_id'=>3,'description'=>'Temperatur TRA']);
        $this->insert('ports',['port_id'=>52,'name'=>'03A','portgroup_id'=>3,'description'=>'Temperatur RECT']);
        $this->insert('ports',['port_id'=>53,'name'=>'04A','portgroup_id'=>3,'description'=>'Temperatur NGN']);
        $this->insert('ports',['port_id'=>54,'name'=>'05A','portgroup_id'=>3,'description'=>'Temp']);
        $this->insert('ports',['port_id'=>55,'name'=>'06A','portgroup_id'=>3,'description'=>'Temp']);
        $this->insert('ports',['port_id'=>56,'name'=>'07A','portgroup_id'=>3,'description'=>'Temp']);
        $this->insert('ports',['port_id'=>57,'name'=>'08A','portgroup_id'=>3,'description'=>'Temp']);
        $this->insert('ports',['port_id'=>58,'name'=>'09A','portgroup_id'=>4,'description'=>'Humidity']);
        $this->insert('ports',['port_id'=>59,'name'=>'10A','portgroup_id'=>4,'description'=>'Humidity']);
        $this->insert('ports',['port_id'=>60,'name'=>'11A','portgroup_id'=>4,'description'=>'Humidity']);
        $this->insert('ports',['port_id'=>61,'name'=>'12A','portgroup_id'=>4,'description'=>'Humidity']);
        $this->insert('ports',['port_id'=>62,'name'=>'13A','portgroup_id'=>5,'description'=>'DC Volt Rect']);
        $this->insert('ports',['port_id'=>63,'name'=>'14A','portgroup_id'=>5,'description'=>'DC Volt Rect']);
        $this->insert('ports',['port_id'=>64,'name'=>'15A','portgroup_id'=>5,'description'=>'DC Volt Rect']);
        $this->insert('ports',['port_id'=>65,'name'=>'16A','portgroup_id'=>5,'description'=>'DC Volt Rect']);
        $this->insert('ports',['port_id'=>66,'name'=>'17A','portgroup_id'=>2,'description'=>'KWH']);
        $this->insert('ports',['port_id'=>68,'name'=>'18A','portgroup_id'=>2,'description'=>'Total Power Factor']);
        $this->insert('ports',['port_id'=>69,'name'=>'19A','portgroup_id'=>2,'description'=>'KVA (current)']);
        $this->insert('ports',['port_id'=>70,'name'=>'20A','portgroup_id'=>2,'description'=>'KVA (max)']);
        $this->insert('ports',['port_id'=>71,'name'=>'21A','portgroup_id'=>2,'description'=>'Ampere R']);
        $this->insert('ports',['port_id'=>72,'name'=>'22A','portgroup_id'=>2,'description'=>'Ampere S']);
        $this->insert('ports',['port_id'=>73,'name'=>'23A','portgroup_id'=>2,'description'=>'Ampere T']);
        $this->insert('ports',['port_id'=>74,'name'=>'24A','portgroup_id'=>2,'description'=>'Voltage R-S']);
        $this->insert('ports',['port_id'=>75,'name'=>'25A','portgroup_id'=>2,'description'=>'Voltage S-T']);
        $this->insert('ports',['port_id'=>76,'name'=>'26A','portgroup_id'=>2,'description'=>'Voltage R-T']);
        $this->insert('ports',['port_id'=>77,'name'=>'27A','portgroup_id'=>2,'description'=>'Voltage R-N']);
        $this->insert('ports',['port_id'=>78,'name'=>'28A','portgroup_id'=>2,'description'=>'Voltage S-N']);
        $this->insert('ports',['port_id'=>79,'name'=>'29A','portgroup_id'=>2,'description'=>'Voltage T-N']);
        $this->insert('ports',['port_id'=>80,'name'=>'30A','portgroup_id'=>1,'description'=>'THD  R-N']);
        $this->insert('ports',['port_id'=>81,'name'=>'31A','portgroup_id'=>1,'description'=>'THD  S-N']);
        $this->insert('ports',['port_id'=>82,'name'=>'32A','portgroup_id'=>1,'description'=>'THD  T-N']);
        $this->insert('ports',['port_id'=>83,'name'=>'33A','portgroup_id'=>7,'description'=>'KVA (current) GENSET']);
        $this->insert('ports',['port_id'=>85,'name'=>'34A','portgroup_id'=>7,'description'=>'Ampere R GENSET']);
        $this->insert('ports',['port_id'=>87,'name'=>'35A','portgroup_id'=>7,'description'=>'Ampere S GENSET']);
        $this->insert('ports',['port_id'=>89,'name'=>'36A','portgroup_id'=>7,'description'=>'Ampere T GENSET']);
        $this->insert('ports',['port_id'=>91,'name'=>'37A','portgroup_id'=>7,'description'=>'Voltage R-S GENSET']);
        $this->insert('ports',['port_id'=>93,'name'=>'38A','portgroup_id'=>7,'description'=>'Voltage S-T GENSET']);
        $this->insert('ports',['port_id'=>95,'name'=>'39A','portgroup_id'=>7,'description'=>'Voltage R-T GENSET']);
        $this->insert('ports',['port_id'=>97,'name'=>'40A','portgroup_id'=>7,'description'=>'Voltage R-N GENSET']);
        $this->insert('ports',['port_id'=>99,'name'=>'41A','portgroup_id'=>7,'description'=>'Voltage S-N GENSET']);
        $this->insert('ports',['port_id'=>101,'name'=>'42A','portgroup_id'=>7,'description'=>'Voltage T-N GENSET']);
        $this->insert('ports',['port_id'=>103,'name'=>'43A','portgroup_id'=>7,'description'=>'Hour Counter GENSET']);
        $this->insert('ports',['port_id'=>105,'name'=>'44A','portgroup_id'=>3,'description'=>'Temperatur']);
        $this->insert('ports',['port_id'=>106,'name'=>'45A','portgroup_id'=>8,'description'=>'Oil Pressure']);
        $this->insert('ports',['port_id'=>107,'name'=>'46A','portgroup_id'=>5,'description'=>'Voltage Batt. Starter']);
        $this->insert('ports',['port_id'=>108,'name'=>'47A','portgroup_id'=>6,'description'=>'Fuel Level']);
        $this->insert('ports',['port_id'=>109,'name'=>'48A','portgroup_id'=>1,'description'=>'']);
        $this->insert('ports',['port_id'=>110,'name'=>'49A','portgroup_id'=>1,'description'=>'']);
        $this->insert('ports',['port_id'=>111,'name'=>'50A','portgroup_id'=>1,'description'=>'']);
        $this->insert('ports',['port_id'=>112,'name'=>'51A','portgroup_id'=>1,'description'=>'']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_port_portgroup', 'ports');
        $this->dropForeignKey('fk_ports_graphtype', 'ports');
        $this->dropTable('ports');
    }
}
