<?php

use yii\db\Migration;

/**
 * Handles the creation of table `widget`.
 */
class m181109_053710_create_widget_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('widget', [
            'widget_id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull()->unique(),
            'description'=> $this->string(255),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'notify'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4),   
        ]);
        
        $this->insert('widget',[
              'widget_id'=>1,
              'name'=>'Temperature',
              'description'=>'Temperature',
              'isactive'=>true  
        ]);
        
        $this->insert('widget',[
                'widget_id'=>2,
                'name'=>'Battery',
                'description'=>'Battery Status',
                'isactive'=>true
        ]);
        
        $this->insert('widget',[
                'widget_id'=>3,
                'name'=>'Electric',
                'description'=>'Electric',
                'isactive'=>true
        ]);
        
        $this->insert('widget',[
                'widget_id'=>4,
                'name'=>'Humidity',
                'description'=>'Humidity',
                'isactive'=>true
        ]);
        
        $this->insert('widget',[
                'widget_id'=>5,
                'name'=>'TotalPower',
                'description'=>'Total Power',
                'isactive'=>true
        ]);
        
        $this->insert('widget',[
                'widget_id'=>6,
                'name'=>'Voltage',
                'description'=>'Voltage',
                'isactive'=>true
        ]);
        
        $this->insert('widget',[
                'widget_id'=>7,
                'name'=>'HighTemperature',
                'description'=>'High Temperature',
                'isactive'=>true
        ]);
        
        $this->insert('widget',[
                'widget_id'=>8,
                'name'=>'OilLevel',
                'description'=>'Oil Level',
                'isactive'=>true
        ]);
        
        $this->insert('widget',[
                'widget_id'=>9,
                'name'=>'OilPressure',
                'description'=>'Oil Pressure',
                'isactive'=>true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('widget');
    }
}
