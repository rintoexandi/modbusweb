<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'name'=>$this->string(100),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);
        
        
        $this->insert('user', [
                'id' => 1,
                'username'=>'admin',
                'email' => 'admin@localhost.com',
                'name'=>'Administrator',
                'password_hash'=>\Yii::$app->getSecurity()->generatePasswordHash('admin'),
                'password_reset_token'=>\Yii::$app->getSecurity()->generatePasswordHash('admin'),
                'status'=>10,
                'auth_key'=>\Yii::$app->security->generateRandomString(),
       ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
