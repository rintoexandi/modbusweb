<?php

use yii\db\Migration;

/**
 * Handles the creation of table `widgetgroup`.
 */
class m190302_112318_create_widgetgroup_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('widgetgroup', [
            'widgetgroup_id' => $this->primaryKey(),
            'name'=>$this->string(150)->notNull(),
            'description'=>$this->string(255),
            'isactive'=>$this->boolean()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4) 
        ]);
        
        $this->insert('widgetgroup',[
              'widgetgroup_id'=>1,
              'name'=>'PLN',
              'description'=>'Temperature',
              'isactive'=>true  
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>2,
                'name'=>'Battery',
                'description'=>'Battery Status',
                'isactive'=>true
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>3,
                'name'=>'Electric',
                'description'=>'Electric',
                'isactive'=>true
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>4,
                'name'=>'Humidity',
                'description'=>'Humidity',
                'isactive'=>true
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>5,
                'name'=>'TotalPower',
                'description'=>'Total Power',
                'isactive'=>true
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>6,
                'name'=>'Voltage',
                'description'=>'Voltage',
                'isactive'=>true
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>7,
                'name'=>'HighTemperature',
                'description'=>'High Temperature',
                'isactive'=>true
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>8,
                'name'=>'OilLevel',
                'description'=>'Oil Level',
                'isactive'=>true
        ]);
        
        $this->insert('widgetgroup',[
                'widgetgroup_id'=>9,
                'name'=>'OilPressure',
                'description'=>'Oil Pressure',
                'isactive'=>true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('widgetgroup');
    }
}
