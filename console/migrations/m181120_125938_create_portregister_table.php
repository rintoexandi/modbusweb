<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portregister`.
 */
class m181120_125938_create_portregister_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('portregister', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(100)->notNull(),
            'port_id'=>$this->integer()->notNull(),
            'registeredports'=>$this->integer()->notNull(), 
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4) 
        ]);
        
        $this->addForeignKey('fk_portregister_port',
                'portregister', 'port_id', 'ports', 'port_id');    
        
        $this->createIndex('idx_portregister_register_and_port',
                'portregister',
                ['port_id','registeredports'],1);
        
        $this->insert('portregister',['id'=>1,'name'=>'PLN','port_id'=>1,'registeredports'=>0]);
        $this->insert('portregister',['id'=>2,'name'=>'Genset-1','port_id'=>2,'registeredports'=>1]);
        $this->insert('portregister',['id'=>3,'name'=>'Genset-2','port_id'=>3,'registeredports'=>2]);
        $this->insert('portregister',['id'=>4,'name'=>'Mains Fail Rectifier #1','port_id'=>4,'registeredports'=>3]);
        $this->insert('portregister',['id'=>5,'name'=>'Rectifier #1 Fault','port_id'=>5,'registeredports'=>4]);
        $this->insert('portregister',['id'=>6,'name'=>'Mains Fail Rectifier #2','port_id'=>6,'registeredports'=>5]);
        $this->insert('portregister',['id'=>7,'name'=>'Rectifier #2 Fault','port_id'=>7,'registeredports'=>6]);
        $this->insert('portregister',['id'=>8,'name'=>'','port_id'=>8,'registeredports'=>7]);
        $this->insert('portregister',['id'=>9,'name'=>'','port_id'=>9,'registeredports'=>8]);
        $this->insert('portregister',['id'=>10,'name'=>'','port_id'=>10,'registeredports'=>9]);
        $this->insert('portregister',['id'=>11,'name'=>'','port_id'=>11,'registeredports'=>10]);
        $this->insert('portregister',['id'=>12,'name'=>'','port_id'=>12,'registeredports'=>11]);
        $this->insert('portregister',['id'=>13,'name'=>'PLN & GENSET','port_id'=>13,'registeredports'=>12]);
        $this->insert('portregister',['id'=>14,'name'=>'Temperatur Room 1','port_id'=>14,'registeredports'=>13]);
        $this->insert('portregister',['id'=>15,'name'=>'Temperatur Room 2','port_id'=>15,'registeredports'=>14]);
        $this->insert('portregister',['id'=>16,'name'=>'Temperatur Room 3','port_id'=>16,'registeredports'=>15]);
        $this->insert('portregister',['id'=>17,'name'=>'Temperatur Room 4','port_id'=>17,'registeredports'=>16]);
        $this->insert('portregister',['id'=>18,'name'=>'Temperatur Room 5','port_id'=>18,'registeredports'=>17]);
        $this->insert('portregister',['id'=>19,'name'=>'Temperatur Room 6','port_id'=>19,'registeredports'=>18]);
        $this->insert('portregister',['id'=>20,'name'=>'Temperatur Room 7','port_id'=>20,'registeredports'=>19]);
        $this->insert('portregister',['id'=>21,'name'=>'Temperatur Room 8','port_id'=>21,'registeredports'=>20]);
        $this->insert('portregister',['id'=>22,'name'=>'Humidity Room 1','port_id'=>22,'registeredports'=>21]);
        $this->insert('portregister',['id'=>23,'name'=>'Humidity Room 2','port_id'=>23,'registeredports'=>22]);
        $this->insert('portregister',['id'=>24,'name'=>'Humidity Room 3','port_id'=>24,'registeredports'=>23]);
        $this->insert('portregister',['id'=>25,'name'=>'Humidity Room 4','port_id'=>25,'registeredports'=>24]);
        $this->insert('portregister',['id'=>26,'name'=>'DC Voltage Rect #1','port_id'=>26,'registeredports'=>25]);
        $this->insert('portregister',['id'=>27,'name'=>'DC Voltage Rect #2','port_id'=>27,'registeredports'=>26]);
        $this->insert('portregister',['id'=>28,'name'=>'DC Voltage Rect #3','port_id'=>28,'registeredports'=>27]);
        $this->insert('portregister',['id'=>29,'name'=>'DC Voltage Rect #4','port_id'=>29,'registeredports'=>28]);
        $this->insert('portregister',['id'=>30,'name'=>'Total Power Factor','port_id'=>30,'registeredports'=>29]);
        $this->insert('portregister',['id'=>31,'name'=>'Voltage R-N','port_id'=>31,'registeredports'=>30]);
        $this->insert('portregister',['id'=>32,'name'=>'Voltage S-N','port_id'=>32,'registeredports'=>31]);
        $this->insert('portregister',['id'=>33,'name'=>'Voltage T-N','port_id'=>33,'registeredports'=>32]);
        $this->insert('portregister',['id'=>34,'name'=>'High Temperatur DEG','port_id'=>34,'registeredports'=>33]);
        $this->insert('portregister',['id'=>35,'name'=>'Low Oil Pressure DEG','port_id'=>35,'registeredports'=>34]);
        $this->insert('portregister',['id'=>36,'name'=>'General Alarm DEG','port_id'=>36,'registeredports'=>35]);
        $this->insert('portregister',['id'=>37,'name'=>'Fuel Level DEG','port_id'=>37,'registeredports'=>36]);
        $this->insert('portregister',['id'=>38,'name'=>'','port_id'=>38,'registeredports'=>37]);
        $this->insert('portregister',['id'=>39,'name'=>'','port_id'=>39,'registeredports'=>38]);
        $this->insert('portregister',['id'=>40,'name'=>'','port_id'=>40,'registeredports'=>39]);
        $this->insert('portregister',['id'=>41,'name'=>'','port_id'=>41,'registeredports'=>40]);
        $this->insert('portregister',['id'=>42,'name'=>'','port_id'=>42,'registeredports'=>41]);
        $this->insert('portregister',['id'=>43,'name'=>'','port_id'=>43,'registeredports'=>42]);
        $this->insert('portregister',['id'=>44,'name'=>'','port_id'=>44,'registeredports'=>43]);
        $this->insert('portregister',['id'=>45,'name'=>'','port_id'=>45,'registeredports'=>44]);
        $this->insert('portregister',['id'=>46,'name'=>'','port_id'=>46,'registeredports'=>45]);
        $this->insert('portregister',['id'=>47,'name'=>'','port_id'=>47,'registeredports'=>46]);
        $this->insert('portregister',['id'=>48,'name'=>'','port_id'=>48,'registeredports'=>47]);
        $this->insert('portregister',['id'=>49,'name'=>'','port_id'=>49,'registeredports'=>48]);
        
        //Analog
        
        $this->insert('portregister',['id'=>50,'name'=>'01A','port_id'=>50,'registeredports'=>0]);
        $this->insert('portregister',['id'=>51,'name'=>'02A','port_id'=>51,'registeredports'=>1]);
        $this->insert('portregister',['id'=>52,'name'=>'03A','port_id'=>52,'registeredports'=>2]);
        $this->insert('portregister',['id'=>53,'name'=>'04A','port_id'=>53,'registeredports'=>3]);
        $this->insert('portregister',['id'=>54,'name'=>'05A','port_id'=>54,'registeredports'=>4]);
        $this->insert('portregister',['id'=>55,'name'=>'06A','port_id'=>55,'registeredports'=>5]);
        $this->insert('portregister',['id'=>56,'name'=>'07A','port_id'=>56,'registeredports'=>6]);
        $this->insert('portregister',['id'=>57,'name'=>'08A','port_id'=>57,'registeredports'=>7]);
        $this->insert('portregister',['id'=>58,'name'=>'09A','port_id'=>58,'registeredports'=>8]);
        $this->insert('portregister',['id'=>59,'name'=>'10A','port_id'=>59,'registeredports'=>9]);
        $this->insert('portregister',['id'=>60,'name'=>'11A','port_id'=>60,'registeredports'=>10]);
        $this->insert('portregister',['id'=>61,'name'=>'12A','port_id'=>61,'registeredports'=>11]);
        $this->insert('portregister',['id'=>62,'name'=>'13A','port_id'=>62,'registeredports'=>12]);
        $this->insert('portregister',['id'=>63,'name'=>'14A','port_id'=>63,'registeredports'=>13]);
        $this->insert('portregister',['id'=>64,'name'=>'15A','port_id'=>64,'registeredports'=>14]);
        $this->insert('portregister',['id'=>65,'name'=>'16A','port_id'=>65,'registeredports'=>15]);
        #PLN
        $this->insert('portregister',['id'=>66,'name'=>'17A','port_id'=>66,'registeredports'=>16]);
        $this->insert('portregister',['id'=>67,'name'=>'18A','port_id'=>66,'registeredports'=>17]);
        
        $this->insert('portregister',['id'=>68,'name'=>'18A','port_id'=>68,'registeredports'=>18]);
        $this->insert('portregister',['id'=>69,'name'=>'19A','port_id'=>69,'registeredports'=>19]);
        $this->insert('portregister',['id'=>70,'name'=>'20A','port_id'=>70,'registeredports'=>20]);
        $this->insert('portregister',['id'=>71,'name'=>'21A','port_id'=>71,'registeredports'=>21]);
        $this->insert('portregister',['id'=>72,'name'=>'22A','port_id'=>72,'registeredports'=>22]);
        $this->insert('portregister',['id'=>73,'name'=>'23A','port_id'=>73,'registeredports'=>23]);
        $this->insert('portregister',['id'=>74,'name'=>'24A','port_id'=>74,'registeredports'=>24]);
        $this->insert('portregister',['id'=>75,'name'=>'25A','port_id'=>75,'registeredports'=>25]);
        $this->insert('portregister',['id'=>76,'name'=>'26A','port_id'=>76,'registeredports'=>26]);
        $this->insert('portregister',['id'=>77,'name'=>'27A','port_id'=>77,'registeredports'=>27]);
        $this->insert('portregister',['id'=>78,'name'=>'28A','port_id'=>78,'registeredports'=>28]);
        $this->insert('portregister',['id'=>79,'name'=>'29A','port_id'=>79,'registeredports'=>29]);
        $this->insert('portregister',['id'=>80,'name'=>'30A','port_id'=>80,'registeredports'=>30]);
        $this->insert('portregister',['id'=>81,'name'=>'31A','port_id'=>81,'registeredports'=>31]);
        $this->insert('portregister',['id'=>82,'name'=>'32A','port_id'=>82,'registeredports'=>32]);
      
        $this->insert('portregister',['id'=>83,'name'=>'33A','port_id'=>83,'registeredports'=>33]);
        $this->insert('portregister',['id'=>84,'name'=>'33A','port_id'=>83,'registeredports'=>34]);
        
        $this->insert('portregister',['id'=>85,'name'=>'34A','port_id'=>85,'registeredports'=>35]);
        $this->insert('portregister',['id'=>86,'name'=>'34A','port_id'=>85,'registeredports'=>36]);
        
        $this->insert('portregister',['id'=>87,'name'=>'35A','port_id'=>87,'registeredports'=>37]);
        $this->insert('portregister',['id'=>88,'name'=>'35A','port_id'=>87,'registeredports'=>38]);
        
        $this->insert('portregister',['id'=>89,'name'=>'36A','port_id'=>89,'registeredports'=>39]); 
        $this->insert('portregister',['id'=>90,'name'=>'36A','port_id'=>89,'registeredports'=>40]);
      
        $this->insert('portregister',['id'=>91,'name'=>'37A','port_id'=>91,'registeredports'=>41]);
        $this->insert('portregister',['id'=>92,'name'=>'37A','port_id'=>91,'registeredports'=>42]);
        
        $this->insert('portregister',['id'=>93,'name'=>'38A','port_id'=>93,'registeredports'=>43]);
        $this->insert('portregister',['id'=>94,'name'=>'38A','port_id'=>93,'registeredports'=>44]);
        
        $this->insert('portregister',['id'=>95,'name'=>'39A','port_id'=>95,'registeredports'=>45]);
        $this->insert('portregister',['id'=>96,'name'=>'39A','port_id'=>95,'registeredports'=>46]);
        
        $this->insert('portregister',['id'=>97,'name'=>'40A','port_id'=>97,'registeredports'=>47]);
        $this->insert('portregister',['id'=>98,'name'=>'40A','port_id'=>97,'registeredports'=>48]);
        
        $this->insert('portregister',['id'=>99,'name'=>'41A','port_id'=>99,'registeredports'=>49]);
        $this->insert('portregister',['id'=>100,'name'=>'41A','port_id'=>99,'registeredports'=>50]);
        
        $this->insert('portregister',['id'=>101,'name'=>'42A','port_id'=>101,'registeredports'=>51]);
        $this->insert('portregister',['id'=>102,'name'=>'42A','port_id'=>101,'registeredports'=>52]);
        
        $this->insert('portregister',['id'=>103,'name'=>'43A','port_id'=>103,'registeredports'=>53]);
        $this->insert('portregister',['id'=>104,'name'=>'43A','port_id'=>103,'registeredports'=>54]);
        
        $this->insert('portregister',['id'=>105,'name'=>'44A','port_id'=>105,'registeredports'=>55]);
        $this->insert('portregister',['id'=>106,'name'=>'45A','port_id'=>106,'registeredports'=>56]);
        $this->insert('portregister',['id'=>107,'name'=>'46A','port_id'=>107,'registeredports'=>57]);
        $this->insert('portregister',['id'=>108,'name'=>'47A','port_id'=>108,'registeredports'=>58]);
        $this->insert('portregister',['id'=>109,'name'=>'48A','port_id'=>109,'registeredports'=>59]);
        $this->insert('portregister',['id'=>110,'name'=>'49A','port_id'=>110,'registeredports'=>60]);
        $this->insert('portregister',['id'=>111,'name'=>'50A','port_id'=>111,'registeredports'=>62]);
        $this->insert('portregister',['id'=>112,'name'=>'51A','port_id'=>112,'registeredports'=>63]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_portregister_port', 'portregister');       
        $this->dropTable('portregister');
    }
}
