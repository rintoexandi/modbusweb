<?php

use yii\db\Migration;

/**
 * Handles the creation of table `alert`.
 */
class m190110_137104_create_alertlog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('alertlog', [
             'id' => $this->primaryKey(),
             'devicealert_id'=>$this->integer()->notNull(),
             'message'=>$this->string(200),
             'port_value'=>$this->integer(),
             'prev_value'=>$this->integer(),       
             'alerted'=>$this->boolean(),   
             'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
             'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')                    
        ]);
        
        $this->addForeignKey('fk_devicealert_alertlog',
                'alertlog', 'devicealert_id', 'devicealert', 'devicealert_id');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_devicealert_alertlog', 'alertlog');
        $this->dropTable('alertlog');
    }
}
