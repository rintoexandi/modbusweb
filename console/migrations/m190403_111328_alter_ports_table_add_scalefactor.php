<?php

use yii\db\Migration;

/**
 * Class m190403_111328_alter_ports_table_add_scalefactor
 */
class m190403_111328_alter_ports_table_add_scalefactor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ports','scalefactor',$this->float());
        $this->update('ports', ['scalefactor'=>0.1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('ports', 'scalefactor');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190403_111328_alter_ports_table_add_scalefactor cannot be reverted.\n";

        return false;
    }
    */
}
