<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%importdevices}}`.
 */
class m190413_062649_create_importdevices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%importdevices}}', [
            'id' => $this->primaryKey(),
            'hostname'=>$this->string(),
            'ipaddress'=>$this->string(),
            'sitename'=>$this->string(),
            'witelname'=>$this->string(),
            'sitetype'=>$this->integer(),
            'message'=>$this->string(255),
            'isvalid'=>$this->boolean(),
            'processed'=>$this->boolean()->notNull(),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%importdevices}}');
    }
}
