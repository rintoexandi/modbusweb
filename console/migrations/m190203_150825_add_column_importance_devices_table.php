<?php

use yii\db\Migration;

/**
 * Class m190203_150825_add_column_importance_devices_table
 */
class m190203_150825_add_column_importance_devices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('devices', 'importance', $this->string(1)->defaultValue('L'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190203_150825_add_column_importance_devices_table cannot be reverted.\n";

        return false;
    }
    */
}
