<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deviceportsetting`.
 */
class m181121_074944_create_deviceportsetting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('deviceportsetting', [
            'id' => $this->primaryKey(),
            'deviceport_id'=>$this->integer()->notNull()->unique(),    
            'settingtype'=>$this->string(1)->notNull(),    
            'description'=> $this->string(255),
            'minvalue'=>$this->integer(),
            'maxvalue'=>$this->integer(),
            'scalefactor'=>$this->integer(),
            'isactive'=>$this->boolean()->notNull()->defaultValue(true),                
            'notify'=>$this->boolean()->notNull()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4) 
        ]);
        
        $this->addForeignKey('fk_deviceportsetting_deviceport',
                'deviceportsetting', 'deviceport_id', 'deviceport', 'deviceport_id');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_deviceportsetting_deviceport', 'deviceportsetting');
        
        $this->dropTable('deviceportsetting');
    }
}
