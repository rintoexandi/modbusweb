<?php

use yii\db\Migration;

/**
 * Handles the creation of table `widgetport`.
 */
class m190302_121955_create_widgetport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('widgetport', [
            'id' => $this->primaryKey(),
            'widget_id'=>$this->integer(),    
            'port_id'=>$this->integer()->notNull()->unique(),   
            'sequence'=>$this->integer(),
            'isactive'=>$this->boolean()->defaultValue(true),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4)
        ]);
    
        
        
        $this->addForeignKey('fk_widgetport_widget',
                'widgetport', 'widget_id', 'widget', 'widget_id');
        
        
        $this->addForeignKey('fk_widgetport_ports',
                'widgetport', 'port_id', 'ports', 'port_id');
        
        
        //Widget PLN Voltage
         $this->insert('widget',[
              'widget_id'=>11,
              'parent_id'=>3,
              'name'=>'PLN-Voltage',
              'description'=>'PLN Voltage',
              'widgetgroup_id'=>1,
              'isactive'=>true  
        ]);
         
        $this->insert('widgetport',['id'=>75,'widget_id'=>'11','port_id'=>75,'sequence'=>'1']);
        $this->insert('widgetport',['id'=>76,'widget_id'=>'11','port_id'=>76,'sequence'=>'2']);
        $this->insert('widgetport',['id'=>77,'widget_id'=>'11','port_id'=>77,'sequence'=>'3']);
        $this->insert('widgetport',['id'=>78,'widget_id'=>'11','port_id'=>78,'sequence'=>'4']);
        $this->insert('widgetport',['id'=>79,'widget_id'=>'11','port_id'=>79,'sequence'=>'5']);
        $this->insert('widgetport',['id'=>80,'widget_id'=>'11','port_id'=>80,'sequence'=>'6']);

         //Widget PLN Amper
         $this->insert('widget',[
              'widget_id'=>12,
              'parent_id'=>3,
              'name'=>'PLN-Ampere',
              'description'=>'PLN Ampere',
              'widgetgroup_id'=>1,
              'isactive'=>true  
        ]);
         
        $this->insert('widgetport',['id'=>71,'widget_id'=>'12','port_id'=>71,'sequence'=>'1']);
        $this->insert('widgetport',['id'=>72,'widget_id'=>'12','port_id'=>72,'sequence'=>'2']);
        $this->insert('widgetport',['id'=>73,'widget_id'=>'12','port_id'=>73,'sequence'=>'3']);
        $this->insert('widgetport',['id'=>74,'widget_id'=>'12','port_id'=>74,'sequence'=>'4']);
        $this->insert('widgetport',['id'=>81,'widget_id'=>'12','port_id'=>81,'sequence'=>'5']);
        $this->insert('widgetport',['id'=>82,'widget_id'=>'12','port_id'=>82,'sequence'=>'6']);
         //Widget KWH Info
        $this->insert('widget',[
              'widget_id'=>13,
             'parent_id'=>3,
              'name'=>'PLN-Kwh-Info',
              'description'=>'PLN KWH Info',
              'widgetgroup_id'=>1,
              'isactive'=>true  
        ]);
         
        $this->insert('widgetport',['id'=>66,'widget_id'=>'13','port_id'=>66,'sequence'=>'4']);
        $this->insert('widgetport',['id'=>68,'widget_id'=>'13','port_id'=>68,'sequence'=>'3']);
        $this->insert('widgetport',['id'=>69,'widget_id'=>'13','port_id'=>69,'sequence'=>'1']);
        $this->insert('widgetport',['id'=>70,'widget_id'=>'13','port_id'=>70,'sequence'=>'2']);
     
        //Genset
         $this->insert('widget',[
              'widget_id'=>14,
              'parent_id'=>5,
              'name'=>'Genset Voltage',
              'description'=>'Genset Voltage',
              'widgetgroup_id'=>3,
              'isactive'=>true  
        ]);
         
        $this->insert('widgetport',['id'=>91,'widget_id'=>'14','port_id'=>91,'sequence'=>'1']);
        $this->insert('widgetport',['id'=>93,'widget_id'=>'14','port_id'=>93,'sequence'=>'2']);
        $this->insert('widgetport',['id'=>95,'widget_id'=>'14','port_id'=>95,'sequence'=>'3']);
        $this->insert('widgetport',['id'=>97,'widget_id'=>'14','port_id'=>97,'sequence'=>'4']);
        $this->insert('widgetport',['id'=>99,'widget_id'=>'14','port_id'=>99,'sequence'=>'5']);
        $this->insert('widgetport',['id'=>101,'widget_id'=>'14','port_id'=>101,'sequence'=>'6']);

          //Genset
         $this->insert('widget',[
              'widget_id'=>15,
              'parent_id'=>5,
              'name'=>'Genset Ampere',
              'description'=>'Genset Ampere',
              'widgetgroup_id'=>3,
              'isactive'=>true  
        ]);
         
        $this->insert('widgetport',['id'=>85,'widget_id'=>'15','port_id'=>85,'sequence'=>'1']);
        $this->insert('widgetport',['id'=>87,'widget_id'=>'15','port_id'=>87,'sequence'=>'2']);
        $this->insert('widgetport',['id'=>89,'widget_id'=>'15','port_id'=>89,'sequence'=>'3']);
           //Genset
         $this->insert('widget',[
              'widget_id'=>16,
              'parent_id'=>5,
              'name'=>'Genset Info',
              'description'=>'Genset Info',
              'widgetgroup_id'=>3,
              'isactive'=>true  
        ]);
         $this->insert('widgetport',['id'=>103,'widget_id'=>'16','port_id'=>103,'sequence'=>'2']);
         $this->insert('widgetport',['id'=>83,'widget_id'=>'16','port_id'=>83,'sequence'=>'1']);
         $this->insert('widgetport',['id'=>107,'widget_id'=>'16','port_id'=>107,'sequence'=>'3']);
         $this->insert('widgetport',['id'=>108,'widget_id'=>'16','port_id'=>108,'sequence'=>'4']);
         
         
         //Temperature, Wiget_id = 1
        $this->insert('widgetport',['id'=>50,'widget_id'=>'1','port_id'=>50,'sequence'=>'1']);
        $this->insert('widgetport',['id'=>51,'widget_id'=>'1','port_id'=>51,'sequence'=>'2']);
        $this->insert('widgetport',['id'=>52,'widget_id'=>'1','port_id'=>52,'sequence'=>'3']);
        $this->insert('widgetport',['id'=>53,'widget_id'=>'1','port_id'=>53,'sequence'=>'4']);
    
        //Battery/Rectifier Widget_id=2
        $this->insert('widgetport',['id'=>62,'widget_id'=>'2','port_id'=>62,'sequence'=>'1']);
        $this->insert('widgetport',['id'=>63,'widget_id'=>'2','port_id'=>63,'sequence'=>'2']);
        $this->insert('widgetport',['id'=>64,'widget_id'=>'2','port_id'=>64,'sequence'=>'3']);
        $this->insert('widgetport',['id'=>65,'widget_id'=>'2','port_id'=>65,'sequence'=>'4']);
        
        
    }

    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('widgetport');
    }
}
