<?php

use yii\db\Migration;

/**
 * Handles the creation of table `devices`.
 */
class m181106_062212_create_devices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('devices', [
            'device_id' => $this->primaryKey(),
            'hostname'=>$this->string()->notNull()->unique(),
            'ipaddress'=>$this->string()->notNull()->unique(),
            'port'=>$this->smallInteger()->notNull()->defaultValue(502),
            'hardware'=>$this->string(50),
            'status'=>$this->boolean()->defaultValue(false),
            'status_reason'=>$this->string(50),
            'prev_status'=>$this->boolean()->defaultValue(false),
            'prev_status_reason'=>$this->string(50),
            'disabled'=>$this->boolean()->defaultValue(false),
            'uptime'=>$this->integer(),
            'agent_uptime'=>$this->integer(),
            'last_polled'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'last_poll_attempted'=>$this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'last_polled_timetaken'=>$this->integer(),         
            'serial'=>$this->string(50),
            'site_id'=>$this->integer(4),
            'latitude'=>$this->decimal(),
            'longitude'=>$this->decimal(),
            'tahundeployment'=>$this->integer(),
            'created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'createdby' => $this->integer(4),
            'updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updateby' => $this->integer(4)
        ]);
        
        $this->addForeignKey(
                'fk-device-site-id',
                'devices',
                'site_id',
                'site',
                'site_id',
                'RESTRICT'
                );
      
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //Drop ForeignKey
        $this->dropForeignKey(
                'fk-device-site-id',
                'devices'
                );
        
        $this->dropTable('devices');
    }
}
