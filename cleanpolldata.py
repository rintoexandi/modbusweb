try:

    import json
    import os
    import io
    import Queue
    import subprocess
    import sys
    import threading
    import time
    import logging
    import ConfigParser
    import MySQLdb
    import socket
except:
    print "ERROR: missing one or more of the following python modules:"
    print "threading, Queue, sys, subprocess, time, os, json"
    sys.exit(2)

try:
    import MySQLdb
except:
    print "ERROR: missing the mysql python module:"
    print "On ubuntu: apt-get install python-mysqldb"
    print "On FreeBSD: cd /usr/ports/*/py-MySQLdb && make install clean"
    sys.exit(2)

#Test Config File
ob_install_dir = os.path.dirname(os.path.realpath(__file__))
config_file = ob_install_dir + '/modbus.cfg'
yii_path= ob_install_dir + '/yii cleanpolldata'

try:
    with open(config_file) as f:
        pass
except IOError as e:
    print "ERROR: Oh dear... %s does not seem readable" % config_file
    sys.exit(2)

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    #Set mysql param
    db_server = config.get("DEFAULT","database_server")
    db_dbname = config.get("DEFAULT","database_dbname")
    db_username = config.get("DEFAULT","database_username")
    db_password = config.get("DEFAULT","database_password")
    db_port = int(config.get("DEFAULT","database_port"))
except ConfigParser.NoOptionError:
    print "ERROR: Options config is not available"
    sys.exit(2)

def db_open():
    try:
        if (db_port==3306):
            db = MySQLdb.connect(host=db_server, user=db_username, passwd=db_password, db=db_dbname)
        else :
            db = MySQLdb.connect(host=db_server,port=db_port, user=db_username, passwd=db_password, db=db_dbname)
        return db
    except ( MySQLdb.Error, MySQLdb.Warning) as err:
        print  err
        sys.exit(2)


#Delete Polldata

query="DELETE FROM polldata WHERE processed='1'"
db = db_open()
cursor = db.cursor()
cursor.execute(query)
db.commit()
db.close()

#Delete Poll
query="DELETE FROM poll WHERE processed='1'"
db = db_open()
cursor = db.cursor()
cursor.execute(query)
db.commit()
db.close()
