try:

    import json
    import os
    import io
    import Queue
    import subprocess
    import sys
    import threading
    import time
    import logging
    import ConfigParser
    import MySQLdb
    import socket
    import memcache
    import uuid
except:
    print "ERROR: missing one or more of the following python modules:"
    print "threading, Queue, sys, subprocess, time, os, json"
    sys.exit(2)

try:
    import MySQLdb
except:
    print "ERROR: missing the mysql python module:"
    print "On ubuntu: apt-get install python-mysqldb"
    print "On FreeBSD: cd /usr/ports/*/py-MySQLdb && make install clean"
    sys.exit(2)

#Test Config File
ob_install_dir = os.path.dirname(os.path.realpath(__file__))
config_file = ob_install_dir + '/modbus.cfg'
socket_timeout=2
socket_port=502
s_time = time.time()
real_duration = 0
per_device_duration = {}
polled_devices = 0
total_down=0
total_up=0

poller_path= ob_install_dir + '/yii processpoll'
devices_list = []

daemon_name="PROCESSPOLLER"

try:
    with open(config_file) as f:
        pass
except IOError as e:
    print "ERROR: Oh dear... %s does not seem readable" % config_file
    sys.exit(2)

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)

    #Set mysql param
    db_server = config.get("DEFAULT","database_server")
    db_dbname = config.get("DEFAULT","database_dbname")
    db_username = config.get("DEFAULT","database_username")
    db_password = config.get("DEFAULT","database_password")
    db_port = int(config.get("DEFAULT","database_port"))
    max_number_thread=int(config.get("DEFAULT","max_number_thread"))
    memcached_host=config.get("DEFAULT","memcached_host")
    memcached_port=config.get("DEFAULT","memcached_port")
    memcached_key_timeout=int(config.get("DEFAULT","memcached_key_timeout"))
    
except ConfigParser.NoOptionError:
    print "ERROR: Options config is not available"
    sys.exit(2)


#set key for process identifier
memcache_key="processpoller"
strkey = "c30fde1b"

def memc_alive():
    try:
        global memc
        key = str(uuid.uuid4())
        memc.set('process.ping.' + key, key, 60)
        if memc.get('process.ping.' + key) == key:
            memc.delete('process.ping.' + key)          
            return True
        else:
            return False
    except:
        return False



try :
    
    memc = memcache.Client([memcached_host +':'+str(memcached_port)], debug=0)
    
    if memc_alive():
        chk_memc = memc.get(memcache_key)
        if chk_memc == strkey :
            print "Another process poll  is running, waiting for another call "
            sys.exit(2)
        else :
            memc.set(memcache_key,strkey,int(memcached_key_timeout))


    else :
        print "Could not connect to memcached, I am outta here ..."
        sys.exit(2)
except SystemExit:
    raise


def db_open():
    try:
        if (db_port==3306):
            db = MySQLdb.connect(host=db_server, user=db_username, passwd=db_password, db=db_dbname)
        else :
            db = MySQLdb.connect(host=db_server,port=db_port, user=db_username, passwd=db_password, db=db_dbname)
        return db
    except ( MySQLdb.Error, MySQLdb.Warning) as err:
        print  err
        sys.exit(2)

#Update Daemon Status
def update_daemon_status(status,message):
    db = db_open()
    cursor = db.cursor()
    
    status_message = MySQLdb.escape_string(message)

    if (status==True) :
        query = "UPDATE daemon set last_started=NOW(),last_finished=NOW(),message='%s',status=1 where name='%s'" % (status_message,daemon_name)
    else:
        query = "UPDATE daemon set last_finished=NOW(),message='%s',status=0 where name='%s'" % (status_message,daemon_name)

    
    response = cursor.execute(query)
    if response == 1:
        db.commit()
    db.close()
    return response

#Update Uptime
def check_daemon_status():
    db = db_open()
    cursor = db.cursor()
    query = "SELECT isactive FROM daemon WHERE name='%s'" % (daemon_name)
    cursor.execute(query)
    row = cursor.fetchone()
    isactive=row[0]
    db.close()
    return isactive

#Worker for printing Result
def print_process():
    nodeso = 0  
    while True:
        global real_duration
        global polled_devices
        
        device_id, elapsed_time = print_queue.get()
       
        #name = threading.currentThread().getName()
        #print "PRINT WORKER   " + worker_id

        real_duration += elapsed_time
        polled_devices += 1

        if elapsed_time < 300:
            print "INFO: Device  %s finished  in %s seconds " % (device_id, elapsed_time)
        else:
            print "WARNING: Device  %s finished  in %s seconds" % (device_id, elapsed_time)
        
        print_queue.task_done()
       
#Worker for polling data
def process_polldata(q):
    while True:
        name = threading.currentThread().getName()
        device_id = q.get()
       
        try:           
            #put to memcache
            memc_touch(dev,120)

            start_time = time.time()

            command = "/usr/bin/env php %s %s" % (poller_path, device_id) #>> /dev/null 2>&1
            subprocess.check_call(command, shell=True)
        
            elapsed_time = int(time.time() - start_time)
        
            #print_queue.put([threading.current_thread().name, device_id, elapsed_time])
            print_queue.put([device_id, elapsed_time])

        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            pass

        poll_queue.task_done()
    

#Memcache
def memc_touch(key, time):
    try:
        global memc
        val = memc.get(key)
        #print "Val on memcached " + str(val)
        if val != key :
            memc.set(key, key, time)
    except:
        pass

#Main Thread
if __name__ == '__main__':

   #Check if this daemon is active
    if (check_daemon_status()!=1):
        print("ERROR : Arghh ..., my superior command me to paused, i'm stuck here!! ")        
        sys.exit(2)
        
    message = "INFO: Starting the processing data at %s with %s threads" % (time.strftime("%Y-%m-%d %H:%M:%S"),max_number_thread)
    print message
    update_daemon_status(True,message)

    #get Devices List from Database
    query = "select device_id from devices where disabled = 0 order by last_polled"

    db = db_open()
    cursor = db.cursor()
    cursor.execute(query)
    devices = cursor.fetchall()

    for row in devices:
        devices_list.append(row[0])
    db.close()

    poll_queue = Queue.Queue()
    print_queue = Queue.Queue()


    for i in range(max_number_thread):
        worker = threading.Thread(name=i,target=process_polldata, args=(poll_queue,))
        worker.setDaemon(True)
        worker.start()

    #Load devices and put to queue
    for dev in devices_list :
        poll_queue.put(dev)
    
    p = threading.Thread(target=print_process)
    p.setDaemon(True)
    p.start()

    try:
        poll_queue.join()
        print_queue.join()
    except (KeyboardInterrupt, SystemExit):
        raise

    total_time = int(time.time() - s_time)

    #Remove memcache
    memc.delete(memcache_key)


    message = "INFO: process-poll-data processed %s devices in %s seconds with %s workers finished at %s" % (polled_devices, total_time, max_number_thread,time.strftime("%Y-%m-%d %H:%M:%S"))
    print message
    update_daemon_status(False,message)
