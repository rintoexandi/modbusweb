-- MySQL dump 10.13  Distrib 5.7.19, for macos10.12 (x86_64)
--
-- Host: localhost    Database: modbus
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alertlog`
--

DROP TABLE IF EXISTS `alertlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alertlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `devicealert_id` int(11) NOT NULL,
  `message` text,
  `port_value` bigint(11) DEFAULT NULL,
  `prev_value` bigint(11) DEFAULT NULL,
  `alerted` tinyint(1) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `poll_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_devicealert_alertlog` (`devicealert_id`),
  CONSTRAINT `fk_devicealert_alertlog` FOREIGN KEY (`devicealert_id`) REFERENCES `devicealert` (`devicealert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alertlog`
--

LOCK TABLES `alertlog` WRITE;
/*!40000 ALTER TABLE `alertlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `alertlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alertrules`
--

DROP TABLE IF EXISTS `alertrules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alertrules` (
  `alertrules_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `isdigital` tinyint(1) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '1',
  `isactive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`alertrules_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alertrules`
--

LOCK TABLES `alertrules` WRITE;
/*!40000 ALTER TABLE `alertrules` DISABLE KEYS */;
INSERT INTO `alertrules` VALUES (1,'Default Digital',1,1,1),(3,'Room Temperature',0,1,1),(4,'Electricity',0,1,1),(5,'Humidity',0,1,1),(6,'Voltage',0,1,1);
/*!40000 ALTER TABLE `alertrules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alertrulesdetails`
--

DROP TABLE IF EXISTS `alertrulesdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alertrulesdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alertrules_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `lowervalue` float DEFAULT NULL,
  `uppervalue` float DEFAULT NULL,
  `useboth` tinyint(1) NOT NULL DEFAULT '0',
  `useduration` tinyint(1) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `useoccurence` tinyint(1) NOT NULL,
  `occurance` int(11) DEFAULT NULL,
  `severity` varchar(1) NOT NULL DEFAULT 'O',
  `isactive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_alertrulesdetail_alertrules` (`alertrules_id`),
  CONSTRAINT `fk_alertrulesdetail_alertrules` FOREIGN KEY (`alertrules_id`) REFERENCES `alertrules` (`alertrules_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alertrulesdetails`
--

LOCK TABLES `alertrulesdetails` WRITE;
/*!40000 ALTER TABLE `alertrulesdetails` DISABLE KEYS */;
INSERT INTO `alertrulesdetails` VALUES (1,1,1,0,0,0,1,5,0,NULL,'W',1),(3,3,1,0,22,0,1,15,0,NULL,'O',1),(4,3,2,23,24,0,1,15,0,NULL,'I',1),(5,3,3,26,28,0,1,15,0,NULL,'W',1),(6,3,4,29,40,0,1,15,0,NULL,'C',1),(7,4,1,0,179,0,1,15,0,NULL,'C',1),(8,4,2,180,224,0,1,15,0,NULL,'O',1),(9,4,3,225,280,0,1,15,0,NULL,'I',1),(10,5,1,0,500,0,1,15,0,NULL,'O',1),(11,6,1,0,20,0,1,5,0,NULL,'C',1),(12,6,2,21,40,0,1,5,0,NULL,'O',1);
/*!40000 ALTER TABLE `alertrulesdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin','1','2019-01-15 09:36:32');
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/*',2,'-',NULL,NULL,'2019-01-15 09:36:32','2019-01-15 09:36:32'),('/site/*',2,'-',NULL,NULL,'2019-01-15 09:36:32','2019-01-15 09:36:32'),('admin',1,'Hak Akses Admin',NULL,NULL,'2019-01-15 09:36:32','2019-01-15 09:36:32'),('default',1,'Hak Akses Default',NULL,NULL,'2019-01-15 09:36:32','2019-01-15 09:36:32');
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('admin','/*'),('default','/site/*');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devicealert`
--

DROP TABLE IF EXISTS `devicealert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devicealert` (
  `devicealert_id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceport_id` int(11) NOT NULL,
  `open` tinyint(1) DEFAULT NULL,
  `severity` varchar(1) NOT NULL,
  `alerted` tinyint(1) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`devicealert_id`),
  KEY `fk_devicealert_deviceport` (`deviceport_id`),
  CONSTRAINT `fk_devicealert_deviceport` FOREIGN KEY (`deviceport_id`) REFERENCES `deviceport` (`deviceport_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devicealert`
--

LOCK TABLES `devicealert` WRITE;
/*!40000 ALTER TABLE `devicealert` DISABLE KEYS */;
/*!40000 ALTER TABLE `devicealert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deviceport`
--

DROP TABLE IF EXISTS `deviceport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deviceport` (
  `deviceport_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `value` float DEFAULT NULL,
  `value_inpercent` decimal(10,0) DEFAULT NULL,
  `value_prev` float DEFAULT NULL,
  `port_status` tinyint(1) DEFAULT NULL,
  `port_status_prev` tinyint(1) DEFAULT NULL,
  `last_uptime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `port_uptime` int(11) DEFAULT NULL,
  `poll_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `poll_prev` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  `configtype` varchar(1) DEFAULT 'G',
  `alertrules_id` int(11) DEFAULT NULL,
  `reportalert` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`deviceport_id`),
  UNIQUE KEY `idx_deviceport_device_and_port` (`device_id`,`port_id`),
  KEY `fk_deviceport_ports` (`port_id`),
  KEY `fk_deviceport_alertrules` (`alertrules_id`),
  CONSTRAINT `fk_deviceport_alertrules` FOREIGN KEY (`alertrules_id`) REFERENCES `alertrules` (`alertrules_id`),
  CONSTRAINT `fk_deviceport_devices` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`),
  CONSTRAINT `fk_deviceport_ports` FOREIGN KEY (`port_id`) REFERENCES `ports` (`port_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deviceport`
--

LOCK TABLES `deviceport` WRITE;
/*!40000 ALTER TABLE `deviceport` DISABLE KEYS */;
/*!40000 ALTER TABLE `deviceport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deviceporthistory`
--

DROP TABLE IF EXISTS `deviceporthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deviceporthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceport_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `value` float DEFAULT NULL,
  `value_prev` float DEFAULT NULL,
  `poll_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_deviceporthistory_ports` (`port_id`),
  KEY `fk_deviceporthistory_devices` (`device_id`),
  CONSTRAINT `fk_deviceporthistory_devices` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`),
  CONSTRAINT `fk_deviceporthistory_ports` FOREIGN KEY (`port_id`) REFERENCES `ports` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deviceporthistory`
--

LOCK TABLES `deviceporthistory` WRITE;
/*!40000 ALTER TABLE `deviceporthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `deviceporthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deviceportsetting`
--

DROP TABLE IF EXISTS `deviceportsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deviceportsetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceport_id` int(11) NOT NULL,
  `settingtype` varchar(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `minvalue` float DEFAULT NULL,
  `maxvalue` float DEFAULT NULL,
  `scalefactor` int(11) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `notify` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  `noticevalue` float DEFAULT NULL,
  `warningvalue` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `deviceport_id` (`deviceport_id`),
  CONSTRAINT `fk_deviceportsetting_deviceport` FOREIGN KEY (`deviceport_id`) REFERENCES `deviceport` (`deviceport_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deviceportsetting`
--

LOCK TABLES `deviceportsetting` WRITE;
/*!40000 ALTER TABLE `deviceportsetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `deviceportsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `port` smallint(6) NOT NULL DEFAULT '502',
  `hardware` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `status_reason` varchar(50) DEFAULT NULL,
  `prev_status` tinyint(1) DEFAULT '1',
  `last_status_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prev_status_reason` varchar(50) DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT '0',
  `uptime` int(11) DEFAULT NULL,
  `agent_uptime` int(11) DEFAULT NULL,
  `last_polled` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_poll_attempted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_polled_timetaken` int(11) DEFAULT NULL,
  `serial` varchar(50) DEFAULT NULL,
  `site_id` int(4) DEFAULT NULL,
  `latitude` decimal(10,0) DEFAULT NULL,
  `longitude` decimal(10,0) DEFAULT NULL,
  `tahundeployment` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  `sitetype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_id`),
  UNIQUE KEY `hostname` (`hostname`),
  UNIQUE KEY `ipaddress` (`ipaddress`),
  KEY `fk-device-site-id` (`site_id`),
  KEY `fk_devices_site_type` (`sitetype_id`),
  CONSTRAINT `fk-device-site-id` FOREIGN KEY (`site_id`) REFERENCES `site` (`site_id`),
  CONSTRAINT `fk_devices_site_type` FOREIGN KEY (`sitetype_id`) REFERENCES `sitetype` (`sitetype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graphtype`
--

DROP TABLE IF EXISTS `graphtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graphtype` (
  `graphtype_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  PRIMARY KEY (`graphtype_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graphtype`
--

LOCK TABLES `graphtype` WRITE;
/*!40000 ALTER TABLE `graphtype` DISABLE KEYS */;
INSERT INTO `graphtype` VALUES (1,'NONE','No Graph',1,'2019-01-15 09:36:37',NULL,'2019-01-15 09:36:37',NULL),(2,'LINE',NULL,1,'2019-01-15 09:36:37',NULL,'2019-01-15 09:36:37',NULL),(3,'KNOB',NULL,1,'2019-01-15 09:36:37',NULL,'2019-01-15 09:36:37',NULL),(4,'BAR',NULL,1,'2019-01-15 09:36:37',NULL,'2019-01-15 09:36:37',NULL),(5,'PIE',NULL,1,'2019-01-15 09:36:37',NULL,'2019-01-15 09:36:37',NULL);
/*!40000 ALTER TABLE `graphtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1547544985),('m130524_201442_init',1547544991),('m140506_102106_rbac_init',1547544993),('m181106_053738_create_site_table',1547544993),('m181106_062212_create_devices_table',1547544994),('m181106_094410_create_poll_table',1547544995),('m181107_091346_create_polldata_table',1547544997),('m181109_052026_create_graphtype_table',1547544997),('m181109_052853_create_pollreport_table',1547544998),('m181109_052902_create_pollreportdetail_table',1547544998),('m181109_053349_create_portgroup_table',1547545001),('m181109_053403_create_ports_table',1547545007),('m181109_053710_create_widget_table',1547545008),('m181109_054218_create_portrelation_table',1547545012),('m181109_062024_create_deviceport_table',1547545015),('m181110_104043_create_settings_table',1547545015),('m181120_125938_create_portregister_table',1547545022),('m181121_074944_create_deviceportsetting_table',1547545023),('m181130_170440_create_notification_table',1547545023),('m181204_013817_create_deviceporthistory_table',1547545026),('m190110_124619_create_devicealert_table',1547545027),('m190110_137104_create_alertlog_table',1547545028),('m190123_062431_add_last_status_changed_column_to_devices_table',1548228261),('m190123_062755_alter_status_column_on_devices_table',1548762990),('m190125_144514_alter_value_colum_deviceport_table',1548763030),('m190130_121557_alter_devicealert_table',1548908551),('m190131_014526_create_alertrules_table',1548909099),('m190131_035737_create_alertrulesdetails_table',1548909100),('m190203_081458_alter_table_alertlog',1549181823),('m190203_085541_alter_alerlog_field_message',1549184234),('m190203_150825_add_column_importance_devices_table',1549206597),('m190205_090240_alter_rules_alert',1551528681),('m190302_112318_create_widgetgroup_table',1551700324),('m190302_112440_alter_widget_table',1551700328),('m190302_121955_create_widgetport_table',1551700386),('m190302_144315_add_fk_widget_group',1551700550),('m190304_112711_alter_portrelation_table',1551700553),('m190306_010154_alter_table_widget_add_sequence',1551834242),('m190313_030413_create_telkomarea_table',1553073364),('m190313_030502_create_telkomwitel_table',1553073364),('m190320_090848_alter_table_site',1553080877),('m190320_111140_create_sitetype_table',1553080880),('m190320_111738_alter_table_site2',1553080883),('m190324_093606_alter_table_deviceport_add_checkalert',1553420467),('m190403_033821_create_uom_table',1554263094),('m190403_034541_alter_ports_add_uom_field',1554263557),('m190403_092612_alter_table_ports_add_group_and_alertrules',1554283691),('m190403_111328_alter_ports_table_add_scalefactor',1554290189),('m190404_021546_alter_devices_add_site_type',1554344550);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `read` tinyint(1) DEFAULT '0',
  `importance` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `deviceport_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poll`
--

DROP TABLE IF EXISTS `poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll` (
  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `success` tinyint(1) DEFAULT '1',
  `processed` tinyint(1) DEFAULT '0',
  `poll_attempted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `poll_prevattempted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`poll_id`),
  KEY `fk-poll-device-id` (`device_id`),
  CONSTRAINT `fk-poll-device-id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=772 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll`
--

LOCK TABLES `poll` WRITE;
/*!40000 ALTER TABLE `poll` DISABLE KEYS */;
/*!40000 ALTER TABLE `poll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polldata`
--

DROP TABLE IF EXISTS `polldata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polldata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `isdigital` tinyint(1) DEFAULT '1',
  `data` text,
  `data_length` int(4) DEFAULT NULL,
  `processed` tinyint(1) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk-poll-data-id` (`poll_id`),
  CONSTRAINT `fk-poll-data-id` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`poll_id`)
) ENGINE=InnoDB AUTO_INCREMENT=725 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polldata`
--

LOCK TABLES `polldata` WRITE;
/*!40000 ALTER TABLE `polldata` DISABLE KEYS */;
/*!40000 ALTER TABLE `polldata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollreport`
--

DROP TABLE IF EXISTS `pollreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollreport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollreport`
--

LOCK TABLES `pollreport` WRITE;
/*!40000 ALTER TABLE `pollreport` DISABLE KEYS */;
/*!40000 ALTER TABLE `pollreport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollreportdetail`
--

DROP TABLE IF EXISTS `pollreportdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollreportdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollreportdetail`
--

LOCK TABLES `pollreportdetail` WRITE;
/*!40000 ALTER TABLE `pollreportdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `pollreportdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portgroup`
--

DROP TABLE IF EXISTS `portgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portgroup` (
  `portgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isdigital` tinyint(1) NOT NULL DEFAULT '0',
  `settingtype` varchar(1) NOT NULL DEFAULT 'R',
  `minvalue` float DEFAULT NULL,
  `maxvalue` float DEFAULT NULL,
  `noticevalue` int(11) DEFAULT NULL,
  `warningvalue` float DEFAULT NULL,
  `errorvalue` float DEFAULT NULL,
  `scalefactor` float DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `notify` tinyint(1) NOT NULL DEFAULT '1',
  `unit` varchar(5) DEFAULT NULL,
  `usegraph` tinyint(1) NOT NULL DEFAULT '1',
  `graphtype` varchar(1) NOT NULL DEFAULT 'N',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  `alertrules_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`portgroup_id`),
  KEY `fk_portgroup_alertrules` (`alertrules_id`),
  CONSTRAINT `fk_portgroup_alertrules` FOREIGN KEY (`alertrules_id`) REFERENCES `alertrules` (`alertrules_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portgroup`
--

LOCK TABLES `portgroup` WRITE;
/*!40000 ALTER TABLE `portgroup` DISABLE KEYS */;
INSERT INTO `portgroup` VALUES (1,'Default','Default Range',0,'R',0,999999,999999,999999,999999,0.1,1,1,'',1,'C','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,4),(2,'Electricity','Default Range',0,'R',0,999,180,240,0,0.1,1,1,'Volt',1,'V','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,2),(3,'Temperature','Default Range',0,'R',-50,150,28,30,45,0.1,1,1,'C',1,'T','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,1),(4,'Humidity','Default Range',0,'R',-50,150,28,30,45,0.1,1,1,NULL,1,'G','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,NULL),(5,'Voltage','Voltage Range',0,'R',0,250,28,30,45,0.1,1,1,'',1,'G','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,2),(6,'Fuel Level','Fuel Level Range',0,'R',0,999999,999999,999999,999999,0.1,1,1,NULL,1,'G','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,NULL),(7,'Genset Watts','Genset Watts Range',0,'R',-999999,999999,999999,999999,999999,0.1,1,1,NULL,1,'G','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,NULL),(8,'Oil Pressure','Oil Pressure Range',0,'R',0,10000,10000,10000,10000,0.1,1,1,NULL,1,'G','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,NULL),(9,'Default Digital','Default Digital',1,'T',0,99,99,99,99,0.1,1,1,NULL,0,'N','2019-01-15 09:36:41',NULL,'2019-01-15 09:36:41',NULL,1);
/*!40000 ALTER TABLE `portgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portregister`
--

DROP TABLE IF EXISTS `portregister`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portregister` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `port_id` int(11) NOT NULL,
  `registeredports` int(11) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  `memory` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_portregister_register_and_port` (`port_id`,`registeredports`),
  CONSTRAINT `fk_portregister_port` FOREIGN KEY (`port_id`) REFERENCES `ports` (`port_id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portregister`
--

LOCK TABLES `portregister` WRITE;
/*!40000 ALTER TABLE `portregister` DISABLE KEYS */;
INSERT INTO `portregister` VALUES (1,'PLN',1,0,1,'2019-01-15 09:36:57',NULL,'2019-01-15 09:36:57',NULL,'%M0'),(2,'Genset-1',2,1,1,'2019-01-15 09:36:57',NULL,'2019-01-15 09:36:57',NULL,'%M1'),(3,'Genset-2',3,2,1,'2019-01-15 09:36:57',NULL,'2019-01-15 09:36:57',NULL,'%M2'),(4,'Mains Fail Rectifier #1',4,3,1,'2019-01-15 09:36:57',NULL,'2019-01-15 09:36:57',NULL,'%M3'),(5,'Rectifier #1 Fault',5,4,1,'2019-01-15 09:36:57',NULL,'2019-01-15 09:36:57',NULL,'%M4'),(6,'Mains Fail Rectifier #2',6,5,1,'2019-01-15 09:36:57',NULL,'2019-01-15 09:36:57',NULL,'%M5'),(7,'Rectifier #2 Fault',7,6,1,'2019-01-15 09:36:57',NULL,'2019-01-15 09:36:57',NULL,'%M6'),(8,'',8,7,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M7'),(9,'',9,8,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M8'),(10,'',10,9,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M9'),(11,'',11,10,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M10'),(12,'',12,11,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M11'),(13,'PLN & GENSET',13,12,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M12'),(14,'Temperatur Room 1',14,13,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M13'),(15,'Temperatur Room 2',15,14,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M14'),(16,'Temperatur Room 3',16,15,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M15'),(17,'Temperatur Room 4',17,16,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M16'),(18,'Temperatur Room 5',18,17,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M17'),(19,'Temperatur Room 6',19,18,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M18'),(20,'Temperatur Room 7',20,19,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M19'),(21,'Temperatur Room 8',21,20,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M20'),(22,'Humidity Room 1',22,21,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M21'),(23,'Humidity Room 2',23,22,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M22'),(24,'Humidity Room 3',24,23,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M23'),(25,'Humidity Room 4',25,24,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M24'),(26,'DC Voltage Rect #1',26,25,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M25'),(27,'DC Voltage Rect #2',27,26,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M26'),(28,'DC Voltage Rect #3',28,27,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M27'),(29,'DC Voltage Rect #4',29,28,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M28'),(30,'Total Power Factor',30,29,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M29'),(31,'Voltage R-N',31,30,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M30'),(32,'Voltage S-N',32,31,1,'2019-01-15 09:36:58',NULL,'2019-01-15 09:36:58',NULL,'%M31'),(33,'Voltage T-N',33,32,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M32'),(34,'High Temperatur DEG',34,33,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M33'),(35,'Low Oil Pressure DEG',35,34,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M34'),(36,'General Alarm DEG',36,35,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M35'),(37,'Fuel Level DEG',37,36,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M36'),(38,'',38,37,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M37'),(39,'',39,38,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M38'),(40,'',40,39,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M39'),(41,'',41,40,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M40'),(42,'',42,41,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M41'),(43,'',43,42,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M42'),(44,'',44,43,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M43'),(45,'',45,44,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M44'),(46,'',46,45,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M45'),(47,'',47,46,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M46'),(48,'',48,47,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M47'),(49,'',49,48,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'%M48'),(50,'01A',50,0,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW0'),(51,'02A',51,1,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW1'),(52,'03A',52,2,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW2'),(53,'04A',53,3,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW3'),(54,'05A',54,4,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW4'),(55,'06A',55,5,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW5'),(56,'07A',56,6,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW6'),(57,'08A',57,7,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW7'),(58,'09A',58,8,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW8'),(59,'10A',59,9,1,'2019-01-15 09:36:59',NULL,'2019-01-15 09:36:59',NULL,'MW9'),(60,'11A',60,10,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW10'),(61,'12A',61,11,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW11'),(62,'13A',62,12,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW12'),(63,'14A',63,13,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW13'),(64,'15A',64,14,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW14'),(65,'16A',65,15,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW15'),(66,'17A',66,16,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW16'),(67,'18A',66,17,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW16'),(68,'18A',68,18,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW18'),(69,'19A',69,19,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW19'),(70,'20A',70,20,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW20'),(71,'21A',71,21,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW21'),(72,'22A',72,22,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW22'),(73,'23A',73,23,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW23'),(74,'24A',74,24,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW24'),(75,'25A',75,25,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW25'),(76,'26A',76,26,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW26'),(77,'27A',77,27,1,'2019-01-15 09:37:00',NULL,'2019-01-15 09:37:00',NULL,'MW27'),(78,'28A',78,28,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW28'),(79,'29A',79,29,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW29'),(80,'30A',80,30,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW30'),(81,'31A',81,31,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW31'),(82,'32A',82,32,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW32'),(83,'33A',83,33,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW33'),(84,'33A',83,34,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW33'),(85,'34A',85,35,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW35'),(86,'34A',85,36,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW35'),(87,'35A',87,37,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW37'),(88,'35A',87,38,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW37'),(89,'36A',89,39,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW39'),(90,'36A',89,40,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW39'),(91,'37A',91,41,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW41'),(92,'37A',91,42,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW41'),(93,'38A',93,43,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW43'),(94,'38A',93,44,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW43'),(95,'39A',95,45,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW45'),(96,'39A',95,46,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW45'),(97,'40A',97,47,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW47'),(98,'40A',97,48,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW47'),(99,'41A',99,49,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW49'),(100,'41A',99,50,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW49'),(101,'42A',101,51,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW51'),(102,'42A',101,52,1,'2019-01-15 09:37:01',NULL,'2019-01-15 09:37:01',NULL,'MW51'),(103,'43A',103,53,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW53'),(104,'43A',103,54,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW53'),(105,'44A',105,55,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW55'),(106,'45A',106,56,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW56'),(107,'46A',107,57,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW57'),(108,'47A',108,58,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW58'),(109,'48A',109,59,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW59'),(110,'49A',110,60,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW60'),(111,'50A',111,62,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW61'),(112,'51A',112,63,1,'2019-01-15 09:37:02',NULL,'2019-01-15 09:37:02',NULL,'MW62');
/*!40000 ALTER TABLE `portregister` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portrelation`
--

DROP TABLE IF EXISTS `portrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portrelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) DEFAULT NULL,
  `port_id` int(11) NOT NULL,
  `portanalog_id` int(11) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `port_id` (`port_id`),
  KEY `fk_portrelation_widget` (`widget_id`),
  KEY `fk_portrelation_ports_analog` (`portanalog_id`),
  CONSTRAINT `fk_portrelation_ports_analog` FOREIGN KEY (`portanalog_id`) REFERENCES `ports` (`port_id`),
  CONSTRAINT `fk_portrelation_ports_digital` FOREIGN KEY (`port_id`) REFERENCES `ports` (`port_id`),
  CONSTRAINT `fk_portrelation_widget` FOREIGN KEY (`widget_id`) REFERENCES `widget` (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portrelation`
--

LOCK TABLES `portrelation` WRITE;
/*!40000 ALTER TABLE `portrelation` DISABLE KEYS */;
INSERT INTO `portrelation` VALUES (1,1,14,50,1,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(2,1,15,51,1,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(3,1,16,52,1,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(4,1,17,53,1,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(5,1,18,54,0,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(6,1,19,55,0,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(7,1,20,56,0,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(8,1,21,57,0,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(9,4,22,58,1,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(10,4,23,59,1,'2019-01-15 09:36:51',NULL,'2019-01-15 09:36:51',NULL),(11,4,24,60,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(12,4,25,61,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(13,2,26,62,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(14,2,27,63,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(15,2,28,64,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(16,2,29,65,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(17,5,30,68,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(18,6,31,77,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(19,6,32,78,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(20,6,33,79,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(21,6,34,105,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(22,7,35,106,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(23,9,37,108,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(24,3,1,1,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(25,3,2,2,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL),(26,3,3,3,1,'2019-01-15 09:36:52',NULL,'2019-01-15 09:36:52',NULL);
/*!40000 ALTER TABLE `portrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports`
--

DROP TABLE IF EXISTS `ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports` (
  `port_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `portgroup_id` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `decimalpoint` int(1) DEFAULT NULL,
  `isdigital` tinyint(1) DEFAULT NULL,
  `alertrules_id` int(11) DEFAULT NULL,
  `scalefactor` float DEFAULT NULL,
  PRIMARY KEY (`port_id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_port_portgroup` (`portgroup_id`),
  CONSTRAINT `fk_port_portgroup` FOREIGN KEY (`portgroup_id`) REFERENCES `portgroup` (`portgroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports`
--

LOCK TABLES `ports` WRITE;
/*!40000 ALTER TABLE `ports` DISABLE KEYS */;
INSERT INTO `ports` VALUES (1,'01D','PLN',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:42',NULL,'2019-01-15 09:36:42',NULL,NULL,0,1,1,0.1),(2,'02D','Genset-1',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(3,'03D','Genset-2',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(4,'04D','Mains Fail Rectifier #1',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(5,'05D','Rectifier #1 Fault',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(6,'06D','Mains Fail Rectifier #2',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(7,'07D','Rectifier #2 Fault',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(8,'08D','Mains Fail Rectifier #1',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(9,'09D','Rectifier #2 Fault',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(10,'10D','Mains Fail Rectifier #1',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(11,'11D','Rectifier #2 Fault',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(12,'12D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(13,'13D','PLN & GENSET',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(14,'14D','Temperatur Room 1',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(15,'15D','Temperatur Room 2',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(16,'16D','Temperatur Room 3',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(17,'17D','Temperatur Room 4',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(18,'18D','Temperatur Room 5',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(19,'19D','Temperatur Room 6',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(20,'20D','Temperatur Room 7',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(21,'21D','Temperatur Room 8',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(22,'22D','Humidity Room 1',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:43',NULL,'2019-01-15 09:36:43',NULL,NULL,0,1,1,0.1),(23,'23D','Humidity Room 2',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(24,'24D','Humidity Room 3',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(25,'25D','Humidity Room 4',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(26,'26D','DC Voltage Rect #1',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(27,'27D','DC Voltage Rect #2',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(28,'28D','DC Voltage Rect #3',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(29,'29D','DC Voltage Rect #4',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(30,'30D','Total Power Factor',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(31,'31D','R-N',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(32,'32D','S-N',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(33,'33D','T-N',9,'glyphicon glyphicon-flash',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(34,'34D','High Temperatur DEG',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(35,'35D','Low Oil Pressure DEG',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(36,'36D','General Alarm DEG',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(37,'37D','Fuel Level DEG',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(38,'38D','',9,'glyphicon glyphicon-dashboard',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(39,'39D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(40,'40D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(41,'41D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(42,'42D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:44',NULL,'2019-01-15 09:36:44',NULL,NULL,0,1,1,0.1),(43,'43D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,1,1,0.1),(44,'44D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,1,1,0.1),(45,'45D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,1,1,0.1),(46,'46D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,1,1,0.1),(47,'47D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,1,1,0.1),(48,'48D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,1,1,0.1),(49,'49D','',9,'glyphicon glyphicon-asterisk',1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,1,1,0.1),(50,'01A','Temperatur SWT',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,2,0,3,0.1),(51,'02A','Temperatur TRA',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,0,0,3,0.1),(52,'03A','Temperatur RECT',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,0,0,3,0.1),(53,'04A','Temperatur NGN',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,0,0,3,0.1),(54,'05A','Temp',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,0,0,3,0.1),(55,'06A','Temp',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,0,0,3,0.1),(56,'07A','Temp',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,0,0,3,0.1),(57,'08A','Temp',3,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,4,0,0,3,0.1),(58,'09A','Humidity',4,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,NULL,0.1),(59,'10A','Humidity',4,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,NULL,0.1),(60,'11A','Humidity',4,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,NULL,0.1),(61,'12A','Humidity',4,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,NULL,0.1),(62,'13A','DC Volt Rect',5,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,2,0.1),(63,'14A','DC Volt Rect',5,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,2,0.1),(64,'15A','DC Volt Rect',5,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,2,0.1),(65,'16A','DC Volt Rect',5,NULL,1,'2019-01-15 09:36:45',NULL,'2019-01-15 09:36:45',NULL,NULL,0,0,2,0.1),(66,'17A','KWH',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,3,2,0,2,0.1),(68,'18A','Total Power Factor',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(69,'19A','KVA (current)',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,6,1,0,1,0.1),(70,'20A','KVA (max)',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,6,2,0,1,0.1),(71,'21A','R',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(72,'22A','S',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(73,'23A','T',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(74,'24A','R-S',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(75,'25A','S-T',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(76,'26A','R-T',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(77,'27A','R-N',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(78,'28A','S-N',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(79,'29A','T-N',2,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,2,0.1),(80,'30A','R-N',1,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,NULL,0.1),(81,'31A','S-N',1,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,NULL,0.1),(82,'32A','T-N',1,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,NULL,0.1),(83,'33A','KVA (current)',7,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,6,2,0,4,0.1),(85,'34A','R',7,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,NULL,0.1),(87,'35A','S',7,NULL,1,'2019-01-15 09:36:46',NULL,'2019-01-15 09:36:46',NULL,NULL,0,0,NULL,0.1),(89,'36A','T',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(91,'37A','R-S',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(93,'38A','S-T',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(95,'39A','R-T',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(97,'40A','R-N',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(99,'41A','S-N',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,1,0,0,NULL,0.1),(101,'42A','T-N',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(103,'43A','Hour Counter',7,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(105,'44A','Temperatur',3,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,1,0.1),(106,'45A','Oil Pressure',8,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(107,'46A','Voltage Batt. Starter',5,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,2,0.1),(108,'47A','Fuel Level',6,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(109,'48A','',1,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(110,'49A','',1,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(111,'50A','',1,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1),(112,'51A','',1,NULL,1,'2019-01-15 09:36:47',NULL,'2019-01-15 09:36:47',NULL,NULL,0,0,NULL,0.1);
/*!40000 ALTER TABLE `ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'max_analog_register','128',1,'2019-01-15 09:36:55','2019-01-15 09:36:55'),(2,'max_digital_register','64',1,'2019-01-15 09:36:55','2019-01-15 09:36:55');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `supervisor_id` int(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedby` int(4) DEFAULT NULL,
  `witel_id` int(4) DEFAULT NULL,
  `sitetype_id` int(4) DEFAULT NULL,
  PRIMARY KEY (`site_id`),
  KEY `fk_site_site_type_id` (`sitetype_id`),
  KEY `fk_site_witel` (`witel_id`),
  CONSTRAINT `fk_site_site_type_id` FOREIGN KEY (`sitetype_id`) REFERENCES `sitetype` (`sitetype_id`),
  CONSTRAINT `fk_site_witel` FOREIGN KEY (`witel_id`) REFERENCES `witel` (`witel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
INSERT INTO `site` VALUES (1,'Medan 2','Medan 2',1,0,NULL,'2019-01-15 09:36:33',NULL,'2019-01-15 09:36:33',NULL,1,1),(2,'Medan','Witel Medan',1,0,NULL,'2019-02-03 09:59:53',NULL,'2019-02-03 09:59:53',NULL,1,1),(3,'Batam','Batam Kepri',1,0,NULL,'2019-02-03 10:02:00',NULL,'2019-02-03 10:02:00',NULL,1,3),(4,'Bandung','',1,0,NULL,'2019-02-03 16:14:47',NULL,'2019-02-03 16:14:47',NULL,1,3),(5,'Jakarta','',1,0,NULL,'2019-02-03 16:14:54',NULL,'2019-02-03 16:14:54',NULL,1,3),(6,'Palembang','',1,0,NULL,'2019-02-03 16:15:02',NULL,'2019-02-03 16:15:02',NULL,1,1),(7,'Pekanbaru','',1,0,NULL,'2019-02-03 16:15:13',NULL,'2019-02-03 16:15:13',NULL,1,3),(8,'Dumai','',1,0,NULL,'2019-02-03 16:15:17',NULL,'2019-02-03 16:15:17',NULL,1,1),(10,'New Sites','',1,0,NULL,'2019-03-29 04:27:54',NULL,'2019-03-29 04:27:54',NULL,1,2),(11,'Test','',1,0,NULL,'2019-03-29 04:33:16',NULL,'2019-03-29 04:33:16',NULL,1,3);
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitetype`
--

DROP TABLE IF EXISTS `sitetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitetype` (
  `sitetype_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `importance_level` int(1) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedby` int(4) DEFAULT NULL,
  PRIMARY KEY (`sitetype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitetype`
--

LOCK TABLES `sitetype` WRITE;
/*!40000 ALTER TABLE `sitetype` DISABLE KEYS */;
INSERT INTO `sitetype` VALUES (1,'Default',1,1,'2019-03-20 11:21:20',NULL,'2019-03-20 11:21:20',NULL),(2,'Primary POP',5,1,'2019-03-20 11:21:20',NULL,'2019-03-20 11:21:20',NULL),(3,'Secondary POP',4,1,'2019-03-20 11:21:20',NULL,'2019-03-20 11:21:20',NULL);
/*!40000 ALTER TABLE `sitetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telkomarea`
--

DROP TABLE IF EXISTS `telkomarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telkomarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedby` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telkomarea`
--

LOCK TABLES `telkomarea` WRITE;
/*!40000 ALTER TABLE `telkomarea` DISABLE KEYS */;
INSERT INTO `telkomarea` VALUES (1,'Telkom Regional - I','',1,'2019-03-20 09:16:04',NULL,'2019-03-20 09:16:04',NULL);
/*!40000 ALTER TABLE `telkomarea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uom`
--

DROP TABLE IF EXISTS `uom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uom` (
  `uom_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  PRIMARY KEY (`uom_id`),
  UNIQUE KEY `symbol` (`symbol`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uom`
--

LOCK TABLES `uom` WRITE;
/*!40000 ALTER TABLE `uom` DISABLE KEYS */;
INSERT INTO `uom` VALUES (1,'Volt','V',1,'2019-04-03 03:44:54',NULL,'2019-04-03 03:44:54',NULL),(2,'Watt','W',1,'2019-04-03 03:44:54',NULL,'2019-04-03 03:44:54',NULL),(3,'Kilowatt Hour','KWh',1,'2019-04-03 03:44:54',NULL,'2019-04-03 03:44:54',NULL),(4,'Celcius','C',1,'2019-04-03 03:44:54',NULL,'2019-04-03 03:44:54',NULL),(5,'Vold DC','VDC',1,'2019-04-03 03:44:54',NULL,'2019-04-03 03:44:54',NULL),(6,'KVA','KVA',1,'2019-04-10 08:13:21',NULL,'2019-04-10 08:13:21',NULL);
/*!40000 ALTER TABLE `uom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','9DEX-0ghPgC6be25eaYXlyDXGm5-p_3a','$2y$13$p.5svFb8NySs7rWP0yK6keIHOUSUuDdbhFPr/zMJ3zuZLr4/DhhTm','$2y$13$eu3s93hJf3QgC3TvAkyTauf0q0Fiyobf./BwH9CGWVD5gpxMBVJA.','admin@localhost.com','Administrator',10,'2019-01-15 09:36:30','2019-01-15 09:36:30');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget` (
  `widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `notify` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  `widgetgroup_id` int(4) DEFAULT NULL,
  `parent_id` int(4) DEFAULT NULL,
  `port_id` int(4) DEFAULT NULL,
  `sameline` tinyint(1) DEFAULT '1',
  `sequence` int(11) DEFAULT '1',
  PRIMARY KEY (`widget_id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_widget_widgetgroup_id` (`widgetgroup_id`),
  CONSTRAINT `fk_widget_widgetgroup_id` FOREIGN KEY (`widgetgroup_id`) REFERENCES `widgetgroup` (`widgetgroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget`
--

LOCK TABLES `widget` WRITE;
/*!40000 ALTER TABLE `widget` DISABLE KEYS */;
INSERT INTO `widget` VALUES (1,'Temperature','Temperature',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,10,0,NULL,1,1),(2,'Battery','Battery Status',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,2,0,NULL,1,1),(3,'PLN','PLN',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,3,0,1,1,1),(4,'Humidity','Humidity',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,4,NULL,NULL,1,1),(5,'Genset','Genset',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,3,0,2,1,1),(6,'Genset 2','Genset 2',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,7,0,3,1,1),(7,'HighTemperature','High Temperature',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,7,NULL,NULL,1,1),(8,'OilLevel','Oil Level',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,8,NULL,NULL,1,1),(9,'OilPressure','Oil Pressure',1,1,'2019-01-15 09:36:48',NULL,'2019-01-15 09:36:48',NULL,9,NULL,NULL,1,1),(11,'Voltage','PLN Voltage',1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL,1,3,NULL,1,2),(12,'PLN-Ampere','PLN Ampere',1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL,1,3,NULL,1,3),(13,'PLN-Kwh-Info','PLN KWH Info',1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL,1,3,NULL,0,1),(14,'Genset Voltage','Genset Voltage',1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL,3,5,NULL,1,2),(15,'Genset Ampere','Genset Ampere',1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL,3,5,NULL,1,3),(16,'Genset Info','Genset Info',1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL,3,5,NULL,0,1),(21,'Ruang Transmisi','',1,1,'2019-03-07 04:58:47',NULL,'2019-03-07 04:58:47',NULL,1,1,14,0,1),(22,'Ruang Transmisi 2','',1,1,'2019-03-07 05:25:36',NULL,'2019-03-07 05:25:36',NULL,1,1,NULL,0,2),(23,'Ruang Transmisi 3','Ruang Transmisi',1,1,'2019-03-07 11:56:19',NULL,'2019-03-07 11:56:19',NULL,1,1,NULL,0,3),(24,'Ruang Transmisi 4','Ruang Transmis 4',1,1,'2019-03-07 11:56:32',NULL,'2019-03-07 11:56:32',NULL,1,1,NULL,0,4),(25,'Rectifier Benning','',1,1,'2019-03-07 12:22:09',NULL,'2019-03-07 12:22:09',NULL,2,2,NULL,0,1),(26,'Rectifier Siemens 2E','',1,1,'2019-03-07 12:22:26',NULL,'2019-03-07 12:22:26',NULL,2,2,NULL,0,2),(27,'Rectifier  Peco/Arguss','',1,1,'2019-03-07 12:22:45',NULL,'2019-03-07 12:22:45',NULL,2,2,NULL,0,3),(28,'Rectifier ZTE','Rectifier ZTE',1,1,'2019-03-07 12:23:03',NULL,'2019-03-07 12:23:03',NULL,2,2,NULL,0,4);
/*!40000 ALTER TABLE `widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgetgroup`
--

DROP TABLE IF EXISTS `widgetgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgetgroup` (
  `widgetgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  PRIMARY KEY (`widgetgroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgetgroup`
--

LOCK TABLES `widgetgroup` WRITE;
/*!40000 ALTER TABLE `widgetgroup` DISABLE KEYS */;
INSERT INTO `widgetgroup` VALUES (1,'Temperature','Temperature',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(2,'Battery','Battery Status',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(3,'Electric','Electric',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(4,'Humidity','Humidity',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(5,'TotalPower','Total Power',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(6,'Voltage','Voltage',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(7,'HighTemperature','High Temperature',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(8,'OilLevel','Oil Level',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(9,'OilPressure','Oil Pressure',1,'2019-03-04 11:52:04',NULL,'2019-03-04 11:52:04',NULL),(10,'Room Temperature','Room Temperature',1,'2019-04-09 12:13:12',NULL,'2019-04-09 12:13:12',NULL);
/*!40000 ALTER TABLE `widgetgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgetport`
--

DROP TABLE IF EXISTS `widgetport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgetport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) DEFAULT NULL,
  `port_id` int(11) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_widgetport_widget` (`widget_id`),
  CONSTRAINT `fk_widgetport_widget` FOREIGN KEY (`widget_id`) REFERENCES `widget` (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgetport`
--

LOCK TABLES `widgetport` WRITE;
/*!40000 ALTER TABLE `widgetport` DISABLE KEYS */;
INSERT INTO `widgetport` VALUES (50,21,50,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(62,2,62,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(63,2,63,2,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(64,2,64,3,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(65,2,65,4,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(66,13,66,4,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(68,13,68,3,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(69,13,69,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(70,13,70,2,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(71,12,71,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(72,12,72,2,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(73,12,73,3,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(74,11,74,4,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(75,11,75,5,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(76,11,76,6,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(77,11,77,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(78,11,78,2,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(79,11,79,3,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(80,12,80,5,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(81,12,81,6,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(82,12,82,7,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(83,16,83,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(85,15,85,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(87,15,87,2,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(89,15,89,3,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(91,14,91,1,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(93,14,93,2,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(95,14,95,3,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(97,14,97,4,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(99,14,99,5,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(101,14,101,6,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(103,16,103,2,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(107,16,107,3,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(108,16,108,4,1,'2019-03-04 11:53:06',NULL,'2019-03-04 11:53:06',NULL),(111,21,58,2,1,'2019-03-07 05:15:50',NULL,'2019-03-07 05:15:50',NULL),(114,22,51,1,1,'2019-03-07 05:33:39',NULL,'2019-03-07 05:33:39',NULL),(116,22,59,2,1,'2019-03-07 11:55:00',NULL,'2019-03-07 11:55:00',NULL),(117,23,52,1,1,'2019-03-07 11:57:36',NULL,'2019-03-07 11:57:36',NULL),(118,23,60,2,1,'2019-03-07 11:57:54',NULL,'2019-03-07 11:57:54',NULL),(119,24,53,1,1,'2019-03-07 11:58:28',NULL,'2019-03-07 11:58:28',NULL),(120,24,61,2,1,'2019-03-07 11:59:20',NULL,'2019-03-07 11:59:20',NULL),(121,25,62,1,1,'2019-03-07 12:39:13',NULL,'2019-03-07 12:39:13',NULL),(122,25,75,2,1,'2019-03-07 12:39:24',NULL,'2019-03-07 12:39:24',NULL),(123,26,63,1,1,'2019-03-07 12:42:54',NULL,'2019-03-07 12:42:54',NULL),(124,26,76,2,1,'2019-03-07 12:43:04',NULL,'2019-03-07 12:43:04',NULL),(125,27,64,1,1,'2019-03-07 12:44:07',NULL,'2019-03-07 12:44:07',NULL),(126,27,76,2,1,'2019-03-07 12:44:16',NULL,'2019-03-07 12:44:16',NULL),(127,28,65,1,1,'2019-03-07 12:45:01',NULL,'2019-03-07 12:45:01',NULL),(128,28,78,2,1,'2019-03-07 12:45:16',NULL,'2019-03-07 12:45:16',NULL),(129,25,4,3,1,'2019-03-09 10:44:58',NULL,'2019-03-09 10:44:58',NULL),(130,25,5,4,1,'2019-03-09 10:45:06',NULL,'2019-03-09 10:45:06',NULL),(131,26,6,3,1,'2019-03-09 10:46:54',NULL,'2019-03-09 10:46:54',NULL),(132,26,7,4,1,'2019-03-09 10:47:03',NULL,'2019-03-09 10:47:03',NULL),(133,27,8,3,1,'2019-03-09 10:47:39',NULL,'2019-03-09 10:47:39',NULL),(134,27,9,4,1,'2019-03-09 10:47:49',NULL,'2019-03-09 10:47:49',NULL),(135,28,10,3,1,'2019-03-09 10:48:18',NULL,'2019-03-09 10:48:18',NULL),(136,28,11,4,1,'2019-03-09 10:48:27',NULL,'2019-03-09 10:48:27',NULL);
/*!40000 ALTER TABLE `widgetport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `witel`
--

DROP TABLE IF EXISTS `witel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `witel` (
  `witel_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `parent_id` int(4) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(4) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedby` int(4) DEFAULT NULL,
  PRIMARY KEY (`witel_id`),
  KEY `fk_witel_area` (`area_id`),
  CONSTRAINT `fk_witel_area` FOREIGN KEY (`area_id`) REFERENCES `telkomarea` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `witel`
--

LOCK TABLES `witel` WRITE;
/*!40000 ALTER TABLE `witel` DISABLE KEYS */;
INSERT INTO `witel` VALUES (1,1,'ACEH',NULL,0,1,'2019-03-20 09:16:04',NULL,'2019-03-20 09:16:04',NULL),(2,1,'MEDAN','',NULL,1,'2019-03-31 15:09:23',NULL,'2019-03-31 15:09:23',NULL),(4,1,'SUMUT','',NULL,1,'2019-04-13 00:52:28',NULL,'2019-04-13 00:52:28',NULL),(5,1,'SUMBAR','Sumatera Barat',NULL,1,'2019-04-13 00:53:15',NULL,'2019-04-13 00:53:15',NULL),(6,1,'RIDAR','Riau Daratan',NULL,1,'2019-04-13 00:53:26',NULL,'2019-04-13 00:53:26',NULL),(7,1,'RIKEP','Riau Kepulauan',NULL,1,'2019-04-13 00:53:44',NULL,'2019-04-13 00:53:44',NULL),(8,1,'JAMBI','Jambi',NULL,1,'2019-04-13 00:54:00',NULL,'2019-04-13 00:54:00',NULL),(9,1,'SUMSEL','Sumatera Selatan',NULL,1,'2019-04-13 00:54:11',NULL,'2019-04-13 00:54:11',NULL),(10,1,'BNK','',NULL,1,'2019-04-13 00:54:33',NULL,'2019-04-13 00:54:33',NULL),(11,1,'LAMPUNG','',NULL,1,'2019-04-13 00:54:42',NULL,'2019-04-13 00:54:42',NULL),(12,1,'BABEL','Bangka Belitung',NULL,1,'2019-04-13 00:54:53',NULL,'2019-04-13 00:54:53',NULL);
/*!40000 ALTER TABLE `witel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-13  7:56:29
