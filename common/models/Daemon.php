<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "daemon".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $isactive
 * @property string $last_started
 * @property string $last_finished
 * @property string $message
 * @property int $status
 */
class Daemon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daemon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['isactive', 'status'], 'integer'],
            [['last_started', 'last_finished'], 'safe'],
            [['name'], 'string', 'max' => 25],
            [['description'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 250],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'isactive' => 'Isactive',
            'last_started' => 'Last Started',
            'last_finished' => 'Last Finished',
            'message' => 'Message',
            'status' => 'Status',
        ];
    }
}
