<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "widget".
 *
 * @property int $widget_id
 * @property string $name
 * @property string $description
 * @property int $isactive
 * @property int $notify
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 * @property int $widgetgroup_id
 * @property int $parent_id
 * @property int $port_id
 * @property int $sameline
 * @property int $sequence
 *
 * @property Portrelation[] $portrelations
 * @property Widgetgroup $widgetgroup
 * @property Widgetport[] $widgetports
 */
class Widgets extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'widget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['isactive', 'notify', 'createdby', 'updateby', 'widgetgroup_id', 'parent_id', 'port_id', 'sameline', 'sequence'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['widgetgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Widgetgroup::className(), 'targetAttribute' => ['widgetgroup_id' => 'widgetgroup_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'widget_id' => 'Widget ID',
            'name' => 'Name',
            'description' => 'Description',
            'isactive' => 'Isactive',
            'notify' => 'Notify',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
            'widgetgroup_id' => 'Widgetgroup ID',
            'parent_id' => 'Parent ID',
            'port_id' => 'Port ID',
            'sameline' => 'Sameline',
            'sequence' => 'Sequence',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPortrelations()
    {
        return $this->hasMany(Portrelation::className(), ['widget_id' => 'widget_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWidgetgroup()
    {
        return $this->hasOne(Widgetgroup::className(), ['widgetgroup_id' => 'widgetgroup_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWidgetports()
    {
        return $this->hasMany(Widgetport::className(), ['widget_id' => 'widget_id']);
    }
    
       //For select2
    public static function listWidgets($id){
        
        $group = Widgets::find()->where(['widget_id'=>$id])->one();
        
        $items = ArrayHelper::map(Widgets::find()->where(['isactive'=>true,'widgetgroup_id'=>$group->widgetgroup_id])->all(), 'widget_id', 'name');
        
        return $items;
    }
}
