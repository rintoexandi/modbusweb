<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "alertrules".
 *
 * @property int $alertrules_id
 * @property string $name
 * @property int $isdigital
 * @property int $notify
 * @property int $isactive
 *
 * @property Alertrulesdetails[] $alertrulesdetails
 * @property Deviceport[] $deviceports
 * @property Portgroup[] $portgroups
 */
class Alertrules extends ActiveRecord
{
    public static $SEVERITY_INFO="I";
    public static $SEVERITY_WARNING="W";
    public static $SEVERITY_CRITICAL="C";
    public static $SEVERITY_OK="O";
    public static $SEVERITY_INVALID="X";
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alertrules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'isdigital'], 'required'],
            [['isdigital', 'notify', 'isactive'], 'integer'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'alertrules_id' => 'Alertrules ID',
            'name' => 'Name',
            'isdigital' => 'Isdigital',
            'notify' => 'Notify',
            'isactive' => 'Isactive',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAlertrulesdetails()
    {
        return $this->hasMany(Alertrulesdetails::className(), ['alertrules_id' => 'alertrules_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDeviceports()
    {
        return $this->hasMany(Deviceport::className(), ['alertrules_id' => 'alertrules_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPortgroups()
    {
        return $this->hasMany(Portgroup::className(), ['alertrules_id' => 'alertrules_id']);
    }
    
    public static function listSeverity(){
         $data = [
             
                "O"=>"OK",
                "I" => "INFO",
                "W" => "WARNING",
                "C" =>"CRITICAL",               
        ];
        
        return $data;
    }
    
    //For Select2
    public static function listRules() {
      
        $dataList=ArrayHelper::map(Alertrules::find()->where(['isactive'=>true])->asArray()->all(), 'alertrules_id', 'name');
        return $dataList;
        
    }
}
