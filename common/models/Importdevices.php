<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "importdevices".
 *
 * @property int $id
 * @property string $hostname
 * @property string $ipaddress
 * @property string $sitename
 * @property string $witelname
 * @property int $sitetype
 * @property int $processed
 * @property int $isvalid 
 * @property string $created
 * @property string $message 
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 */
class Importdevices extends ActiveRecord
{
    public $csvfile;
    public $filename;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'importdevices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           
            [['sitetype', 'processed', 'isvalid','createdby', 'updateby'], 'integer'],
            [['created', 'updated','filename'], 'safe'],
            [['hostname', 'ipaddress', 'sitename', 'witelname','filename','message'], 'string', 'max' => 255],
            [['csvfile'], 'file','extensions' => 'csv','maxSize'=>1024 * 1024],
            [['csvfile'],'required','on'=>'uploadcsv']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hostname' => 'Hostname',
            'ipaddress' => 'Ipaddress',
            'sitename' => 'Sitename',
            'witelname' => 'Witelname',
            'sitetype' => 'Sitetype',
             'isvalid' => 'Isvalid',
            'message' => 'Message', 
            'processed' => 'Processed',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
            'csvfile'=>'CSV File,'
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
      
        $scenarios['uploadcsv'] = ['csvfile'];
      
        return $scenarios;
    }
    
    public function uploadFile() {
        // get the uploaded file instance
        $csvFile = UploadedFile::getInstance($this, 'csvfile');
 
        // if no image was uploaded abort the upload
        if (empty($csvFile)) {
            return false;
        }
 
        // generate random name for the file
        $this->filename = time(). '-' . $csvFile->baseName . '.' . $csvFile->extension;
 
        // the uploaded image instance
        return $csvFile;
    }
 
    public function getUploadedFile() {
        // return a default image placeholder if your source avatar is not found
        $log_file = isset($this->filename) ? $this->filename : 'default.csv';
        return  $log_file;
        
    }
    
    public function upload() {
        if ($this->validate()) {
            $this->logFile->saveAs(Yii::$app->params['uploadcsv'] . $this->csvfile->baseName . '.' . $this->csvfile->extension);
            return true;
        } else {
            return false;
        }
    }
    
    
    public function processImport(){
        
        $device = new Devices();
        
        $model = Importdevices::findOne($this->id);
        
        $valid = true;
        
        $site = Telkomsites::validateSite($this->sitename);
        
        if (empty($site)) {
          
            $valid=false;
        }
       else {

           $device->site_id=$site->site_id;
       }
                             
                            
        $device->hostname=$this->hostname;
        $device->ipaddress=$this->ipaddress;
       
        $device->sitetype_id=1;
         
       if ($valid==true) { 
            if($device->Save(false)) {

                $model->message='RTU ' . $this->hostname . '  ' . $this->ipaddress . '   is added';
                $model->processed=true;
                $model->save(false);
            }
            else {

                $model->message='Failed to import ' . $this->hostname . '  ' . $this->ipaddress;
                $model->processed=false;
                $model->save(false);

            }
       }
       
       else {
           
           
                $model->message='Data Invalid for ' . $this->hostname . '  ' . $this->ipaddress;
                $model->processed=false;
                $model->save(false);
       }
        
        
    }
    
    
    
   
}
