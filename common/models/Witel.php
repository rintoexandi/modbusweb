<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "witel".
 *
 * @property int $witel_id
 * @property int $area_id
 * @property string $name
 * @property string $description
 * @property int $parent_id
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updatedby
 */
class Witel extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'witel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area_id', 'name'], 'required'],
            [['area_id', 'parent_id', 'isactive', 'createdby', 'updatedby'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
            [['area_id'], 'exist', 'skipOnError' => true, 
                'targetClass' => Telkomarea::className(), 
                'targetAttribute' => ['area_id' => 'id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'witel_id' => 'Witel ID',
            'area_id' => 'Area ID',
            'name' => 'Name',
            'description' => 'Description',
            'parent_id' => 'Parent ID',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updatedby' => 'Updatedby',
        ];
    }
    
    /** 
    *  
    * @param DeviceAlert $severity 
    * @return type 
    */ 
   public static function getAlerts($severity,$witel_id){ 
       
       
       $query = Devicealert::find(); 
       $query->joinWith('deviceport'); 
       $query->join('INNER JOIN','devices','deviceport.device_id = devices.device_id'); 
       $query->join('INNER JOIN','site','devices.site_id = site.site_id'); 
       $query->join('INNER JOIN','witel','site.witel_id = witel.witel_id'); 
       $query->andWhere(['devicealert.open'=>true]); 
       $query->andWhere(['devicealert.severity'=>$severity]); 
       $query->andWhere(['witel.witel_id'=>$witel_id]); 
        
        
       $model = new ActiveDataProvider([ 
               'query' => $query, 
       ]); 
        
       return $model->getTotalCount(); 
      
       
   }
   
   public static function countDevices($witel_id, $status = true){ 
       $query = Devices::find(); 
       $query->joinWith('site'); 
       $query->join('INNER JOIN','witel','site.witel_id = witel.witel_id'); 
       $query->andWhere(['devices.status'=>$status]); 
       
       $query->andWhere(['witel.witel_id'=>$witel_id]); 
        
        
       $model = new ActiveDataProvider([ 
               'query' => $query, 
       ]); 
        
       return $model->getTotalCount(); 
      
       
   }
   
    /**
    * @return ActiveQuery
    */
   public function getArea()
   {
       return $this->hasOne(Telkomarea::className(), ['id' => 'area_id']);
   }
   
    //For select2
    public static function listWitel(){
        
        $items = ArrayHelper::map(Witel::find()->where(['isactive'=>true])->all(), 'witel_id', 'name');
        
        return $items;
    }
    
    
    
    public static function validateWitel($witelname){
        
        $witel = Witel::find()->where(['name'=>$witelname])->one();
        
        if ($witel !==null) {
            
            return $witel;
        }
        
        else {
            return '';
        }
    }
    
}
