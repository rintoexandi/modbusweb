<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Deviceport;

/**
 * DeviceportSearch represents the model behind the search form of `common\models\Deviceport`.
 */
class DeviceportSearch extends Deviceport
{
    public $name;
    public $isdigital;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deviceport_id', 'device_id', 'port_id', 'value', 'value_prev', 'port_status', 'port_status_prev', 'port_uptime', 'isactive', 'createdby', 'updateby'], 'integer'],
            [['value_inpercent'], 'number'],
           
            [['name','isdigital','last_uptime', 'poll_time', 'poll_prev', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Deviceport::find();
        $query->joinWith('ports');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'deviceport_id' => $this->deviceport_id,
            'device_id' => $this->device_id,
            'port_id' => $this->port_id,
            'ports.isdigital'=>$this->isdigital,
            'ports.name'=>$this->name,
          
        ]);

        return $dataProvider;
    }
}
