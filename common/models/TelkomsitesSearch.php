<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * SitesSearch represents the model behind the search form of `common\models\Sites`.
 */
class TelkomsitesSearch extends Telkomsites
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_id', 'isactive','supervisor_id', 'sitetype_id','witel_id'], 'integer'],
            [['name', 'description', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Telkomsites::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'site_id' => $this->site_id,
          
            'sitetype_id' => $this->sitetype_id,
            'supervisor_id' => $this->supervisor_id,
           
            'witel_id' => $this->witel_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
