<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "polldata".
 *
 * @property int $id
 * @property int $poll_id
 * @property int $isdigital
 * @property string $data
 * @property int $data_length
 * @property int $processed
 * @property string $created
 *
 * @property Poll $poll
 */
class Polldata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'polldata';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poll_id'], 'required'],
            [['poll_id', 'isdigital', 'data_length', 'processed'], 'integer'],
            [['data'], 'string'],
            [['created'], 'safe'],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'poll_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poll_id' => 'Poll ID',
            'isdigital' => 'Isdigital',
            'data' => 'Data',
            'data_length' => 'Data Length',
            'processed' => 'Processed',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Poll::className(), ['poll_id' => 'poll_id']);
    }
    
    public static function Process($id){
        
        $polldata = Polldata::findOne(['id'=>$id]);
        
        $polldata->processed=true;
        
        return $polldata->save(false);
    }
}
