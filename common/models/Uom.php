<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "uom".
 *
 * @property int $uom_id
 * @property string $name
 * @property string $symbol
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 */
class Uom extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'symbol'], 'required'],
            [['isactive', 'createdby', 'updateby'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['symbol'], 'string', 'max' => 10],
            [['symbol'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uom_id' => 'Uom ID',
            'name' => 'Name',
            'symbol' => 'Symbol',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
        ];
    }
    
     //For Select2
    public static function listUom() {
        
        $dataList=ArrayHelper::map(Uom::find()->asArray()->all(), 'uom_id', 'symbol');
        return $dataList;
        
    }
}
