<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "poll".
 *
 * @property int $poll_id
 * @property int $device_id
 * @property int $success
 * @property string $poll_attempted
 * @property string $poll_prevattempted
 *
 * @property Devices $device
 * @property Polldata[] $polldatas
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'poll';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id'], 'required'],
            [['device_id', 'success'], 'integer'],
            [['poll_attempted', 'poll_prevattempted'], 'safe'],
            [['device_id'], 'exist', 'skipOnError' => true, 'targetClass' => Devices::className(), 'targetAttribute' => ['device_id' => 'device_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'poll_id' => 'Poll ID',
            'device_id' => 'Device ID',
            'success' => 'Success',
            'poll_attempted' => 'Poll Attempted',
            'poll_prevattempted' => 'Poll Prevattempted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Devices::className(), ['device_id' => 'device_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolldatas()
    {
        return $this->hasMany(Polldata::className(), ['poll_id' => 'poll_id']);
    }
    
    /*
     * Get Poll Data
     */
    public static function getPoll($device_id) {
        $poll = Poll::find()->where(['device_id'=>$device_id,'processed'=>false])->one();
        return $poll;
    }
    
    /*
     *
     */
    public function getData($isdigital) {
        
        //print_r($poll_id);
        //print_r($isdigital);
        $arrResult='';
        //die();
        
        $polldata = Polldata::find()->where(['poll_id'=>$this->poll_id,
                'isdigital'=>$isdigital,'processed'=>false])->one();
        
        //print_r($polldata);
        //die();
        
        //$polldata = new Polldata();
        //$datapoller = json_encode($polldata['data']);
        
        if (!empty($polldata)) {
            $strdata = str_replace("[", "", $polldata->data);
            
            $strdata = str_replace("]", "", $strdata);
            
            //print_r($polldata);
            
            $result = explode(",", trim($strdata));
            
            $arrResult = array("id"=>$polldata->id,"data"=>$result,'poll_time'=>$polldata->created);
        }
        //print_r($polldata);
        
        return $arrResult;
        
    }
    
    public static function Process($id){
        
        $polldata = Poll::findOne(['poll_id'=>$id]);
        
        $polldata->processed=true;
        
        return $polldata->save(false);
    }
}
