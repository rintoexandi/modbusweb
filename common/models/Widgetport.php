<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widgetport".
 *
 * @property int $id
 * @property int $widget_id
 * @property int $port_id
 * @property int $sequence
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 *
 * @property Ports $port
 * @property Widget $widget
 */
class Widgetport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'widgetport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['widget_id', 'port_id', 'sequence', 'isactive', 'createdby', 'updateby'], 'integer'],
            [['port_id'], 'required'],
            [['created', 'updated'], 'safe'],
           
            [['port_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ports::className(), 'targetAttribute' => ['port_id' => 'port_id']],
            [['widget_id'], 'exist', 'skipOnError' => true, 'targetClass' => Widgets::className(), 'targetAttribute' => ['widget_id' => 'widget_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'widget_id' => 'Widget ID',
            'port_id' => 'Port ID',
            'sequence' => 'Sequence',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort()
    {
        return $this->hasOne(Ports::className(), ['port_id' => 'port_id']);
    }

    public function getDeviceport()
    {
        return $this->hasMany(Deviceport::className(), ['port_id' => 'port_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidget()
    {
        return $this->hasOne(Widgets::className(), ['widget_id' => 'widget_id']);
    }
    
    //Select 2
     public static function listSequence(){
        
         //$numItem = Widgetport::find()->where(['widget_id'=>$widget_id])->count();
         
        
         $sequence= array(               
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
              '8' => '8',
              '9' => '9',
              '10' => '10'
        );
        
      return $sequence;
         
    }
}
