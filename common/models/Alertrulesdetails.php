<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "alertrulesdetails".
 *
 * @property int $id
 * @property int $alertrules_id
 * @property int $sequence
 * @property double $lowervalue
 * @property double $uppervalue
 * @property int $useboth
 * @property int $useduration
 * @property int $duration
 * @property int $useoccurence
 * @property int $occurance
 * @property string $severity
 * @property int $isactive
 *
 * @property Alertrules $alertrules
 */
class Alertrulesdetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alertrulesdetails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alertrules_id', 'sequence', 'useduration', 'useoccurence'], 'required'],
            [['alertrules_id', 'sequence', 'useboth', 'useduration', 'duration', 'useoccurence', 'occurance', 'isactive'], 'integer'],
            [['lowervalue', 'uppervalue'], 'number'],
            [['severity'], 'string', 'max' => 1],
            [['alertrules_id'], 'exist', 'skipOnError' => true, 'targetClass' => Alertrules::className(), 'targetAttribute' => ['alertrules_id' => 'alertrules_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alertrules_id' => 'Alertrules ID',
            'sequence' => 'Sequence',
            'lowervalue' => 'Lowervalue',
            'uppervalue' => 'Uppervalue',
            'useboth' => 'Useboth',
            'useduration' => 'Useduration',
            'duration' => 'Duration',
            'useoccurence' => 'Useoccurence',
            'occurance' => 'Occurance',
            'severity' => 'Severity',
            'isactive' => 'Isactive',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertrules()
    {
        return $this->hasOne(Alertrules::className(), ['alertrules_id' => 'alertrules_id']);
    }
    
    
    public function createSequence($rules_id){
        
        $rules = Alertrulesdetails::find()->where(['alertrules_id'=>$rules_id])->count();
        
        return ++$rules;
    }
}
