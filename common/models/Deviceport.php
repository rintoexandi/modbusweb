<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "deviceport".
 *
 * @property int $deviceport_id
 * @property int $device_id
 * @property int $port_id
 * @property double $value
 * @property string $value_inpercent
 * @property double $value_prev
 * @property int $port_status
 * @property int $port_status_prev
 * @property string $last_uptime
 * @property int $port_uptime
 * @property string $poll_time
 * @property string $poll_prev
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 * @property string $configtype 
 * @property int $alertrules_id
 * @property int $rrdcreated 
 * @property Devices $device
 * @property Alertrules $alertrules
 * @property int $reportalert 
 * @property Ports $port
 * @property Deviceportsetting $deviceportsetting
 */
class Deviceport extends ActiveRecord
{
    public static $CONFIG_TYPE_GLOBAL="G";
    public static $CONFIG_TYPE_CUSTOM="C";
      
    public static $BG_ACTIVE="bg_green enabled";
    public static $BG_INACTIVE="bg-gray disabled color-palette-box";
    
    public static $MONTHLY="monthly";
    public static $DAYLY="dayly";
    public static $YEARLY="yearly";
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deviceport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id', 'port_id'], 'required'],
            [['device_id', 'port_id',  'port_status', 'port_status_prev', 'port_uptime', 'isactive', 'createdby', 'updateby','alertrules_id','reportalert','rrdcreated'], 'integer'],
            [['value_inpercent','value', 'value_prev'], 'number'],
            [['last_uptime', 'poll_time', 'poll_prev', 'created', 'updated'], 'safe'],
            [['device_id', 'port_id'], 'unique', 'targetAttribute' => ['device_id', 'port_id']],
            [['device_id'], 'exist', 'skipOnError' => true, 'targetClass' => Devices::className(), 'targetAttribute' => ['device_id' => 'device_id']],
            [['port_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ports::className(), 'targetAttribute' => ['port_id' => 'port_id']],
            [['alertrules_id'], 'exist', 'skipOnError' => true, 'targetClass' => Alertrules::className(), 'targetAttribute' => ['alertrules_id' => 'alertrules_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'deviceport_id' => 'Deviceport ID',
            'device_id' => 'Device ID',
            'port_id' => 'Port ID',
            'value' => 'Value',
            'value_inpercent' => 'Value Inpercent',
            'value_prev' => 'Value Prev',
            'port_status' => 'Port Status',
            'port_status_prev' => 'Port Status Prev',
            'last_uptime' => 'Last Uptime',
            'port_uptime' => 'Port Uptime',
            'poll_time' => 'Poll Time',
            'poll_prev' => 'Poll Prev',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
            'configtype' => 'Configtype', 
            'alertrules_id' => 'Alertrules ID',
            'reportalert'=>'Report Alert',
            'rrdcreated'=>'RRD Created'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Devices::className(), ['device_id' => 'device_id']);
    }

     /** 
    * @return ActiveQuery 
    */ 
   public function getAlertrules() 
   { 
       return $this->hasOne(Alertrules::className(), ['alertrules_id' => 'alertrules_id']); 
   }
   
    /**
     * @return ActiveQuery
     */
    public function getPorts()
    {
        return $this->hasOne(Ports::className(), ['port_id' => 'port_id']);
    }
    
    public function getPort()
    {
        $port = Ports::find()->where(['port_id'=>$this->port_id])->one();
        return $port;
    }
    
    public function getDeviceportsetting()
    {
        return $this->hasOne(Deviceportsetting::className(), ['deviceport_id' => 'deviceport_id']);
    }
    
    
    public static function listPorts($device_id) {
        $ports = Deviceport::find()->where(['device_id'=>$device_id])->all();
        return $ports;
    }
    
    public function bgColor(){
        if ($this->value==1) {
            
            return "bg-light-blue disabled color-palette";
        }
        
        else {
           
            return "bg-gray-active color-palette";
        }
    }
    
   
    /**
     * 
     * @return string normal vs fault
     */
    public function getValueAsNormal(){
       
        if ($this->value==1) {
            return '<i class="fa fa-check-square text-green"></i><span class="text">Normal</span>';
        }
        
        else {
            return '<i class="fa fa-warning text-yellow"></i>Fault';
        }
    }
    
    public static function listReportAlert() {
        $data=[
            '0'=>'Disable',
            '1'=>'Enable'
        ];
        
        return $data;
    }
    
    public function getRRDfile(){
        $rrdpath = Yii::getAlias('@rrd') . "/" . $this->device_id . '/' . $this->deviceport_id . '.rrd';
   
        if (!file_exists ($rrdpath )) {
            
            return $rrdpath;
        }
        
        else {
            return $rrdpath;
        }
        
    }
    
     public function getJsonfile($type="daily") {
        
        $jsonpath = Yii::getAlias('@rrd') . '/' . $this->device_id . '/' . $this->deviceport_id . '-'. $type . '.json';
   
        if (!file_exists ($jsonpath )) {
            
            return $jsonpath;
        }
        
        else {
            return $jsonpath;
        }        
    }
    
    public function openJsonFile($type="daily"){
        
        $json = file_get_contents($this->getJsonfile($type));
        
        if ($json==false) {
            
            return '';
        }
        else {
            return json_decode($json,true);
        }

        
    }
    
     public static function setRRDCreated($id,$status=false){
        
        $deviceport = Deviceport::findOne(['deviceport_id'=>$id]);
        
        $deviceport->rrdcreated=$status;
        
        return $deviceport->save(false);
    }
    
    
}
