<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deviceportsetting".
 *
 * @property int $id
 * @property int $deviceport_id
 * @property string $settingtype
 * @property string $description
 * @property double $minvalue
 * @property double $maxvalue
 * @property int $scalefactor
 * @property int $isactive
 * @property int $notify
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 * @property double $noticevalue
 * @property double $warningvalue
 *
 * @property Deviceport $deviceport
 */
class Deviceportsetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deviceportsetting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deviceport_id', 'settingtype'], 'required'],
            [['deviceport_id', 'scalefactor', 'isactive', 'notify', 'createdby', 'updateby'], 'integer'],
            [['minvalue', 'maxvalue', 'noticevalue', 'warningvalue'], 'number'],
            [['created', 'updated'], 'safe'],
            [['settingtype'], 'string', 'max' => 1],
            [['description'], 'string', 'max' => 255],
            [['deviceport_id'], 'unique'],
            [['deviceport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deviceport::className(), 'targetAttribute' => ['deviceport_id' => 'deviceport_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deviceport_id' => 'Deviceport ID',
            'settingtype' => 'Settingtype',
            'description' => 'Description',
            'minvalue' => 'Minvalue',
            'maxvalue' => 'Maxvalue',
            'scalefactor' => 'Scalefactor',
            'isactive' => 'Isactive',
            'notify' => 'Notify',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
            'noticevalue' => 'Noticevalue',
            'warningvalue' => 'Warningvalue',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceport()
    {
        return $this->hasOne(Deviceport::className(), ['deviceport_id' => 'deviceport_id']);
    }
}
