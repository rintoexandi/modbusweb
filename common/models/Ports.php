<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ports".
 *
 * @property int $port_id
 * @property string $name
 * @property string $description
 * @property int $portgroup_id
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 * @property string $icon
 * @property Deviceport[] $deviceports
 * @property Devices[] $devices
 * @property Portregister[] $portregisters
 * @property Portrelation[] $portrelations
 * @property Portrelation $portrelation
 * @property Portgroup $portgroup
 * @property int $uom_id
 * @property int $decimalpoint 
 * @property int $isdigital
 * @property int $alertrules_id
 * @property double $scalefactor
 */
class Ports extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ports';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'portgroup_id'], 'required'],
            [['portgroup_id', 'isactive', 'createdby', 'updateby','uom_id', 'decimalpoint','isdigital', 'alertrules_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 100],
             [['scalefactor'], 'number'], 
            [['description','icon'], 'string', 'max' => 255],
            [['name'], 'unique'],
           // [['portgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portgroup::className(), 'targetAttribute' => ['portgroup_id' => 'portgroup_id']],
            [['uom_id'], 'exist', 'skipOnError' => true, 'targetClass' => Uom::className(), 'targetAttribute' => ['uom_id' => 'uom_id']],
            [['alertrules_id'], 'exist', 'skipOnError' => true, 'targetClass' => Alertrules::className(), 'targetAttribute' => ['alertrules_id' => 'alertrules_id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        
        return [
            'port_id' => 'Port ID',
            'name' => 'Name',
            'description' => 'Description',
            'portgroup_id' => 'Port Type',
            'icon' =>'Icon',
            'isactive' => 'Active',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
            'uom_id' => 'UOM', 
            'decimalpoint' => 'Decimal Point', 
            'isdigital' => 'Isdigital',
            'alertrules_id' => 'Alertrules',
            'scalefactor' => 'Scale Factor', 
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDeviceports()
    {
        return $this->hasMany(Deviceport::className(), ['port_id' => 'port_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Devices::className(), ['device_id' => 'device_id'])->viaTable('deviceport', ['port_id' => 'port_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPortregisters()
    {
        return $this->hasMany(Portregister::className(), ['port_id' => 'port_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPortrelations()
    {
        return $this->hasMany(Portrelation::className(), ['portanalog_id' => 'port_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPortrelation()
    {
        return $this->hasOne(Portrelation::className(), ['port_id' => 'port_id']);
    }
    
      public function getUom()
    {
        $port = Uom::find()->where(['uom_id'=>$this->uom_id])->one();
        return $port;
    }

    /**
     * @return ActiveQuery
     */
    public function Portgroup()
    {
        $portgroup = Portgroup::find()->where(['portgroup_id'=>$this->portgroup_id])->one();
        return $portgroup;
    }
    
    public function getAlertrules()
    {
        return $this->hasOne(Alertrules::className(), ['alertrules_id' => 'alertrules_id']);
        
    }
    
    public function getPortAnalog(){
        
        $query = Ports::find();  
        $query->andWhere(['isdigital'=>false]);
    }
    
      //For select2
    public static function listPorts(){
        
        $items = ArrayHelper::map(Ports::find()->where(['isactive'=>true])->all(), 'port_id', 'name');
        
        return $items;
    }
    
    public static function listPortType(){
        $data = [
            '0'=>'Analog',
            '1'=>'Digital'
            ];
        
        return $data;
    }
   
}
