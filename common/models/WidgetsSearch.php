<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Widgets;

/**
 * WidgetsSearch represents the model behind the search form of `common\models\Widgets`.
 */
class WidgetsSearch extends Widgets
{
    public $onlymaster=false;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['widget_id', 'isactive', 'notify', 'createdby', 'updateby', 'widgetgroup_id',
            'parent_id', 'port_id', 'sameline', 'sequence'], 'integer'],
            [['onlymaster'],'boolean'],
            [['name','onlymaster', 'description', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Widgets::find();

        if ($this->onlymaster==true) {
            $query->andWhere(['parent_id'=>0]);
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'widget_id' => $this->widget_id,
            'isactive' => $this->isactive,
            'notify' => $this->notify,
            'created' => $this->created,
            'createdby' => $this->createdby,
            'updated' => $this->updated,
            'updateby' => $this->updateby,
            'widgetgroup_id' => $this->widgetgroup_id,
            'parent_id' => $this->parent_id,
            'port_id' => $this->port_id,
            'sameline' => $this->sameline,
            'sequence' => $this->sequence,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
