<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Devices;

/**
 * DevicesSearch represents the model behind the search form of `common\models\Devices`.
 */
class DevicesSearch extends Devices
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id', 'port', 'disabled', 'uptime', 'agent_uptime', 'last_polled_timetaken', 'site_id', 'tahundeployment', 'createdby', 'updateby'], 'integer'],
            [['hostname', 'ipaddress', 'hardware', 'status', 'status_reason', 'last_polled', 'last_poll_attempted', 'serial', 'created', 'updated'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Devices::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'device_id' => $this->device_id,
            'port' => $this->port,
            'disabled' => $this->disabled,
            'uptime' => $this->uptime,
            'agent_uptime' => $this->agent_uptime,
            'last_polled' => $this->last_polled,
            'last_poll_attempted' => $this->last_poll_attempted,
            'last_polled_timetaken' => $this->last_polled_timetaken,
            'site_id' => $this->site_id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'tahundeployment' => $this->tahundeployment,
            'created' => $this->created,
            'createdby' => $this->createdby,
            'updated' => $this->updated,
            'updateby' => $this->updateby,
        ]);

        $query->andFilterWhere(['like', 'hostname', $this->hostname])
            ->andFilterWhere(['like', 'ipaddress', $this->ipaddress])
            ->andFilterWhere(['like', 'hardware', $this->hardware])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'status_reason', $this->status_reason])
            ->andFilterWhere(['like', 'serial', $this->serial]);

        return $dataProvider;
    }
}
