<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Daemon;

/**
 * DemonSearch represents the model behind the search form of `common\models\Daemon`.
 */
class DemonSearch extends Daemon
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'isactive', 'duration'], 'integer'],
            [['name', 'description', 'last_started', 'last_finished', 'message'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Daemon::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'isactive' => $this->isactive,
            'last_started' => $this->last_started,
            'last_finished' => $this->last_finished,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
