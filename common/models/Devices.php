<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "devices".
 *
 * @property int $device_id
 * @property string $hostname
 * @property string $ipaddress
 * @property int $port
 * @property string $hardware
 * @property int $status
 * @property string $status_reason
 * @property int $prev_status 
 * @property string $last_status_changed
 * @property string $prev_status_reason 
 * @property int $disabled
 * @property int $uptime
 * @property int $agent_uptime
 * @property string $last_polled
 * @property string $last_poll_attempted
 * @property int $last_polled_timetaken
 * @property string $serial
 * @property int $site_id
 * @property string $latitude
 * @property string $longitude
 * @property int $tahundeployment
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 * @property Sitetype $sitetype  
 * @property Site $site
 * @property Poll[] $polls
 * @property Deviceport[] $deviceports  
 * @property Ports[] $ports 
 */
class Devices extends \yii\db\ActiveRecord
{
    public static $IMPORTANCE_HIGH="H";
    public static $IMPORTANCE_NORMAL="N";
    public static $IMPORTANCE_LOW="L";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hostname', 'ipaddress'], 'required'],
            [['port', 'disabled', 'uptime', 'agent_uptime', 'last_polled_timetaken','status', 'prev_status', 'site_id', 'tahundeployment', 'createdby', 'updateby','sitetype_id'], 'integer'],
            [['last_polled', 'last_status_changed','last_poll_attempted', 'created', 'updated'], 'safe'],
            [['latitude', 'longitude'], 'number'],
            [['hostname'], 'string', 'max' => 255],
            [['hardware', 'status_reason','prev_status_reason', 'serial'], 'string', 'max' => 50],
            [['ipaddress'], 'ip'],
            [['hostname'], 'unique'],
            [['ipaddress'], 'unique'],
            [['sitetype_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sitetype::className(), 'targetAttribute' => ['sitetype_id' => 'sitetype_id']], 
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'site_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'hostname' => 'Hostname',
            'ipaddress' => 'IP Address',
            'port' => 'Port',
            'hardware' => 'Hardware',
            'status' => 'Status',
            'status_reason' => 'Status Reason',
            'prev_status' => 'Prev Status',
            'last_status_changed'=> 'Last Status Changed',
            'prev_status_reason' => 'Prev Status Reason', 
            'disabled' => 'Disabled',
            'uptime' => 'Uptime',
            'agent_uptime' => 'Agent Uptime',
            'last_polled' => 'Last Polled',
            'last_poll_attempted' => 'Last Poll Attempted',
            'last_polled_timetaken' => 'Last Polled Timetaken',
            'serial' => 'Serial',
            'site_id' => 'Site ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'tahundeployment' => 'Tahundeployment',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
        
            'sitetype_id' => 'Sitetype ID',
        ];
    }

    
    
    /**
     * {@inheritDoc}
     * @see \yii\db\BaseActiveRecord::beforeSave()
     */
    public function beforeSave ($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord)
            {
                $this->status=true;
                $this->disabled=false;             
            }
            
            return true;
        }
        
        else {
           return false; 
        }
    
    }//end before save

    /**
     * {@inheritDoc}
     * @see \yii\db\BaseActiveRecord::afterSave()
     */
    public function afterSave ($insert, $changedAttributes)
    {
         parent::afterSave($insert, $changedAttributes);
        //if new populate DevicePorts
        if ($insert){
           
            $ports = Ports::find()->where(['isactive'=>true])->all();
            
            foreach ($ports as $port) {
                
                $deviceport = new Deviceport();
                $deviceport->device_id = $this->device_id;
                $deviceport->port_id=$port->port_id;
                $deviceport->isactive=true;
                $deviceport->value=0;
                $deviceport->reportalert=1;              
                $deviceport->save(false);
            }
            
        }
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Telkomsites::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolls()
    {
        return $this->hasMany(Poll::className(), ['device_id' => 'device_id']);
    }
    
    /*
     * 
     */
    public function getPorts()
    {
        return $this->hasMany(Deviceport::className(), ['device_id' => 'device_id']);
    }
    
    public static function countDevices(){
        
        $count = Devices::find()->where(['disabled'=>false])->count();
        return $count;
    }
    
    public static function countDevicesUp($witel_id=0){
        
        if ($witel_id==0) {
             $count = Devices::find()->where(['disabled'=>false,'status'=>1])->count();
     
        }
        
        else {
            $count = Devices::find()->where(['disabled'=>false,'site_id'=>$witel_id,'status'=>1])->count();
     
        }
         return $count;
    }
    
    /*
     * Get Poll Data
     */
    public static function getPoll($device_id) {
        
        $poll = Poll::find()->where(['device_id'=>$device_id,'processed'=>false])->all();
        return $poll;
    }
    
    /*
     * List Devices
     */
    public static function listDevices(){
        
        $devices = Devices::find()->where(['disabled'=>false])->all();
        
        return $devices;
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getSitetype() 
    { 
       return $this->hasOne(Sitetype::className(), ['sitetype_id' => 'sitetype_id']); 
    } 
}
