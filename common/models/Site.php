<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "site".
 *
 * @property int $site_id
 * @property string $name
 * @property string $description
 * @property int $isactive
 * @property int $isdefault
 * @property int $supervisor_id
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updatedby
 *
 * @property Devices[] $devices
 */
class Site extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['isactive', 'isdefault', 'supervisor_id', 'createdby', 'updatedby'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'name' => 'Name',
            'description' => 'Description',
            'isactive' => 'Isactive',
            'isdefault' => 'Isdefault',
            'supervisor_id' => 'Supervisor ID',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updatedby' => 'Updatedby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Devices::className(), ['site_id' => 'site_id']);
    }
    
    //For Select2
    public static function listSite() {
      
        $dataList=ArrayHelper::map(Site::find()->asArray()->all(), 'site_id', 'name');
        return $dataList;
        
    }
}
