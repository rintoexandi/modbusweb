<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Devicealert;

/**
 * DevicealertSearch represents the model behind the search form of `common\models\Devicealert`.
 */
class DevicealertSearch extends Devicealert
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['devicealert_id', 'deviceport_id', 'open', 'severity', 'alerted'], 'integer'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Devicealert::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'devicealert_id' => $this->devicealert_id,
            'deviceport_id' => $this->deviceport_id,
            'open' => $this->open,
            'severity' => $this->severity,
            'alerted' => $this->alerted,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        return $dataProvider;
    }
}
