<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "alertlog".
 *
 * @property int $id
 * @property int $devicealert_id
 * @property string $message
 * @property int $port_value
 * @property int $prev_value
 * @property int $alerted
 * @property string $poll_time
 * @property string $created
 * @property string $updated
 *
 * @property Devicealert $devicealert
 */
class Alertlog extends \yii\db\ActiveRecord
{
    const STATUS_OPEN=1;
    const STATUS_CLOSE=0;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alertlog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['devicealert_id'], 'required'],
            [['devicealert_id', 'port_value', 'prev_value', 'alerted'], 'integer'],
            [['created', 'updated','poll_time'], 'safe'],
            [['message'], 'string'],
            [['devicealert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Devicealert::className(), 'targetAttribute' => ['devicealert_id' => 'devicealert_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'devicealert_id' => 'Devicealert ID',
            'message' => 'Message',
            'port_value' => 'Port Value',
            'prev_value' => 'Prev Value',
            'alerted' => 'Alerted',
            'created' => 'Created',
            'updated' => 'Updated',
            'poll_time' => 'Poll Time', 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevicealert()
    {
        return $this->hasOne(Devicealert::className(), ['devicealert_id' => 'devicealert_id']);
    }
    
    /*
     *
     */
    public function alert($devicealert,$message,$port_value=0,$prev_value=0)
    {
        /** @var Notification $instance */
        $device_alert = Devicealert::findOne(['devicealert_id' => $devicealert->devicealert_id]);
        
        if ($device_alert) {
            $instance = new Alertlog([
                    'devicealert_id' => $devicealert->devicealert_id,
                    'message'=>json_encode($message),
                    'port_value' => $port_value,
                    'prev_value'=>$prev_value,
            ]);
            return $instance->save(false);    
        }
        
       return true;
       
    } //
    
    
    
    
    
}
