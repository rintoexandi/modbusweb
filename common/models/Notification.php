<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property string $key
 * @property string $message
 * @property int $read
 * @property int $importance
 * @property int $user_id
 * @property string $created
 * @property string $updated
 */
class Notification extends \yii\db\ActiveRecord
{
    
    const IMPORTANCE_DEFAULT=0;
    const IMPORTANCE_MEDIUM=10;    
    const IMPORTANCE_HIGH=100;
    
    /**
     * Default notification
     */
    const TYPE_DEFAULT = 'default';
    /**
     * Error notification
     */
    const TYPE_ERROR   = 'error';
    /**
     * Warning notification
     */
    const TYPE_WARNING = 'warning';
    /**
     * Success notification type
     */
    const TYPE_SUCCESS = 'success';
    
    const DEVICE_OFFLINE="device_offline";
    
    const DEVICE_ONLINE="device_online";
    /**
     * @var array List of all enabled notification types
     */
    public static $types = [
            self::TYPE_WARNING,
            self::TYPE_DEFAULT,
            self::TYPE_ERROR,
            self::TYPE_SUCCESS,
            self::DEVICE_OFFLINE,
            self::DEVICE_ONLINE,
    ];
    
    public $data = [];
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'user_id','deviceport_id'], 'required'],
            [['read', 'importance', 'user_id','deviceport_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['key'], 'string', 'max' => 32],
            [['message'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }
    
    /**
     * Gets notification data
     *
     * @return array
     */
    public function getData(){
        return $this->data;
    }
    /**
     * Sets notification data
     *
     * @return self
     */
    public function setData($data = []){
        $this->data = $data;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'message' => 'Message',
            'read' => 'Read',
            'importance' => 'Importance',
            'user_id' => 'User ID',
            'deviceport_id'=>'Device Port ID',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }
    
    public function getTitle(){
        switch($this->key){
            case self::DEVICE_OFF_LINE:
                return Yii::t('app', 'Device {device} off-line', ['device' => '#'.$device->ipaddress]);
            case self::DEVICE_ON_LINE:
                return Yii::t('app', 'Device {device} on-line', ['device' => '#'.$device->ipaddress]);
            case self::TRESHOLD_VALUE_INFO:
                return Yii::t('app', 'Device value treshold info for {device}', ['device' => '#'.$device->ipaddress]);
            case self::TRESHOLD_VALUE_WARNING:
                return Yii::t('app', 'Device value treshold warning for {device}', ['device' => '#'.$device->ipaddress]);
            case self::TRESHOLD_VALUE_FATAL:
                return Yii::t('app', 'Device value treshold fatal error {device}', ['device' => '#'.$device->ipaddress]);
                
        }
    }
    
    
    /**
     * Creates a notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param string $key_id The foreign instance id
     * @param string $type
     * @return bool Returns TRUE on success, FALSE on failure
     * @throws \Exception
     */
    public static function notify($user_id, $deviceport_id,$message, $importance = self::IMPORTANCE_DEFAULT,$key = self::TYPE_DEFAULT)
    {
         /** @var Notification $instance */
        $instance = Notification::findOne(['user_id' => $user_id,'key' => $key, 'deviceport_id' => $deviceport_id]);
        if (!$instance) {
            $instance = new Notification([
                    'key' => $key,
                    'user_id' => $user_id,
                    'importance'=>$importance,
                    'message'=>json_encode($message),
                    'read'=>false,
                    'deviceport_id' => $deviceport_id,
  
            ]);
            return $instance->save(false);
        }
        return true;
    }
    
    /**
     * Creates a warning notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param string $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function warning($key, $user_id,$importance,$deviceport_id)
    {
        return static::notify(self::TYPE_WARNING, $user_id, self::IMPORTANCE_HIGH,$deviceport_id );
    }
    /**
     * Creates an error notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param string $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function error($key, $user_id,$importance,$deviceport_id)
    {
        return static::notify(self::TYPE_ERROR, $user_id, self::IMPORTANCE_HIGH,$deviceport_id );
    }
    /**
     * Creates a success notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param string $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function success($key, $user_id,$importance,$deviceport_id)
    {
        return static::notify(self::TYPE_SUCCESS, $user_id, self::IMPORTANCE_DEFAULT,$deviceport_id );
    }
    
   
    
    public static function countNotification($user_id) {
       $nofitication = Notification::find()->where(['user_id'=>$user_id,'read'=>false])->count(); 
        
       return $nofitication;
    }
    
}
