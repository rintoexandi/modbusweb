<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deviceporthistory".
 *
 * @property int $id
 * @property int $deviceport_id
 * @property int $device_id
 * @property int $port_id
 * @property int $value
 * @property int $value_prev
 * @property string $poll_time
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 *
 * @property Devices $device
 * @property Ports $port
 */
class Deviceporthistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deviceporthistory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deviceport_id', 'device_id', 'port_id'], 'required'],
            [['deviceport_id', 'device_id', 'port_id', 'value', 'value_prev', 'isactive', 'createdby', 'updateby'], 'integer'],
            [['poll_time', 'created', 'updated'], 'safe'],
            [['device_id'], 'exist', 'skipOnError' => true, 'targetClass' => Devices::className(), 'targetAttribute' => ['device_id' => 'device_id']],
            [['port_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ports::className(), 'targetAttribute' => ['port_id' => 'port_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deviceport_id' => 'Deviceport ID',
            'device_id' => 'Device ID',
            'port_id' => 'Port ID',
            'value' => 'Value',
            'value_prev' => 'Value Prev',
            'poll_time' => 'Poll Time',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Devices::className(), ['device_id' => 'device_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort()
    {
        return $this->hasOne(Ports::className(), ['port_id' => 'port_id']);
    }
    
    public static function addtoHistory($deviceport) {
        
        $hist = new Deviceporthistory();
        $hist->deviceport_id=$deviceport->deviceport_id;
        $hist->port_id = $deviceport->port_id;
        $hist->device_id=$deviceport->device_id;
        $hist->value=$deviceport->value;
        $hist->value_prev=$deviceport->value_prev;
        $hist->poll_time=$deviceport->poll_time;
        $hist->isactive=true;
        
        return $hist->save();
    }
}
