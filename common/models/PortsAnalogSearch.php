<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Ports;

/**
 * PortsSearch represents the model behind the search form of `common\models\Ports`.
 */
class PortsAnalogSearch extends Ports
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['port_id', 'portgroup_id','isactive','createdby', 'updateby'], 'integer'],
            [['name', 'description', 'unit', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ports::find();
        //$query->joinWith('portgroup');
        $query->andWhere(['isdigital'=>false]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'port_id' => $this->port_id,
           
            'isactive' => $this->isactive,
         
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);
          
        return $dataProvider;
    }
}
