<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "site".
 *
 * @property int $site_id
 * @property string $name
 * @property string $description
 * @property int $isactive
 * @property int $isdefault
 * @property int $supervisor_id
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updatedby
 * @property int $witel_id
 * @property int $sitetype_id
 *
 * @property Devices[] $devices
 * @property Sitetype $sitetype
 */
class Telkomsites extends ActiveRecord
{
    public $csvfile;
    public $filename;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','sitetype_id','witel_id'], 'required'],
            [['isactive', 'isdefault', 'supervisor_id', 'createdby', 'updatedby', 'witel_id', 'sitetype_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
            [['sitetype_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sitetype::className(), 'targetAttribute' => ['sitetype_id' => 'sitetype_id']],
            [['witel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Witel::className(), 'targetAttribute' => ['witel_id' => 'witel_id']],
            [['csvfile'], 'file','extensions' => 'csv','maxSize'=>1024 * 1024],
            [['csvfile'],'required','on'=>'uploadcsv']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'name' => 'Name',
            'description' => 'Description',
            'isactive' => 'Isactive',
            'isdefault' => 'Isdefault',
            'supervisor_id' => 'Supervisor ID',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updatedby' => 'Updatedby',
            'witel_id' => 'Witel ID',
            'sitetype_id' => 'Sitetype ID',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
      
        $scenarios['uploadcsv'] = ['csvfile'];
      
        return $scenarios;
    }
    
    /**
     * @return ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Devices::className(), ['site_id' => 'site_id']);
    }

     /**
    * @return ActiveQuery
    */
   public function getWitel()
   {
       return $this->hasOne(Witel::className(), ['witel_id' => 'witel_id']);
   }
   
    /**
     * @return ActiveQuery
     */
    public function getSitetype()
    {
        return $this->hasOne(Sitetype::className(), ['sitetype_id' => 'sitetype_id']);
    }
    
     public static function getAlertsOnSite($severity,$site_id){
       
      $query = Devicealert::find();
      $query->joinWith('deviceport');
      $query->join('INNER JOIN','devices','deviceport.device_id = devices.device_id');
      $query->join('INNER JOIN','site','devices.site_id = site.site_id');
      $query->andWhere(['devicealert.open'=>true]);
      $query->andWhere(['devicealert.severity'=>$severity]);
      $query->andWhere(['site.site_id'=>$site_id]);
       
       
      $model = new ActiveDataProvider([
              'query' => $query,
      ]);
       
      return $model->getTotalCount();
  }
  
     //For select2
    public static function listSites(){
        
        $items = ArrayHelper::map(Sites::find()->where(['isactive'=>true])->all(), 'site_id', 'name');
        
        return $items;
    }
    
    public static function countDevicesOnSite($site_id, $status = true){ 
       $query = Devices::find(); 
       $query->joinWith('site'); 
       $query->andWhere(['devices.status'=>$status]); 
       
       $query->andWhere(['devices.site_id'=>$site_id]); 
        
        
       $model = new ActiveDataProvider([ 
               'query' => $query, 
       ]); 
        
       return $model->getTotalCount(); 
      
       
   }
   
   public function uploadFile() {
        // get the uploaded file instance
        $csvFile = UploadedFile::getInstance($this, 'csvfile');
 
        // if no image was uploaded abort the upload
        if (empty($csvFile)) {
            return false;
        }
 
        // generate random name for the file
        $this->filename = time(). '-' . $csvFile->baseName . '.' . $csvFile->extension;
 
        // the uploaded image instance
        return $csvFile;
    }
 
    public function getUploadedFile() {
        // return a default image placeholder if your source avatar is not found
        $log_file = isset($this->filename) ? $this->filename : 'default.csv';
        return  $log_file;
        
    }
    
    public function upload() {
        if ($this->validate()) {
            $this->logFile->saveAs(Yii::$app->params['uploadcsv'] . $this->csvfile->baseName . '.' . $this->csvfile->extension);
            return true;
        } else {
            return false;
        }
    }
    
    public static function validateSite($sitename){
        
        $sites = Telkomsites::find()->where(['name'=>$sitename])->one();
        
        if ($sites !==null) {
            
            return $sites;
        }
        
        else {
            return '';
        }
    }
}
