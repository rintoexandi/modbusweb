<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sitetype".
 *
 * @property int $sitetype_id
 * @property string $name
 * @property int $importance_level
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updatedby
 */
class Sitetype extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sitetype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['importance_level', 'isactive', 'createdby', 'updatedby'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sitetype_id' => 'Sitetype ID',
            'name' => 'Name',
            'importance_level' => 'Importance Level',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updatedby' => 'Updatedby',
        ];
    }
    
    //For select2
    public static function listSitetype(){
        
        $items = ArrayHelper::map(Sitetype::find()->where(['isactive'=>true])->all(), 'sitetype_id', 'name');
        
        return $items;
    }
    
    //For select2
    public static function listImportanceLevel(){
        
        
        $items = [
            '1'=>'Low',
            '2'=>'Normal',
            '3'=>'Medium',
            '4'=>'High',
            '5'=>'Very High'
        ];
        
        return $items;
    }
    
    
}
