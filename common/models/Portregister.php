<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "portregister".
 *
 * @property int $id
 * @property string $name
 * @property int $port_id
 * @property int $registeredports
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 * @property string $memory
 *
 * @property Ports $port
 */
class Portregister extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portregister';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'port_id', 'registeredports'], 'required'],
            [['port_id', 'registeredports', 'isactive', 'createdby', 'updateby'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['memory'], 'string', 'max' => 5],
            [['port_id', 'registeredports'], 'unique', 'targetAttribute' => ['port_id', 'registeredports']],
            [['port_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ports::className(), 'targetAttribute' => ['port_id' => 'port_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'port_id' => 'Port ID',
            'registeredports' => 'Registeredports',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
            'memory' => 'Memory',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort()
    {
        return $this->hasOne(Ports::className(), ['port_id' => 'port_id']);
    }
}
