<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widgetgroup".
 *
 * @property int $widgetgroup_id
 * @property string $name
 * @property string $description
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 *
 * @property Widget[] $widgets
 */
class Widgetgroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'widgetgroup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['isactive', 'createdby', 'updateby'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 150],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'widgetgroup_id' => 'Widgetgroup ID',
            'name' => 'Name',
            'description' => 'Description',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgets()
    {
        return $this->hasMany(Widget::className(), ['widgetgroup_id' => 'widgetgroup_id']);
    }
    
    public static function bgBox($val=0){
        
        if ($val==0) {
            return "box-default";
        }
        
        else {
            return "box-success";
        }
    }
    
    public static function panelBox($val=0){
        
        if ($val==0) {
            return "panel-default";
        }
        
        else {
            return "panel-success";
        }
    }
    
    public static function arrowStatus($val=0){
        
        if ($val==0) {
            return "fa fa-arrow-circle-down";
        }
        
        else {
            return "fa fa-arrow-circle-up";
        }
    }
}
