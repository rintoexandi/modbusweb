<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "alert".
 *
 * @property int $id
 * @property int $deviceport_id
 * @property string $message 
 * @property int $port_value
 * @property int $prev_value
 * @property int $open
 * @property int $severity 
 * @property int $alerted
 * @property string $created
 * @property string $updated
 *
 * @property Deviceport $deviceport
 */
class Alert extends \yii\db\ActiveRecord
{
    
    const STATUS_OPEN=1;
    const STATUS_CLOSE=0;
    
    const SEVERITY_NONE=0;
    const SEVERITY_INFO=1;
    const SEVERITY_WARNING=5;
    const SEVERITY_CRITICAL=9;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deviceport_id'], 'required'],
                [['deviceport_id', 'port_value', 'prev_value', 'open','severity', 'alerted'], 'integer'],
            [['created', 'updated'], 'safe'],
                [['message'], 'string', 'max' => 200], 
            [['deviceport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deviceport::className(), 'targetAttribute' => ['deviceport_id' => 'deviceport_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deviceport_id' => 'Deviceport ID',
                'message' => 'Message', 
            'port_value' => 'Port Value',
            'prev_value' => 'Prev Value',
            'open' => 'Open',
            'severity' => 'Severity', 
            'alerted' => 'Alerted',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceport()
    {
        return $this->hasOne(Deviceport::className(), ['deviceport_id' => 'deviceport_id']);
    }
    
    
    /*
     * 
     */
    public static function alert($deviceport_id,$message,$port_value=0,$prev_value=0,$severity=self::SEVERITY_INFO,$open=self::STATUS_OPEN)
    {
        /** @var Notification $instance */
        $instance = Alert::findOne(['deviceport_id' => $deviceport_id,'open' => $open]);
        
        if (!$instance) {
            $instance = new Alert([
                    'deviceport_id' => $deviceport_id,
                    'message'=>json_encode($message),
                    'port_value' => $port_value,
                    'prev_value'=>$prev_value,
                    'severity'=>$severity,
                    'open'=>$open,
                    
            ]);
            return $instance->save(false);
        }
        return true;
    }
    
    public static function close($deviceport_id,$open=self::STATUS_OPEN) {
        /** @var Notification $instance */
        $instance = Alert::findOne(['deviceport_id' => $deviceport_id,'open' => $open]);
        
        if($instance){
            
            $instance->open = self::STATUS_CLOSE;
            
            return $instance->save(false);
        }
        
        return true;
    }
    
}
