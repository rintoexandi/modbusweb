<?php

namespace common\models;

use Yii;
use yii\db\ArrayExpression;
use yii\db\Expression;

/**
 * This is the model class for table "devicealert".
 *
 * @property int $devicealert_id
 * @property int $deviceport_id
 * @property int $open
 * @property int $severity
 * @property int $alerted
 * @property string $created
 * @property string $updated
 *
 * @property Alertlog[] $alertlogs
 * @property Deviceport $deviceport
 */
class Devicealert extends \yii\db\ActiveRecord
{
    const STATUS_OPEN=1;
    const STATUS_CLOSE=0;
    
    const SEVERITY_INVALID="X";
    const SEVERITY_OK="O";
    const SEVERITY_INFO="I";
    const SEVERITY_WARNING="W";
    const SEVERITY_CRITICAL="C";
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devicealert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deviceport_id'], 'required'],
            [['deviceport_id', 'open', 'alerted'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['severity'], 'string', 'max' => 1],
            [['deviceport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deviceport::className(), 'targetAttribute' => ['deviceport_id' => 'deviceport_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'devicealert_id' => 'Devicealert ID',
            'deviceport_id' => 'Deviceport ID',
            'open' => 'Open',
            'severity' => 'Severity',
            'alerted' => 'Alerted',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertlogs()
    {
        return $this->hasMany(Alertlog::className(), ['devicealert_id' => 'devicealert_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceport()
    {
        return $this->hasOne(Deviceport::className(), ['deviceport_id' => 'deviceport_id']);
    }
    
    /*
     * Create Alert
     */
    public function alert($deviceport_id,$severity=self::SEVERITY_INFO,$open=self::STATUS_OPEN)
    {
        /** @var Notification $instance */
        $instance = Devicealert::findOne(['deviceport_id' => $deviceport_id,'open' => $open]);
        
        if (!$instance) {
            $instance = new Devicealert([
                    'deviceport_id' => $deviceport_id,
                    'severity'=>$severity,
                    'open'=>$open,
                    
            ]);
            
           $instance->save(false);
           
           return $instance;            
         
        }
        return $instance;
    }
    
    /*
     * Close Alert
     */
    public function close($deviceport_id,$open=self::STATUS_OPEN) {
        /** @var Notification $instance */
        $instance = Devicealert::findOne(['deviceport_id' => $deviceport_id,'open' => $open]);
        
        if($instance){
            
            $instance->open = self::STATUS_CLOSE;
            $instance->updated = new Expression('CURRENT_TIMESTAMP');
            $instance->save(false);
            return $instance;
        }
        
        return $instance;
    }
    
    /*
     * Create AlertLog
     */
    public function alertlog($message,$port_value,$prev_value,$poll_time) {
        
        /** @var Notification $instance */
        $devicealert = Devicealert::findOne(['devicealert_id' => $this->devicealert_id]);
        
         
        if($devicealert){
           
            $alertlog = Alertlog::find()->where(['devicealert_id' => $this->devicealert_id,'poll_time'=>$poll_time])->all();
            
            if (!$alertlog) {
                $instance = new Alertlog([
                        'devicealert_id' => $this->devicealert_id,
                        'message'=>json_encode($message),
                        'port_value' => $port_value,
                        'prev_value'=>$prev_value,
                        'poll_time'=>$poll_time
                ]);
                
                return $instance->save(false);  
            }
             
            else {
                return true;
            }
            
        }
        
        return true;
        
    }
    
    public function getDeviceAlert($deviceport_id) {
        
        $devalalert = Devicealert::findOne(['deviceport_id' => $deviceport_id,'open'=>1]);
        return $devalalert;
                
    }
    
   
}
