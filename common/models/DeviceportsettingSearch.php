<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Deviceportsetting;

/**
 * DeviceportsettingSearch represents the model behind the search form of `common\models\Deviceportsetting`.
 */
class DeviceportsettingSearch extends Deviceportsetting
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'deviceport_id', 'minvalue', 'maxvalue', 'scalefactor', 'isactive', 'notify', 'createdby', 'updateby'], 'integer'],
            [['settingtype', 'description', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Deviceportsetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'deviceport_id' => $this->deviceport_id,
            'minvalue' => $this->minvalue,
            'maxvalue' => $this->maxvalue,
            'scalefactor' => $this->scalefactor,
            'isactive' => $this->isactive,
            'notify' => $this->notify,
            'created' => $this->created,
            'createdby' => $this->createdby,
            'updated' => $this->updated,
            'updateby' => $this->updateby,
        ]);

        $query->andFilterWhere(['like', 'settingtype', $this->settingtype])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
