<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Deviceporthistory;

/**
 * DeviceporthistorySearch represents the model behind the search form of `common\models\Deviceporthistory`.
 */
class DeviceporthistorySearch extends Deviceporthistory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'deviceport_id', 'device_id', 'port_id', 'value', 'value_prev', 'isactive', 'createdby', 'updateby'], 'integer'],
            [['poll_time', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Deviceporthistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'deviceport_id' => $this->deviceport_id,
            'device_id' => $this->device_id,
            'port_id' => $this->port_id,
            'value' => $this->value,
            'value_prev' => $this->value_prev,
            'poll_time' => $this->poll_time,
            'isactive' => $this->isactive,
            'created' => $this->created,
            'createdby' => $this->createdby,
            'updated' => $this->updated,
            'updateby' => $this->updateby,
        ]);

        return $dataProvider;
    }
}
