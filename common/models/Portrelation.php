<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "portrelation".
 *
 * @property int $id
 * @property int $widget_id
 * @property int $port_id
 * @property int $portanalog_id
 * @property int $isactive
 * @property string $created
 * @property int $createdby
 * @property string $updated
 * @property int $updateby
 *
 * @property Ports $portanalog
 * @property Ports $port
 * @property Widget $widget
 */
class Portrelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portrelation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['widget_id', 'port_id', 'portanalog_id', 'isactive', 'createdby', 'updateby'], 'integer'],
            [['port_id', 'portanalog_id'], 'required'],
            [['created', 'updated'], 'safe'],
            [['port_id'], 'unique'],
            [['portanalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ports::className(), 'targetAttribute' => ['portanalog_id' => 'port_id']],
            [['port_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ports::className(), 'targetAttribute' => ['port_id' => 'port_id']],
            [['widget_id'], 'exist', 'skipOnError' => true, 'targetClass' => Widget::className(), 'targetAttribute' => ['widget_id' => 'widget_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'widget_id' => 'Widget ID',
            'port_id' => 'Port ID',
            'portanalog_id' => 'Portanalog ID',
            'isactive' => 'Isactive',
            'created' => 'Created',
            'createdby' => 'Createdby',
            'updated' => 'Updated',
            'updateby' => 'Updateby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortanalog()
    {
        return $this->hasOne(Ports::className(), ['port_id' => 'portanalog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort()
    {
        return $this->hasOne(Ports::className(), ['port_id' => 'port_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidget()
    {
        return $this->hasOne(Widget::className(), ['widget_id' => 'widget_id']);
    }
}
