<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@modbusrrd' => '@backend/extension/modbus-rrd',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
            
            'authManager' => [//component authmanager untuk RBAC
                    'class' => 'yii\rbac\DbManager',
                    'defaultRoles'=>['guest'],
                    // uncomment if you want to cache RBAC items hierarchy
                    // 'cache' => 'cache',
            ],
            
            
            'urlManager' => [
                    'class' => 'yii\web\UrlManager',
                    'showScriptName' => false,
                    'enablePrettyUrl' => true,
                    'rules' => [
                            '<controller:\w+>/<id:\d+>' => '<controller>/view',
                            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                            '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                            
                    ],
            ],
        'telegram' => [
        'class' => 'aki\telegram\Telegram',
        'botToken' => '677578478:AAGyGdl0hS-7mexGJmsM-XBrhV4Y-WCqYo0', //osasetest_bot
            ],
        'amqp' => [
          'class' => 'devmustafa\amqp\components\Amqp',
          'host' => '127.0.0.1',
          'port' => 5672,
          'user' => 'admin',
          'password' => 'admin',
          'vhost' => '/',
      ],    
        'rabbitmq'  => [
          // ...
           'class' => \mikemadisonweb\rabbitmq\Configuration::class,
            'connections' => [
                    [
                            // You can pass these parameters as a single `url` option: https://www.rabbitmq.com/uri-spec.html
                            'name'=>'default',
                            'host' => 'localhost',
                            'port' => '5672',
                            'user' => 'admin',
                            'password' => 'admin',
                            'vhost' => '/',
                    ]
                    // When multiple connections is used you need to specify a `name` option for each one and define them in producer and consumer configuration blocks
            ],
            'exchanges' => [
                    [
                            'name' => 'modbusexchange',
                            'type' => 'direct'
                            // Refer to Defaults section for all possible options
                    ],
            ],
            'queues' => [
                    [
                            'name' => 'modbuspoller',
                            // Queue can be configured here the way you want it:
                            'durable' => true,
                           // 'auto_delete' => true,
                    ],
                   
            ],
           'bindings' => [
                    [
                            'queue' => 'modbuspoller',
                            'exchange' => 'modbusexchange',
                            'routing_keys' => ['123456789'],
                    ],
            ],
            'producers' => [
                    [
                            'name' => 'yii2-producer',
                    ],
            ],
          'consumers' => [
                  [
                          'name' => 'modbus-consumer',
                          // Every consumer should define one or more callbacks for corresponding queues
                          'callbacks' => [
                                  // queue name => callback class name
                                  'modbuspoller' => Yii2Consumer::class,
                          ],
                ],
            ],
         ]
    ], //end of components
        
        'modules'=> [
                'authorization' => [
                        'class' => 'mdm\admin\Module',
                        'controllerMap' => [
                                'assignment' => [
                                        'class' => 'mdm\admin\controllers\AssignmentController',
                                        'userClassName' => 'common\models\User',
                                        'idField' => 'id', // id field of model User
                                        'usernameField' => 'username', // username field of model User
                                ],
                        ],
                ],//end parameter pengaturan hak akses
                
                //kartik-v gridView
                'gridviewKartik' =>  [
                        'class' => '\kartik\grid\Module',
                        // your other grid module settings
                ],
                
                //kartik-v gridView
                'gridview' =>  [
                        'class' => '\kartik\grid\Module',
                        // your other grid module settings
                ],
                
        ]
];
