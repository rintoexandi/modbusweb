/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */
/**
 * Author:  Rinto Exandi <rinto at sinaga.or.id>
 * Created: Apr 4, 2019
 */

DELETE FROM deviceport WHERE 1>0;
DELETE FROM devices WHERE 1>0;
