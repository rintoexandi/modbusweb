/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */
/**
 * Author:  Rinto Exandi <rinto at sinaga.or.id>
 * Created: Mar 9, 2019
 */

DELETE FROM alertlog WHERE id>0;
DELETE FROM devicealert WHERE devicealert_id>0;
DELETE FROM deviceporthistory WHERE id>0;
DELETE FROM polldata WHERE id>0;
DELETE FROM poll WHERE poll_id>0;
UPDATE deviceport SET value=0,value_prev=0,port_uptime=0 where device_id=0;