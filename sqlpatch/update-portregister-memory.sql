/* 
 * @link : http://rinto.sinaga.or.id
 * @copyright : Rinto Exandi
 * @author : Rinto Exandi Sinaga <rinto@sinaga.or.id>
 */
/**
 * Author:  Rinto Exandi <rinto at sinaga.or.id>
 * Created: Mar 4, 2019
 */

UPDATE portregister SET memory='%M0' WHERE port_id=(SELECT port_id FROM ports WHERE name='01D');
UPDATE portregister SET memory='%M1' WHERE port_id=(SELECT port_id FROM ports WHERE name='02D');
UPDATE portregister SET memory='%M2' WHERE port_id=(SELECT port_id FROM ports WHERE name='03D');
UPDATE portregister SET memory='%M3' WHERE port_id=(SELECT port_id FROM ports WHERE name='04D');
UPDATE portregister SET memory='%M4' WHERE port_id=(SELECT port_id FROM ports WHERE name='05D');
UPDATE portregister SET memory='%M5' WHERE port_id=(SELECT port_id FROM ports WHERE name='06D');
UPDATE portregister SET memory='%M6' WHERE port_id=(SELECT port_id FROM ports WHERE name='07D');
UPDATE portregister SET memory='%M7' WHERE port_id=(SELECT port_id FROM ports WHERE name='08D');
UPDATE portregister SET memory='%M8' WHERE port_id=(SELECT port_id FROM ports WHERE name='09D');
UPDATE portregister SET memory='%M9' WHERE port_id=(SELECT port_id FROM ports WHERE name='10D');
UPDATE portregister SET memory='%M10' WHERE port_id=(SELECT port_id FROM ports WHERE name='11D');
UPDATE portregister SET memory='%M11' WHERE port_id=(SELECT port_id FROM ports WHERE name='12D');
UPDATE portregister SET memory='%M12' WHERE port_id=(SELECT port_id FROM ports WHERE name='13D');
UPDATE portregister SET memory='%M13' WHERE port_id=(SELECT port_id FROM ports WHERE name='14D');
UPDATE portregister SET memory='%M14' WHERE port_id=(SELECT port_id FROM ports WHERE name='15D');
UPDATE portregister SET memory='%M15' WHERE port_id=(SELECT port_id FROM ports WHERE name='16D');
UPDATE portregister SET memory='%M16' WHERE port_id=(SELECT port_id FROM ports WHERE name='17D');
UPDATE portregister SET memory='%M17' WHERE port_id=(SELECT port_id FROM ports WHERE name='18D');
UPDATE portregister SET memory='%M18' WHERE port_id=(SELECT port_id FROM ports WHERE name='19D');
UPDATE portregister SET memory='%M19' WHERE port_id=(SELECT port_id FROM ports WHERE name='20D');
UPDATE portregister SET memory='%M20' WHERE port_id=(SELECT port_id FROM ports WHERE name='21D');
UPDATE portregister SET memory='%M21' WHERE port_id=(SELECT port_id FROM ports WHERE name='22D');
UPDATE portregister SET memory='%M22' WHERE port_id=(SELECT port_id FROM ports WHERE name='23D');
UPDATE portregister SET memory='%M23' WHERE port_id=(SELECT port_id FROM ports WHERE name='24D');
UPDATE portregister SET memory='%M24' WHERE port_id=(SELECT port_id FROM ports WHERE name='25D');
UPDATE portregister SET memory='%M25' WHERE port_id=(SELECT port_id FROM ports WHERE name='26D');
UPDATE portregister SET memory='%M26' WHERE port_id=(SELECT port_id FROM ports WHERE name='27D');
UPDATE portregister SET memory='%M27' WHERE port_id=(SELECT port_id FROM ports WHERE name='28D');
UPDATE portregister SET memory='%M28' WHERE port_id=(SELECT port_id FROM ports WHERE name='29D');
UPDATE portregister SET memory='%M29' WHERE port_id=(SELECT port_id FROM ports WHERE name='30D');
UPDATE portregister SET memory='%M30' WHERE port_id=(SELECT port_id FROM ports WHERE name='31D');
UPDATE portregister SET memory='%M31' WHERE port_id=(SELECT port_id FROM ports WHERE name='32D');
UPDATE portregister SET memory='%M32' WHERE port_id=(SELECT port_id FROM ports WHERE name='33D');
UPDATE portregister SET memory='%M33' WHERE port_id=(SELECT port_id FROM ports WHERE name='34D');
UPDATE portregister SET memory='%M34' WHERE port_id=(SELECT port_id FROM ports WHERE name='35D');
UPDATE portregister SET memory='%M35' WHERE port_id=(SELECT port_id FROM ports WHERE name='36D');
UPDATE portregister SET memory='%M36' WHERE port_id=(SELECT port_id FROM ports WHERE name='37D');
UPDATE portregister SET memory='%M37' WHERE port_id=(SELECT port_id FROM ports WHERE name='38D');
UPDATE portregister SET memory='%M38' WHERE port_id=(SELECT port_id FROM ports WHERE name='39D');
UPDATE portregister SET memory='%M39' WHERE port_id=(SELECT port_id FROM ports WHERE name='40D');
UPDATE portregister SET memory='%M40' WHERE port_id=(SELECT port_id FROM ports WHERE name='41D');
UPDATE portregister SET memory='%M41' WHERE port_id=(SELECT port_id FROM ports WHERE name='42D');
UPDATE portregister SET memory='%M42' WHERE port_id=(SELECT port_id FROM ports WHERE name='43D');
UPDATE portregister SET memory='%M43' WHERE port_id=(SELECT port_id FROM ports WHERE name='44D');
UPDATE portregister SET memory='%M44' WHERE port_id=(SELECT port_id FROM ports WHERE name='45D');
UPDATE portregister SET memory='%M45' WHERE port_id=(SELECT port_id FROM ports WHERE name='46D');
UPDATE portregister SET memory='%M46' WHERE port_id=(SELECT port_id FROM ports WHERE name='47D');
UPDATE portregister SET memory='%M47' WHERE port_id=(SELECT port_id FROM ports WHERE name='48D');
UPDATE portregister SET memory='%M48' WHERE port_id=(SELECT port_id FROM ports WHERE name='49D');
UPDATE portregister SET memory='MW0' WHERE port_id=(SELECT port_id FROM ports WHERE name='01A');
UPDATE portregister SET memory='MW1' WHERE port_id=(SELECT port_id FROM ports WHERE name='02A');
UPDATE portregister SET memory='MW2' WHERE port_id=(SELECT port_id FROM ports WHERE name='03A');
UPDATE portregister SET memory='MW3' WHERE port_id=(SELECT port_id FROM ports WHERE name='04A');
UPDATE portregister SET memory='MW4' WHERE port_id=(SELECT port_id FROM ports WHERE name='05A');
UPDATE portregister SET memory='MW5' WHERE port_id=(SELECT port_id FROM ports WHERE name='06A');
UPDATE portregister SET memory='MW6' WHERE port_id=(SELECT port_id FROM ports WHERE name='07A');
UPDATE portregister SET memory='MW7' WHERE port_id=(SELECT port_id FROM ports WHERE name='08A');
UPDATE portregister SET memory='MW8' WHERE port_id=(SELECT port_id FROM ports WHERE name='09A');
UPDATE portregister SET memory='MW9' WHERE port_id=(SELECT port_id FROM ports WHERE name='10A');
UPDATE portregister SET memory='MW10' WHERE port_id=(SELECT port_id FROM ports WHERE name='11A');
UPDATE portregister SET memory='MW11' WHERE port_id=(SELECT port_id FROM ports WHERE name='12A');
UPDATE portregister SET memory='MW12' WHERE port_id=(SELECT port_id FROM ports WHERE name='13A');
UPDATE portregister SET memory='MW13' WHERE port_id=(SELECT port_id FROM ports WHERE name='14A');
UPDATE portregister SET memory='MW14' WHERE port_id=(SELECT port_id FROM ports WHERE name='15A');
UPDATE portregister SET memory='MW15' WHERE port_id=(SELECT port_id FROM ports WHERE name='16A');
UPDATE portregister SET memory='MW16' WHERE port_id=(SELECT port_id FROM ports WHERE name='17A');

UPDATE portregister SET memory='MW18' WHERE port_id=(SELECT port_id FROM ports WHERE name='18A');
UPDATE portregister SET memory='MW19' WHERE port_id=(SELECT port_id FROM ports WHERE name='19A');
UPDATE portregister SET memory='MW20' WHERE port_id=(SELECT port_id FROM ports WHERE name='20A');
UPDATE portregister SET memory='MW21' WHERE port_id=(SELECT port_id FROM ports WHERE name='21A');
UPDATE portregister SET memory='MW22' WHERE port_id=(SELECT port_id FROM ports WHERE name='22A');
UPDATE portregister SET memory='MW23' WHERE port_id=(SELECT port_id FROM ports WHERE name='23A');
UPDATE portregister SET memory='MW24' WHERE port_id=(SELECT port_id FROM ports WHERE name='24A');
UPDATE portregister SET memory='MW25' WHERE port_id=(SELECT port_id FROM ports WHERE name='25A');
UPDATE portregister SET memory='MW26' WHERE port_id=(SELECT port_id FROM ports WHERE name='26A');
UPDATE portregister SET memory='MW27' WHERE port_id=(SELECT port_id FROM ports WHERE name='27A');
UPDATE portregister SET memory='MW28' WHERE port_id=(SELECT port_id FROM ports WHERE name='28A');
UPDATE portregister SET memory='MW29' WHERE port_id=(SELECT port_id FROM ports WHERE name='29A');
UPDATE portregister SET memory='MW30' WHERE port_id=(SELECT port_id FROM ports WHERE name='30A');
UPDATE portregister SET memory='MW31' WHERE port_id=(SELECT port_id FROM ports WHERE name='31A');
UPDATE portregister SET memory='MW32' WHERE port_id=(SELECT port_id FROM ports WHERE name='32A');
UPDATE portregister SET memory='MW33' WHERE port_id=(SELECT port_id FROM ports WHERE name='33A');

UPDATE portregister SET memory='MW35' WHERE port_id=(SELECT port_id FROM ports WHERE name='34A');

UPDATE portregister SET memory='MW37' WHERE port_id=(SELECT port_id FROM ports WHERE name='35A');

UPDATE portregister SET memory='MW39' WHERE port_id=(SELECT port_id FROM ports WHERE name='36A');

UPDATE portregister SET memory='MW41' WHERE port_id=(SELECT port_id FROM ports WHERE name='37A');

UPDATE portregister SET memory='MW43' WHERE port_id=(SELECT port_id FROM ports WHERE name='38A');

UPDATE portregister SET memory='MW45' WHERE port_id=(SELECT port_id FROM ports WHERE name='39A');

UPDATE portregister SET memory='MW47' WHERE port_id=(SELECT port_id FROM ports WHERE name='40A');

UPDATE portregister SET memory='MW49' WHERE port_id=(SELECT port_id FROM ports WHERE name='41A');

UPDATE portregister SET memory='MW51' WHERE port_id=(SELECT port_id FROM ports WHERE name='42A');

UPDATE portregister SET memory='MW53' WHERE port_id=(SELECT port_id FROM ports WHERE name='43A');

UPDATE portregister SET memory='MW55' WHERE port_id=(SELECT port_id FROM ports WHERE name='44A');
UPDATE portregister SET memory='MW56' WHERE port_id=(SELECT port_id FROM ports WHERE name='45A');
UPDATE portregister SET memory='MW57' WHERE port_id=(SELECT port_id FROM ports WHERE name='46A');
UPDATE portregister SET memory='MW58' WHERE port_id=(SELECT port_id FROM ports WHERE name='47A');
UPDATE portregister SET memory='MW59' WHERE port_id=(SELECT port_id FROM ports WHERE name='48A');
UPDATE portregister SET memory='MW60' WHERE port_id=(SELECT port_id FROM ports WHERE name='49A');
UPDATE portregister SET memory='MW61' WHERE port_id=(SELECT port_id FROM ports WHERE name='50A');
UPDATE portregister SET memory='MW62' WHERE port_id=(SELECT port_id FROM ports WHERE name='51A');
